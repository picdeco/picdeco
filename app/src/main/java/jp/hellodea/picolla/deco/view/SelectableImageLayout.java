package jp.hellodea.picolla.deco.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * 選択状態を表現できるImageViewを配置する土台
 * @author kazuhiro
 *
 */
public class SelectableImageLayout extends LinearLayout {

	public SelectableImageLayout(Context context) {
		super(context);
	}
	
	
	
	public SelectableImageLayout(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
	}



	public SelectableImageLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}



	public void selectItem(int number, boolean exclusive){
		//選択状態を更新
		for (int j = 0; j < this.getChildCount(); j++) {
			SelectableImageView item = (SelectableImageView) this.getChildAt(j);
			if(j == number)
				item.setSelectedStatus(true);
			else if(exclusive == true){
				item.setSelectedStatus(false);
			}
		}
		
	}
	
	public void selectAll(boolean select){
		//選択状態を更新
		for (int j = 0; j < this.getChildCount(); j++) {
			SelectableImageView item = (SelectableImageView) this.getChildAt(j);
			if(select == true)
				item.setSelectedStatus(true);
			else{
				item.setSelectedStatus(false);
			}
		}
		
	}
	
}
