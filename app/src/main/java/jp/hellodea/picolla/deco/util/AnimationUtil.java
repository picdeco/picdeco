package jp.hellodea.picolla.deco.util;

import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;

public class AnimationUtil {

      public static Animation inFromRight() {

          Animation inFromRight = new TranslateAnimation(
                  Animation.RELATIVE_TO_PARENT, +1.0f,
                  Animation.RELATIVE_TO_PARENT, 0.0f,
                  Animation.RELATIVE_TO_PARENT, 0.0f,
                  Animation.RELATIVE_TO_PARENT, 0.0f);
          inFromRight.setDuration(350);
          inFromRight.setInterpolator(new AccelerateInterpolator());
          return inFromRight;
      }

      public static Animation outToLeft() {
          Animation outtoLeft = new TranslateAnimation(
                  Animation.RELATIVE_TO_PARENT, 0.0f,
                  Animation.RELATIVE_TO_PARENT, -1.0f,
                  Animation.RELATIVE_TO_PARENT, 0.0f,
                  Animation.RELATIVE_TO_PARENT, 0.0f);
          outtoLeft.setDuration(350);
          outtoLeft.setInterpolator(new AccelerateInterpolator());
          return outtoLeft;
      }

      public static Animation inFromLeft() {
          Animation inFromLeft = new TranslateAnimation(
                  Animation.RELATIVE_TO_PARENT, -1.0f,
                  Animation.RELATIVE_TO_PARENT, 0.0f,
                  Animation.RELATIVE_TO_PARENT, 0.0f,
                  Animation.RELATIVE_TO_PARENT, 0.0f);
          inFromLeft.setDuration(350);
          inFromLeft.setInterpolator(new AccelerateInterpolator());
          return inFromLeft;
      }

      public static Animation outToRight() {
          Animation outtoRight = new TranslateAnimation(
                  Animation.RELATIVE_TO_PARENT, 0.0f,
                  Animation.RELATIVE_TO_PARENT, +1.0f,
                  Animation.RELATIVE_TO_PARENT, 0.0f,
                  Animation.RELATIVE_TO_PARENT, 0.0f);
          outtoRight.setDuration(350);
          outtoRight.setInterpolator(new AccelerateInterpolator());
          return outtoRight;
      }

      public static Animation inFromTop() {

          Animation inFromTop = new TranslateAnimation(
                  Animation.RELATIVE_TO_PARENT, 0.0f,
                  Animation.RELATIVE_TO_PARENT, 0.0f,
                  Animation.RELATIVE_TO_PARENT, -1.0f,
                  Animation.RELATIVE_TO_PARENT, 0.0f);
          inFromTop.setDuration(350);
          inFromTop.setInterpolator(new AccelerateInterpolator());
          return inFromTop;
      }

      public static Animation outToBottom() {
          Animation outToBottom = new TranslateAnimation(
                  Animation.RELATIVE_TO_PARENT, 0.0f,
                  Animation.RELATIVE_TO_PARENT, 0.0f,
                  Animation.RELATIVE_TO_PARENT, 0.0f,
                  Animation.RELATIVE_TO_PARENT, +1.0f);
          outToBottom.setDuration(350);
          outToBottom.setInterpolator(new AccelerateInterpolator());
          return outToBottom;
      }

      public static Animation inFromBottom() {
          Animation inFromBottom = new TranslateAnimation(
                  Animation.RELATIVE_TO_PARENT, 0.0f,
                  Animation.RELATIVE_TO_PARENT, 0.0f,
                  Animation.RELATIVE_TO_PARENT, 1.0f,
                  Animation.RELATIVE_TO_PARENT, 0.0f);
          inFromBottom.setDuration(350);
          inFromBottom.setInterpolator(new AccelerateInterpolator());
          return inFromBottom;
      }

      public static Animation outToTop() {
          Animation outToTop = new TranslateAnimation(
                  Animation.RELATIVE_TO_PARENT, 0.0f,
                  Animation.RELATIVE_TO_PARENT, 0.0f,
                  Animation.RELATIVE_TO_PARENT, 0.0f,
                  Animation.RELATIVE_TO_PARENT, -1.0f);
          outToTop.setDuration(350);
          outToTop.setInterpolator(new AccelerateInterpolator());
          return outToTop;
      }
}
