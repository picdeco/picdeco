package jp.hellodea.picolla.deco.entity;

import com.parse.ParseClassName;
import com.parse.ParseObject;


/**
 * TextHist
 * Created by kazuhiro on 2015/03/09.
 */
@ParseClassName("TextHist")
public class TextHist extends ParseObject{

    public static final String F_APP_USER = "APP_USER";
    public static final String F_FONT_NAME = "FONT_NAME";
    public static final String F_KIND = "KIND";
    public static final String F_COLOR = "COLOR";
    public static final String F_MID_COLOR = "MID_COLOR";
    public static final String F_OUT_COLOR = "OUT_COLOR";
    public static final String F_TOTAL_COUNT = "TOTAL_COUNT";
    public static final String F_LAST_USE_TIME = "LAST_USE_TIME";


}
