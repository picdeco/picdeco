package jp.hellodea.picolla.deco.graphics.parts;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.FontMetrics;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.RectF;

import jp.hellodea.picolla.deco.graphics.SavableMatrix;
import jp.hellodea.picolla.deco.graphics.SavablePath;
import timber.log.Timber;

public class TextObject extends PartObject {

	private CharSequence mText;
	
	private int mKind; //線の種類
	private String mFont; //フォントの種類
	private Paint mMidPaint; //内側の線
	private Paint mOutPaint; //外側の線
	
	private Path mTempPath; //作業用
	private RectF mTempRectF; //作業用
	private Path mTempMaskPath; //作業用

	public TextObject(PartsGroup group, String fontName, int kind,SavableMatrix matrix, CharSequence text,
			int color, int midColor, int outColor, int alpha, boolean undoable,int displaySampleSize) {

		super(group,PartObject.KIND_TEXT,displaySampleSize);
		
		//ペン取得
		mKind = kind;
		Paint[] paints = PartUtil.getPaint(kind, 1, color, midColor, outColor,alpha);
		mPaint = paints[0];
		mMidPaint = paints[1];
		mOutPaint = paints[2];
		
		//フォントの設定
		mFont = fontName;
		mPaint.setTypeface(PartUtil.getTypeface(fontName));
		mPaint.setTextSize(60);
		if(mMidPaint.getStrokeWidth() > 0){
			mMidPaint.setTypeface(PartUtil.getTypeface(fontName));
			mMidPaint.setTextSize(60);
		}
		if(mOutPaint.getStrokeWidth() > 0){
			mOutPaint.setTypeface(PartUtil.getTypeface(fontName));
			mOutPaint.setTextSize(60);
		}
		
		mText = text;
		
		//文章を改行で分割する
		String stringCrlf = System.getProperty("line.separator");
		char charCrlf = stringCrlf.charAt(0);
		ArrayList<CharSequence> texts = new ArrayList<CharSequence>();
		StringBuffer stringBufferLine = new StringBuffer();
		for (int i = 0; i < text.length(); i++) {
			char cha = text.charAt(i);
			if(cha == charCrlf){
				texts.add(stringBufferLine.toString());
				stringBufferLine = new StringBuffer();
			}else{
				stringBufferLine.append(cha);
			}
		}
		if(stringBufferLine.length() > 0) texts.add(stringBufferLine);
		
		mPaint.setStyle(Style.FILL_AND_STROKE);

		//文字のパス作成
		mPath = new SavablePath();
		FontMetrics fontMetrics = mPaint.getFontMetrics();
		float fontHeight = fontMetrics.bottom - fontMetrics.top; //フォントの高さ
		for (int i = 0; i < texts.size(); i++) {
			SavablePath pathLine = new SavablePath();
			mPaint.getTextPath(texts.get(i).toString(), 0,texts.get(i).toString().length(),
					0,fontHeight * (float)i,pathLine); 
			mPath.addPath(pathLine);
		}
		mPath.computeBounds(mMatrixRectF, true); //文字の領域を取得
		
		mMatrix = matrix;
		
		mTempPath = new Path();
		mTempRectF = new RectF();
		mTempMaskPath = new Path();
		
		//文字のマスク作成
		//文字ははじめからマスク領域を計算して常に同期するようにする
		//そうしないと回転したときに大きくなってしまうから
		mMaskPath = new SavablePath();
		mMaskPath.addPath(mPath); //マスク用パス作成
		
		group.addPart(this,undoable);

	}

	public TextObject(PartsGroup group,JSONObject json, boolean undoable) throws JSONException{

		this(
				group,
				json.getString("font"),
				json.getInt("kind"),
				new SavableMatrix(json.getJSONArray("matrix")),
				json.getString("text"),
				json.getInt("color"),
				json.optInt("midColor",0),
				json.optInt("outColor",0),
				json.optInt("alpha",255),
				undoable,
				json.getInt("displaySampleSize")
		);

	}

	public String getFontName(){
		return mFont;
	}
	public int getKind() {
		return mKind;
	}
	public int getColor(){
		return mPaint.getColor();
	}
	public int getMidColor(){
		if(mMidPaint != null)
			return mMidPaint.getColor();
		else
			return 0;
	}
	public int getOutColor(){
		if(mOutPaint != null)
			return mOutPaint.getColor();
		else
			return 0;
	}
	
	public CharSequence getText(){
		return mText;
	}
	
	@Override
	public void flashToBitmap(Canvas canvas,Canvas maskCanves) {
		mTempPath.set(mPath);
		mTempPath.transform(mMatrix);
		
		if(mOutPaint.getStrokeWidth() > 0) canvas.drawPath(mTempPath, mOutPaint);
		if(mMidPaint.getStrokeWidth() > 0) canvas.drawPath(mTempPath, mMidPaint);
		
		canvas.drawPath(mTempPath, mPaint);
		
		//マスク作成
		if(maskCanves != null){
			mMaskPaint.setStyle(Style.FILL_AND_STROKE);
			mTempMaskPath.set(mMaskPath);
			mTempMaskPath.transform(mMatrix);
			maskCanves.drawPath(mTempMaskPath, mMaskPaint);
		}
	}

	@Override
	public void rotate(float degrees){
		mTempRectF.set(mMatrixRectF);
		mMatrix.mapRect(mTempRectF);
		mMatrix.postRotate(degrees,mTempRectF.centerX(),mTempRectF.centerY());
	}

	@Override
	public void scale(float scaleX,float scaleY) {
		
		SavableMatrix tempMatrix = new SavableMatrix(mMatrix);

		mTempRectF.set(mMatrixRectF);
		mMatrix.mapRect(mTempRectF);
		mMatrix.postScale(scaleX,scaleY,mTempRectF.centerX(),mTempRectF.centerY());
		
		//NaNになっていないかチェック
		mTempRectF.set(mMatrixRectF);
		mMatrix.mapRect(mTempRectF);
		if(Math.abs(mTempRectF.width()) < 2 || Math.abs(mTempRectF.height()) < 2){
			mMatrix = tempMatrix;
			return;
		}
		if(mMatrix.toShortString().contains("NaN") == true)
			mMatrix = tempMatrix;
		
	}

	@Override
	public void move(int x, int y) {
		mMatrix.postTranslate(x, y);
	}

	@Override
	public RectF getBounds() {
		mTempRectF.set(mMatrixRectF);
		mMatrix.mapRect(mTempRectF);

		return mTempRectF;
	}

	@Override
	public JSONObject getJsonObject() {
		JSONObject json = new JSONObject();
		try {
			json.put("partKind", PartObject.KIND_TEXT);
			json.put("font", mFont);
			json.put("kind", mKind);
			json.put("text", mText.toString());
			json.put("matrix", mMatrix.getJson());
			json.put("color", mPaint.getColor());
			json.put("displaySampleSize", mDisplaySampleSize);
			if(mMidPaint != null) json.put("midColor", mMidPaint.getColor());
			if(mOutPaint != null) json.put("outColor", mOutPaint.getColor());
			json.put("alpha", mPaint.getAlpha());
			
			
		} catch (JSONException e) {
			Timber.e(e, "");
			throw new RuntimeException(e);
		}
		
		return json;
	}

	@Override
	public void startEditTarget() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void endEditTarget() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void recycle() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void drawRealSize(Canvas canvas) {
		Matrix matrix = new Matrix(mMatrix);
		
		//ペン取得
		Paint[] paints = PartUtil.getPaint(mKind, 1, mPaint.getColor(), 
				mMidPaint.getColor(), mOutPaint.getColor(), mPaint.getAlpha(), mDisplaySampleSize); //実際のサイズに戻す
		Paint paint = paints[0];
		paint.setStyle(Style.FILL_AND_STROKE);

		Paint midPaint = paints[1];
		Paint outPaint = paints[2];

		//実際の縮尺を適用する
		matrix.postScale((float)mDisplaySampleSize, (float)mDisplaySampleSize);
		
		mTempPath.set(mPath);
		mTempPath.transform(matrix);
		
		if(outPaint.getStrokeWidth() > 0)
			canvas.drawPath(mTempPath, outPaint);
		
		if(midPaint.getStrokeWidth() > 0)
			canvas.drawPath(mTempPath, midPaint);
		
		canvas.drawPath(mTempPath, paint);

	
	}

	@Override
	public void deleteAsset() {
		return;
	}


}
