package jp.hellodea.picolla.deco.view;

public interface IClickPlaceListener {

	//操作が選択された時のコールバック
	public void onSelected(float x, float y, int logicalX, int logicalY);
	
}
