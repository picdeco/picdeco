package jp.hellodea.picolla.deco.entity;

import com.parse.ParseClassName;
import com.parse.ParseObject;


/**
 * ClipartHist
 * Created by kazuhiro on 2015/03/09.
 */
@ParseClassName("ClipartHist")
public class ClipartHist extends ParseObject{

    public static final String F_APP_USER = "APP_USER";
    public static final String F_CLIPART_CATEGORY = "CLIPART_CATEGORY";
    public static final String F_CLIPART_KIND = "CLIPART_KIND";
    public static final String F_CLIPART = "CLIPART";
    public static final String F_TOTAL_COUNT = "TOTAL_COUNT";
    public static final String F_LAST_USE_TIME = "LAST_USE_TIME";


}
