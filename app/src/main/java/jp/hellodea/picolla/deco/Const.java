package jp.hellodea.picolla.deco;

public class Const {

	//定数情報
	public static final int DISPLAY_SAMPLE_SIZE = 2; //表示上の縮尺率（実データはSIZE分大きくなる）

	//クリップアートカテゴリ
	public static final String CLIPART_CATEGORY_CLIPART = "clipart";
	public static final String CLIPART_CATEGORY_BACKGROUND = "background";
	public static final String CLIPART_CATEGORY_FRAME = "frame";

	//activityの種類
	public static final int REQUEST_COLOR = 1;	//色選択
	public static final int REQUEST_SIZE = 2;	//サイズ選択
	public static final int REQUEST_PEN = 3;	//ペン選択
	public static final int REQUEST_MODIFY_IMAGE = 4;	//画像加工
	public static final int REQUEST_TEXT = 5;	//テキスト選択
	public static final int REQUEST_FONT = 8;	//フォント選択
	public static final int REQUEST_CROP = 13;	//画像の切り抜き
	public static final int REQUEST_FONT_MANAGER = 23;	//フォント管理
	public static final int REQUEST_FILTER = 25;	//フィルター機能
    public static final int REQUEST_GALLERY_INTENT_CALLED = 27; //画像選択
    public static final int REQUEST_GALLERY_KITKAT_INTENT_CALLED = 28; //画像選択 for kitkat

    //MIME
	public static final String MIME_JPEG = "image/jpeg";
}
