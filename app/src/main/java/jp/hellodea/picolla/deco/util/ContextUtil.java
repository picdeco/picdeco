package jp.hellodea.picolla.deco.util;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import hugo.weaving.DebugLog;
import jp.hellodea.picolla.deco.graphics.WidthHeight;
import timber.log.Timber;

/**
 * Created by kazuhiro on 2015/05/14.
 */
public class ContextUtil {

    //メモリクラスを取得
    @DebugLog
    public static int getAvailableMemoryMega(Context context){

        System.gc();
        System.gc();

        Runtime runtime = Runtime.getRuntime();

        long usedMemory = runtime.totalMemory() - runtime.freeMemory();
        long availableMemory = runtime.maxMemory() - usedMemory;

        return (int) (availableMemory / 1024 / 1024);
    }

    //現在のヒープメモリ状況をデバッグ表示
    @DebugLog
    public static void showHeapMemoryInfo(Context context){
        ActivityManager activityManager = ((ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE));
        Runtime runtime = Runtime.getRuntime();

        Timber.d("LargeMemoryClass %d M",activityManager.getLargeMemoryClass());
        Timber.d("MemoryClass %d M",activityManager.getMemoryClass());
        Timber.d("maxMemory %d KB",runtime.maxMemory() / 1024 );
        Timber.d("freeMemory %d KB",runtime.freeMemory() / 1024 );
        Timber.d("totalMemory %d KB",runtime.totalMemory() / 1024 );
        long usedMemory = runtime.totalMemory() - runtime.freeMemory();
        Timber.d("usedMemory %d KB",usedMemory / 1024 );
        long availableMemory = runtime.maxMemory() - usedMemory;
        Timber.d("availableMemory %d KB",availableMemory / 1024);
    }

    //端末のディスプレイサイズを取得
    public static Point getDisplaySize(Context context){
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size;
    }

    //バージョン情報の取得関連
    public static int getVersionCode( Context context ) {
        int ver;
        try {
            ver = context.getPackageManager().getPackageInfo( context.getPackageName(), 1 ).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            ver = -1;
        }
        return ver;
    }

    public static String getVersionName( Context context ) {
        String ver;
        try {
            ver = context.getPackageManager().getPackageInfo( context.getPackageName(), 1 ).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            ver = "";
        }
        return ver;
    }

    //壁紙サイズを取得する
    public static WidthHeight getWallpaperSize(Context context){
        WidthHeight widthHeight = new WidthHeight();

        // WindowManager の取得
        WallpaperManager manager = WallpaperManager.getInstance(context);

        //解像度取得
        int width = manager.getDesiredMinimumWidth();
        int height = manager.getDesiredMinimumHeight();

        //もし壁紙サイズが取得できなければ画面のサイズを元に計算
        if(width < 0 || height < 0){
            Point size = getDisplaySize(context);
            width = size.x;
            height = size.y;
        }

        widthHeight.width = width;
        widthHeight.height = height;

        return widthHeight;
    }


}
