package jp.hellodea.picolla.deco.graphics.manipulate;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import jp.hellodea.picolla.deco.graphics.SavableMatrix;
import jp.hellodea.picolla.deco.graphics.parts.PartObject;
import jp.hellodea.picolla.deco.graphics.parts.PartsGroup;
import jp.hellodea.picolla.deco.util.ImageUtil;
import timber.log.Timber;

import android.graphics.Bitmap;

public class UndoObject {
	private int mKbn; //Undo区分
	private PartObject mPartObject; //対象オブジェクト
	private int mPosition; //Z位置
	private SavableMatrix mMatrix; //位置情報
	private File mFileBackground; //背景画像
	private PartsGroup mPartsGroup;

	public UndoObject(){
		mKbn = UndoManager.KBN_NONE;
	}

	public UndoObject(int kbn, PartObject part){
		mKbn = kbn;
		mPartObject = part;
		mMatrix = new SavableMatrix();
		mMatrix.set(mPartObject.getMatrix());
		mPosition = mPartObject.getPartsGroup().getPartPosition(mPartObject);
	}
	
	//バックグラウンド用
	public UndoObject(File backgroundImageFile, PartsGroup partsGroup){
		mKbn = UndoManager.KBN_BACKGROUND;
		mFileBackground = backgroundImageFile;
		mPartsGroup = partsGroup;
	}

	public int getKbn() {
		return mKbn;
	}

	//Undo実行
	public void undo(){
		switch (mKbn) {
		
		case UndoManager.KBN_NEW:
			mPartObject.getPartsGroup().removePart(mPartObject,false);
			break;
		
		case UndoManager.KBN_MODIFY:
			mPartObject.setMatrix(mMatrix);
			break;

		case UndoManager.KBN_REMOVE:
			mPartObject.getPartsGroup().addPart(mPosition,mPartObject,false);
			break;
		
		case UndoManager.KBN_POSITION:
			mPartObject.getPartsGroup().changePosition(mPartObject, mPosition,false);
			break;
			
		case UndoManager.KBN_BACKGROUND:
    		Bitmap bitmap;
			try {
				bitmap = ImageUtil.restoreImageFromBufferCache(mFileBackground, mPartsGroup.getDisplaySampleSize(), null);
				mPartsGroup.setBackground(bitmap, true, mFileBackground, false);
			} catch (IOException e) {
				Timber.e(e,"");
				throw new RuntimeException(e);
			}
			break;
		}
		
	}
	
	//すべての情報をクリアする
	public void clear(){
		mPartObject = null;
		mMatrix = null;
	}
}


