package jp.hellodea.picolla.deco.entity;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import jp.hellodea.picolla.common.AWSConst;


/**
 * Clipart
 * Created by kazuhiro on 2015/03/09.
 */
@ParseClassName("Filter")
public class Filter extends ParseObject{

    public static final String F_PATH = "PATH";
    public static final String F_ORDER = "ORDER";
    public static final String F_IS_ACTIVE = "IS_ACTIVE";


    public String getUrl(){
        String url = AWSConst.ASSETS_FILTER_PATH +
                this.getString(F_PATH) + ".acv";
        return url;
    }

    public String getSampleUrl(){
        String url = AWSConst.ASSETS_FILTER_PATH +
                this.getString(F_PATH) + ".jpg";
        return url;
    }


}
