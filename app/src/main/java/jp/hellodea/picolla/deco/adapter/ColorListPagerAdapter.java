package jp.hellodea.picolla.deco.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import hugo.weaving.DebugLog;
import jp.hellodea.picolla.R;
import jp.hellodea.picolla.deco.view.ColorListView;

/**
 * Created by kazuhiro on 2015/04/22.
 */
public class ColorListPagerAdapter extends PagerAdapter{

    List<ColorListView> colorListViews;
    Context context;

    public ColorListPagerAdapter(Context pContext, List<ColorListView> views) {
        context = pContext;
        colorListViews = views;
    }

    @Override
    @DebugLog
    public Object instantiateItem(ViewGroup container, int position) {

        ColorListView colorListView = colorListViews.get(position);

        container.addView(colorListView);

        return colorListView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return colorListViews.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return context.getString(R.string.color1);
            case 1:
                return context.getString(R.string.color2);
            case 2:
                return context.getString(R.string.color3);
        }

        return null;
    }
}
