package jp.hellodea.picolla.deco.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

import jp.hellodea.picolla.deco.entity.Font;
import jp.hellodea.picolla.deco.view.FontSampleItemView;
import jp.hellodea.picolla.deco.view.FontSampleItemView_;

/**
 * フォントアダプター
 * Created by kazuhiro on 2015/04/03.
 */
public class FontSampleImageAdapter extends ArrayAdapter<Font> {

    private List<Font> downloadedFonts;


    public FontSampleImageAdapter(Context context, int resource, List<Font> objects, List<Font> pDownloadedFonts) {
        super(context, resource, objects);

        downloadedFonts = pDownloadedFonts;
    }

    public void addDownloadedFont(Font font){
        downloadedFonts.add(font);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        FontSampleItemView itemView = null;

        if (convertView == null) {  // if it's not recycled, initialize some attributes
            itemView = FontSampleItemView_.build(getContext());
        } else {
            itemView = (FontSampleItemView)convertView;
        }

        itemView.bind(getItem(position).getSampleUrl(),downloadedFonts.contains(getItem(position)));

        return itemView;
    }
}