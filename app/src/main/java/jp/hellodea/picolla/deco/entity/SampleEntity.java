package jp.hellodea.picolla.deco.entity;

import com.parse.ParseClassName;
import com.parse.ParseObject;


/**
 * AppUser
 * Created by kazuhiro on 2015/03/09.
 */
@ParseClassName("SampleEntity")
public class SampleEntity extends ParseObject{

    public static final String F_COLUMN0 = "COLUMN0";
    public static final String F_COLUMN1 = "COLUMN1";
    public static final String F_COLUMN2 = "COLUMN2";
    public static final String F_COLUMN3 = "COLUMN3";
    public static final String F_COLUMN4 = "COLUMN4";
    public static final String F_COLUMN5 = "COLUMN5";
    public static final String F_COLUMN6 = "COLUMN6";
    public static final String F_COLUMN7 = "COLUMN7";
    public static final String F_COLUMN8 = "COLUMN8";
    public static final String F_COLUMN9 = "COLUMN9";

}
