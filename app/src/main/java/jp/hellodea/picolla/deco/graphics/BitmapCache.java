package jp.hellodea.picolla.deco.graphics;

import java.io.File;
import java.io.IOException;

import android.graphics.Bitmap;
import android.util.LruCache;

import hugo.weaving.DebugLog;
import jp.hellodea.picolla.deco.util.ImageUtil;
import timber.log.Timber;

/**
 * bitmap cache
 */
public class BitmapCache {

    private LruCache<File, Bitmap> lruCache;

    @DebugLog
    public BitmapCache() {

        int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        int cacheSize = maxMemory / 3;

        Timber.d("maxMemory:%d  cacheSize:%d",maxMemory,cacheSize);

        //Lruキャッシュを作成
        lruCache = new LruCache<File, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(File keyFile, Bitmap bitmap) {
                return bitmap.getByteCount() / 1024;
            }
        };

    }

    public void evictAll(){
        lruCache.evictAll();
        lruCache = null;
    }

    public Bitmap setOriginalImage(File keyFile, Bitmap originalBitmap, int displaySampleSize) {

        //ディスクに書き込み
        try {
            ImageUtil.storeBitmapToCache(originalBitmap, keyFile);
        } catch (Exception e) {
            Timber.e(e,"bitmap.compress");
            throw new RuntimeException(e);
        }

        Bitmap bitmap;

        //メモリに書き込み
        if(displaySampleSize > 1){
            //縮小が指定されていたら縮小する
            bitmap = Bitmap.createScaledBitmap(originalBitmap, originalBitmap.getWidth() / displaySampleSize,
                    originalBitmap.getHeight() / displaySampleSize, true);
        }else{
            bitmap = originalBitmap;
        }

        lruCache.put(keyFile, bitmap);

        return bitmap;

    }

    /**
     * メモリにだけキャッシュさせる
     * @param keyFile
     * @param cachedBitmap
     */
    public void setCachedImage(File keyFile, Bitmap cachedBitmap){
        lruCache.put(keyFile,cachedBitmap);
    }

    public Bitmap getCachedImage(File keyFile, int displaySampleSize) {
        Bitmap bitmap = lruCache.get(keyFile);

        if(bitmap == null){
            //ファイルの存在チェック。あればビットマップを読み込み
            if(keyFile.exists() == true){

                try {
                    //縮小版で読込
                    bitmap = ImageUtil.restoreImageFromBufferCache(
                            keyFile, displaySampleSize, Bitmap.Config.ARGB_8888);
                } catch (IOException e) {
                    Timber.e(e,"ImageUtil.restoreImageFromBufferCache");
                    throw new RuntimeException(e);
                }

                //メモリにキャッシュ
                lruCache.put(keyFile,bitmap);
            }

        }

        return bitmap;
    }

    public void remove(File keyFile){
        lruCache.remove(keyFile);
    }

}
