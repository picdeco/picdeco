package jp.hellodea.picolla.deco.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;

import hugo.weaving.DebugLog;
import jp.hellodea.picolla.deco.graphics.parts.PartUtil;
import jp.hellodea.picolla.deco.util.ImageUtil;
import jp.hellodea.picolla.deco.util.Util;
import jp.hellodea.picolla.deco.view.SampleTextView;

/**
 * ペン選択画面に表示されるView
 * Created by kazuhiro on 2015/04/20.
 */
public class PenImageAdapter extends BaseAdapter {
    private Context mContext;

    // ペンの種類
    private String[][] mPens = {
            {"1","#0000ff","#000000","#000000"},
            {"2","#7cfc00","#228b22","#000000"},
            {"3","#ffff00","#ffa500","#ff0000"},
            {"4","#9932cc","#000000","#000000"},
            {"5","#ff0000","#000000","#000000"},
            {"6","#ff69b4","#000000","#000000"},
            {"7","#ffffff","#0000ff","#000000"},
            {"8","#ffa500","#000000","#000000"},
            {"9","#ff69b4","#ffffff","#000000"},
            {"10","#000000","#000000","#000000"},
            {"11","#000000","#000000","#000000"},
            {"12","#000000","#000000","#000000"},
            {"13","#000000","#000000","#000000"},
            {"14","#000000","#000000","#000000"},
            {"15","#000000","#000000","#000000"},
            {"16","#000000","#000000","#000000"},
            {"17","#000000","#000000","#000000"},
    };

    public PenImageAdapter(Context c) {
        mContext = c;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mPens.length;
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    @DebugLog
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        SampleTextView textView;
        if (convertView == null) {  // if it's not recycled, initialize some attributes
            textView = new SampleTextView(mContext);
            textView.setLayoutParams(new GridView.LayoutParams(ImageUtil.getPx(mContext, 68), ImageUtil.getPx(mContext, 53)));
        } else {
            textView = (SampleTextView)convertView;
        }

        //描画するペン情報取得
        Paint[] paints = PartUtil.getPaint(
                Integer.valueOf(mPens[position][0]),
                1,
                Color.parseColor(mPens[position][1]),
                Color.parseColor(mPens[position][2]),
                Color.parseColor(mPens[position][3]),
                255
        );
        textView.setText("abc",30);
        textView.setPaints(Integer.valueOf(mPens[position][0]),paints,"TYPEFACE_SANS_SERIF");
        return textView;
    }
}
