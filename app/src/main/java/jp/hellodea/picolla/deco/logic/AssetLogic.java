package jp.hellodea.picolla.deco.logic;

import android.content.Context;

import com.koushikdutta.ion.Ion;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import hugo.weaving.DebugLog;
import jp.hellodea.picolla.R;
import jp.hellodea.picolla.common.ParseCol;
import jp.hellodea.picolla.common.exception.ErrorCode;
import jp.hellodea.picolla.common.exception.LogicException;
import jp.hellodea.picolla.deco.entity.Clipart;
import jp.hellodea.picolla.deco.entity.ClipartKind;
import jp.hellodea.picolla.deco.entity.CuttingShape;
import jp.hellodea.picolla.deco.entity.Effect;
import jp.hellodea.picolla.deco.entity.Filter;
import jp.hellodea.picolla.deco.entity.Font;
import jp.hellodea.picolla.deco.util.StorageFileUtil;
import timber.log.Timber;

/**
 * Created by kazuhiro on 2015/04/06.
 */
public class AssetLogic {

    static final String PIN_CLIPART = "PIN_CLIPART";
    static final String PIN_CUTTING_SHAPE = "PIN_CUTTING_SHAPE";
    static final String PIN_EFFECT = "PIN_EFFECT";
    static final String PIN_FILTER = "PIN_FILTER";
    static final String PIN_FONT = "PIN_FONT";

    //クリップアート用キャッシュ
    HashMap<String,List<ClipartKind>> kindHashMap = new HashMap<>();
    HashMap<ClipartKind,List<Clipart>> clipartHashMap = new HashMap<>();


    /**
     * 全てのクリップアートデータをキャッシュする
     * @throws LogicException
     */
    @DebugLog
    public void refreshAllCliparts(long lastSyncTime) throws LogicException{
        List<Clipart> cliparts = null;
        try {
            cliparts = ParseQuery.getQuery(Clipart.class)
                    .whereGreaterThan(ParseCol.F_UPDATED_AT, new Date(lastSyncTime))
                    .whereEqualTo(Clipart.F_IS_ACTIVE, true)
                    .orderByAscending(Clipart.F_CATEGORY_AND_KIND)
                    .include(Clipart.F_CLIPART_KIND)
                    .setLimit(1000)
                    .find();

            List<Clipart> pinGroup = new ArrayList<>();
            String pinName = "";

            //検索時に遅いので種類ごとにPINする
            for(Clipart clipart: cliparts){
                if(!pinName.equals(PIN_CLIPART + clipart.getString(Clipart.F_CATEGORY_AND_KIND))){

                    //今までのリストをPINする
                    if(pinGroup.size() > 0){
                        ParseObject.pinAll(pinName,pinGroup);
                        Timber.d("pin: %s size:%d", pinName, pinGroup.size());
                    }

                    //新しいPINの準備
                    pinName = PIN_CLIPART + clipart.getString(Clipart.F_CATEGORY_AND_KIND);
                    pinGroup.clear();
                }

                pinGroup.add(clipart);
            }

            //残りのリストをPINする
            if(pinGroup.size() > 0) ParseObject.pinAll(pinName,pinGroup);

        } catch (ParseException e) {
            Timber.e(e, "ParseQuery.getQuery");
            throw new LogicException(ErrorCode.CanNotFind, R.string.error_get_clipart,e);
        }
    }



    /**
     * カテゴリ配下の種類を返す
     * @param category
     * @return
     */
    @DebugLog
    public List<ClipartKind> getKindsInCategory(String category) throws LogicException {

        List<ClipartKind> kinds = kindHashMap.get(category);
        if(kinds != null) return kinds;

        try {
            kinds = ParseQuery.getQuery(ClipartKind.class)
                    .fromLocalDatastore()
                    .whereEqualTo(ClipartKind.F_CATEGORY, category)
                    .whereEqualTo(ClipartKind.F_IS_ACTIVE, true)
                    .orderByAscending(ClipartKind.F_ORDER)
                    .addAscendingOrder(ParseCol.F_CREATED_AT)
                    .find();

            kindHashMap.put(category,kinds);

        } catch (ParseException e) {
            Timber.e(e, "ParseQuery.getQuery");
            throw new LogicException(ErrorCode.CanNotFind, R.string.error_get_clipart_kind,e);
        }

        return kinds;

    }

    /**
     * 指定された種類配下のクリップアートを返す
     * @param kind
     * @param kind
     * @return
     */
    @DebugLog
    public List<Clipart> getClipartsInKind(ClipartKind kind) throws LogicException {

        List<Clipart> cliparts = clipartHashMap.get(kind);
        if(cliparts != null) return cliparts;

        Timber.d("step1");

        try {
            cliparts = ParseQuery.getQuery(Clipart.class)
                    .fromPin(PIN_CLIPART + kind.getString(ClipartKind.F_CATEGORY) + kind.getString(ClipartKind.F_NAME))
                    .whereEqualTo(Clipart.F_IS_ACTIVE, true)
                    .orderByAscending(Clipart.F_ORDER)
                    .addAscendingOrder(ParseCol.F_CREATED_AT)
                    .find();

            Timber.d("step2");

            clipartHashMap.put(kind,cliparts);

            Timber.d("step3");

        } catch (ParseException e) {
            Timber.e(e, "ParseQuery.getQuery");
            throw new LogicException(ErrorCode.CanNotFind, R.string.error_get_clipart,e);
        }

        return cliparts;
    }

    /**
     * 指定されたアセットの画像を返す
     * まだダウンロードしていなければダウンロードする
     * @param context
     * @param url
     * @return
     */
    @DebugLog
    public File getAssetFile(Context context, String url) throws LogicException{


        File file = new File(StorageFileUtil.getTempCacheDir(context),StorageFileUtil.getSafeFileName(url));

        //キャッシュされていない時だけダウンロードする
        if(!file.exists()) try {
            Ion.with(context).load(url).write(file).get();
        } catch (Exception e) {
            Timber.e(e, "Ion");
            throw new LogicException(ErrorCode.CanNotDownload, R.string.error_get_asset_file,e);
        }

        return file;
    }

    /**
     * フィルターデータをキャッシュする
     * @throws LogicException
     */
    @DebugLog
    public void refreshFilters(long lastSyncTime) throws LogicException{
        List<Filter> filters = null;
        try {
            filters = ParseQuery.getQuery(Filter.class)
                    .whereGreaterThan(ParseCol.F_UPDATED_AT,new Date(lastSyncTime))
                    .setLimit(1000)
                    .find();

            ParseObject.pinAll(PIN_FILTER,filters);
        } catch (ParseException e) {
            Timber.e(e, "ParseQuery.getQuery");
            throw new LogicException(ErrorCode.CanNotFind, R.string.error_get_filter,e);
        }
    }

    /**
     * エフェクトデータをキャッシュする
     * @throws LogicException
     */
    @DebugLog
    public void refreshEffects(long lastSyncTime) throws LogicException{
        List<Effect> effects = null;
        try {
            effects = ParseQuery.getQuery(Effect.class)
                    .whereGreaterThan(ParseCol.F_UPDATED_AT, new Date(lastSyncTime))
                    .setLimit(1000)
                    .find();

            ParseObject.pinAll(PIN_EFFECT,effects);
        } catch (ParseException e) {
            Timber.e(e, "ParseQuery.getQuery");
            throw new LogicException(ErrorCode.CanNotFind, R.string.error_get_effect,e);
        }
    }

    /**
     * フィルターを返す
     * @return
     */
    @DebugLog
    public List<Filter> getFilters() throws LogicException {

        List<Filter> filters = null;
        try {
            filters = ParseQuery.getQuery(Filter.class)
                    .fromPin(PIN_FILTER)
                    .orderByAscending(Clipart.F_ORDER)
                    .addAscendingOrder(ParseCol.F_CREATED_AT)
                    .find();
        } catch (ParseException e) {
            Timber.e(e, "ParseQuery.getQuery");
            throw new LogicException(ErrorCode.CanNotFind, R.string.error_get_filter,e);
        }

        return filters;
    }

    /**
     * エフェクトを返す
     * @return
     */
    @DebugLog
    public List<Effect> getEffects() throws LogicException {

        List<Effect> effects = null;
        try {
            effects = ParseQuery.getQuery(Effect.class)
                    .fromPin(PIN_EFFECT)
                    .orderByAscending(Clipart.F_ORDER)
                    .addAscendingOrder(ParseCol.F_CREATED_AT)
                    .find();
        } catch (ParseException e) {
            Timber.e(e, "ParseQuery.getQuery");
            throw new LogicException(ErrorCode.CanNotFind, R.string.error_get_effect,e);
        }

        return effects;
    }

    /**
     * フォントデータをキャッシュする
     * @throws LogicException
     */
    @DebugLog
    public void refreshFonts(long lastSyncTime) throws LogicException{
        List<Font> fonts = null;
        try {
            fonts = ParseQuery.getQuery(Font.class)
                    .whereGreaterThan(ParseCol.F_UPDATED_AT, new Date(lastSyncTime))
                    .setLimit(1000)
                    .find();

            ParseObject.pinAll(PIN_FONT,fonts);
        } catch (ParseException e) {
            Timber.e(e, "ParseQuery.getQuery");
            throw new LogicException(ErrorCode.CanNotFind, R.string.error_get_effect,e);
        }
    }

    /**
     * フォントを返す
     * @return
     */
    @DebugLog
    public List<Font> getFonts() throws LogicException {

        List<Font> fonts = null;
        try {
            fonts = ParseQuery.getQuery(Font.class)
                    .fromPin(PIN_FONT)
                    .orderByAscending(Clipart.F_ORDER)
                    .addAscendingOrder(ParseCol.F_CREATED_AT)
                    .find();
        } catch (ParseException e) {
            Timber.e(e, "ParseQuery.getQuery");
            throw new LogicException(ErrorCode.CanNotFind, R.string.error_get_font,e);
        }

        return fonts;
    }

    /**
     * 切り取りデータをキャッシュする
     * @throws LogicException
     */
    @DebugLog
    public void refreshCuttingShape(long lastSyncTime) throws LogicException{
        List<CuttingShape> cuttingShapes = null;
        try {
            cuttingShapes = ParseQuery.getQuery(CuttingShape.class)
                    .whereGreaterThan(ParseCol.F_UPDATED_AT, new Date(lastSyncTime))
                    .setLimit(1000)
                    .find();

            ParseObject.pinAll(PIN_CUTTING_SHAPE,cuttingShapes);
        } catch (ParseException e) {
            Timber.e(e, "ParseQuery.getQuery");
            throw new LogicException(ErrorCode.CanNotFind, R.string.error_get_cutting_shape,e);
        }
    }

    /**
     * 切り取りデータを返す
     * @return
     */
    @DebugLog
    public List<CuttingShape> getCuttingShapes() throws LogicException {

        List<CuttingShape> cuttingShapes = null;
        try {
            cuttingShapes = ParseQuery.getQuery(CuttingShape.class)
                    .fromPin(PIN_CUTTING_SHAPE)
                    .orderByAscending(Clipart.F_ORDER)
                    .addAscendingOrder(ParseCol.F_CREATED_AT)
                    .find();
        } catch (ParseException e) {
            Timber.e(e, "ParseQuery.getQuery");
            throw new LogicException(ErrorCode.CanNotFind, R.string.error_get_cutting_shape,e);
        }

        return cuttingShapes;
    }

}
