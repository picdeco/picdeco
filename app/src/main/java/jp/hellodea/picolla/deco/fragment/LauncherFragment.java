package jp.hellodea.picolla.deco.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ShareCompat;
import android.view.MenuItem;
import android.widget.ListView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.ads.AdView;
import com.soundcloud.android.crop.Crop;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import bolts.Continuation;
import bolts.Task;
import de.greenrobot.event.EventBus;
import hugo.weaving.DebugLog;
import jp.hellodea.picolla.R;
import jp.hellodea.picolla.common.AWSConst;
import jp.hellodea.picolla.common.eventbus.CallScreenEvent;
import jp.hellodea.picolla.common.eventbus.CompleteBackgroundEvent;
import jp.hellodea.picolla.common.eventbus.ErrorEvent;
import jp.hellodea.picolla.common.eventbus.ScreenResultEvent;
import jp.hellodea.picolla.common.eventbus.SelectEvent;
import jp.hellodea.picolla.common.exception.LogicException;
import jp.hellodea.picolla.common.fragment.AlertDialogFragment;
import jp.hellodea.picolla.common.fragment.AlertDialogFragment_;
import jp.hellodea.picolla.common.objectgraph.ObjectGraphHolder;
import jp.hellodea.picolla.deco.Const;
import jp.hellodea.picolla.deco.Flurry;
import jp.hellodea.picolla.deco.activity.MainEditorActivity;
import jp.hellodea.picolla.deco.adapter.PictureAdapter;
import jp.hellodea.picolla.deco.entity.AppUser;
import jp.hellodea.picolla.deco.entity.Work;
import jp.hellodea.picolla.deco.eventbus.ProgressDialogEvent;
import jp.hellodea.picolla.deco.graphics.BitmapCacheHolder;
import jp.hellodea.picolla.deco.logic.AppUserLogic;
import jp.hellodea.picolla.deco.logic.AssetLogic;
import jp.hellodea.picolla.deco.logic.EditLogic;
import jp.hellodea.picolla.deco.logic.WorkLogic;
import jp.hellodea.picolla.deco.util.AdUtil;
import jp.hellodea.picolla.deco.util.EventUtil;
import jp.hellodea.picolla.deco.util.ImageUtil;
import jp.hellodea.picolla.deco.util.PreferenceUtil;
import jp.hellodea.picolla.deco.util.StorageFileUtil;
import jp.hellodea.picolla.deco.util.Util;
import jp.hellodea.picolla.deco.view.PictureItemView;
import timber.log.Timber;

/**
 * Created by kazuhiro on 2015/04/09.
 */
@EFragment(resName = "fragment_launcher")
@OptionsMenu(resName = "menu_launcher")
public class LauncherFragment extends Fragment {

    public static final String EVENT_KEY_DELETE = "EVENT_KEY_DELETE";
    public static final String EVENT_KEY_SAVE = "EVENT_KEY_SAVE";

    @Inject
    AssetLogic assetLogic;

    @Inject
    WorkLogic workLogic;

    @Inject
    EditLogic editLogic;

    @Inject
    AppUserLogic appUserLogic;

    @ViewById
    ListView listViewPicture;

    @ViewById
    AdView adView;


    File outImageFile;

    Work currentWork;

    @DebugLog
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ObjectGraphHolder.get().inject(this);

        initializeAssetData();

        ShareCompat.IntentReader intentReader = ShareCompat.IntentReader.from(getActivity());
        Uri uri = intentReader.getStream();

        //外部から共有されたら受け取る
        if(uri != null) receiveImage(uri);

        // AdView をリソースとしてルックアップしてリクエストを読み込む
        adView.loadAd(AdUtil.getAdRequest());

    }

    @DebugLog
    @Override
    public void onStart() {
        super.onStart();

        EventBus.getDefault().registerSticky(this);
    }

    @DebugLog
    @Override
    public void onStop() {
        super.onStop();

        EventBus.getDefault().unregister(this);

    }

    @Background
    void initializeAssetData() {

        AppUser appUser = AppUser.getCurrent();

        try {

            if (appUser == null) {
                appUserLogic.createAnonymousUser(getActivity());
            }

            long lastSyncTime =
                    (long) PreferenceUtil.read(getActivity(), PreferenceUtil.Key.LAST_SYNC_ASSETS, 0L);

            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, -1);

            if(lastSyncTime < calendar.getTime().getTime()){
                //前回より１日経過したら素材の更新
                assetLogic.refreshAllCliparts(lastSyncTime);
                assetLogic.refreshEffects(lastSyncTime);
                assetLogic.refreshFilters(lastSyncTime);
                assetLogic.refreshFonts(lastSyncTime);
                assetLogic.refreshCuttingShape(lastSyncTime);

                PreferenceUtil.write(getActivity(), PreferenceUtil.Key.LAST_SYNC_ASSETS, System.currentTimeMillis());

            }

        } catch (LogicException e) {
            EventBus.getDefault().post(new ErrorEvent(this.getClass(), e));
            return;
        }
    }

    @Background
    void getPictures() {

        List<Work> works;

        try {
            works = workLogic.getWorks();
        } catch (LogicException e) {
            EventBus.getDefault().post(new ErrorEvent(this.getClass(), e));
            return;
        }

        EventBus.getDefault().postSticky(new CompleteBackgroundEvent(LauncherFragment.class, works));

    }


    @DebugLog
    public void onEventMainThread(CompleteBackgroundEvent event) {
        if (!event.getEventKey().equals(LauncherFragment.class)) return;

        List<Work> works = (List<Work>) event.getArg();

        PictureAdapter pictureAdapter = new PictureAdapter(getActivity(), R.layout.item_picture, works);
        listViewPicture.setAdapter(pictureAdapter);

        Timber.d("works.size %d", works.size());

        if(works.size() == 0){
            actionSelectSize();
        }

        EventBus.getDefault().removeStickyEvent(event);

    }


    /**
     * サイズ選択
     */
    private void actionSelectSize(){
        FlurryAgent.logEvent(Flurry.EVENT_ID_CREATE_NEW_PICTURE);

        SizeSelectorDialogFragment fragment = new SizeSelectorDialogFragment();
        fragment.show(getFragmentManager(), SizeSelectorDialogFragment.class.getName());
    }


    /**
     * 写真選択
     */
    private void actionSelectPicture() {
        EventBus.getDefault().post(new CallScreenEvent(Crop.REQUEST_PICK));
    }

    @DebugLog
    private void actionSelectWork(Work work) {

        FlurryAgent.logEvent(Flurry.EVENT_ID_UPDATE_PICTURE);

        //store work proxy id to preference
        PreferenceUtil.write(getActivity(), PreferenceUtil.Key.CURRENT_WORK, work.get(Work.F_PROXY_ID));

        System.gc();
        EventBus.getDefault().post(new CallScreenEvent(MainEditorActivity.class));


    }

    /**
     * ギャラリーに保存
     * @param work
     */
    private void actionSave(final Work work) {

        FlurryAgent.logEvent(Flurry.EVENT_ID_EXPORT_PICTURE);

        EventBus.getDefault().postSticky(
                new ProgressDialogEvent(ProgressDialogEvent.Status.START,
                        R.string.progress_processing_final_picture, ProgressDialog.STYLE_SPINNER));

        //キャッシュクリア
        BitmapCacheHolder.deleteCache();

        Task<Void> task = Task.callInBackground(new Callable<Void>() {
            @Override
            public Void call() throws Exception {

                //ギャラリーに登録
                editLogic.storeGallery(getActivity(), work);

                return null;
            }
        }).onSuccess(new Continuation<Void, Void>() {
            @Override
            public Void then(Task task) throws Exception {

                AlertDialogFragment dialogFragment = AlertDialogFragment_.builder()
                        .eventKey(EVENT_KEY_SAVE)
                        .titleRes(R.string.alert_title_info)
                        .messageRes(R.string.completed_storing_picture)
                        .positiveRes(android.R.string.ok)
                        .build();
                dialogFragment.show(getFragmentManager(), AlertDialogFragment.class.getName());

                return null;
            }
        }, Task.UI_THREAD_EXECUTOR).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(Task task) throws Exception {
                EventBus.getDefault().postSticky(new ProgressDialogEvent(ProgressDialogEvent.Status.END,0,0));

                if(task.isFaulted()){
                    Timber.e(task.getError(), "");
                    EventBus.getDefault().postSticky(new ErrorEvent(MainEditorActivity.class,task.getError()));
                }
                return null;
            }
        },Task.UI_THREAD_EXECUTOR);

    }

    /**
     * 共有
     */
    @DebugLog
    private void actionShare(final Work work){

        FlurryAgent.logEvent(Flurry.EVENT_ID_SHARE_PICTURE);

        EventBus.getDefault().postSticky(
                new ProgressDialogEvent(ProgressDialogEvent.Status.START,
                        R.string.progress_processing_final_picture, ProgressDialog.STYLE_SPINNER));

        //キャッシュクリア
        BitmapCacheHolder.deleteCache();

        Task<Object> task = Task.callInBackground(new Callable<Object>() {
            @Override
            public Object call() throws Exception {

                //ギャラリーに登録
                File pictureFile = editLogic.outputPicture(getActivity(), work);

                return pictureFile;
            }
        }).onSuccess(new Continuation<Object, Object>() {
            @Override
            public Object then(Task task) throws Exception {

                File file = (File) task.getResult();

                ShareCompat.IntentBuilder builder = ShareCompat.IntentBuilder.from(getActivity());
                builder.setType(Const.MIME_JPEG).setStream(Uri.fromFile(file)).startChooser();

                return null;
            }
        }, Task.UI_THREAD_EXECUTOR).continueWith(new Continuation<Object, Object>() {
            @Override
            public Object then(Task task) throws Exception {
                EventBus.getDefault().postSticky(new ProgressDialogEvent(ProgressDialogEvent.Status.END,0,0));

                if(task.isFaulted()){
                    Timber.e(task.getError(),"");
                    EventBus.getDefault().postSticky(new ErrorEvent(MainEditorActivity.class,task.getError()));
                }
                return null;
            }
        },Task.UI_THREAD_EXECUTOR);

    }

    /**
     * 削除確認
     */
    @DebugLog
    private void confirmDelete(final Work work){
        currentWork = work;

        AlertDialogFragment dialogFragment = AlertDialogFragment_.builder()
                .eventKey(EVENT_KEY_DELETE)
                .titleRes(R.string.alert_title_inquiry)
                .messageRes(R.string.confirm_delete)
                .positiveRes(android.R.string.ok)
                .negativeRes(android.R.string.cancel)
                .build();

        dialogFragment.show(getFragmentManager(), AlertDialogFragment.class.getName());

    }



    /**
     * 削除
     */
    @DebugLog
    private void actionDelete(){

        FlurryAgent.logEvent(Flurry.EVENT_ID_DELETE_PICTURE);

        EventBus.getDefault().postSticky(
                new ProgressDialogEvent(ProgressDialogEvent.Status.START,
                        R.string.progress_deleting_picture, ProgressDialog.STYLE_SPINNER));

        Task<Object> task = Task.callInBackground(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                //作品を削除
                workLogic.deleteWork(getActivity(),currentWork);
                currentWork = null;

                return null;
            }
        }).onSuccess(new Continuation<Object, Object>() {
            @Override
            public Object then(Task task) throws Exception {

                //リスト再読み込み
                getPictures();

                return null;
            }
        }, Task.UI_THREAD_EXECUTOR).continueWith(new Continuation<Object, Object>() {
            @Override
            public Object then(Task task) throws Exception {
                EventBus.getDefault().postSticky(new ProgressDialogEvent(ProgressDialogEvent.Status.END,0,0));

                if(task.isFaulted()){
                    Timber.e(task.getError(),"");
                    EventBus.getDefault().postSticky(new ErrorEvent(MainEditorActivity.class,task.getError()));
                }
                return null;
            }
        },Task.UI_THREAD_EXECUTOR);

    }

    /**
     * 新規作品の作成開始
     */
    private void startNewWork(SizeSelectorDialogFragment.Size size) {

        Work work;
        try {
            work = workLogic.createWork();
        } catch (LogicException e) {
            EventBus.getDefault().post(new ErrorEvent(this.getClass(), e));
            return;
        }

        //store work proxy id to preference
        PreferenceUtil.write(getActivity(), PreferenceUtil.Key.CURRENT_WORK, work.get(Work.F_PROXY_ID));

        System.gc();

        if(size != null){
            //白紙で新規作成
            int width = 0;
            int height = 0;

            switch (size){
                case SQUARE:
                    FlurryAgent.logEvent(Flurry.EVENT_ID_PICTURE_SIZE_SQUARE);
                    width = ImageUtil.getMaxImageSize(getActivity());
                    height = width;
                    break;
                case LANDSCAPE:
                    FlurryAgent.logEvent(Flurry.EVENT_ID_PICTURE_SIZE_LANDSCAPE);
                    width = ImageUtil.getMaxImageSize(getActivity());
                    height = (int) ((float)width / 1.333);
                    break;
                case PORTRAIT:
                    FlurryAgent.logEvent(Flurry.EVENT_ID_PICTURE_SIZE_PORTRAIT);
                    height = ImageUtil.getMaxImageSize(getActivity());
                    width = (int) ((float)height / 1.333);
                    break;
            }

            EventBus.getDefault().post(new CallScreenEvent(MainEditorActivity.class,width,height));

        } else{
            FlurryAgent.logEvent(Flurry.EVENT_ID_PICTURE_FROM_GALLERY);

            //選んだ写真で新規作成
            EventBus.getDefault().post(new CallScreenEvent(MainEditorActivity.class, Uri.fromFile(outImageFile)));
        }

    }

    /**
     * get selected picture
     *
     * @param event
     */
    @DebugLog
    public void onEventMainThread(ScreenResultEvent event) {
        Intent intent = (Intent) event.getArg();

        //写真選択後、画像切り取りを呼び出し
        if (event.getEventKey().equals(Crop.REQUEST_PICK)) {
            //get a picture from this intent
            Uri inImageUri = intent.getData();

//            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
//                getActivity().getContentResolver().takePersistableUriPermission(inImageUri,Intent.FLAG_GRANT_READ_URI_PERMISSION);
//            }

            outImageFile = StorageFileUtil.createNewRandomFile(StorageFileUtil.getTempCacheDir(getActivity()), "");
            Uri outImageUri = Uri.fromFile(outImageFile);

            EventBus.getDefault().post(new CallScreenEvent(Crop.REQUEST_CROP, inImageUri, outImageUri));

            //remove the sticky event after using
            EventBus.getDefault().removeStickyEvent(event);

        }

        //写真切抜き後、編集画面表示
        if (event.getEventKey().equals(Crop.REQUEST_CROP)) {
            startNewWork(null);

            //remove the sticky event after using
            EventBus.getDefault().removeStickyEvent(event);
        }

    }

    /**
     * error handling
     *
     * @param event
     */
    @DebugLog
    public void onEventMainThread(ErrorEvent event) {

        EventUtil.showErrorAlertDialog(getActivity(), event);
    }

    /**
     * select event
     *
     * @param event
     */
    @DebugLog
    public void onEventMainThread(SelectEvent event) {

        if (event.getEventKey().equals(PictureItemView.class)){
            //写真カード内の選択結果
            PictureItemView.Action action = (PictureItemView.Action) event.getArgs()[0];
            Work work = (Work) event.getArgs()[1];

            switch (action) {
                case SHARE:
                    actionShare(work);
                    break;
                case SAVE:
                    actionSave(work);
                    break;
                case EDIT:
                    actionSelectWork(work);
                    break;
                case DELETE:
                    confirmDelete(work);
                    break;
            }

        } else if (event.getEventKey().equals(EVENT_KEY_DELETE)){
            //削除確認の選択結果
            if (event.getArg().equals(android.R.string.ok)) actionDelete();

        } else if(event.getEventKey().equals(SizeSelectorDialogFragment.class)){
            //新規作成の画像サイズ選択結果
            if(event.getArg().equals(SizeSelectorDialogFragment.Size.PICTURE)){

                //ギャラリーから選択
                actionSelectPicture();
            }else{
                SizeSelectorDialogFragment.Size size = (SizeSelectorDialogFragment.Size) event.getArg();

                //白紙を作成
                startNewWork(size);
            }
        }



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case R.id.actionNew:
                actionSelectSize();
                return true;
            case R.id.actionLicense:
                actionShowLicenses();
                return true;
            case R.id.actionAgreement:
                actionShowAgreement();
                return true;
            case R.id.actionPrivacy:
                actionShowPrivacy();
                return true;
        }

        return false;
    }

    @Override
    public void onResume() {
        super.onResume();

        getPictures();
    }

    /**
     * show progress dialog
     * @param event
     */
    public void onEventMainThread(ProgressDialogEvent event){
        EventUtil.showProgressDialog(getActivity(), event);
    }

    @DebugLog
    private void receiveImage(Uri uri){
        outImageFile = StorageFileUtil.createNewRandomFile(StorageFileUtil.getTempCacheDir(getActivity()), "");
        Uri outImageUri = Uri.fromFile(outImageFile);

        EventBus.getDefault().postSticky(new CallScreenEvent(Crop.REQUEST_CROP, uri, outImageUri));

    }

    /**
     * サイズ選択ダイアログ
     */
    public static class SizeSelectorDialogFragment extends DialogFragment {

        public enum Size{
            PICTURE, SQUARE, LANDSCAPE, PORTRAIT
        }

        @Override
        public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

            String[] selector = new String[4];
            selector[0] = getActivity().getString(R.string.select_size_picture);
            selector[1] = getActivity().getString(R.string.select_size_square);
            selector[2] = getActivity().getString(R.string.select_size_landscape);
            selector[3] = getActivity().getString(R.string.select_size_portrait);

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.choose_picture)
                    .setItems(selector, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Size size = null;
                            switch (which) {
                                case 0:
                                    size = Size.PICTURE;
                                    break;
                                case 1:
                                    size = Size.SQUARE;
                                    break;
                                case 2:
                                    size = Size.LANDSCAPE;
                                    break;
                                case 3:
                                    size = Size.PORTRAIT;
                                    break;
                            }

                            EventBus.getDefault().post(new SelectEvent(SizeSelectorDialogFragment.class, size));
                        }
                    });

            return builder.create();
        }
    }

    @DebugLog
    private void actionShowLicenses(){
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.picolla.club/licenses/"));
        startActivity(intent);
    }

    @DebugLog
    private void actionShowAgreement(){
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(AWSConst.LEGAL_DOCUMENTS_PATH + "agreement.html"));
        startActivity(intent);
    }

    @DebugLog
    private void actionShowPrivacy(){
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(AWSConst.LEGAL_DOCUMENTS_PATH + "privacy.html"));
        startActivity(intent);
    }

}

