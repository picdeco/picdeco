package jp.hellodea.picolla.deco.util;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.ProgressDialog;

import de.greenrobot.event.EventBus;
import jp.hellodea.picolla.R;
import jp.hellodea.picolla.common.eventbus.ErrorEvent;
import jp.hellodea.picolla.common.eventbus.ProgressEvent;
import jp.hellodea.picolla.common.exception.LogicException;
import jp.hellodea.picolla.common.fragment.AlertDialogFragment;
import jp.hellodea.picolla.common.fragment.AlertDialogFragment_;
import jp.hellodea.picolla.common.fragment.ProgressDialogFragment;
import jp.hellodea.picolla.common.fragment.ProgressDialogFragment_;
import jp.hellodea.picolla.deco.eventbus.ProgressDialogEvent;

/**
 * Created by kazuhiro on 2015/04/22.
 */
public class EventUtil {

    /**
     * show progress dialog
     * @param activity
     * @param event
     */
    public static void showProgressDialog(Activity activity, ProgressDialogEvent event){
        if(event.getEventKey().equals(ProgressDialogEvent.Status.START)){
            ProgressDialogFragment fragment = ProgressDialogFragment_.builder()
                    .titleRes(R.string.progress_title)
                    .messageRes((Integer) event.getArgs()[0])
                    .progressStyle((Integer) event.getArgs()[1])
                    .build();
            fragment.show(activity.getFragmentManager(),ProgressDialogFragment.class.getName());
        }else{
            ProgressDialogFragment fragment = (ProgressDialogFragment) activity.getFragmentManager().
                    findFragmentByTag(ProgressDialogFragment.class.getName());
            fragment.dismiss();
        }

        //remove sticky event after using.
        EventBus.getDefault().removeStickyEvent(event);

    }

    /**
     * update progress in the progress dialog
     * @param activity
     * @param event
     */
    public static void updateProgressDialog(Activity activity, ProgressEvent event){

        ProgressDialogFragment fragment = (ProgressDialogFragment) activity.getFragmentManager().
                findFragmentByTag(ProgressDialogFragment.class.getName());

        if(fragment == null) return;

        ProgressDialog progressDialog = (ProgressDialog) fragment.getDialog();

        progressDialog.setProgress(event.progress);
        progressDialog.setMax(event.max);

        //remove sticky event after using.
        EventBus.getDefault().removeStickyEvent(event);

    }

    /**
     * show error alert dialog
     * @param activity
     * @param event
     */
    public static void showErrorAlertDialog(Activity activity, ErrorEvent event){
        LogicException e = (LogicException) event.getException();

        //show dialog when error was happened
        DialogFragment dialogFragment = AlertDialogFragment_.builder()
                .eventKey(AlertDialogFragment.class.getName())
                .titleRes(e.getErrorCode().getErrorRes())
                .messageRes(e.getReasonRes())
                .positiveRes(android.R.string.ok).build();
        dialogFragment.show(activity.getFragmentManager()
                ,AlertDialogFragment.class.getName());

        //remove sticky event after using.
        EventBus.getDefault().removeStickyEvent(event);

    }
}
