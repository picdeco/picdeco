package jp.hellodea.picolla.deco.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

import jp.hellodea.picolla.deco.entity.Clipart;
import jp.hellodea.picolla.deco.view.SelectableImageView;
import jp.hellodea.picolla.deco.view.SelectableImageView_;

/**
 * クリップアートアダプター
 * Created by kazuhiro on 2015/04/03.
 */
public class ClipartAdapter extends ArrayAdapter<Clipart> {


    public ClipartAdapter(Context context, int resource, List<Clipart> objects) {
        super(context, resource, objects);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        SelectableImageView itemView;

        if (convertView == null) {  // if it's not recycled, initialize some attributes
            itemView = SelectableImageView_.build(getContext());
        } else {
            itemView = (SelectableImageView)convertView;
        }

        itemView.bind(Uri.parse(getItem(position).getThumbUrl()));

        return itemView;
    }
}