package jp.hellodea.picolla.deco.activity;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.InterstitialAd;
import com.parse.ParseAnalytics;
import com.soundcloud.android.crop.Crop;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;

import java.util.Random;

import de.greenrobot.event.EventBus;
import hugo.weaving.DebugLog;
import jp.hellodea.picolla.R;
import jp.hellodea.picolla.common.eventbus.CallScreenEvent;
import jp.hellodea.picolla.common.eventbus.ScreenResultEvent;
import jp.hellodea.picolla.deco.Const;
import jp.hellodea.picolla.deco.fragment.LauncherFragment_;
import jp.hellodea.picolla.deco.util.AdUtil;
import jp.hellodea.picolla.deco.util.ImageUtil;
import jp.hellodea.picolla.deco.util.Util;
import timber.log.Timber;

@EActivity(resName = "activity_launcher")
public class LauncherActivity extends ActionBarActivity {

    private InterstitialAd interstitial;

    @Extra
    Boolean fromMainEditor;


    @Override
    @DebugLog
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ParseAnalytics.trackAppOpenedInBackground(getIntent());

        setContentView(R.layout.activity_launcher);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new LauncherFragment_())
                    .commit();
        }


        initInterstitial();
    }

    private void initInterstitial(){
        interstitial = new InterstitialAd(this);
        interstitial.setAdUnitId("ca-app-pub-2660194064048264/4013574633");
        interstitial.loadAd(AdUtil.getAdRequest());
    }

    // Invoke displayInterstitial() when you are ready to display an interstitial.
    @DebugLog
    private boolean displayInterstitial() {

        Random random = new Random(System.currentTimeMillis());
        int n = random.nextInt(3);
        Timber.d("random %d", n);

//        if(n > 0) return false;

        if (interstitial != null && interstitial.isLoaded()) {
            interstitial.show();
            initInterstitial();
            return true;
        }

        return false;
    }

    @Override
    @DebugLog
    protected void onNewIntent(Intent intent) {
        setIntent(intent);

        // エディターからの戻りだったら広告準備
        if(fromMainEditor != null && fromMainEditor == true){
            displayInterstitial();
        }
    }

    @DebugLog
    @Override
    public void onStart() {
        super.onStart();

        EventBus.getDefault().registerSticky(this);
    }

    @DebugLog
    @Override
    public void onStop() {
        super.onStop();

        EventBus.getDefault().unregister(this);

    }

    /**
     * change new fragment screen
     * @param event
     */
    @DebugLog
    public void onEventMainThread(CallScreenEvent event){

        if(event.getEventKey().equals(Crop.REQUEST_PICK)){
            //写真選択
            Crop.pickImage(this);

        }else if(event.getEventKey().equals(Crop.REQUEST_CROP)){
            //画像切り抜き
            Uri inImageUri = (Uri) event.getArgs()[0];
            Uri outImageUri = (Uri)event.getArgs()[1];

            int maxSize = ImageUtil.getMaxImageSize(this);

            new Crop(inImageUri).output(outImageUri).withMaxSize(
                    maxSize * Const.DISPLAY_SAMPLE_SIZE,
                    maxSize * Const.DISPLAY_SAMPLE_SIZE).start(this);
        } else if(event.getEventKey().equals(MainEditorActivity.class)){
            //編集画面
            Intent intent = new Intent(this,MainEditorActivity.class);

            if(event.getArg() != null){

                if(event.getArg() instanceof Uri){
                    intent.setData((Uri) event.getArg());
                }else{
                    int width = (int) event.getArgs()[0];
                    int height = (int) event.getArgs()[1];
                    intent.putExtra("width",width);
                    intent.putExtra("height",height);
                }

            }

            startActivity(intent);

        }

        EventBus.getDefault().removeStickyEvent(event);
    }

    @DebugLog
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode != RESULT_OK || data == null) return;

        //post activity result event
        EventBus.getDefault().postSticky(new ScreenResultEvent(requestCode,data));

    }


}
