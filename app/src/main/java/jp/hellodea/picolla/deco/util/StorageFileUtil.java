package jp.hellodea.picolla.deco.util;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.util.Arrays;
import java.util.Comparator;
import java.util.UUID;

import hugo.weaving.DebugLog;
import jp.hellodea.picolla.R;
import timber.log.Timber;

/**
 * Created by kazuhiro on 2015/04/30.
 */
public class StorageFileUtil {

    //外部ストレージのキャッシュファイルを一括クリアする（全てではない）
    public static void clearExternalTempFiles(Context context){
        //ついでにSDカードの古くなったキャッシュファイルも削除する
        //リミットを超えていたら超えた分を古い順に削除する
        final int limitFiles = 500;
        File[] oldFiles = getTempCacheDir(context).listFiles();
        if(oldFiles.length > limitFiles){
            Comparator<File> comparator = new Comparator<File>() {
                @Override
                public int compare(File object1, File object2) {
                    Long obj1Time = object1.lastModified();
                    Long obj2Time = object2.lastModified();

                    return obj1Time.compareTo(obj2Time);
                }
            };
            Arrays.sort(oldFiles, comparator);
            for (int i = 0; i < oldFiles.length - limitFiles; i++) {
                Log.d("kazino", "date:" + oldFiles[i].lastModified());
                oldFiles[i].delete();
            }
        }
    }

    /**
     * 共有の写真の出力先ディレクトリを返す
     * @param context
     * @return
     */
    public static File getPictureOutputDir(Context context){
        File parentDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File outputDir = new File(parentDir, context.getString(R.string.app_name));

        if(!outputDir.exists()) outputDir.mkdirs();

        return outputDir;
    }

    /**
     * チャネルを使って高速ファイルコピー
     * @param in
     * @param out
     * @throws Exception
     */
    public static void copyFile(File in, File out) throws IOException {
        FileChannel sourceChannel = new FileInputStream(in).getChannel();
        FileChannel destinationChannel = new FileOutputStream(out).getChannel();
        sourceChannel.transferTo(0, sourceChannel.size(), destinationChannel);

        sourceChannel.close();
        destinationChannel.close();
    }

    /**
     * 入力ストリームから出力ストリームへデータの書き込みを行います。
     * 尚、コピー処理終了後、入力・出力ストリームを閉じます。
     * @param inputStream    入力ストリーム
     * @param outputStream    出力ストリーム
     * @throws IOException    何らかの入出力処理例外が発生した場合
     */
    @DebugLog
    public static void copyStream(InputStream inputStream, OutputStream outputStream) throws IOException {
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);

        int len = -1;
        byte[] b = new byte[10 * 1024];
        try {
            while ((len = bufferedInputStream.read(b, 0, b.length)) != -1) {
                bufferedOutputStream.write(b, 0, len);
            }
            bufferedOutputStream.flush();
        } finally {
            if (bufferedInputStream != null) {
                try {
                    bufferedInputStream.close();
                } catch (IOException e) {
                    Timber.e(e, "");
                }
            }
            if (bufferedOutputStream != null) {
                try {
                    bufferedOutputStream.close();
                } catch (IOException e) {
                    Timber.e(e, "");
                }
            }
        }
    }


    //新しいランダムファイルを作成する
    public static File createNewRandomFile(File dir, String ext){
        File cacheFile = new File(dir, UUID.randomUUID().toString() + ext);

        return cacheFile;
    }

    //フォントデータの保存先ディレクトリ取得
    public static File getFontsDir(Context context){
        //フォントフォルダ作成
        File fontDir = new File(context.getExternalFilesDir(null) + "/fonts");
        if(fontDir.exists() == false) fontDir.mkdir();

        return fontDir;

    }


    //内部ストレージのキャッシュ先ディレクトリを取得する
    //AndroidSDKのディレクトリ取得APIは絶対に利用しない
    public static File getImagePartsDir(Context context){
        //return context.getDir(Setting.TEMP_DIR,Context.MODE_PRIVATE);

        //過去の写真を再編集できるようにデータを残すため写真用のキャッシュを外部ストレージに変更する
        File tmpDir = new File(context.getExternalFilesDir(null) + "/parts");
        if(tmpDir.exists() == false) tmpDir.mkdir();

        return tmpDir;

    }

    //外部ストレージのキャッシュ先ディレクトリを取得する
    //AndroidSDKのディレクトリ取得APIは絶対に利用しない
    public static File getTempCacheDir(Context context){
        return context.getExternalCacheDir();
    }

    //ファイル名に使える文字置換
    public static String getSafeFileName(String original){
        String safeName = original.replaceAll("[<>:*?\"/\\|]", "_"); //ファイル名に使える文字に変換
        return safeName;
    }


}
