package jp.hellodea.picolla.deco.logic;

import android.content.Context;

import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.json.JSONObject;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import bolts.Continuation;
import bolts.Task;
import hugo.weaving.DebugLog;
import jp.hellodea.picolla.R;
import jp.hellodea.picolla.common.exception.ErrorCode;
import jp.hellodea.picolla.common.exception.LogicException;
import jp.hellodea.picolla.deco.entity.AppUser;
import jp.hellodea.picolla.deco.entity.Work;
import jp.hellodea.picolla.deco.graphics.parts.PartsGroup;
import jp.hellodea.picolla.deco.util.StorageFileUtil;
import timber.log.Timber;

/**
 * Created by kazuhiro on 2015/04/18.
 */
public class WorkLogic {

    static final String PIN_WORK = "PIN_WORK";

    /**
     * create new work
     * @return
     */
    @DebugLog
    public Work createWork() throws LogicException {
        Work work = new Work();

        work.put(Work.F_APP_USER, AppUser.getCurrent());
        work.put(Work.F_PROXY_ID, UUID.randomUUID().toString());
        work.put(Work.F_POST_TIME, new Date());
        ParseACL parseACL = new ParseACL();
        parseACL.setReadAccess(ParseUser.getCurrentUser(),true);
        parseACL.setWriteAccess(ParseUser.getCurrentUser(),true);
        work.setACL(parseACL);

        saveWork(work);

        return work;
    }

    @DebugLog
    public List<Work> getWorks() throws LogicException{
        List<Work> works = null;

        try {
            works = ParseQuery.getQuery(Work.class)
                    .fromPin(PIN_WORK)
                    .whereEqualTo(Work.F_APP_USER,AppUser.getCurrent())
                    .orderByDescending(Work.F_POST_TIME)
                    .find();
        } catch (ParseException e) {
            Timber.e(e, "ParseException");
            throw new LogicException(ErrorCode.CanNotFind, R.string.error_get_work, e);
        }

        return works;
    }


    /**
     * get work by proxy id
     * @param proxyId
     * @return
     * @throws LogicException
     */
    @DebugLog
    public Work getWork(String proxyId) throws LogicException {
        Work work;
        try {
            work = ParseQuery.getQuery(Work.class)
                    .fromPin(PIN_WORK)
                    .whereEqualTo(Work.F_PROXY_ID, proxyId)
                    .getFirst();
        } catch (ParseException e) {
            Timber.e(e, "ParseException");
            throw new LogicException(ErrorCode.CanNotFind, R.string.error_get_work, e);
        }

        return work;
    }

    /**
     * save work
     * @param work
     * @throws LogicException
     */
    @DebugLog
    public void saveWork(Work work) throws LogicException{

        try {
            work.pin(PIN_WORK);
            Timber.d("work.pin");
        } catch (ParseException e) {
            Timber.e(e, "ParseException");
            throw new LogicException(ErrorCode.CanNotSave, R.string.error_save_work, e);
        }

        work.saveEventually().continueWith(new Continuation<Void, Object>() {
            @Override
            public Object then(Task<Void> task) throws Exception {
                Timber.d("finish work.saveEventually");
                if(task.isFaulted()){
                    Timber.e(task.getError(),"");
                }
                return null;
            }
        });

        Timber.d("work.saveEventually");
    }

    @DebugLog
    public void deleteWork(Context context, Work work) throws LogicException {
        //jsonファイル読み込み
        JSONObject json;
        PartsGroup partsGroup;

        try {
            json = new JSONObject(work.getString(Work.F_DATA));
            partsGroup = new PartsGroup(json, StorageFileUtil.getImagePartsDir(context));

            //アセットファイル削除
            partsGroup.deleteAsset();

            //サムネイル画像を削除
            File thumbFile = work.getThumbFile(context);
            thumbFile.delete();

        } catch (Exception e) {
            Timber.e(e, "");
            //データが消せなくなるので、ここのエラーは無視する。
        }

        try {
            work.unpin();
        } catch (ParseException e) {
            Timber.e(e, "ParseException");
            throw new LogicException(ErrorCode.CanNotDelete, R.string.error_delete_work, e);
        }

        work.deleteEventually();


    }
}
