package jp.hellodea.picolla.deco.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.widget.ImageView;

import jp.hellodea.picolla.R;

public class DirectionIcon extends ImageView implements OnGestureListener{

	private GestureDetector mGestureDetector;

	private IDirectionListener mDirectionListener; //コールバック
	
	private int mTargetLeft; //ターゲットのLEFT
	private int mTargetTop; //ターゲットのTOP
	private int mTargetWidth; //ターゲットのWIDTH
	private int mTargetHeight; //ターゲットのHEIGHT
	
	private int mLeft; //現在の位置
	private int mTop; //現在の位置
	
	private float mLastLeft; //以前の位置
	private float mLastTop; //以前の位置
	
	private float mLastRawX; //以前の全画面上での位置
	private float mLastRawY; //以前の全画面上での位置
	
	private int mMode; //現在のモード
	public final static int MODE_RESIZE = 0; 
	public final static int MODE_ROTATE = 1; 
	public final static int MODE_MOVE = 2; 
	public final static int MODE_RESIZE_FREE = 3; 
	public final static int MODE_DELETE = 4; 
	public final static int MODE_UP = 5; 
	public final static int MODE_DOWN = 6; 
	public final static int MODE_CHANGE = 7; 
	
	public final static float ICON_SIZE_DIP = 40.0f; //指示アイコンの大きさ(dip)
	
	private int mIconSize; //アイコンのサイズ（ディスプレイサイズ非依存）
	
	private boolean mIsMoved; //選択後移動したかどうか
	
	public DirectionIcon(Context context, IDirectionListener listener, int mode) {
		super(context);
		
		this.setImageResource(R.drawable.icon_move);
		this.setScaleType(ScaleType.FIT_CENTER);
		
		mGestureDetector = new GestureDetector(this);
		
		mMode = mode;
		
		mDirectionListener = listener;
		
		switch(mMode){
		case MODE_MOVE:
			this.setImageResource(R.drawable.icon_move);
			break;
		case MODE_RESIZE:
			this.setImageResource(R.drawable.icon_scale);
			break;
		case MODE_RESIZE_FREE:
			this.setImageResource(R.drawable.icon_scale_free);
			break;
		case MODE_ROTATE:
			this.setImageResource(R.drawable.icon_rotate);
			break;
		case MODE_DELETE:
			this.setImageResource(R.drawable.icon_delete);
			break;
		case MODE_UP:
			this.setImageResource(R.drawable.icon_up);
			break;
		case MODE_DOWN:
			this.setImageResource(R.drawable.icon_down);
			break;
		case MODE_CHANGE:
			this.setImageResource(R.drawable.icon_update);
			break;
		}

		setActive(false);
		
		//アイコンの表示サイズを求める
		mIconSize = (int) (ICON_SIZE_DIP * context.getResources().getDisplayMetrics().density);
	}
	
	//表示する
	public void show(int targetLeft, int targetTop, int targetWidth, int targetHeight){

		mTargetLeft = targetLeft;
		mTargetTop = targetTop;
		mTargetWidth = targetWidth;
		mTargetHeight = targetHeight;

		switch(mMode){
		case MODE_MOVE:
			mLeft = mTargetLeft + (int)(mTargetWidth * 0.8f) + mIconSize * -1;
			mTop = mTargetTop + mTargetHeight;
			break;
		case MODE_RESIZE:
			mLeft = mTargetLeft + (int)(mTargetWidth * 0.8f) + mIconSize * 0;
			mTop = mTargetTop + mTargetHeight;
			break;
		case MODE_RESIZE_FREE:
			mLeft = mTargetLeft + (int)(mTargetWidth * 0.8f) + mIconSize * 1;
			mTop = mTargetTop + mTargetHeight;
			break;
		case MODE_ROTATE:
			mLeft = mTargetLeft + (int)(mTargetWidth * 0.8f) + mIconSize * 2;
			mTop = mTargetTop + mTargetHeight;
			break;
		case MODE_DELETE:
			mLeft = mTargetLeft + mIconSize * -1;
			mTop = mTargetTop - mIconSize;
			break;
		case MODE_UP:
			mLeft = mTargetLeft + mIconSize * 0;
			mTop = mTargetTop - mIconSize;
			break;
		case MODE_DOWN:
			mLeft = mTargetLeft + mIconSize * 1;
			mTop = mTargetTop - mIconSize;
			break;
		case MODE_CHANGE:
			mLeft = mTargetLeft + mIconSize * 2;
			mTop = mTargetTop - mIconSize;
			break;
		}


		this.layout(mLeft, mTop, mLeft + mIconSize, mTop + mIconSize);
		setActive(true);
		invalidate();

Log.d("kazino", "shown:" + isShown() + ":visiblity:" + getVisibility() + ":left" + mLeft + ":top" + mTop);
	
	}
	
	//ON,OFF
	public void setActive(boolean flg){
		if(flg == true){
			this.setEnabled(true);
			this.setVisibility(VISIBLE);
		}else{
			this.setEnabled(false);
			this.setVisibility(INVISIBLE);
		}
	}
	
	public boolean getActive(){
		if(this.getVisibility() == VISIBLE) return true;
		else return false;
	}
	
	//移動
	public void postMove(int x, int y){
		mLeft += x;
		mTop += y;
		this.layout(mLeft, mTop, mLeft + mIconSize, mTop + mIconSize);

	}
	

	@Override
	protected void onDraw(Canvas canvas) {
//		canvas.drawColor(Color.CYAN);
		super.onDraw(canvas);

		this.layout(mLeft, mTop, mLeft + mIconSize, mTop + mIconSize);
		
		Log.d("kazino", "mode " + mMode);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {

		//GestureDetectorに離されたイベントがないので独自判断
		if(event.getAction() == MotionEvent.ACTION_UP){
			mDirectionListener.onFinishMove(mMode,mIsMoved);
			mIsMoved = false;
			return true;
		}
		
		return mGestureDetector.onTouchEvent(event);
		
	}

	@Override
	public boolean onDown(MotionEvent e) {
    	mLastLeft = mLeft;
    	mLastTop = mTop;
    	
    	mLastRawX = e.getRawX();
    	mLastRawY = e.getRawY();
    	
    	mIsMoved = false;
    	
    	mDirectionListener.onSelected(mMode);

    	return true;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onLongPress(MotionEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		
		float distX,distY;

		//現在地を移動
        mLeft += (int)(e2.getRawX() - mLastRawX);
    	mTop += (int)(e2.getRawY() - mLastRawY);
    	
    	mIsMoved = true;

		Log.d("kazino", "Scroll(" + distanceX + "," + distanceY + ")");
		
		//モードによってコールバックする
        switch (mMode) {
			case MODE_MOVE:
				//移動した距離をコールバック
				mDirectionListener.onMove((int)(e2.getRawX() - mLastRawX), (int)(e2.getRawY() - mLastRawY));
				break;
			case MODE_RESIZE:
				//縮尺をコールバック
				//前回の時点での距離
				distX = mLastLeft - mTargetLeft;
				distY = mLastTop - mTargetTop;
				double lastDistance = Math.sqrt(distX * distX + distY * distY); 

				//今回の距離
				distX = mLeft - mTargetLeft;
				distY = mTop - mTargetTop;
				double currentDistance = Math.sqrt(distX * distX + distY * distY); 

				mDirectionListener.onResize((float) (currentDistance / lastDistance));
				
				break;
			case MODE_RESIZE_FREE:
				//縮尺をコールバック
				//前回の時点での距離
				float lastDistX = mLastLeft - mTargetLeft;
				float lastDistY = mLastTop - mTargetTop;

				//今回の距離
				float currentDistX = mLeft - mTargetLeft;
				float currentDistY = mTop - mTargetTop;

				mDirectionListener.onResizeFree(currentDistX / lastDistX, currentDistY / lastDistY);
				
				break;
			case MODE_ROTATE:
				//回転をコールバック
				//前回の時点の角度をアークタンジェントで求める
				distX = mLastLeft - (mTargetLeft + (mTargetWidth / 2));
				distY = mLastTop - (mTargetTop + (mTargetHeight / 2));
				double lastTan = Math.atan2(distY,distX);
				double lastDegrees = Math.toDegrees(lastTan);

				//今回の時点の角度をアークタンジェントで求める
				distX = mLeft - (mTargetLeft + (mTargetWidth / 2));
				distY = mTop - (mTargetTop + (mTargetHeight / 2));
				double currentTan = Math.atan2(distY,distX);
				double currentDegrees = Math.toDegrees(currentTan);

				Log.d("kazino", "leftTop:" + mLeft + ":" + mTop);
				Log.d("kazino", "target:" + mTargetLeft + ":" + mTargetTop );
				Log.d("kazino", "widthHeight:" + mTargetWidth  + ":" + mTargetHeight );
				Log.d("kazino", "center:" + (mTargetLeft + (mTargetWidth / 2))  + ":" + (mTargetTop + (mTargetHeight / 2)) );
				Log.d("kazino", "degree:" + currentDegrees + ":" + distX + ":" + distY);
				
				mDirectionListener.onRotate((float) (currentDegrees - lastDegrees));
				
//				Log.d("kazino", "last " + lastTan + ":" + lastDegrees + " current " + currentTan + ":" + currentDegrees);				

double a = Math.atan2(100, 50);
double b = Math.atan2(-100, 50);
double c = Math.atan2(-100, -50);
double d = Math.atan2(100, -50);
double ad = Math.toDegrees(a);
double bd = Math.toDegrees(b);
double cd = Math.toDegrees(c);
double dd = Math.toDegrees(d);
int g = 1;

				break;
		}



    	//現在地を保存
    	mLastLeft = mLeft;
    	mLastTop = mTop;
    	mLastRawX = e2.getRawX();
    	mLastRawY = e2.getRawY();


    	this.invalidate();
		
		
		return true;
	}

	@Override
	public void onShowPress(MotionEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		return false;
	}
	
	
	
	

}
