package jp.hellodea.picolla.deco.entity;

import android.content.Context;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.io.File;

import jp.hellodea.picolla.deco.util.StorageFileUtil;


/**
 * Work
 * Created by kazuhiro on 2015/03/09.
 */
@ParseClassName("Work")
public class Work extends ParseObject{

    public static final String F_APP_USER = "APP_USER";
    public static final String F_PROXY_ID = "PROXY_ID";
    public static final String F_DATA = "DATA";
    public static final String F_POST_TIME = "POST_TIME";

    /**
     * get thumbnail image file
     * @param context
     * @return
     */
    public File getThumbFile(Context context){
        File dir = StorageFileUtil.getImagePartsDir(context);

        File thumbFile = new File(dir,this.getString(F_PROXY_ID));

        return thumbFile;
    }


}
