package jp.hellodea.picolla.deco.util;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Set;

/**
 * Created by kazuhiro on 2015/04/18.
 */
public class PreferenceUtil {

    public enum Key{
        CURRENT_WORK,
        LAST_SYNC_ASSETS,
    }


    public static final String FILE_NAME = "picolla_preferences";

    public static void write(Context context, Key key, Object data) {
        SharedPreferences preferences = context.getSharedPreferences(FILE_NAME, context.MODE_PRIVATE);

        SharedPreferences.Editor editor = preferences.edit();

        String keyString = key.name();

        if (data instanceof Boolean) {
            editor.putBoolean(keyString, (Boolean) data);
        } else if (data instanceof Float) {
            editor.putFloat(keyString, (Float) data);
        } else if (data instanceof Integer) {
            editor.putInt(keyString, (Integer) data);
        } else if (data instanceof Long) {
            editor.putLong(keyString, (Long) data);
        } else if (data instanceof String) {
            editor.putString(keyString, (String) data);
        } else if (data instanceof Set<?>) {
            editor.putStringSet(keyString, (Set<String>) data);
        }

        editor.apply();
    }

    public static Object read(Context context, Key key, Object defaultVal){
        SharedPreferences preferences = context.getSharedPreferences(FILE_NAME, context.MODE_PRIVATE);

        Object ret = null;
        String keyString = key.name();

        if (defaultVal instanceof Boolean) {
            ret = preferences.getBoolean(keyString, (Boolean) defaultVal);
        } else if (defaultVal instanceof Float) {
            ret = preferences.getFloat(keyString, (Float) defaultVal);
        } else if (defaultVal instanceof Integer) {
            ret = preferences.getInt(keyString, (Integer) defaultVal);
        } else if (defaultVal instanceof Long) {
            ret = preferences.getLong(keyString, (Long) defaultVal);
        } else if (defaultVal instanceof String) {
            ret = preferences.getString(keyString, (String) defaultVal);
        } else if (defaultVal instanceof Set<?>) {
            ret = preferences.getStringSet(keyString, (Set<String>) defaultVal);
        }

        return ret;
    }
}
