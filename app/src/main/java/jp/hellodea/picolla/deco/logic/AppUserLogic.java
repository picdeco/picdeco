package jp.hellodea.picolla.deco.logic;

import android.content.Context;

import com.parse.ParseACL;
import com.parse.ParseAnonymousUtils;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import bolts.Task;
import hugo.weaving.DebugLog;
import jp.hellodea.picolla.R;
import jp.hellodea.picolla.common.exception.ErrorCode;
import jp.hellodea.picolla.common.exception.LogicException;
import jp.hellodea.picolla.deco.entity.AppUser;
import jp.hellodea.picolla.deco.entity.Clipart;
import jp.hellodea.picolla.deco.entity.PenHist;
import jp.hellodea.picolla.deco.entity.TextHist;
import timber.log.Timber;

/**
 * Created by kazuhiro on 2015/03/18.
 */
public class AppUserLogic {

    static final String PIN_APP_USER = "PIN_APP_USER";
    static final String PIN_PEN_HIST = "PIN_PEN_HIST";
    static final String PIN_TEXT_HIST = "PIN_TEXT_HIST";


    /**
     * create anonymous user
     * @param context
     * @throws LogicException
     */
    @DebugLog
    public void createAnonymousUser(Context context) throws LogicException {

        //login as anonymous user
        Task<ParseUser> task = ParseAnonymousUtils.logInInBackground();

        try {
            task.waitForCompletion();
        } catch (InterruptedException e) {
            Timber.e(e, "InterruptedException");
            throw new LogicException(ErrorCode.UnexpectedError, R.string.error_create_anonymous_user,e);
        }

        ParseUser parseUser = ParseUser.getCurrentUser();

        //set default access control
        ParseACL defaultACL = new ParseACL(parseUser);
        defaultACL.setPublicReadAccess(true);
        defaultACL.setPublicWriteAccess(false);
        ParseACL.setDefaultACL(defaultACL, true);

        //create appUser for this app.
        AppUser appUser = new AppUser();
        appUser.put(AppUser.F_LANG, Locale.getDefault().getLanguage());

        try {
            //store in local
            appUser.pin(PIN_APP_USER);

            //store in cloud
            parseUser.put(AppUser.class.getSimpleName(), appUser);
            ParseACL acl = new ParseACL(parseUser);
            acl.setPublicReadAccess(false);
            acl.setPublicWriteAccess(false);
            parseUser.setACL(acl);
            parseUser.save();
        } catch (ParseException e) {
            Timber.e(e,"ParseException");
            throw new LogicException(ErrorCode.ErrorOnDB,R.string.error_create_anonymous_user,e);
        }

    }

    /**
     * get user's text histories
     * @return
     * @throws LogicException
     */
    @DebugLog
    public List<TextHist> getTextHists() throws LogicException {
        List<TextHist> textHists = null;

        ParseQuery query = ParseQuery.getQuery(TextHist.class)
                .whereEqualTo(TextHist.F_APP_USER, AppUser.getCurrent())
                .orderByDescending(TextHist.F_LAST_USE_TIME);

        try {
            textHists = query.fromPin(PIN_TEXT_HIST).find();

            //なければサーバから取得
            if(textHists.size() == 0){
                textHists = query.find();
                ParseObject.pinAll(PIN_TEXT_HIST,textHists);
            }
        } catch (ParseException e) {
            Timber.e(e, "ParseException");
            throw new LogicException(ErrorCode.CanNotFind,R.string.error_get_text_hist,e);
        }

        return textHists;
    }

    /**
     * save text hist
     * @param fontName
     * @param kind
     * @param color
     * @param midColor
     * @param outColor
     * @throws LogicException
     */
    @DebugLog
    public void saveTextHist(String fontName, int kind, int color, int midColor, int outColor) throws LogicException {

        TextHist textHist = null;

        try {
            textHist = ParseQuery.getQuery(TextHist.class)
                    .fromPin(PIN_TEXT_HIST)
                    .whereEqualTo(TextHist.F_APP_USER, AppUser.getCurrent())
                    .whereEqualTo(TextHist.F_FONT_NAME, fontName)
                    .whereEqualTo(TextHist.F_KIND, kind)
                    .whereEqualTo(TextHist.F_COLOR, color)
                    .whereEqualTo(TextHist.F_MID_COLOR, midColor)
                    .whereEqualTo(TextHist.F_OUT_COLOR, outColor)
                    .getFirst();
        } catch (ParseException e) {
            if(e.getCode() == ParseException.OBJECT_NOT_FOUND){
                //create new entity
                textHist = new TextHist();
                textHist.put(TextHist.F_APP_USER, AppUser.getCurrent());
                textHist.put(TextHist.F_FONT_NAME, fontName);
                textHist.put(TextHist.F_KIND, kind);
                textHist.put(TextHist.F_COLOR, color);
                textHist.put(TextHist.F_MID_COLOR, midColor);
                textHist.put(TextHist.F_OUT_COLOR, outColor);
                textHist.put(TextHist.F_TOTAL_COUNT, 0);
            }else{
                Timber.e(e, "ParseException");
                throw new LogicException(ErrorCode.CanNotFind,R.string.error_get_text_hist,e);
            }
        }

        int total = textHist.getInt(TextHist.F_TOTAL_COUNT);
        textHist.put(TextHist.F_TOTAL_COUNT,total + 1);
        textHist.put(TextHist.F_LAST_USE_TIME, new Date());

        try {
            textHist.pin(PIN_TEXT_HIST);
        } catch (ParseException e) {
            Timber.e(e, "ParseException");
            throw new LogicException(ErrorCode.CanNotSave,R.string.error_save_text_hist,e);
        }

        textHist.saveEventually();

    }

    /**
     * get user's pen histories
     * @return
     * @throws LogicException
     */
    @DebugLog
    public List<PenHist> getPenHists() throws LogicException {
        List<PenHist> penHists = null;

        ParseQuery query = ParseQuery.getQuery(PenHist.class)
                .whereEqualTo(PenHist.F_APP_USER, AppUser.getCurrent())
                .orderByDescending(PenHist.F_LAST_USE_TIME);

        try {
            penHists = query.fromPin(PIN_PEN_HIST).find();

            //なければサーバから取得
            if(penHists.size() == 0){
                penHists = query.find();
                if(penHists.size() > 0)
                    ParseObject.pinAll(PIN_PEN_HIST,penHists);
            }
        } catch (ParseException e) {
            Timber.e(e, "ParseException");
            throw new LogicException(ErrorCode.CanNotFind,R.string.error_get_text_hist,e);
        }

        return penHists;
    }

    /**
     * save pen hist
     * @param kind
     * @param color
     * @param midColor
     * @param outColor
     * @throws LogicException
     */
    @DebugLog
    public void savePenHist(int kind, int color, int midColor, int outColor) throws LogicException {

        PenHist penHist = null;

        try {

            penHist = ParseQuery.getQuery(PenHist.class)
                    .fromPin(PIN_PEN_HIST)
                    .whereEqualTo(PenHist.F_APP_USER, AppUser.getCurrent())
                    .whereEqualTo(PenHist.F_KIND, kind)
                    .whereEqualTo(PenHist.F_COLOR, color)
                    .whereEqualTo(PenHist.F_MID_COLOR, midColor)
                    .whereEqualTo(PenHist.F_OUT_COLOR, outColor)
                    .getFirst();
        } catch (ParseException e) {
            if(e.getCode() == ParseException.OBJECT_NOT_FOUND){
                //create new entity
                penHist = new PenHist();
                penHist.put(PenHist.F_APP_USER, AppUser.getCurrent());
                penHist.put(PenHist.F_KIND, kind);
                penHist.put(PenHist.F_COLOR, color);
                penHist.put(PenHist.F_MID_COLOR, midColor);
                penHist.put(PenHist.F_OUT_COLOR, outColor);
                penHist.put(PenHist.F_TOTAL_COUNT, 0);
            }else{
                Timber.e(e, "ParseException");
                throw new LogicException(ErrorCode.CanNotFind,R.string.error_get_pen_hist,e);
            }
        }

        int total = penHist.getInt(PenHist.F_TOTAL_COUNT);
        penHist.put(PenHist.F_TOTAL_COUNT,total + 1);
        penHist.put(PenHist.F_LAST_USE_TIME, new Date());

        try {
            penHist.pin(PIN_PEN_HIST);
        } catch (ParseException e) {
            Timber.e(e, "ParseException");
            throw new LogicException(ErrorCode.CanNotSave,R.string.error_save_pen_hist,e);
        }

        penHist.saveEventually();

    }


}
