package jp.hellodea.picolla.deco.view;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.bumptech.glide.Glide;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import hugo.weaving.DebugLog;
import jp.hellodea.picolla.R;

/**
 * Created by kazuhiro on 2015/04/05.
 */
@EViewGroup(resName = "item_selectable_image")
public class SelectableImageView extends LinearLayout{

    @ViewById
    View viewWrap;

    @ViewById
    ImageView imageViewClipArt;

    public SelectableImageView(Context context) {
        super(context);
    }

    public SelectableImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * bind diary columns to view
     */
    @DebugLog
    public void bind(Uri imageUri){
        Glide.with(getContext()).load(imageUri).fitCenter().into(imageViewClipArt);
    }

    public void setSelectedStatus(boolean selected){
        if(selected == true) viewWrap.setBackgroundColor(getResources().getColor(R.color.sub3_dark));
        else viewWrap.setBackgroundColor(Color.TRANSPARENT);
    }

}
