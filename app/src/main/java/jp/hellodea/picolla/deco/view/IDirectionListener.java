package jp.hellodea.picolla.deco.view;

public interface IDirectionListener {

	//操作が選択された時のコールバック
	public void onSelected(int mode);
	
	//サイズ変更のコールバック
	public void onResize(float scale);

	//サイズ変更のコールバック
	public void onResizeFree(float scaleX, float scaleY);

	//回転変更のコールバック
	public void onRotate(float degrees);

	//位置変更のコールバック
	public void onMove(int moveX, int moveY);
	
	//操作完了のコールバック
	public void onFinishMove(int mode, boolean isMoved);
	
}
