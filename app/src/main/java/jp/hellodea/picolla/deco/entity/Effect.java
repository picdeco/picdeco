package jp.hellodea.picolla.deco.entity;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import jp.hellodea.picolla.common.AWSConst;


/**
 * Effect
 * Created by kazuhiro on 2015/03/09.
 */
@ParseClassName("Effect")
public class Effect extends ParseObject{

    public static final String F_PATH = "PATH";
    public static final String F_ORDER = "ORDER";
    public static final String F_IS_ACTIVE = "IS_ACTIVE";


    public String getUrl(){
        String url = AWSConst.ASSETS_EFFECT_PATH +
                this.getString(F_PATH) + ".png";
        return url;
    }

    public String getSampleUrl(){
        String url = AWSConst.ASSETS_EFFECT_PATH +
                this.getString(F_PATH) + "_t.png";
        return url;
    }


}
