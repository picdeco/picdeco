package jp.hellodea.picolla.deco.graphics;

/**
 * bitmap cache holder
 * Created by kazuhiro on 2015/02/15.
 */
public class BitmapCacheHolder {
    private static BitmapCache bitmapCache;

    public static BitmapCache get(){
        if(bitmapCache == null)
            bitmapCache = new BitmapCache();

        return bitmapCache;
    }

    public static void deleteCache(){
        if(bitmapCache != null){
            bitmapCache.evictAll();
            bitmapCache = null;
        }

        System.gc();
    }
}
