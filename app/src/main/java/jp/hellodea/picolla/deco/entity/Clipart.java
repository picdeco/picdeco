package jp.hellodea.picolla.deco.entity;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import jp.hellodea.picolla.common.AWSConst;


/**
 * Clipart
 * Created by kazuhiro on 2015/03/09.
 */
@ParseClassName("Clipart")
public class Clipart extends ParseObject{

    public static final String F_CLIPART_KIND = "CLIPART_KIND";
    public static final String F_CATEGORY_AND_KIND = "CATEGORY_AND_KIND";
    public static final String F_PATH = "PATH";
    public static final String F_EXT = "EXT";
    public static final String F_ORDER = "ORDER";
    public static final String F_IS_ACTIVE = "IS_ACTIVE";


    public String getUrl(){
        String url = AWSConst.ASSETS_CLIPART_PATH +
                this.getParseObject(Clipart.F_CLIPART_KIND).getString(ClipartKind.F_CATEGORY) + "/" +
                this.getParseObject(Clipart.F_CLIPART_KIND).getString(ClipartKind.F_NAME) + "/" +
                this.getString(Clipart.F_PATH) + this.getString(Clipart.F_EXT);
        return url;
    }

    public String getThumbUrl(){
        String url = AWSConst.ASSETS_CLIPART_PATH +
                this.getParseObject(Clipart.F_CLIPART_KIND).getString(ClipartKind.F_CATEGORY) + "/" +
                this.getParseObject(Clipart.F_CLIPART_KIND).getString(ClipartKind.F_NAME) + "/" +
                this.getString(Clipart.F_PATH) + "_t" + this.getString(Clipart.F_EXT);
        return url;
    }


}
