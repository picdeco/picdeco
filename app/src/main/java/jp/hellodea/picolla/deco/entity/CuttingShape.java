package jp.hellodea.picolla.deco.entity;

import com.parse.ParseClassName;
import com.parse.ParseObject;


/**
 * CuttingShape
 * Created by kazuhiro on 2015/03/09.
 */
@ParseClassName("CuttingShape")
public class CuttingShape extends ParseObject{

    public static final String F_SVG = "SVG";
    public static final String F_ORDER = "ORDER";
    public static final String F_IS_ACTIVE = "IS_ACTIVE";

}
