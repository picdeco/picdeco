package jp.hellodea.picolla.deco.activity;

import bolts.Continuation;
import bolts.Task;
import de.greenrobot.event.EventBus;
import hugo.weaving.DebugLog;
import jp.hellodea.picolla.R;
import jp.hellodea.picolla.common.eventbus.ErrorEvent;
import jp.hellodea.picolla.common.exception.LogicException;
import jp.hellodea.picolla.common.fragment.AlertDialogFragment;
import jp.hellodea.picolla.common.fragment.AlertDialogFragment_;
import jp.hellodea.picolla.common.objectgraph.ObjectGraphHolder;
import jp.hellodea.picolla.deco.Const;
import jp.hellodea.picolla.deco.adapter.PenImageAdapter;
import jp.hellodea.picolla.deco.entity.TextHist;
import jp.hellodea.picolla.deco.graphics.parts.PartUtil;
import jp.hellodea.picolla.deco.logic.AppUserLogic;
import jp.hellodea.picolla.deco.util.ImageUtil;
import jp.hellodea.picolla.deco.util.Util;
import jp.hellodea.picolla.deco.view.SampleTextView;
import timber.log.Timber;

import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

public class SelectTextActivity extends ActionBarActivity {

    @Inject
    AppUserLogic appUserLogic;

	private String mCurrentFontName;		//現在のフォント
	private int mCurrentPenKind;		//現在のペンの種類
	private int mCurrentPenColor;		//色
	private int mCurrentPenMidColor;	//色
	private int mCurrentPenOutColor;	//色
	private int mCurrentPenAlpha;	//透過
	private String mCurrentText;

	private SampleTextView mSampleTextView; //プレビュー用ペン
	private GridView mPenListGridView; //ペンリスト
	private Button mOkButton;
	private ImageButton mColorButton;
	private ImageButton mFontButton;
	private EditText mEditText;
	private LinearLayout mLinearLayoutHistories; //履歴表示レイアウト
	private SeekBar mSeekBarAlpha;

	
	@Override
    @DebugLog
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        ObjectGraphHolder.get().inject(this);

		//レイアウトを指定
        setContentView(R.layout.activity_select_text);

		//ソフトキーボードを非表示
		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		//UI取得
        mPenListGridView = (GridView) findViewById(R.id.gridViewPenList);
        mPenListGridView.setAdapter(new PenImageAdapter(this));
        mSampleTextView = (SampleTextView)findViewById(R.id.sampleTextView);
        mOkButton = (Button)findViewById(R.id.buttonOk);
        mColorButton = (ImageButton)findViewById(R.id.buttonColor);
        mEditText = (EditText)findViewById(R.id.editText);
        mFontButton = (ImageButton)findViewById(R.id.buttonFont);
        mLinearLayoutHistories = (LinearLayout)findViewById(R.id.linearLayoutHistories);
        mSeekBarAlpha = (SeekBar)findViewById(R.id.seekBarAlpha);
        
        //フォントのロード
        PartUtil.loadPartAssets(this, false);

        //現在のペンをパラメータより取得
        Intent intent = getIntent();
        mCurrentFontName = intent.getStringExtra("fontName");
        mCurrentPenKind = intent.getIntExtra("penKind",0);
        mCurrentPenColor = intent.getIntExtra("penColor",0);
        mCurrentPenMidColor = intent.getIntExtra("penMidColor",0);
        mCurrentPenOutColor = intent.getIntExtra("penOutColor",0);
        if(intent.getStringExtra("text") != null){
        	actionChangeText(intent.getStringExtra("text"));
        	mEditText.setText(intent.getStringExtra("text"));
        }else
        	actionChangeText(getText(R.string.sample_text).toString());
        mCurrentPenAlpha = intent.getIntExtra("penAlpha",255);
        mSeekBarAlpha.setProgress(mCurrentPenAlpha);
		setPenToPreview();


        //イベントリスナー設定
        mPenListGridView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
				actionSelectPen(view);
			}
		});
        mColorButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				actionShowColorActivity();
			}
		});
        mFontButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				actionShowFontActivity();
			}
		});
        mEditText.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				actionChangeText(s.toString());
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {
			}
			@Override
			public void afterTextChanged(Editable s) {
			}
		});
        mSeekBarAlpha.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {
				actionChangeAlpha(progress);
			}
		});

        setTextHistory();
        
	}

    @DebugLog
    private void setTextHistory(){

        Task.callInBackground(new Callable<List<TextHist>>() {
            @Override
            public List<TextHist> call() throws Exception {
                return appUserLogic.getTextHists();
            }
        }).onSuccess(new Continuation<List<TextHist>, Object>() {
            @Override
            public Object then(Task<List<TextHist>> task) throws Exception {

                List<TextHist> textHists = task.getResult();

                for(TextHist textHist: textHists){
                    SampleTextView textView = new SampleTextView(getApplicationContext());
                    Paint[] paints = PartUtil.getPaint(
                            textHist.getInt(TextHist.F_KIND),
                            1,
                            textHist.getInt(TextHist.F_COLOR),
                            textHist.getInt(TextHist.F_MID_COLOR),
                            textHist.getInt(TextHist.F_OUT_COLOR),
                            255);
                    textView.setPaints(
                            textHist.getInt(TextHist.F_KIND),
                            paints,
                            textHist.getString(TextHist.F_FONT_NAME));

                    textView.setText("abc",30);
                    textView.setLayoutParams(new LinearLayout.LayoutParams(
                            ImageUtil.getPx(SelectTextActivity.this, 68), ImageUtil.getPx(SelectTextActivity.this, 53)));

                    //クリックイベント追加
                    textView.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            actionSelectPen(v);
                        }
                    });
                    mLinearLayoutHistories.addView(textView);

                }

                return null;
            }
        },Task.UI_THREAD_EXECUTOR).continueWith(new Continuation<Object, Object>() {
            @Override
            public Object then(Task<Object> task) throws Exception {
                if(task.isFaulted()){
                    Timber.e(task.getError(),"");
                    EventBus.getDefault().post(
                            new ErrorEvent(SelectTextActivity.this,task.getError()));
                }
                return null;
            }
        },Task.UI_THREAD_EXECUTOR);


    }

	//色、大きさの選択結果を受けて終了
	@Override
    @DebugLog
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		if(resultCode == RESULT_OK){
			
			switch(requestCode){
			case Const.REQUEST_COLOR:
				//色情報取得
				mCurrentPenColor = data.getIntExtra("penColor",0);
				mCurrentPenMidColor = data.getIntExtra("penMidColor",0);
				mCurrentPenOutColor = data.getIntExtra("penOutColor",0);
				setPenToPreview();
				break;
			case Const.REQUEST_FONT:
				mCurrentFontName = data.getStringExtra("fontName");
				setPenToPreview();
				break;
			}
			
			
		}
		
	}
	
	//一覧から選択されたペンをプレビューに表示
    @DebugLog
	private void actionSelectPen(View view){
        SampleTextView textView = (SampleTextView)view;
        
        mCurrentFontName = textView.getFontName();
        mCurrentPenKind = textView.getPen();
        mCurrentPenColor = textView.getColor();
        mCurrentPenMidColor = textView.getMidColor();
        mCurrentPenOutColor = textView.getOutColor();
        
        setPenToPreview();		
	}
	
	//透明度の変更
    @DebugLog
	private void actionChangeAlpha(int progress){
		mCurrentPenAlpha = progress;
		
		setPenToPreview();
	}

	//現在選択されているペンを呼び出し元に返す
    @DebugLog
	private void actionReturnToParent(){
		
		//履歴を更新
        try {
            appUserLogic.saveTextHist(mCurrentFontName, mCurrentPenKind, mCurrentPenColor, mCurrentPenMidColor, mCurrentPenOutColor);
        } catch (LogicException e) {
            EventBus.getDefault().post(new ErrorEvent(this.getClass(),e));
            return;
        }

        Intent intent = new Intent();
		intent.putExtra("fontName",mCurrentFontName);
		intent.putExtra("penKind",mCurrentPenKind);
		intent.putExtra("penColor", mCurrentPenColor);
		intent.putExtra("penMidColor", mCurrentPenMidColor);
		intent.putExtra("penOutColor", mCurrentPenOutColor);
		intent.putExtra("penAlpha", mCurrentPenAlpha);
		intent.putExtra("text", mCurrentText);

		setResult(RESULT_OK, intent);
        finish();
        
	}
	
	//色選択画面を呼び出す
	private void actionShowColorActivity(){
		Intent intent = new Intent(this,SelectColorActivity.class);
		intent.putExtra("penKind", mCurrentPenKind);
		intent.putExtra("penWidth", 15.0f);
		intent.putExtra("penColor", mCurrentPenColor);
		intent.putExtra("penMidColor", mCurrentPenMidColor);
		intent.putExtra("penOutColor", mCurrentPenOutColor);
		startActivityForResult(intent, Const.REQUEST_COLOR);

	}
	
	//サイズ選択画面を呼び出す
	private void actionShowFontActivity(){
		Intent intent = new Intent(this,SelectFontActivity.class);
		intent.putExtra("fontName", mCurrentFontName);
		intent.putExtra("penKind", mCurrentPenKind);
		intent.putExtra("penColor", mCurrentPenColor);
		intent.putExtra("penAlpha", mCurrentPenAlpha);
		intent.putExtra("penMidColor", mCurrentPenMidColor);
		intent.putExtra("penOutColor", mCurrentPenOutColor);
		intent.putExtra("text", mCurrentText);
		startActivityForResult(intent, Const.REQUEST_FONT);
		

	}
	
	//テキスト変更
    @DebugLog
	private void actionChangeText(String text){
		mCurrentText = text;
		mSampleTextView.setText(mCurrentText, 30);
		setPenToPreview();
	}

	//選択されたペンをプレビューに設定する
    @DebugLog
	private void setPenToPreview(){
        Paint[] paints = PartUtil.getPaint(mCurrentPenKind, 1,mCurrentPenColor,mCurrentPenMidColor,mCurrentPenOutColor,mCurrentPenAlpha);
        mSampleTextView.setPaints(mCurrentPenKind,paints,mCurrentFontName);
	}
	

	@Override
	protected void onStart() {
		super.onStart();
        EventBus.getDefault().registerSticky(this);

    }

	@Override
	protected void onStop() {
		super.onStop();
        EventBus.getDefault().unregister(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater menuInflater = this.getMenuInflater();
        menuInflater.inflate(R.menu.menu_select_text,menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()){
            case R.id.actionOk:
                actionReturnToParent();
                return true;
        }

        return false;
    }

    /**
     * error handling
     * @param event
     */
    @DebugLog
    public void onEventMainThread(ErrorEvent event){

        LogicException e = (LogicException) event.getException();

        //show dialog when error was happened
        DialogFragment dialogFragment = AlertDialogFragment_.builder()
                .eventKey(AlertDialogFragment.class.getName())
                .titleRes(e.getErrorCode().getErrorRes())
                .messageRes(e.getReasonRes())
                .positiveRes(android.R.string.ok).build();
        dialogFragment.show(this.getFragmentManager()
                ,AlertDialogFragment.class.getName());

        //remove sticky event after using.
        EventBus.getDefault().removeStickyEvent(event);

    }


}

