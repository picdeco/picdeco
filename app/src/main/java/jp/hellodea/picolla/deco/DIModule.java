package jp.hellodea.picolla.deco;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import jp.hellodea.picolla.deco.activity.CutPictureActivity;
import jp.hellodea.picolla.deco.activity.DownloadFontsActivity;
import jp.hellodea.picolla.deco.activity.SelectFilterActivity;
import jp.hellodea.picolla.deco.activity.MainEditorActivity;
import jp.hellodea.picolla.deco.activity.SelectPenActivity;
import jp.hellodea.picolla.deco.activity.SelectTextActivity;
import jp.hellodea.picolla.deco.fragment.LauncherFragment_;
import jp.hellodea.picolla.deco.logic.AppUserLogic;
import jp.hellodea.picolla.deco.logic.AssetLogic;
import jp.hellodea.picolla.deco.logic.EditLogic;
import jp.hellodea.picolla.deco.logic.WorkLogic;
import jp.hellodea.picolla.deco.view.ClipartSelectorView_;

/**
 * Created by kazuhiro on 2014/12/18.
 */
@Module(
        injects = {
                ClipartSelectorView_.class,
                MainEditorActivity.class,
                LauncherFragment_.class,
                EditLogic.class,
                SelectFilterActivity.class,
                SelectTextActivity.class,
                DownloadFontsActivity.class,
                SelectPenActivity.class,
                CutPictureActivity.class

        },
        complete = false
)
public class DIModule {

    @Provides
    @Singleton
    public AssetLogic provideAssetLogic(){
        AssetLogic assetLogic = new AssetLogic();
        return assetLogic;
    }

    @Provides
    @Singleton
    public EditLogic provideEditorManager(){
        EditLogic editLogic = new EditLogic();
        return editLogic;
    }

    @Provides
    @Singleton
    public WorkLogic provideWorkLogic(){
        WorkLogic workLogic = new WorkLogic();
        return workLogic;
    }

    @Provides
    @Singleton
    public AppUserLogic provideAppUserLogic(){
        AppUserLogic appUserLogic = new AppUserLogic();
        return appUserLogic;
    }
}
