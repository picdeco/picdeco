package jp.hellodea.picolla.deco.activity;

import java.io.File;
import java.util.HashMap;

import bolts.Continuation;
import bolts.Task;
import de.greenrobot.event.EventBus;
import hugo.weaving.DebugLog;
import jp.hellodea.picolla.R;
import jp.hellodea.picolla.common.eventbus.ErrorEvent;
import jp.hellodea.picolla.common.exception.LogicException;
import jp.hellodea.picolla.common.fragment.AlertDialogFragment;
import jp.hellodea.picolla.common.fragment.AlertDialogFragment_;
import jp.hellodea.picolla.deco.Const;
import jp.hellodea.picolla.deco.entity.Clipart;
import jp.hellodea.picolla.deco.entity.ClipartKind;
import jp.hellodea.picolla.deco.eventbus.ProgressDialogEvent;
import jp.hellodea.picolla.deco.eventbus.ProgressViewEvent;
import jp.hellodea.picolla.deco.graphics.parts.ImageObject;
import jp.hellodea.picolla.deco.graphics.parts.PartsGroup;
import jp.hellodea.picolla.deco.util.CropImageAsyncTask;
import jp.hellodea.picolla.deco.util.EventUtil;
import jp.hellodea.picolla.deco.util.ImageUtil;
import jp.hellodea.picolla.deco.util.StorageFileUtil;
import jp.hellodea.picolla.deco.util.UriUtil;
import jp.hellodea.picolla.deco.view.ClipartSelectorView;
import jp.hellodea.picolla.deco.view.DirectionIcon;
import jp.hellodea.picolla.deco.view.DrawBoardView;
import timber.log.Timber;

import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

/**
 * @author kazuhiro
 * 画像にクリップアート版フレームを追加する
 */
public class AddClipartFrameActivity extends ActionBarActivity{

	//ドローエリア
	private DrawBoardView mDrawBoardView;
	private PartsGroup mPartsGroup;

	//お待ちくださいダイアログ
	private ProgressDialog mProgressDialog;

	//画面のコンポーネント
	private HorizontalScrollView mHorizontalScrollView;
	private FrameLayout mBaseLayout;
	private ImageButton mButtonZoomUp;
	private ImageButton mButtonZoomDown;
	private Button mButtonFrame;
	private ProgressBar mProgressBar;
	private ClipartSelectorView mClipartSelectorView;
	
	//オリジナルの画像
	private Uri mOriginalImageUri;
	private int mOriginalImageWidth;
	private int mOriginalImageHeight;
	
	//縮小した編集対象の画像
	private Uri mWorkImageUri;
	private int mWorkImageWidth;
	private int mWorkImageHeight;

	//画像オブジェクト
	private ImageObject mImageObject;
	private ImageObject mFrameImageObject;
	
	//初期化処理完了フラグ
	private boolean mIsFinishInit = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//画面レイアウト設定
        setContentView(R.layout.activity_add_clipart_frame);

        //UI取得
        mHorizontalScrollView = (HorizontalScrollView)findViewById(R.id.horizontalScrollView);
        mBaseLayout = (FrameLayout)findViewById(R.id.frameLayout);
        mButtonZoomUp = (ImageButton)findViewById(R.id.buttonZoomUp);
        mButtonZoomDown = (ImageButton)findViewById(R.id.buttonZoomDown);
        mButtonFrame = (Button)findViewById(R.id.buttonFrame);
        mProgressBar = (ProgressBar)findViewById(R.id.progressBar);
	    mClipartSelectorView = (ClipartSelectorView)findViewById(R.id.clipartSelectorView);
	    mClipartSelectorView.setVisibility(View.GONE);
        mClipartSelectorView.initialize();


        //UIイベント設定
        mButtonZoomUp.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				actionZoomUp();
			}
		});
        mButtonZoomDown.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				actionZoomDown();
			}
		});
        mButtonFrame.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				actionShowClipartSelector(Const.CLIPART_CATEGORY_FRAME);
			}
		});

        //プログレスダイアログ生成
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getText(R.string.please_wait));
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setCancelable(false);


        //パラメータ取得
        Intent intent = getIntent();
        mOriginalImageUri = Uri.parse(intent.getStringExtra("originalImageUriString"));
        mOriginalImageWidth = intent.getIntExtra("originalImageWidth", 0);
        mOriginalImageHeight = intent.getIntExtra("originalImageHeight", 0);
        mWorkImageUri = Uri.parse(intent.getStringExtra("workImageUriString"));
        mWorkImageWidth = intent.getIntExtra("workImageWidth", 0);
        mWorkImageHeight = intent.getIntExtra("workImageHeight", 0);
        
        //描画エリア
	    mDrawBoardView = new DrawBoardView(this);
	    mDrawBoardView.setBackground(BitmapFactory.decodeResource(getResources(),R.drawable.background));
	    mPartsGroup = new PartsGroup(mDrawBoardView,null,(int)(mWorkImageWidth), (int)(mWorkImageHeight),1, StorageFileUtil.getTempCacheDir(this)); //少し大きめサイズ
	    mPartsGroup.setBackground(BitmapFactory.decodeResource(getResources(), R.drawable.background));

	    //ドローエリア設定
	    mBaseLayout.addView(mDrawBoardView);
	    mDrawBoardView.setBackgroundColor(Color.GRAY);

	    DirectionIcon directionIconMove = new DirectionIcon(this, mDrawBoardView,DirectionIcon.MODE_MOVE);
	    DirectionIcon directionIconResize = new DirectionIcon(this, mDrawBoardView,DirectionIcon.MODE_RESIZE);
	    DirectionIcon directionIconRotate = new DirectionIcon(this, mDrawBoardView,DirectionIcon.MODE_ROTATE);
	    DirectionIcon directionIconResizeFree = new DirectionIcon(this, mDrawBoardView,DirectionIcon.MODE_RESIZE_FREE);
	    mDrawBoardView.setDirectionIcon(directionIconMove,directionIconResize,directionIconRotate,directionIconResizeFree,null,null,null,null);
	    mBaseLayout.addView(directionIconMove,1,1);
	    mBaseLayout.addView(directionIconResize,1,1);
	    mBaseLayout.addView(directionIconRotate,1,1);
	    mBaseLayout.addView(directionIconResizeFree,1,1);

	}

    @Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);

		if(mIsFinishInit == true) return; //既に処理済みなら何もしない

		mIsFinishInit = true;

		//
		//viewの幅、高さはこのイベントでしかとれないので画像の縮尺の計算をここでする
		//
		Log.d("kazino", "AddClipartFrame onWindowFocusChanged start");
		
	    //キャンバス全体が見えるように縮尺
	    mDrawBoardView.autoScale();

	    //キャンパスが中心にくるように移動
	    mDrawBoardView.moveToCenter();
	    
	    //画像表示
    	mImageObject = 
    		new ImageObject(mPartsGroup, 0, 0, new File(mWorkImageUri.getPath()),null,false,null, false,1);

    	//画像をキャンバスの中心に移動
    	mImageObject.moveToCenter();
        mImageObject.setLock(true);


        Task task = mPartsGroup.flashToBitmap().onSuccess(new Continuation() {
            @Override
            public Object then(Task task) throws Exception {
                //フレーム選択画面を自動で表示
                actionShowClipartSelector(Const.CLIPART_CATEGORY_FRAME);

                dismissProgress();

                Log.d("kazino", "AddClipartFrame onWindowFocusChanged end");

                return null;
            }
        },Task.UI_THREAD_EXECUTOR);

	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		mPartsGroup.recycle();
		mDrawBoardView.clear();
		
		mPartsGroup = null;
		mDrawBoardView = null;
	}

	private void actionZoomUp(){
		mDrawBoardView.postScale(0.1f);
		mDrawBoardView.invalidate();
	}

	private void actionZoomDown(){
		mDrawBoardView.postScale(-0.1f);
		mDrawBoardView.invalidate();
	}
	
	private void actionUndo(){
		mDrawBoardView.undo();
	}

	//色選択画面を呼び出す
	private void actionShowClipartSelector(String categoryName){
		mClipartSelectorView.show(categoryName);
		
	}

	//画像を作成しメイン画面に遷移する
	private void actionCreateNewImageWithFrame(){
		
		//作業中かチェック
		if(mDrawBoardView.getMode() != DrawBoardView.MODE_SCROLL){
			Toast.makeText(this, R.string.please_complete_the_final_task, Toast.LENGTH_SHORT).show();
			return;
		}
		
		//もしフレームが指定されていなければエラー
		if(mPartsGroup.getPartsCount() == 1){
			Toast.makeText(this, R.string.range_of_cut_has_not_been_selected, Toast.LENGTH_SHORT).show();
			return;
		}
		
		//フレームのエリアを取得
		ImageObject imageObject = (ImageObject) mPartsGroup.getPart(1);
		final RectF finalBounds = imageObject.getBounds();

		File originalImageFile = UriUtil.getFileFromUri(this, mOriginalImageUri);
		File workImageFile = UriUtil.getFileFromUri(this, mWorkImageUri);
		
		//切り抜き実行
		CropImageAsyncTask.execute(
                this,
                originalImageFile, mOriginalImageWidth, mOriginalImageHeight,
                workImageFile, mWorkImageWidth, mWorkImageHeight,
                finalBounds,
                0,
                1,
                new CropImageAsyncTask.CropImageListener() {
                    @Override
                    public void onPreExecute() {
                        mProgressDialog.show();
                    }

                    @Override
                    public void onPostExecute(File imageFile, int width, int height) {

                        if (imageFile == null) {
                            Toast.makeText(AddClipartFrameActivity.this, R.string.this_picture_does_not_exist_or_the_picture_cannot_be_handled_by_this_application, Toast.LENGTH_LONG).show();
                            return;
                        }

                        //workから切り取る際に上下左右ではみ出している長さをもとめる
                        float leftOffset = 0;
                        float topOffset = 0;
                        float rightOffset = 0;
                        float bottomOffset = 0;

                        if (finalBounds.left < 0) leftOffset = finalBounds.left;
                        if (finalBounds.top < 0) topOffset = finalBounds.top;
                        if (finalBounds.right > mWorkImageWidth)
                            rightOffset = finalBounds.right - mWorkImageWidth;
                        if (finalBounds.bottom > mWorkImageHeight)
                            bottomOffset = finalBounds.bottom - mWorkImageHeight;

                        executeCreateNewImageWithFrame(imageFile, width, height, finalBounds, leftOffset, topOffset, rightOffset, bottomOffset); //パスで切り取り実行
                    }
                }
        );
		


	}
	
	//フレームを合成してメインに戻る
	private void executeCreateNewImageWithFrame(final File imageFile,final int width, final int height, final RectF bounds,
			final float leftOffset,final float topOffset,final float rightOffset,final float bottomOffset){
		
		////////////////////////////
		//非同期処理
		//フレームを合成する
		////////////////////////////
		AsyncTask<Void, Integer, File> task = new AsyncTask<Void, Integer, File>() {

			private int mHeight, mWidth;
			
			@Override
			protected void onPreExecute() {
			}

			@Override
			protected File doInBackground(Void... params) {
				
				mPartsGroup.freeBitmap(); //もう使用しないので解放
				
				////
				// 処理手順
				// 1.フレームの透明以外の部分を元にオリジナル画像を切り抜く
				// 2.切り抜いた画像とフレームを合成する
				// 3.余白を切り抜き完成
				////
				
				System.gc();				

				//張付け先キャンバス
				Bitmap imageBitmap = BitmapFactory.decodeFile(imageFile.getPath());
				Bitmap distBitmap = Bitmap.createBitmap(imageBitmap.getWidth(), imageBitmap.getHeight(), Config.ARGB_8888);
				Canvas distCanvas = new Canvas(distBitmap);
				distCanvas.drawBitmap(imageBitmap, 0, 0, null);
				imageBitmap.recycle();
				
				System.gc();

				///////////////////////////////
				//最終的なパスの位置と縮尺を求める
				///////////////////////////////
				float originalWidth = bounds.width();
				float originalHeight = bounds.height();
				int leftTranslate = 0;
				int topTranslate = 0;
				float rateWidth,rateHeight;
				
				if(leftOffset < 0) originalWidth += leftOffset; //左にはみ出ていたらその分縮める
				if(topOffset < 0) originalHeight += topOffset; //上にはみ出ていたらその分縮める
				if(rightOffset > 0) originalWidth -= rightOffset; //左にはみ出ていたらその分縮める
				if(bottomOffset > 0) originalHeight -= bottomOffset; //上にはみ出ていたらその分縮める
				
				if(bounds.left > 0) leftTranslate = (int) (bounds.left * -1); //右に配置されていたらその分左に移動
				if(bounds.top > 0) topTranslate = (int) (bounds.top * -1); //下に配置されていたらその分上に移動
				
				rateWidth = distBitmap.getWidth() / originalWidth;
				rateHeight = distBitmap.getHeight() / originalHeight;

				//フレームマスクの準備
				Bitmap frameBitmap = Bitmap.createBitmap(width, height, Config.ARGB_8888);
				Canvas frameCanvas = new Canvas(frameBitmap);
				ImageObject frameImageObject = (ImageObject) mPartsGroup.getPart(1);
				frameImageObject.move(leftTranslate, topTranslate); 
				frameImageObject.scaleFromLeftAndTop(rateWidth, rateHeight);
				frameImageObject.flashToBitmap(frameCanvas, null);
				
				//フレームを色変換して透明以外を白にする
				ImageUtil.applyColorMatrix(frameBitmap,
                        new ColorMatrix(
                                new float[]{
                                        0, 0, 0, 0, 255,
                                        0, 0, 0, 0, 255,
                                        0, 0, 0, 0, 255,
                                        0, 0, 0, 255, 0}
                        ));
				
				//マスクを貼付け
				Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
				paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
//				paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER));
				distCanvas.drawBitmap(frameBitmap, 0, 0, paint);
				frameBitmap.recycle();

				//フレームを貼付け
				frameBitmap = Bitmap.createBitmap(width, height, Config.ARGB_8888);
				frameCanvas = new Canvas(frameBitmap);
				frameImageObject.flashToBitmap(frameCanvas, null);
				distCanvas.drawBitmap(frameBitmap,0,0,null);
				frameBitmap.recycle();
				
				//メモリ解放
				mPartsGroup.recycle();
				mDrawBoardView.clear();

				//余白を取り除く
				System.gc();
				HashMap<String, Object> ret = ImageUtil.getRealSizeBitmap(distBitmap);
				distBitmap.recycle();
				distBitmap = (Bitmap) ret.get("bitmap");
				mHeight = distBitmap.getHeight();
				mWidth = distBitmap.getWidth();
				
				//切り取り完了画像をキャッシュへ書き出し
		    	File cacheFile = StorageFileUtil.createNewRandomFile(StorageFileUtil.getTempCacheDir(AddClipartFrameActivity.this),"");
				try {
					ImageUtil.storeBitmapToCache(distBitmap, cacheFile);
				} catch (Exception e) {
					Timber.e(e,"");
					throw new RuntimeException(e);
				}
		        
				distBitmap.recycle();
				distCanvas = null;

				return cacheFile;
			}

			@Override
			protected void onPostExecute(File cacheFile) {
				mProgressDialog.dismiss();
				
				//切り取り完了後メイン編集画面へ遷移
				Intent send_intent = new Intent(AddClipartFrameActivity.this,MainEditorActivity.class);
				send_intent.setData(Uri.fromFile(cacheFile));
				send_intent.putExtra("executeMode", Const.REQUEST_MODIFY_IMAGE);
				send_intent.putExtra("height", mHeight);
				send_intent.putExtra("width", mWidth);

				// 戻るときに開いていたActivityを全部閉じる
				send_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

				startActivity(send_intent);

				finish();

			}

		};
		
		//タスクの実行
		task.execute();

	}

    /**
     * クリップアートが選択された時のイベント処理
     * @param event
     */
    @DebugLog
    public void onEventMainThread(ClipartSelectorView.ClipArtSelectedEvent event){
        ClipartKind kind = (ClipartKind) event.getArgs()[0];
        Clipart clipart = (Clipart) event.getArgs()[1];
        File file = (File) event.getArgs()[2];

        if(kind.getString(ClipartKind.F_CATEGORY).equals(Const.CLIPART_CATEGORY_FRAME))
            resultForRequestClipart(kind,clipart, file);

    }

	private void resultForRequestClipart(ClipartKind kind, Clipart clipart, File file) {
		ImageObject imageObject;
		//既にフレームがあったら削除
		if(mPartsGroup.getPartsCount() > 1){
	    	imageObject = (ImageObject) mPartsGroup.getPart(1); //下から２番目がフレーム
			mPartsGroup.removePart(imageObject, true);
		}
		
		//新しいフレームを追加
		imageObject = new ImageObject(mPartsGroup, 0, 0, file, StorageFileUtil.getTempCacheDir(this), true, AddClipartFrameActivity.this, true, 1);
		imageObject.moveToCenter();
		
		//大きさを調整
		Bitmap bitmap = imageObject.getBitmap();
		float rateX,rateY;
		rateX = (float)mWorkImageWidth / (float)bitmap.getWidth();
		rateY = (float)mWorkImageHeight / (float)bitmap.getHeight();
		imageObject.scale(Math.min(rateX, rateY), Math.min(rateX, rateY));
		
		final ImageObject finalImageObject = imageObject;

        mPartsGroup.flashToBitmap().onSuccess(new Continuation() {
            @Override
            public Object then(Task task) throws Exception {
                //編集モードスタート
                mDrawBoardView.startEditMode(finalImageObject);
                mDrawBoardView.getUndoManager().addModify(finalImageObject); //確定後、描画されるようにダミー

                mDrawBoardView.invalidate();
                dismissProgress();
                return null;
            }
        },Task.UI_THREAD_EXECUTOR);

	}

	public void showProgress() {
		mProgressBar.setVisibility(View.VISIBLE);
	}

	public void dismissProgress() {
		mProgressBar.setVisibility(View.INVISIBLE);
	}


	@Override
	protected void onStart() {
		super.onStart();

        EventBus.getDefault().registerSticky(this);
	}

	@Override
	protected void onStop() {
		super.onStop();

        EventBus.getDefault().unregister(this);
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater menuInflater = this.getMenuInflater();
        menuInflater.inflate(R.menu.menu_add_clipart_frame,menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()){
            case R.id.actionUndo:
                actionUndo();
                return true;
            case R.id.actionSave:
                actionCreateNewImageWithFrame();
                return true;
        }

        return false;
    }

    /**
     * show progress dialog
     * @param event
     */
    public void onEventMainThread(ProgressDialogEvent event){
        EventUtil.showProgressDialog(this, event);
    }

    /**
     * show progress view
     * @param event
     */
    public void onEventMainThread(ProgressViewEvent event){
        if(event.getEventKey().equals(ProgressViewEvent.Status.START)){
            mProgressBar.setVisibility(View.VISIBLE);
        }else {
            mProgressBar.setVisibility(View.INVISIBLE);
        }

        //remove sticky event after using.
        EventBus.getDefault().removeStickyEvent(event);

    }

    /**
     * error handling
     * @param event
     */
    @DebugLog
    public void onEventMainThread(ErrorEvent event){

        LogicException e = (LogicException) event.getException();

        //show dialog when error was happened
        DialogFragment dialogFragment = AlertDialogFragment_.builder()
                .eventKey(AlertDialogFragment.class.getName())
                .titleRes(e.getErrorCode().getErrorRes())
                .messageRes(e.getReasonRes())
                .positiveRes(android.R.string.ok).build();
        dialogFragment.show(this.getFragmentManager()
                ,AlertDialogFragment.class.getName());

        //remove sticky event after using.
        EventBus.getDefault().removeStickyEvent(event);

    }


}
