package jp.hellodea.picolla.deco.view;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.GridView;
import android.widget.LinearLayout;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;

import de.greenrobot.event.EventBus;
import hugo.weaving.DebugLog;
import jp.hellodea.picolla.common.eventbus.SelectEvent;
import jp.hellodea.picolla.deco.adapter.ColorAdapter;
import jp.hellodea.picolla.deco.graphics.ColorTable;

/**
 * Created by kazuhiro on 2015/04/22.
 */
@EViewGroup(resName = "view_color_list")
public class ColorListView extends LinearLayout{

    public static final int KBN_COLOR1 = 0;
    public static final int KBN_COLOR2 = 1;
    public static final int KBN_COLOR3 = 2;

    private int mKbn; //内側、真ん中、外側


    @ViewById
    GridView gridView;

    ColorAdapter colorAdapter;

    @ItemClick
    @DebugLog
    public void gridViewItemClicked(int position) {
        actionSelectColor(position);
    }

    public ColorListView(Context context) {
        super(context);
    }

    public ColorListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void bind(int kbn,int initColor){

        mKbn = kbn;

        colorAdapter = new ColorAdapter(getContext());
        gridView.setAdapter(colorAdapter);

        //現在の色をデフォルトで選択
        int currentColor = initColor;
        if(currentColor != 0){
            currentColor = Color.argb(255, Color.red(currentColor), Color.green(currentColor), Color.blue(currentColor)); //透過をクリア
            for (int i = 0; i < ColorTable.COLORS.length; i++) {
                if(currentColor == Color.parseColor(ColorTable.COLORS[i])){
                    colorAdapter.selectedPosition = i;
                    break;
                }
            }
        }


    }


    //色選択
    @DebugLog
    private void actionSelectColor(int position){

        colorAdapter.selectedPosition = position;
        colorAdapter.notifyDataSetChanged();

        int selectedColor = Color.parseColor(ColorTable.COLORS[position]);

        EventBus.getDefault().post(new SelectEvent(ColorListView.class, mKbn, selectedColor));

    }

}
