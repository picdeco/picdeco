package jp.hellodea.picolla.deco.entity;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import jp.hellodea.picolla.common.AWSConst;


/**
 * ClipartKind
 * Created by kazuhiro on 2015/03/09.
 */
@ParseClassName("ClipartKind")
public class ClipartKind extends ParseObject{

    public static final String F_CATEGORY = "CATEGORY";
    public static final String F_NAME = "NAME";
    public static final String F_PATH = "PATH";
    public static final String F_EXT = "EXT";
    public static final String F_ORDER = "ORDER";
    public static final String F_IS_ACTIVE = "IS_ACTIVE";

    public String getUrl(){
        String url = AWSConst.ASSETS_CLIPART_PATH +
                this.getString(ClipartKind.F_CATEGORY) + "/" +
                this.getString(ClipartKind.F_NAME) + "/" +
                this.getString(ClipartKind.F_PATH) + this.getString(ClipartKind.F_EXT);
        return url;
    }

    public String getThumbUrl(){
        String url = AWSConst.ASSETS_CLIPART_PATH +
                this.getString(ClipartKind.F_CATEGORY) + "/" +
                this.getString(ClipartKind.F_NAME) + "/" +
                this.getString(ClipartKind.F_PATH) + "_t" + this.getString(ClipartKind.F_EXT);
        return url;
    }


}
