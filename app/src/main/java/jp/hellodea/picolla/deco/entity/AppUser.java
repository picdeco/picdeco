package jp.hellodea.picolla.deco.entity;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;


/**
 * AppUser
 * Created by kazuhiro on 2015/03/09.
 */
@ParseClassName("AppUser")
public class AppUser extends ParseObject{

    public static final String F_LANG = "LANG";

    /**
     * get current app user
     * @return
     */
    public static AppUser getCurrent(){

        ParseUser parseUser = ParseUser.getCurrentUser();

        if(parseUser == null) return null;

        return (AppUser) parseUser.getParseObject(AppUser.class.getSimpleName());
    }


}
