package jp.hellodea.picolla.deco.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import timber.log.Timber;

public class ResizeImageAsyncTask {

	//呼び出し元に実装すべきリスナー
	public interface ResizeImageListener{
		public void onPreExecute();
		public void onPostExecute(ResizeImage resizeImage);
	}
		
	//オリジナル画像を作業用画像にリサイズ
	public static void execute(final Context context, final File originalImageFile, final ResizeImageListener listener){

		////////////////////////////
		//非同期処理
		////////////////////////////
		AsyncTask<File, Void, ResizeImage> task = new AsyncTask<File, Void, ResizeImage>() {

			@Override
			protected void onPreExecute() {
				listener.onPreExecute();
			}

			@Override
			protected ResizeImage doInBackground(File... params) {
				
				try{
					ResizeImage resizeImage = new ResizeImage();
					File newFile;

					//オリジナル画像のサイズを調べる
					BitmapFactory.Options options = new BitmapFactory.Options();
					options.inJustDecodeBounds = true;
					InputStream is;
					is = new FileInputStream(originalImageFile);
					BitmapFactory.decodeStream(is, null, options);
					resizeImage.mOriginalImageFile = originalImageFile;
					resizeImage.mOriginalImageWidth = options.outWidth;
					resizeImage.mOriginalImageHeight = options.outHeight;
					is.close();

					Log.d("kazino", "ResizeImageAsyncTask.execute.1st original:" + resizeImage.mOriginalImageWidth + ":" + resizeImage.mOriginalImageHeight);
					
					//画面表示用に小さなサイズの画像を作成する(最大数のハーフサイズ)
					int dispImageSize = ImageUtil.getMaxImageSize(context);
					double scale = ImageUtil.getImageScale(resizeImage.mOriginalImageWidth, resizeImage.mOriginalImageHeight, dispImageSize);
					if(scale <= 1.0D){
						//表示サイズより小さければコピーする(オリジナルを壊さないように)
						File workFile = StorageFileUtil.createNewRandomFile(StorageFileUtil.getTempCacheDir(context),"");
						StorageFileUtil.copyFile(resizeImage.mOriginalImageFile, workFile);
					    resizeImage.mWorkImageFile = workFile;
					    resizeImage.mWorkImageWidth = resizeImage.mOriginalImageWidth;
					    resizeImage.mWorkImageHeight = resizeImage.mOriginalImageHeight;
					}else{
						//大きければ縮小
						is = new FileInputStream(resizeImage.mOriginalImageFile);
						scale = ImageUtil.getImageScale(is, dispImageSize);
						is.close();
						is = new FileInputStream(resizeImage.mOriginalImageFile);
						ImageInfo imageInfo = ImageUtil.decodeAndStoreResizedImage(is,StorageFileUtil.getTempCacheDir(context),
								dispImageSize,(int)scale,1,0); //ファイルを取得
					    is.close();
						newFile = imageInfo.file;
					    resizeImage.mWorkImageFile = newFile;
					    resizeImage.mWorkImageWidth = imageInfo.width;
					    resizeImage.mWorkImageHeight = imageInfo.height;
					    imageInfo.bitmap.recycle();
					}
					
					Log.d("kazino", "ResizeImageAsyncTask.execute.original:" + resizeImage.mOriginalImageFile.getPath() + ":" + 
							resizeImage.mOriginalImageWidth + ":" + resizeImage.mOriginalImageHeight);
					Log.d("kazino", "ResizeImageAsyncTask.execute.work:" + resizeImage.mWorkImageFile.getPath() + ":" +
							resizeImage.mWorkImageWidth + ":" +resizeImage.mWorkImageHeight);

					return resizeImage;
				}
				catch (Exception e) {
					Timber.e(e, "");
					throw new RuntimeException(e);
				}
					
			}

			@Override
			protected void onPostExecute(ResizeImage resizeImage) {
				
				listener.onPostExecute(resizeImage);

			}

		};
		
		//タスクの実行
		task.execute();

	}
	
}
