package jp.hellodea.picolla.deco.graphics.manipulate;

import java.io.File;
import java.util.ArrayList;

import jp.hellodea.picolla.deco.graphics.parts.PartObject;
import jp.hellodea.picolla.deco.graphics.parts.PartsGroup;

public class UndoManager {

	public static final int KBN_NONE = 0;
	public static final int KBN_NEW = 1;
	public static final int KBN_MODIFY = 2;
	public static final int KBN_POSITION = 3;
	public static final int KBN_REMOVE = 4;
	public static final int KBN_BACKGROUND = 5;
	
	private UndoObject mCurrentUndoObject; //現在のターゲット
	
	private ArrayList<UndoObject> mUndoList; //確定しているUNDO履歴
	
	public UndoManager(){
		mUndoList = new ArrayList<UndoObject>();
	}

	//直前の修正中情報を確定
	public void fixModify(){
		if(mCurrentUndoObject != null && mCurrentUndoObject.getKbn() == KBN_MODIFY){
			mUndoList.add(mCurrentUndoObject);
			mCurrentUndoObject = null;
		}
	}
	
	
	//パーツの新規追加、削除、ポジション変更
	public void add(int kbn, PartObject part){

		//直前の修正中情報を確定
		fixModify();
		
		//新規追加を確定
		mUndoList.add(new UndoObject(kbn,part));
	}
	
	//パーツの修正開始
	public void addModify(PartObject part){
		
		if(mCurrentUndoObject == null)
			mCurrentUndoObject = new UndoObject(KBN_MODIFY,part);
		
	}
	
	//背景の変更
	public void addBackground(File backgroundImageFile, PartsGroup partsGroup){
		//直前の修正中情報を確定
		fixModify();

		mUndoList.add(new UndoObject(backgroundImageFile, partsGroup));
	}

	//UNDO実行
	public void undo(){
		if(mCurrentUndoObject != null){
			mCurrentUndoObject.undo();
			mCurrentUndoObject = null;
		}else{
			if(mUndoList.size() > 0){
				UndoObject target = mUndoList.remove(mUndoList.size() - 1);
				target.undo();
			}
		}
	}
	
	public UndoObject getCurrentUndoObject(){
		return mCurrentUndoObject;
	}
	
	//すべての情報をクリアする
	public void clear(){
		if(mCurrentUndoObject != null){
			mCurrentUndoObject.clear();
			mCurrentUndoObject = null;
		}

		if(mUndoList != null){
			for (int i = 0; i < mUndoList.size(); i++) {
				mUndoList.get(i).clear();
			}
			mUndoList.clear();
			mUndoList = null;
		}
	}

}
