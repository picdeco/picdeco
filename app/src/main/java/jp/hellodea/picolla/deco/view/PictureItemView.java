package jp.hellodea.picolla.deco.view;

import android.content.Context;
import android.text.format.DateUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.StringSignature;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import java.io.File;

import de.greenrobot.event.EventBus;
import jp.hellodea.picolla.common.eventbus.SelectEvent;
import jp.hellodea.picolla.deco.entity.Work;

/**
 * Created by kazuhiro on 2015/04/22.
 */
@EViewGroup(resName = "item_picture")
public class PictureItemView extends LinearLayout {

    public enum Action{
        SHARE,EDIT,SAVE,DELETE
    }

    @ViewById
    ImageView imageViewThumb;

    @ViewById
    TextView textViewCreatedAt;

    Work work;

    @Click
    void buttonShare() {
        EventBus.getDefault().post(new SelectEvent(PictureItemView.class,Action.SHARE,work));
    }

    @Click
    void buttonSave() {
        EventBus.getDefault().post(new SelectEvent(PictureItemView.class,Action.SAVE,work));
    }


    @Click
    void buttonEdit() {
        EventBus.getDefault().post(new SelectEvent(PictureItemView.class,Action.EDIT,work));
    }

    @Click
    void buttonDelete() {
        EventBus.getDefault().post(new SelectEvent(PictureItemView.class,Action.DELETE,work));
    }

    public PictureItemView(Context context) {
        super(context);
    }


    public void bind(Work pWork) {
        work = pWork;

        File pictureFile = work.getThumbFile(getContext());
        Glide.with(getContext()).load(pictureFile)
                .signature(new StringSignature(String.valueOf(pictureFile.lastModified())))
                .centerCrop().into(imageViewThumb);
        textViewCreatedAt.setText(
                DateUtils.getRelativeTimeSpanString(work.getDate(Work.F_POST_TIME).getTime()));
    }
}
