package jp.hellodea.picolla.deco.eventbus;

import jp.hellodea.picolla.common.eventbus.BusEvent;

/**
 * Created by kazuhiro on 2015/04/08.
 */
public class ProgressDialogEvent extends BusEvent{
    public enum Status{
        START, END
    }

    public ProgressDialogEvent(Status status, int messageRes,int progressStyle) {
        super(status, messageRes,progressStyle);
    }

}
