package jp.hellodea.picolla.deco.view;

import jp.hellodea.picolla.deco.util.ImageUtil;
import jp.hellodea.picolla.deco.util.Util;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

public class SamplePathView extends View {

	private Path mPath;
	private Paint mPaint;
	
	public SamplePathView(Context context) {
		super(context);
				
	}
	
	public SamplePathView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	
	@Override
	public void onWindowFocusChanged(boolean hasWindowFocus) {
		super.onWindowFocusChanged(hasWindowFocus);
		
		//Pathをリサイズ
		fitToViewSize();
	}

	public void setPath(Path path, Paint paint){
		mPath = path;
		mPaint = paint;
		mPaint.setAntiAlias(true);
		
		//Pathをリサイズ
		fitToViewSize();
	}
	
	
	
	//表示先のViewのサイズに合わせてPathをリサイズする
	private void fitToViewSize(){
		//サイズが取得できるようになっていたらリサイズ
		if(this.getWidth() > 0 && this.getHeight() > 0){
			Matrix matrix = new Matrix();

            RectF bounds = new RectF();
            mPath.computeBounds(bounds,true);
						
			//100ピクセルを基準に大きさを調整する
			matrix.setScale((float)this.getWidth() / bounds.width(), (float)this.getHeight() / bounds.height());
			mPath.transform(matrix);
		}
	}

	@Override
	protected void onDraw(Canvas canvas) {
		

		if(mPath != null){
			canvas.drawColor(Color.WHITE);
			canvas.drawPath(mPath, mPaint);
		}
		
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		setMeasuredDimension(ImageUtil.getPx(getContext(), 75),ImageUtil.getPx(getContext(), 75));
	}
}
