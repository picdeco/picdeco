package jp.hellodea.picolla.deco.graphics.parts;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.Callable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;
import android.util.Log;

import bolts.Continuation;
import bolts.Task;
import hugo.weaving.DebugLog;
import jp.hellodea.picolla.deco.graphics.BitmapCacheHolder;
import jp.hellodea.picolla.deco.graphics.manipulate.UndoManager;
import jp.hellodea.picolla.deco.util.ImageUtil;
import jp.hellodea.picolla.deco.view.DrawBoardView;
import timber.log.Timber;

public class PartsGroup extends PartObject {

    private File mBaseDir; //画像ファイルの保存先ルート

	private Bitmap mBitmap; //メインのビットマップ
	private Canvas mCanvas; //メインのキャンバス

	private File mBackgroundBitmapFile; //背景画像のキャッシュパス
	private boolean mBackgroundBitmapIsRepeat; //背景画像の並べ方
	private BitmapShader mBitmapShaderBackground; //リソースを背景に表する場合、こちらを使う
	
	private DrawBoardView mDrawBoardView; //貼られている描画ビュー
	
	private Bitmap mMaskBitmap; //タッチされたパーツを探すため
	private Canvas mMaskCanvas; //タッチされたパーツを探すため
	private int mMaskColorRed = 0; //タッチされたパーツを探すための色（赤）
	private int mMaskColorGreen = 0; //タッチされたパーツを探すための色（緑）
	private int mMaskColorBlue = 0; //タッチされたパーツを探すための色（青）
	public static final int MASK_LOCK_COLOR = 0xff000000; //オブジェクトロック用色
	
	private Canvas mOffScreenCanvas; //オブジェクト操作時のスピードアップのため利用
	private Bitmap mOffScreenBitmap; //オブジェクト操作時のスピードアップのため利用
	private Canvas mOffScreenMaskCanvas; //オブジェクト操作時のスピードアップのため利用
	private Bitmap mOffScreenMaskBitmap; //オブジェクト操作時のスピードアップのため利用
	private PartObject mEditTargetObject; //現在編集対象のオブジェクト
	
	private ArrayList<PartObject> mPartsList = new ArrayList<PartObject>();
	
	private int mTouchX = 0;
	private int mTouchY = 0;
	
	
	public PartsGroup(DrawBoardView view, PartsGroup group,int width, int height, int displaySampleSize, File baseDir) {
		
		super(group, PartObject.KIND_GROUP, displaySampleSize);
		
		System.gc();
		
		mDrawBoardView = view;
		mBitmap = Bitmap.createBitmap(width, height, Config.RGB_565);
		mCanvas = new Canvas(mBitmap);
		
		//クリック位置を調べるためのマスクビットマップ
		mMaskBitmap = Bitmap.createBitmap(width, height, Config.RGB_565);
		mMaskCanvas = new Canvas(mMaskBitmap);

        mBaseDir = baseDir;

		view.setPartsGroup(this);
		
	}
	
	public PartsGroup(DrawBoardView view, PartsGroup group, JSONObject json, File baseDir, boolean undoable) throws JSONException{
		this(view, group,json.getInt("width"),json.getInt("height"),json.getInt("displaySampleSize"), baseDir);
		
		//背景画像設定
		String path = json.optString("background", null);
		if(path != null){
			File file = new File(mBaseDir,path);
//			Bitmap bitmap = BitmapFactory.decodeFile(path);

			Bitmap bitmap = null;
			try {
				bitmap = ImageUtil.restoreImageFromBufferCache(file, mDisplaySampleSize, Config.RGB_565);
			} catch (Exception e) {
				Timber.e(e,"");
				throw new RuntimeException(e);
			}
			
			boolean repeat = json.optBoolean("backgroundRepeat");
			
			setBackground(bitmap,repeat, file, undoable);
		}

		//子供のパーツを読み込み
		JSONArray parts = json.getJSONArray("parts");
		for (int i = 0; i < parts.length(); i++) {
			addPart(parts.getJSONObject(i),undoable);
		}
	}

	//jsonファイルのみ展開する。表示はできない
	public PartsGroup(JSONObject json, File baseDir) throws JSONException, IOException {
		super(null,PartObject.KIND_GROUP,1);

        mBaseDir = baseDir;

		//背景画像設定
		String path = json.optString("background", null);
		if(path != null){
			File file = new File(mBaseDir, path);
//			Bitmap bitmap = BitmapFactory.decodeFile(path);

			Bitmap bitmap = ImageUtil.restoreImageFromBufferCache(file,mDisplaySampleSize,Config.RGB_565);

			boolean repeat = json.optBoolean("backgroundRepeat");
			
			setBackground(bitmap,repeat, file, false);
		}

		//子供のパーツを読み込み
		JSONArray parts = json.getJSONArray("parts");
		for (int i = 0; i < parts.length(); i++) {
			addPart(parts.getJSONObject(i),false);
		}
	}

	//描画ビューを設定
	public void setDrawBoardView(DrawBoardView view){
		mDrawBoardView = view;
	}
	
	//バックグラウンドを設定
	public void setBackground(Bitmap bitmap,boolean isRepeat, File cachePath, boolean undoable){
		
		//undoに記録
		if(undoable == true && mBackgroundBitmapFile != null)
			mDrawBoardView.getUndoManager().addBackground(mBackgroundBitmapFile, this);

        BitmapCacheHolder.get().setCachedImage(cachePath,bitmap);
		mBackgroundBitmapFile = cachePath;
		mBackgroundBitmapIsRepeat = isRepeat;
	}
	
	//バックグラウンドを設定
	public void setBackground(Bitmap bitmap){
		mBitmapShaderBackground = new BitmapShader(bitmap, TileMode.REPEAT, TileMode.REPEAT);
	}
	

	//パーツの数を返す
	public int getPartsCount(){
		if(mPartsList != null)
			return mPartsList.size();
		else return 0;
	}
	
    /**
     * ビューに指定された領域のビットマップを描画する
     * @param canvas
     * @param left
     * @param top
     * @param scale
     */
	public void drawToView(Canvas canvas,int left, int top, float scale ){
        drawToView(mBitmap,canvas,left,top,scale);
	}
	
    /**
     * ビューに指定された領域のマスク用ビットマップを描画する
     * @param canvas
     * @param left
     * @param top
     * @param scale
     */
	public void drawMaskToView(Canvas canvas,int left, int top, float scale ){
        drawToView(mMaskBitmap, canvas, left, top, scale);
	}

    /**
     * ビットマップをキャンバスに書き込む
     * @param fromBitmap
     * @param toCanvas
     * @param left
     * @param top
     * @param scale
     */
    public void drawToView(Bitmap fromBitmap, Canvas toCanvas,int left, int top, float scale ){
        if(fromBitmap == null) return;

        float scaledCanvasWidth = (toCanvas.getWidth() / scale);
        float scaledCanvasHeight = (toCanvas.getHeight() / scale);

        float right = left + scaledCanvasWidth;
        float bottom = top + scaledCanvasHeight;

        Bitmap tempBitmap = Bitmap.createBitmap((int)scaledCanvasWidth,(int)scaledCanvasHeight,Config.ARGB_8888);
        Canvas tempCanvas = new Canvas(tempBitmap);
        tempCanvas.drawBitmap(fromBitmap,left * -1, top * -1,null);

        toCanvas.drawBitmap(tempBitmap, new Rect(0,0,tempBitmap.getWidth(),tempBitmap.getHeight()),
                new Rect(0,0,toCanvas.getWidth(),toCanvas.getHeight()),new Paint());

        tempBitmap.recycle();

    }

	//ﾊﾟｰﾂを追加
	public void addPart(int index, PartObject part, boolean undoable){
		//undoに記録
		if(undoable == true)
			mDrawBoardView.getUndoManager().add(UndoManager.KBN_NEW, part);

		mPartsList.add(index,part);
		
		////
		//マスク用色を取得（RGB_565用のRGBからフルカラーを求める）
		////
		int maskColor;
		
		//青は３２階調
		if(mMaskColorBlue == 31){
			mMaskColorGreen += 1;
			mMaskColorBlue = 1;
		}else
			mMaskColorBlue++;
		
		//緑は６４階調
		if(mMaskColorGreen == 63){
			mMaskColorRed += 1;
			mMaskColorGreen = 1;
		}else
			mMaskColorGreen++;
		
		maskColor = Color.argb(255, mMaskColorRed * 8, mMaskColorGreen * 4, mMaskColorBlue * 8);
		
		//一度キャンバスに描いて実際のフルカラーを取得する（そうしないと値が微妙に変わってしまうので）
		if(mMaskBitmap !=null){
			mMaskBitmap.setPixel(0, 0, maskColor);
			int realMaskColor = mMaskBitmap.getPixel(0, 0);
			Log.d("kazino", "mask:" + maskColor);
			Log.d("kazino", "realMask:" + realMaskColor);
			part.setMaskColor(realMaskColor);
		}
		
	}

	//ﾊﾟｰﾂを追加
	public void addPart(PartObject part, boolean undoable){
		addPart(mPartsList.size(), part, undoable);
	}


	//ﾊﾟｰﾂを追加
	public void addPart(JSONObject part,boolean undoable) throws JSONException{
		switch (part.getInt("partKind")) {
		case PartObject.KIND_GROUP:
			break;
		case PartObject.KIND_PATH:
			new PathObject(this, part,undoable);
			break;
		case PartObject.KIND_PICT:
			new ImageObject(this, part,mBaseDir, undoable);
			break;
		case PartObject.KIND_TEXT:
			new TextObject(this, part,undoable);
			break;

		default:
			break;
		}
	}
	
	//指定された座標にあるパーツを返す
	public PartObject getPart(int x, int y){
		
		//範囲チェック
		if(x < 0 || y < 0 || x >= mBitmap.getWidth() || y >= mBitmap.getHeight()) return null;
		
		mTouchX = x;
		mTouchY = y;
		
		int hitColor = mMaskBitmap.getPixel(mTouchX, mTouchY);
		Log.d("kazino", "color:" + hitColor);
		Log.d("kazino", "A:" + Color.alpha(hitColor) + " R:" + Color.red(hitColor) + 
				" G:" + Color.green(hitColor) + " B:" + Color.blue(hitColor));
		
		
		for (int i = mPartsList.size() - 1; i > -1 ; i--) {
			int color = mPartsList.get(i).getMaskColor();
			Log.d("kazino", "getMaskColor:" + color);
			if(color == hitColor) return mPartsList.get(i);
		}
		
		return null;
	}

	//指定されたZindexにあるパーツを返す
	public PartObject getPart(int index){
		return mPartsList.get(index);
	}

	//指定されたパーツのZindexを返す
	public int getPartPosition(PartObject part){
		return mPartsList.indexOf(part);
	}
	
	//指定されたパーツを削除する
	public void removePart(PartObject part,boolean undoable){
		
		//undoに記録
		if(undoable == true)
			mDrawBoardView.getUndoManager().add(UndoManager.KBN_REMOVE, part);

		//パーツ削除
		mPartsList.remove(part);
		
	}
	
	//指定されたパーツのZIndexを変更する
	public void changePosition(PartObject part, int newPosition, boolean undoable){
		//undoに記録
		if(undoable == true)
			mDrawBoardView.getUndoManager().add(UndoManager.KBN_POSITION, part);

		//パーツの移動
		mPartsList.remove(part);
		mPartsList.add(newPosition, part);
		
	}
	
	public Bitmap getBitmap(){
		return mBitmap;
	}

    @Override
    public void flashToBitmap(Canvas canvas, Canvas maskCanvas) {
        // 何もしない
        throw new RuntimeException();
    }

    @Override
	public void rotate(float degrees) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void scale(float scaleX, float scaleY) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void move(int x, int y) {
		// TODO Auto-generated method stub
		
	}

    /**
     * 全てのパーツをビットマップに描画する
     * @return task
     */
    @DebugLog
	public Task flashToBitmap() {

        Task task = Task.callInBackground(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                ////
                // もしパーツが編集モードになっていたらスピードアップのため
                // オフスクリーンのbitmapを利用する
                ////

                if(mEditTargetObject != null){
                    //オフスクリーンモードの時は同期処理
                    flashToBitmapWithCache();
                }
                else{
                    //全再描画
                    flashAllToBitmap();
                }

                return null;
            }
        });

        return task;

	}

    /**
     * キャッシュを使って再描画
     */
    @DebugLog
    private void flashToBitmapWithCache(){
        //オフスクリーンON
        //キャッシュされたbitmapをコピーして、編集対象パーツのみ再描画
        mCanvas.drawBitmap(mOffScreenBitmap, 0, 0, mPaint);
        mCanvas.drawARGB(127, 0, 0, 0); //背景を少し暗くする
//				mMaskCanvas.drawBitmap(mOffScreenMaskBitmap, 0, 0, mPaint);
        mMaskCanvas.drawColor(Color.WHITE);

        mEditTargetObject.flashToBitmap(mCanvas, mMaskCanvas);

    }

    /**
     * 全てのパーツを再描画する
     */
    @DebugLog
    private void flashAllToBitmap(){
        long startTime = System.currentTimeMillis();

        //オフスクリーンOFF
        //通常動作
        //すべてのパーツを再描画する

        Timber.d("flash a:%d",System.currentTimeMillis());

        //背景設定
        Bitmap bitmap = getBackgroundBitmap();
        if(bitmap != null){
            if(mBackgroundBitmapIsRepeat == true){
                BitmapShader shader = new BitmapShader(bitmap, TileMode.REPEAT, TileMode.REPEAT);
                mPaint.setShader(shader);
                mCanvas.drawPaint(mPaint);
            }else{
                mCanvas.drawBitmap(bitmap, 0, 0, null);
            }
        }else if(mBitmapShaderBackground != null){
            mPaint.setShader(mBitmapShaderBackground);
            mCanvas.drawPaint(mPaint);
        }else{
            mCanvas.drawColor(Color.WHITE);
        }

        Timber.d("flash b:%d",System.currentTimeMillis());

        //マスクの背景
        mMaskCanvas.drawColor(Color.WHITE);

        Timber.d("flash c:%d",System.currentTimeMillis());

        //全てのパーツを描画
        if(mPartsList == null) return; //未設定なら何もしない

        ArrayList<PartObject> list = mPartsList;
        for (int i = 0; i < list.size(); i++) {
            PartObject part = list.get(i);
            if(part != null)
                part.flashToBitmap(mCanvas, mMaskCanvas);
        }

        Timber.d("flash d:%d",System.currentTimeMillis());

        //自分自身を親のビットマップに描画
        //canvas.drawBitmap(bitmap, src, dst, paint)
        if(mMaskCanvas != null)
            mMaskCanvas.drawCircle(mTouchX, mTouchY, 10, new Paint());

        Timber.d("flash e:%d",System.currentTimeMillis());

        Timber.d("FLASH INFO  object count:%d take time:%d",getPartsCount(),(System.currentTimeMillis() - startTime) );
    }

	@Override
	public RectF getBounds() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JSONObject getJsonObject() {
		JSONObject json = new JSONObject();
		try {
			json.put("partKind", PartObject.KIND_GROUP);
			if(mBackgroundBitmapFile != null){
				json.put("background", mBackgroundBitmapFile.getName());
				json.put("backgroundRepeat", mBackgroundBitmapIsRepeat);
			}
			json.put("width", mBitmap.getWidth());
			json.put("height", mBitmap.getHeight());
			json.put("displaySampleSize", mDisplaySampleSize);
			JSONArray partList = new JSONArray();
			for (int i = 0; i < mPartsList.size(); i++) {
				partList.put(mPartsList.get(i).getJsonObject());
			}
			json.put("parts", partList);
			
		} catch (JSONException e) {
			Timber.e(e,"");
			throw new RuntimeException(e);
		}
		
		return json;
	}
	
	//編集対象にオブジェクトを設定する
	//オブジェクトが設定されるとオフスクリーンモードになる
	//このモードでは編集対象以外のオブジェクトは再描画されなくなる
	//編集対象のオブジェクトは常に最上位に表示される
	public Task setEditTarget(PartObject target){

		if(target == mEditTargetObject){
			Task.TaskCompletionSource source = Task.create();
			source.setResult(null);
			return source.getTask();
		}
		
//		mOffScreenBitmap = Bitmap.createBitmap(bitmap.getWidth(),
//				bitmap.getHeight(), Config.RGB_565);
		mOffScreenBitmap = mBitmap.copy(Config.RGB_565, true);
//		mOffScreenCanvas = new Canvas(mOffScreenBitmap);
		mOffScreenMaskBitmap = mMaskBitmap.copy(Config.RGB_565, true);
		
		mEditTargetObject = target;
		mEditTargetObject.startEditTarget();


		//再描画
		return flashToBitmap();

	}

    /**
     * 編集中から抜けるために再描画する
     * @param needToFlash
     * @return
     */
	public Task clearEditTarget(boolean needToFlash){

        //編集対象がなければ何もしない
		if(mEditTargetObject == null){
            Task<String>.TaskCompletionSource nopTask = Task.create();
            nopTask.setResult("no result");
            return nopTask.getTask();
        }
		
		mEditTargetObject.endEditTarget();
		mEditTargetObject = null;

        Task firstTask;

		//再描画
		if(needToFlash == true)
			//更新の必要がある場合すべて再描画
            firstTask = flashToBitmap();
        else{
            //更新の必要がない場合、オフスクリーンを利用して元に戻す
            firstTask = Task.callInBackground(new Callable() {
                @Override
                public Object call() throws Exception {
                    mCanvas.drawBitmap(mOffScreenBitmap, 0, 0, mPaint);
                    mMaskCanvas.drawBitmap(mOffScreenMaskBitmap, 0, 0, mPaint);
                    return null;
                }
            });
		}

        //必要ないメモリを解放
        Task secondTask = firstTask.continueWith(new Continuation() {
            @Override
            public Object then(Task task) throws Exception {
                if(mOffScreenBitmap != null) mOffScreenBitmap.recycle();
                mOffScreenBitmap = null;
                mOffScreenCanvas = null;

                if(mOffScreenMaskBitmap != null) mOffScreenMaskBitmap.recycle();
                mOffScreenMaskBitmap = null;
                mOffScreenMaskCanvas = null;
                return null;
            }
        });

        return secondTask;
	}
	
	//すべてのBitmapオブジェクトをリサイクルする
	public void freeBitmap(){
//		if(mBitmap != null && mBitmap.isRecycled() == false)
//			mBitmap.recycle();
		mBitmap = null;
		mCanvas = null;
				
//		if(mMaskBitmap != null && mMaskBitmap.isRecycled() == false)
//			mMaskBitmap.recycle();
		mMaskBitmap = null;
		mMaskCanvas = null;
		
	}

	@Override
	public void startEditTarget() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void endEditTarget() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void recycle() {
		freeBitmap();
		
		if(mPartsList != null){
			for (int i = 0; i < getPartsCount(); i++) {
				PartObject part = mPartsList.get(i);
				part.recycle();
			}
			mPartsList.clear();
			mPartsList = null;
		}
		
		mDrawBoardView = null;
		
	}
	
	public boolean isRecycled(){
		if(mPartsList == null)
			return true;
		else
			return false;
	}

	@Override
	public void drawRealSize(Canvas canvas) throws Exception{
        Timber.d("step1");

		//背景設定
		if(mBackgroundBitmapFile != null){
			//フルサイズのビットマップを取得
			Bitmap bitmap = ImageUtil.restoreImageFromBufferCache(mBackgroundBitmapFile, 1,Config.RGB_565);

			if(mBackgroundBitmapIsRepeat == true){
				BitmapShader shader = new BitmapShader(bitmap, TileMode.REPEAT, TileMode.REPEAT);
				mPaint.setShader(shader);
				canvas.drawPaint(mPaint);			
			}else{
				canvas.drawBitmap(bitmap, 0, 0, null);
			}
			
			bitmap.recycle();
			bitmap = null;
		}else{
			canvas.drawColor(Color.WHITE);
		}

        Timber.d("step2");
	
		//全てのパーツを描画
		for (int i = 0; i < mPartsList.size(); i++) {
			System.gc();
			PartObject part = mPartsList.get(i);
			part.drawRealSize(canvas);
		}

        Timber.d("step3");
		
	}

	@Override
	public void deleteAsset() {
		if(mBackgroundBitmapFile != null){
			mBackgroundBitmapFile.delete();
			mBackgroundBitmapFile = null;
		}

		//全てのパーツのアセットを削除
		for (int i = 0; i < mPartsList.size(); i++) {
			PartObject part = mPartsList.get(i);
			part.deleteAsset();
		}
	}

	//背景ビットマップをキャッシュから取得する
	private Bitmap getBackgroundBitmap(){
		
		if(mBackgroundBitmapFile == null) return null;

		return BitmapCacheHolder.get().getCachedImage(mBackgroundBitmapFile,mDisplaySampleSize);
	}
	
	//背景画像を取得
	public File getBackgroundFile(){
		return mBackgroundBitmapFile;
	}

}
