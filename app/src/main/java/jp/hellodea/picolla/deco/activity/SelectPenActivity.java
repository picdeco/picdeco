package jp.hellodea.picolla.deco.activity;

import bolts.Continuation;
import bolts.Task;
import de.greenrobot.event.EventBus;
import hugo.weaving.DebugLog;
import jp.hellodea.picolla.R;
import jp.hellodea.picolla.common.eventbus.ErrorEvent;
import jp.hellodea.picolla.common.exception.LogicException;
import jp.hellodea.picolla.common.fragment.AlertDialogFragment;
import jp.hellodea.picolla.common.fragment.AlertDialogFragment_;
import jp.hellodea.picolla.common.objectgraph.ObjectGraphHolder;
import jp.hellodea.picolla.deco.Const;
import jp.hellodea.picolla.deco.entity.PenHist;
import jp.hellodea.picolla.deco.graphics.parts.PartUtil;
import jp.hellodea.picolla.deco.logic.AppUserLogic;
import jp.hellodea.picolla.deco.view.SamplePenView;
import timber.log.Timber;

import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

public class SelectPenActivity extends ActionBarActivity {

    @Inject
    AppUserLogic appUserLogic;
	
	private int mCurrentPenKind;		//現在のペンの種類
	private float mCurrentPenWidth;		//ペンの幅
	private int mCurrentPenColor;		//色
	private int mCurrentPenMidColor;	//色
	private int mCurrentPenOutColor;	//色
	private int mCurrentPenAlpha;	//透明度
	private float mCurrentPenMinWidth;	//選択できるペン幅の最小値
	private float mCurrentPenMaxWidth;	//選択できるペン幅の最大値

	private SamplePenView mSamplePenView; //プレビュー用ペン
	private GridView mPenListGridView; //ペンリスト
	private ImageButton mColorButton;
	private SeekBar mSeekBarSize;
	private SeekBar mSeekBarAlpha;
	private LinearLayout mLinearLayoutHistories; //履歴表示レイアウト
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        ObjectGraphHolder.get().inject(this);

		//レイアウトを指定
        setContentView(R.layout.activity_select_pen);

        //UI取得
        mPenListGridView = (GridView) findViewById(R.id.gridViewPenList);
        mPenListGridView.setAdapter(new PenImageAdapter(this));
        mSamplePenView = (SamplePenView)findViewById(R.id.samplePenView);
        mColorButton = (ImageButton)findViewById(R.id.buttonColor);
        mSeekBarSize = (SeekBar)findViewById(R.id.seekBarSize);
        mSeekBarAlpha = (SeekBar)findViewById(R.id.seekBarAlpha);
        mLinearLayoutHistories = (LinearLayout)findViewById(R.id.linearLayoutHistories);
        
        //現在のペンをパラメータより取得
        Intent intent = getIntent();
        mCurrentPenMinWidth = intent.getFloatExtra("penMinWidth",0.0f);
        mCurrentPenMaxWidth = intent.getFloatExtra("penMaxWidth",0.0f);
        mCurrentPenKind = intent.getIntExtra("penKind",0);
        mCurrentPenWidth = intent.getFloatExtra("penWidth",0);
        if(mCurrentPenMinWidth == 0.0f){
        	PartUtil.resetPenSize();
        }else{
        	PartUtil.setPenSize(mCurrentPenMinWidth, mCurrentPenMaxWidth);
        }
        mCurrentPenAlpha = intent.getIntExtra("penAlpha",255);
        mSeekBarAlpha.setProgress(mCurrentPenAlpha);
        mSeekBarSize.setProgress(PartUtil.getPenSize(mCurrentPenWidth));
        mCurrentPenColor = intent.getIntExtra("penColor",0);
        mCurrentPenMidColor = intent.getIntExtra("penMidColor",0);
        mCurrentPenOutColor = intent.getIntExtra("penOutColor",0);
        setPenToPreview();
        
        //アセット読み込み
        PartUtil.loadPartAssets(this, false);
        
        //イベントリスナー設定
        mPenListGridView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
                actionSelectPen(view);
            }
        });
        mColorButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				actionShowColorActivity();
			}
		});
        mSeekBarSize.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {
				actionChangeWidth(progress);
			}
		});
        mSeekBarAlpha.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {
				actionChangeAlpha(progress);
			}
		});
        
        //履歴情報を読み込み
        setPenHistory();
	}

    @DebugLog
    private void setPenHistory(){

        Task.callInBackground(new Callable<List<PenHist>>() {
            @Override
            public List<PenHist> call() throws Exception {
                return appUserLogic.getPenHists();
            }
        }).onSuccess(new Continuation<List<PenHist>, Object>() {
            @Override
            public Object then(Task<List<PenHist>> task) throws Exception {

                List<PenHist> penHists = task.getResult();

                for(PenHist penHist: penHists){
                    SamplePenView penView = new SamplePenView(getApplicationContext());

                    Paint[] paints = PartUtil.getPaint(
                            penHist.getInt(PenHist.F_KIND),
                            20,
                            penHist.getInt(PenHist.F_COLOR),
                            penHist.getInt(PenHist.F_MID_COLOR),
                            penHist.getInt(PenHist.F_OUT_COLOR),
                            255);
                    penView.setPaints(penHist.getInt(PenHist.F_KIND),paints);

                    //クリックイベント追加
                    penView.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            actionSelectPen(v);
                        }
                    });
                    mLinearLayoutHistories.addView(penView);
                }

                return null;
            }
        },Task.UI_THREAD_EXECUTOR).continueWith(new Continuation<Object, Object>() {
            @Override
            public Object then(Task<Object> task) throws Exception {
                if (task.isFaulted()) {
                    Timber.e(task.getError(), "");
                    EventBus.getDefault().post(
                            new ErrorEvent(SelectPenActivity.this,task.getError()));
                }
                return null;
            }
        }, Task.UI_THREAD_EXECUTOR);


    }

    //色、大きさの選択結果を受けて終了
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		if(resultCode == RESULT_OK){
			
			switch(requestCode){
			case Const.REQUEST_COLOR:
				//色情報取得
				mCurrentPenColor = data.getIntExtra("penColor",0);
				mCurrentPenMidColor = data.getIntExtra("penMidColor",0);
				mCurrentPenOutColor = data.getIntExtra("penOutColor",0);
				setPenToPreview();
				break;
			case Const.REQUEST_SIZE:
				break;
			}
			
			
		}
		
	}
	
	//一覧から選択されたペンをプレビューに表示
	private void actionSelectPen(View view){
        SamplePenView penView = (SamplePenView)view;
        
        mCurrentPenKind = penView.getPen();
//        mCurrentPenWidth = penView.getPenWidth();
        mCurrentPenColor = penView.getColor();
        mCurrentPenMidColor = penView.getMidColor();
        mCurrentPenOutColor = penView.getOutColor();
        
        mSeekBarSize.setProgress(PartUtil.getPenSize(mCurrentPenWidth));
        setPenToPreview();		
	}
	
	//現在選択されているペンを呼び出し元に返す
	@DebugLog
	private void actionReturnToParent(){

		Task.callInBackground(new Callable<Object>() {
			@Override
			@DebugLog
			public Object call() throws Exception {
				//履歴を更新
				try {
					appUserLogic.savePenHist(mCurrentPenKind,mCurrentPenColor,mCurrentPenMidColor,mCurrentPenOutColor);
				} catch (LogicException e) {
					EventBus.getDefault().postSticky(new ErrorEvent(SelectPenActivity.this,e));
					return null;
				}
				return null;
			}
		}).onSuccess(new Continuation<Object, Object>() {
			@Override
			@DebugLog
			public Object then(Task<Object> task) throws Exception {
				Intent intent = new Intent();
				intent.putExtra("penKind", mCurrentPenKind);
				intent.putExtra("penWidth", mCurrentPenWidth);
				intent.putExtra("penColor", mCurrentPenColor);
				intent.putExtra("penMidColor", mCurrentPenMidColor);
				intent.putExtra("penOutColor", mCurrentPenOutColor);
				intent.putExtra("penAlpha", mCurrentPenAlpha);

				setResult(RESULT_OK, intent);
				finish();

				return null;
			}
		});


	}
	
	//色選択画面を呼び出す
	private void actionShowColorActivity(){
		Intent intent = new Intent(this,SelectColorActivity.class);
		intent.putExtra("penKind", mCurrentPenKind);
		intent.putExtra("penWidth", mCurrentPenWidth);
		intent.putExtra("penColor", mCurrentPenColor);
		intent.putExtra("penMidColor", mCurrentPenMidColor);
		intent.putExtra("penOutColor", mCurrentPenOutColor);
		startActivityForResult(intent, Const.REQUEST_COLOR);
	}

	//ペンのサイズ変更
	private void actionChangeWidth(int progress){
		mCurrentPenWidth = PartUtil.mPenSizes[progress];
		
		setPenToPreview();
	}
	
	//透明度の変更
	private void actionChangeAlpha(int progress){
		mCurrentPenAlpha = progress;
		
		setPenToPreview();
	}


	//選択されたペンをプレビューに設定する
	private void setPenToPreview(){
        Paint[] paints = PartUtil.getPaint(mCurrentPenKind, mCurrentPenWidth,mCurrentPenColor,
        		mCurrentPenMidColor,mCurrentPenOutColor,mCurrentPenAlpha);
        mSamplePenView.setPaints(mCurrentPenKind,paints);
	}
	
	//ペン選択画面に表示されるView
	public class PenImageAdapter extends BaseAdapter{
	    private Context mContext;

		// ペンの種類
		private String[][] mPens = {
//				{"1","#0000ff","#000000","#000000"},
//				{"1","#87cefa","#000000","#000000"},
//				{"1","#228b22","#000000","#000000"},
//				{"1","#7cfc00","#000000","#000000"},
//				{"1","#ffff00","#000000","#000000"},
//				{"1","#ffa500","#000000","#000000"},
//				{"1","#ff0000","#000000","#000000"},
//				{"1","#ff69b4","#000000","#000000"},
//				{"1","#9932cc","#000000","#000000"},

//				{"1","#000000","#000000","#000000"},
				
				{"1","#0000ff","#000000","#000000"},
				{"2","#7cfc00","#228b22","#000000"},
				{"3","#ffff00","#ffa500","#ff0000"},
				{"4","#9932cc","#000000","#000000"},
				{"5","#ff0000","#000000","#000000"},
				{"6","#ff69b4","#000000","#000000"},
				{"7","#ffffff","#0000ff","#000000"},
				{"8","#ffa500","#000000","#000000"},
				{"9","#ff69b4","#ffffff","#000000"},
				{"10","#000000","#000000","#000000"},
				{"11","#000000","#000000","#000000"},
				{"12","#000000","#000000","#000000"},
				{"13","#000000","#000000","#000000"},
				{"14","#000000","#000000","#000000"},
				{"15","#000000","#000000","#000000"},
				{"16","#000000","#000000","#000000"},
				{"17","#000000","#000000","#000000"},
		};

		public PenImageAdapter(Context c) {
	        mContext = c;
	        
	    }

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mPens.length;
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup viewGroup) {
	        SamplePenView penView;
	        if (convertView == null) {  // if it's not recycled, initialize some attributes
	            penView = new SamplePenView(mContext);
	            penView.setLayoutParams(new GridView.LayoutParams(85, 85));
//	            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
//	            imageView.setPadding(8, 8, 8, 8);
	        } else {
	            penView = (SamplePenView)convertView;
	        }
	        
	        //描画するペン情報取得
	        Paint[] paints = PartUtil.getPaint(
	        		Integer.valueOf(mPens[position][0]), 
	        		20,
	        		Color.parseColor(mPens[position][1]), 
	        		Color.parseColor(mPens[position][2]), 
	        		Color.parseColor(mPens[position][3]),
	        		255
	        );
	        
	        penView.setPaints(Integer.valueOf(mPens[position][0]),paints);
	        return penView;
		}
	}
	
	@Override
	protected void onStart() {
		super.onStart();
        EventBus.getDefault().registerSticky(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
        EventBus.getDefault().unregister(this);
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater menuInflater = this.getMenuInflater();
        menuInflater.inflate(R.menu.menu_select_pen,menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()){
            case R.id.actionOk:
                actionReturnToParent();
                return true;
        }

        return false;
    }

    /**
     * error handling
     * @param event
     */
    @DebugLog
    public void onEventMainThread(ErrorEvent event){

        LogicException e = (LogicException) event.getException();

        //show dialog when error was happened
        DialogFragment dialogFragment = AlertDialogFragment_.builder()
                .eventKey(AlertDialogFragment.class.getName())
                .titleRes(e.getErrorCode().getErrorRes())
                .messageRes(e.getReasonRes())
                .positiveRes(android.R.string.ok).build();
        dialogFragment.show(this.getFragmentManager()
                ,AlertDialogFragment.class.getName());

        //remove sticky event after using.
        EventBus.getDefault().removeStickyEvent(event);

    }


}

