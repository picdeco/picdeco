package jp.hellodea.picolla.deco.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.widget.Spinner;

import jp.hellodea.picolla.deco.Const;
import jp.hellodea.picolla.deco.graphics.WidthHeight;
import timber.log.Timber;

public class Util {


	//スピナーを初期選択する
	public static void selectDataOnSpinner(String key,Object data,Spinner spinner){
		for (int i = 0; i < spinner.getCount(); i++) {
			HashMap<String, Object> hashMapAlbum = (HashMap<String, Object>) spinner.getItemAtPosition(i);
			if(data.equals(hashMapAlbum.get(key)) == true){
				spinner.setSelection(i);
				break;
			}
		}
	}

	//MD5文字列を取得
	public static String createDigest(String source) {
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			Timber.e(e, "");
			new RuntimeException(e);
		}

		byte[] data = source.getBytes();
		md.update(data);
            
		byte[] digest = md.digest();
            
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < digest.length; i++) {
            String b = Integer.toHexString(0xff & digest[i]);
            if (b.length() == 1) {
                sb.append("0");
            }
            sb.append(b);
		}
		return sb.toString();
	}	
	



}

