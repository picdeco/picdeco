package jp.hellodea.picolla.deco.eventbus;

import jp.hellodea.picolla.common.eventbus.BusEvent;

/**
 * Created by kazuhiro on 2015/04/08.
 */
public class ProgressViewEvent extends BusEvent{
    public enum Status{
        START, END
    }

    public ProgressViewEvent(Status status) {
        super(status);
    }

}
