package jp.hellodea.picolla.deco.activity;

import java.io.File;
import java.util.HashMap;
import java.util.concurrent.Callable;

import bolts.Continuation;
import bolts.Task;
import de.greenrobot.event.EventBus;
import hugo.weaving.DebugLog;
import jp.hellodea.picolla.R;
import jp.hellodea.picolla.common.eventbus.ErrorEvent;
import jp.hellodea.picolla.common.exception.LogicException;
import jp.hellodea.picolla.common.fragment.AlertDialogFragment;
import jp.hellodea.picolla.common.fragment.AlertDialogFragment_;
import jp.hellodea.picolla.common.objectgraph.ObjectGraphHolder;
import jp.hellodea.picolla.deco.Const;
import jp.hellodea.picolla.deco.entity.Clipart;
import jp.hellodea.picolla.deco.entity.ClipartKind;
import jp.hellodea.picolla.deco.entity.Work;
import jp.hellodea.picolla.deco.eventbus.ProgressDialogEvent;
import jp.hellodea.picolla.deco.eventbus.ProgressViewEvent;
import jp.hellodea.picolla.deco.graphics.BitmapCacheHolder;
import jp.hellodea.picolla.deco.graphics.SavableMatrix;
import jp.hellodea.picolla.deco.graphics.SavablePath;
import jp.hellodea.picolla.deco.graphics.parts.ImageObject;
import jp.hellodea.picolla.deco.graphics.parts.PartObject;
import jp.hellodea.picolla.deco.graphics.parts.PartUtil;
import jp.hellodea.picolla.deco.graphics.parts.PartsGroup;
import jp.hellodea.picolla.deco.graphics.parts.PathObject;
import jp.hellodea.picolla.deco.graphics.parts.TextObject;
import jp.hellodea.picolla.deco.logic.EditLogic;
import jp.hellodea.picolla.deco.logic.WorkLogic;
import jp.hellodea.picolla.deco.util.ContextUtil;
import jp.hellodea.picolla.deco.util.EventUtil;
import jp.hellodea.picolla.deco.util.ImageInfo;
import jp.hellodea.picolla.deco.util.ImageUtil;
import jp.hellodea.picolla.deco.util.StorageFileUtil;
import jp.hellodea.picolla.deco.util.UriUtil;
import jp.hellodea.picolla.deco.view.ClipartSelectorView;
import jp.hellodea.picolla.deco.view.DirectionIcon;
import jp.hellodea.picolla.deco.view.DrawBoardView;
import jp.hellodea.picolla.deco.view.IClickPlaceListener;
import timber.log.Timber;

import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import javax.inject.Inject;

public class MainEditorActivity extends ActionBarActivity {

    @Inject
    EditLogic editLogic;

    @Inject
    WorkLogic workLogic;

    //各種操作モード
    public static final int OPERATION_MODE_SCROLL = 0; //スクロールモード
    public static final int OPERATION_MODE_DRAW = 1; //手書きモード
    public static final int OPERATION_MODE_STAMP = 2; //スタンプモード
    public static final int OPERATION_MODE_PLACE = 3; //位置選択モード

    private int mOperationMode; //操作モード

    private FrameLayout mBaseLayout;
    private LinearLayout mDebugLayout;

    private RelativeLayout mRelativeLayoutBase;
    private FrameLayout mHorizontalScrollView;
    private Button mButtonImage;
    private ImageButton mButtonZoomUp;
    private ImageButton mButtonZoomDown;
    private ImageButton mButtonAutoScaleHeight;
    private ImageButton mButtonAutoScaleWidth;
    private Button mButtonPen;
    private Button mButtonFont;
    private Button mButtonClipart;
    private Button mButtonBackground;
    private Button mButtonFilter;
    private Button mButtonFinish;
    private ProgressBar mProgressBar;
    private ClipartSelectorView mClipartSelectorView;

    private int mCurrentPenKind = 1;            //現在のペンの種類
    private float mCurrentPenWidth = 20;        //ペンの幅
    private int mCurrentPenColor = Color.RED;        //色
    private int mCurrentPenMidColor;    //色
    private int mCurrentPenOutColor;    //色
    private int mCurrentPenAlpha = 255;    //透過


    private String mCurrentFontName = "MONOSPACE";    //現在のフォント
    private int mCurrentFontPenKind = 1;            //現在のペンの種類
    private int mCurrentFontPenColor = Color.RED;        //色
    private int mCurrentFontPenMidColor;    //色
    private int mCurrentFontPenOutColor;    //色
    private int mCurrentFontPenAlpha = 255;    //透過

    private DrawBoardView mDrawBoardView;  //メインキャンパス
    private PartsGroup mPartsGroup;  //メインオブジェクトのルート

    private boolean mIsFinishInit = false;

    private PartObject mPartObjectChangeTarget; //変更対象オブジェクト

    //お待ちくださいダイアログ
    private ProgressDialog mProgressDialog;

    //進捗表示ダイアログ
    private ProgressDialog mProgressBarDialog;


    @Override
    @DebugLog
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ContextUtil.showHeapMemoryInfo(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ObjectGraphHolder.get().inject(this);

        setContentView(R.layout.activity_main_editor);
        setViews();

        mClipartSelectorView.initialize();

        final Paint paint = new Paint();
        final Paint midPaint = new Paint(paint);
        final Paint outPaint = new Paint(paint);

        //フォントのロード
        PartUtil.loadPartAssets(this, false);

        setDirectionIcons();

        setActionListeners();

        setDebugView();

        //起動処理分岐
        //1.サブ機能からの戻りだったら作業を再開し、各戻り処理
        //2.一時フィルが存在したら作業の再開
        //3.URIが指定されていたら背景画像に設定
        //4.URIが指定されていなければ白紙
        Work work = null;
        try {
            work = editLogic.getCurrentWork(this);
        } catch (LogicException e) {
            EventBus.getDefault().postSticky(new ErrorEvent(this.getClass(),e));
            return;
        }

        File baseDir = StorageFileUtil.getImagePartsDir(this);

        try {
            int mode = getIntent().getIntExtra("executeMode", 0);

            if (mode == Const.REQUEST_MODIFY_IMAGE) {
                //画像選択処理からの戻りだったら一時ファイルを読も込み、画像を追加モードにする
                mPartsGroup = editLogic.restoreFromJson(work, mDrawBoardView,baseDir);
                resultForRequestEditImage(getIntent());
            } else if (mode == Const.REQUEST_FILTER) {
                //フィルター処理からの戻りだったら一時ファイルを読も込み、背景にフィルター適用済み画像を設定
                mPartsGroup = editLogic.restoreFromJson(work,mDrawBoardView,baseDir);
                resultForRequestFilter(getIntent());
            } else if (mode == Const.REQUEST_CROP) {
                //切り取り処理からの戻りだったら一時ファイルを読み込む
                //（戻ってきたということはキャンセルされたので追加処理は特に無し）
                mPartsGroup = editLogic.restoreFromJson(work, mDrawBoardView, baseDir);
                controllOperation(OPERATION_MODE_SCROLL);
            } else if (work.getString(Work.F_DATA) != null) {
                //一時保存があったら優先的に読み込み
                mPartsGroup = editLogic.restoreFromJson(work,mDrawBoardView,baseDir);
                controllOperation(OPERATION_MODE_SCROLL);
            } else if (getIntent().getData() != null) {
                //指定された写真を背景に設定
                mPartsGroup = editLogic.loadBackgroundPicture(this,baseDir,getIntent().getData(),mDrawBoardView);
                controllOperation(OPERATION_MODE_SCROLL);

            } else {
                int maxSize = ImageUtil.getMaxImageSize(this);
                //白紙作成
                int width = getIntent().getIntExtra("width",maxSize ); //白紙のサイズ
                int height = getIntent().getIntExtra("height",maxSize); //白紙のサイズ
                mPartsGroup = new PartsGroup(mDrawBoardView, null, width, height, Const.DISPLAY_SAMPLE_SIZE,baseDir);
                controllOperation(OPERATION_MODE_SCROLL);
            }
        } catch (Exception e) {
            Timber.e(e,"");
            throw new RuntimeException(e);
        }


    }



    @Override
    @DebugLog
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = this.getMenuInflater();
        menuInflater.inflate(R.menu.menu_main_editor, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    @DebugLog
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()){
            case android.R.id.home:
                Timber.d("click return button");

                returnToLauncher();

                return true;
            case R.id.actionUndo:
                actionUndo();
                return true;
            case R.id.actionSave:
                actionSave();
                return true;
            case R.id.actionShare:
                actionShare();
                return true;
        }

        return false;
    }


    /**
     * データを保存してランチャーに戻る
     */
    private void returnToLauncher(){
        LauncherActivity_.intent(this).fromMainEditor(true).flags(Intent.FLAG_ACTIVITY_CLEAR_TOP).start();
    }

    private void setDebugView(){
        final View maskView = new View(this) {

            @Override
            protected void onDraw(Canvas canvas) {
                mPartsGroup.drawMaskToView(canvas, 0, 0, 0.2f);
            }

            @Override
            public boolean onTouchEvent(MotionEvent event) {
                invalidate();

                return true;
            }


        };
        mDebugLayout.addView(maskView);
        maskView.setBackgroundColor(Color.MAGENTA);

    }


    /**
     * Viewのインスタンスを取得
     */
    private void setViews() {
        mBaseLayout = (FrameLayout) findViewById(R.id.baseLayout);
        mDebugLayout = (LinearLayout) findViewById(R.id.debugLayout);
        mButtonImage = (Button) findViewById(R.id.buttonImage);
        mButtonZoomUp = (ImageButton) findViewById(R.id.buttonZoomUp);
        mButtonZoomDown = (ImageButton) findViewById(R.id.buttonZoomDown);
        mButtonPen = (Button) findViewById(R.id.buttonPen);
        mButtonFont = (Button) findViewById(R.id.buttonFont);
        mButtonClipart = (Button) findViewById(R.id.buttonClipart);
        mButtonBackground = (Button) findViewById(R.id.buttonBackground);
        mButtonFilter = (Button) findViewById(R.id.buttonFilter);
        mHorizontalScrollView = (FrameLayout) findViewById(R.id.horizontalScrollView);
        mButtonFinish = (Button) findViewById(R.id.buttonFinish);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mButtonAutoScaleHeight = (ImageButton) findViewById(R.id.imageButtonHeight);
        mButtonAutoScaleWidth = (ImageButton) findViewById(R.id.imageButtonWidth);
        mRelativeLayoutBase = (RelativeLayout) findViewById(R.id.relativeLayoutBase);
        mClipartSelectorView = (ClipartSelectorView) findViewById(R.id.clipartSelectorView);
        mClipartSelectorView.setVisibility(View.GONE);

        //プログレスダイアログ生成
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getText(R.string.please_wait));

        //進捗表示ダイアログ生成
        mProgressBarDialog = new ProgressDialog(this);
        mProgressBarDialog.setMessage(getText(R.string.please_wait));
        mProgressBarDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressBarDialog.setCancelable(false);

        mDrawBoardView = new DrawBoardView(this);
        mDrawBoardView.setBackground(BitmapFactory.decodeResource(getResources(), R.drawable.background));
        mDrawBoardView.setLeftTop(0, 0);
        mBaseLayout.addView(mDrawBoardView);

    }

    /**
     * 描画オブジェクトの操作アイコン設定
     */
    private void setDirectionIcons() {
        DirectionIcon directionIconMove = new DirectionIcon(this, mDrawBoardView, DirectionIcon.MODE_MOVE);
        DirectionIcon directionIconResize = new DirectionIcon(this, mDrawBoardView, DirectionIcon.MODE_RESIZE);
        DirectionIcon directionIconRotate = new DirectionIcon(this, mDrawBoardView, DirectionIcon.MODE_ROTATE);
        DirectionIcon directionIconResizeFree = new DirectionIcon(this, mDrawBoardView, DirectionIcon.MODE_RESIZE_FREE);
        DirectionIcon directionIconDelete = new DirectionIcon(this, mDrawBoardView, DirectionIcon.MODE_DELETE);
        DirectionIcon directionIconUp = new DirectionIcon(this, mDrawBoardView, DirectionIcon.MODE_UP);
        DirectionIcon directionIconDown = new DirectionIcon(this, mDrawBoardView, DirectionIcon.MODE_DOWN);
        DirectionIcon directionIconChange = new DirectionIcon(this, mDrawBoardView, DirectionIcon.MODE_CHANGE);

        mDrawBoardView.setDirectionIcon(directionIconMove, directionIconResize, directionIconRotate,
                directionIconResizeFree, directionIconDelete, directionIconUp, directionIconDown, directionIconChange);
        mBaseLayout.addView(directionIconMove, 1, 1); //画面がちらつくので初期は最小の大きさ（０ではだめ）
        mBaseLayout.addView(directionIconResize, 1, 1);
        mBaseLayout.addView(directionIconRotate, 1, 1);
        mBaseLayout.addView(directionIconResizeFree, 1, 1);
        mBaseLayout.addView(directionIconDelete, 1, 1);
        mBaseLayout.addView(directionIconUp, 1, 1);
        mBaseLayout.addView(directionIconDown, 1, 1);
        mBaseLayout.addView(directionIconChange, 1, 1);

    }

    @Override
    @DebugLog
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);


        if (hasFocus == true) {
            if (mIsFinishInit == true) return; //既に処理済みなら何もしない

            mIsFinishInit = true;

            //
            //viewの幅、高さはこのイベントでしかとれないので画像の縮尺の計算をここでする
            //

            //キャンバス全体が見えるように縮尺
            mDrawBoardView.autoScale();

            //キャンパスが中心にくるように移動
            mDrawBoardView.moveToCenter();


            flash(false);

        }

    }

    //他のactivityからの戻り
    @DebugLog
    protected void onActivityResult(int requestCode, int resultCode, final Intent intent) {

        super.onActivityResult(requestCode, resultCode, intent);

        if (resultCode != RESULT_OK) {
            return;
        }

        //なぜかintentがnullの時があるようなのでチェック（開発環境では再現せず）
        if (intent == null) {
            Toast.makeText(this, R.string.processing_was_not_able_to_be_completed_correctly, Toast.LENGTH_LONG).show();
            return;
        }

        switch (requestCode) {
            case Const.REQUEST_GALLERY_INTENT_CALLED:
            case Const.REQUEST_GALLERY_KITKAT_INTENT_CALLED:
                Uri uri = UriUtil.getResultFromImagePicker(this,intent,requestCode);
                resultForRequestImage(uri);

            //選択されたペンの取得
            case Const.REQUEST_PEN:
                resultForRequestPen(intent);
                break;

            //選択されたテキスト取得
            case Const.REQUEST_TEXT:
                resultForRequestText(intent);
                break;

            default:
                break;
        }

    }

    @DebugLog
    @Override
    protected void onResume() {
        super.onResume();

        flash(false);

        ContextUtil.showHeapMemoryInfo(this);
    }

    @DebugLog
    @Override
    protected void onPause() {
        super.onPause();

        //キャッシュクリア
        BitmapCacheHolder.deleteCache();


        if (mPartsGroup != null) {

            actionScrollMode(); //操作待ちへ戻す

            Task.callInBackground(new Callable<Object>() {
                @Override
                public Object call() throws Exception {
                    editLogic.savePictureData(getApplication(), mPartsGroup.getJsonObject(),mPartsGroup.getBitmap());
                    return null;
                }
            }).continueWith(new Continuation<Object, Object>() {
                @Override
                public Object then(Task<Object> task) throws Exception {
                    if(task.isFaulted()){
                        Timber.e(task.getError(),"");
                        Toast.makeText(getApplication(), R.string.error_on_saving_work,Toast.LENGTH_LONG).show();
                    }
                    return null;
                }
            });
        }

    }

    @Override
    @DebugLog
    protected void onNewIntent(Intent intent) {

		Uri uri;
		ContentResolver contReslv;

		switch(intent.getIntExtra("executeMode", 0)){

		//画像を追加
		case Const.REQUEST_MODIFY_IMAGE:
			resultForRequestEditImage(intent);
			break;

		}
    }

    @DebugLog
    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mPartsGroup != null) {
            mPartsGroup.recycle();
            mPartsGroup = null;
        }

        if (mDrawBoardView != null) {
            mDrawBoardView.clear();
            mDrawBoardView = null;
        }
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {

            //処理中ならキャンセル
            if (mProgressBar.getVisibility() == View.VISIBLE) return true;

            //戻るボタンが押されたとき、クリップアートを表示していたらクリップアートを非表示にする
            if(event.getKeyCode() == KeyEvent.KEYCODE_BACK){
                if (mClipartSelectorView.getVisibility() == View.VISIBLE) {
                    mClipartSelectorView.actionClose();
                    return true;
                }else{
                    returnToLauncher();
                    return true;
                }
            }

        }

        return super.dispatchKeyEvent(event);

    }

    //各アクションのイベントリスナー設定
    private void setActionListeners() {
        //スクロールモード
        mButtonFinish.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                actionScrollMode();
            }
        });

        //画像選択
        mButtonImage.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                actionSelectImage();
            }
        });

        //ズームアップ
        mButtonZoomUp.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                actionZoomUp();
            }
        });
        //ズームダウン
        mButtonZoomDown.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                actionZoomDown();
            }
        });

        //ペン選択
        mButtonPen.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                actionSelectPen();
            }
        });

        //フォント選択
        mButtonFont.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                actionSelectFont();
            }
        });

        //クリップアート選択
        mButtonClipart.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                actionSelectClipart(Const.CLIPART_CATEGORY_CLIPART);
            }
        });

        //背景選択
        mButtonBackground.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                actionSelectClipart(Const.CLIPART_CATEGORY_BACKGROUND);
            }
        });

        //フィルター選択
        mButtonFilter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                actionSelectFilter();
            }
        });

        //高さを合わせる
        mButtonAutoScaleHeight.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                actionZoomHeight();
            }
        });

        //幅を合わせる
        mButtonAutoScaleWidth.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                actionZoomWidth();
            }
        });

    }


    /**
     * クリップアートが選択された時のイベント処理
     *
     * @param event
     */
    @DebugLog
    public void onEventMainThread(ClipartSelectorView.ClipArtSelectedEvent event) {
        ClipartKind kind = (ClipartKind) event.getArgs()[0];
        Clipart clipart = (Clipart) event.getArgs()[1];
        File file = (File) event.getArgs()[2];

        if(kind.getString(ClipartKind.F_CATEGORY).equals(Const.CLIPART_CATEGORY_CLIPART)){
            resultForRequestClipart(kind, clipart, file);
        }else if(kind.getString(ClipartKind.F_CATEGORY).equals(Const.CLIPART_CATEGORY_BACKGROUND)){
            resultForRequestBackground(kind,clipart, file);
        }

    }



    //スクロールモードにする
    private void actionScrollMode() {
        HashMap<String, Object> ret = mDrawBoardView.startScrollMode();

        switch ((Integer) ret.get("mode")) {
            case DrawBoardView.MODE_DRAW_PATH:
                //直前が手書きモードの場合パスオブジェクト追加
                SavablePath path = (SavablePath) ret.get("path");
                SavableMatrix matrix = (SavableMatrix) ret.get("matrix");
                new PathObject(
                        mPartsGroup,
                        path,
                        matrix,
                        mCurrentPenKind,
                        mCurrentPenWidth,
                        mCurrentPenColor,
                        mCurrentPenMidColor,
                        mCurrentPenOutColor,
                        mCurrentPenAlpha,
                        true,
                        Const.DISPLAY_SAMPLE_SIZE);
                flash(false);
                break;
            case DrawBoardView.MODE_STAMP:
                //直前がスタンプモードの場合、スタンプ画像を追加
                if (ret.get("bitmap") != null) {
                    new ImageObject(
                            mPartsGroup,
                            (Float) ret.get("x"),
                            (Float) ret.get("y"),
                            (Bitmap) ret.get("bitmap"),
                            StorageFileUtil.getImagePartsDir(MainEditorActivity.this),
                            true,
                            Const.DISPLAY_SAMPLE_SIZE);
                    flash(false);
                }
                break;
            case DrawBoardView.MODE_EDIT:
                //特に何もしない
                break;
            default:
        }

        controllOperation(OPERATION_MODE_SCROLL);


    }

    //画像選択処理
    private void actionSelectImage() {
        //ギャラリーを表示
        UriUtil.startImagePicker(this);

    }

    //ズームダウン
    private void actionZoomDown() {
        mDrawBoardView.postScale(-0.1f);
        mDrawBoardView.invalidate();
    }

    //ズームアップ
    private void actionZoomUp() {
        mDrawBoardView.postScale(0.1f);
        mDrawBoardView.invalidate();
    }

    //高さに合わせる
    private void actionZoomHeight() {
        mDrawBoardView.autoScaleHeight();
        mDrawBoardView.invalidate();
    }

    //幅に合わせる
    private void actionZoomWidth() {
        mDrawBoardView.autoScaleWidth();
        mDrawBoardView.invalidate();
    }

    //保存
    private void actionSave() {

        //作業中かチェック
        if (mDrawBoardView.getMode() != DrawBoardView.MODE_SCROLL) {
            Toast.makeText(this, R.string.please_complete_the_final_task, Toast.LENGTH_SHORT).show();
            return;
        }

        EventBus.getDefault().postSticky(
                new ProgressDialogEvent(ProgressDialogEvent.Status.START,
                        R.string.progress_processing_final_picture, ProgressDialog.STYLE_SPINNER));

        //キャッシュクリア
        BitmapCacheHolder.deleteCache();

        Task<Void> task = Task.callInBackground(new Callable<Void>() {
            @Override
            public Void call() throws Exception {

                //データを保存
                editLogic.savePictureData(getApplication(), mPartsGroup.getJsonObject(),mPartsGroup.getBitmap());

                Work work = editLogic.getCurrentWork(MainEditorActivity.this);

                //ギャラリーに登録
                editLogic.storeGallery(MainEditorActivity.this, work);

                return null;
            }
        }).onSuccess(new Continuation<Void, Void>() {
            @Override
            public Void then(Task task) throws Exception {

                AlertDialogFragment dialogFragment = AlertDialogFragment_.builder()
                        .titleRes(R.string.alert_title_info)
                        .messageRes(R.string.completed_storing_picture)
                        .positiveRes(android.R.string.ok)
                        .build();
                dialogFragment.show(getFragmentManager(),AlertDialogFragment.class.getName());

                return null;
            }
        }, Task.UI_THREAD_EXECUTOR).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(Task task) throws Exception {
                EventBus.getDefault().postSticky(new ProgressDialogEvent(ProgressDialogEvent.Status.END,0,0));

                if(task.isFaulted()){
                    Timber.e(task.getError(),"");
                    EventBus.getDefault().postSticky(new ErrorEvent(MainEditorActivity.class,task.getError()));
                }
                return null;
            }
        },Task.UI_THREAD_EXECUTOR);

    }

    /**
     * 共有
     */
    @DebugLog
    private void actionShare(){

        //作業中かチェック
        if (mDrawBoardView.getMode() != DrawBoardView.MODE_SCROLL) {
            Toast.makeText(this, R.string.please_complete_the_final_task, Toast.LENGTH_SHORT).show();
            return;
        }

        EventBus.getDefault().postSticky(
                new ProgressDialogEvent(ProgressDialogEvent.Status.START,
                        R.string.progress_processing_final_picture, ProgressDialog.STYLE_SPINNER));

        //キャッシュクリア
        BitmapCacheHolder.deleteCache();

        Task<Object> task = Task.callInBackground(new Callable<Object>() {
            @Override
            public Object call() throws Exception {

                //データを保存
                editLogic.savePictureData(getApplication(), mPartsGroup.getJsonObject(),mPartsGroup.getBitmap());

                Work work = editLogic.getCurrentWork(MainEditorActivity.this);

                //ギャラリーに登録
                File pictureFile = editLogic.outputPicture(MainEditorActivity.this, work);

                return pictureFile;
            }
        }).onSuccess(new Continuation<Object, Object>() {
            @Override
            public Object then(Task task) throws Exception {

                File file = (File) task.getResult();

                ShareCompat.IntentBuilder builder = ShareCompat.IntentBuilder.from(MainEditorActivity.this);
                builder.setType(Const.MIME_JPEG).setStream(Uri.fromFile(file)).startChooser();

                return null;
            }
        }, Task.UI_THREAD_EXECUTOR).continueWith(new Continuation<Object, Object>() {
            @Override
            public Object then(Task task) throws Exception {
                EventBus.getDefault().postSticky(new ProgressDialogEvent(ProgressDialogEvent.Status.END,0,0));

                if(task.isFaulted()){
                    Timber.e(task.getError(),"");
                    EventBus.getDefault().postSticky(new ErrorEvent(MainEditorActivity.class,task.getError()));
                }
                return null;
            }
        },Task.UI_THREAD_EXECUTOR);

    }


    //Undo
    private void actionUndo() {
        mDrawBoardView.undo();
    }

    //フィルター選択処理
    private void actionSelectFilter() {

        if (mPartsGroup.getBackgroundFile() == null) {
            Toast.makeText(this, R.string.background_is_not_set_up, Toast.LENGTH_SHORT).show();
            return;
        }

        //背景にフィルターを掛ける
        Intent send_intent = new Intent(MainEditorActivity.this, SelectFilterActivity.class);
        Uri uri = Uri.fromFile(mPartsGroup.getBackgroundFile());
        send_intent.setData(uri);
        send_intent.putExtra("parent", MainEditorActivity.class.getName());
        startActivity(send_intent);

        finish();

    }

    //ペン選択処理
    private void actionSelectPen() {
        mPartObjectChangeTarget = null; //新規追加なので変更対象はクリアする

        Intent intent = new Intent(MainEditorActivity.this, SelectPenActivity.class);
        intent.putExtra("penKind", mCurrentPenKind);
        intent.putExtra("penWidth", mCurrentPenWidth);
        intent.putExtra("penColor", mCurrentPenColor);
        intent.putExtra("penMidColor", mCurrentPenMidColor);
        intent.putExtra("penOutColor", mCurrentPenOutColor);
        intent.putExtra("penAlpha", mCurrentPenAlpha);
        startActivityForResult(intent, Const.REQUEST_PEN);

    }

    //フォント選択処理
    private void actionSelectFont() {
        mPartObjectChangeTarget = null; //新規追加なので変更対象はクリアする

        Intent intent = new Intent(MainEditorActivity.this, SelectTextActivity.class);
        intent.putExtra("fontName", mCurrentFontName);
        intent.putExtra("penKind", mCurrentFontPenKind);
        intent.putExtra("penColor", mCurrentFontPenColor);
        intent.putExtra("penMidColor", mCurrentFontPenMidColor);
        intent.putExtra("penOutColor", mCurrentFontPenOutColor);
        intent.putExtra("penAlpha", mCurrentFontPenAlpha);
        startActivityForResult(intent, Const.REQUEST_TEXT);

    }


    //クリップアート選択処理
    private void actionSelectClipart(String name) {
        mClipartSelectorView.show(name);

    }


    //画像選択後処理
    private void resultForRequestImage(Uri uri) {

        //メモリ節約のためメモリ開放
        mPartsGroup.recycle();
        mPartsGroup = null;
        mDrawBoardView.clear();

        Intent newIntent = new Intent(MainEditorActivity.this, PreviewPictureActivity.class);
        newIntent.setData(uri);
        startActivity(newIntent);

        finish();


    }

    //透過画像選択後処理
    private void resultForRequestAlphaImage(Intent intent) {
        //画像をリサイズ
        int maxImageSize = ImageUtil.getMaxImageSize(this);
        ImageInfo imageInfo = null;
        try {

            imageInfo = ImageUtil.decodeAndStoreResizedImage(
                    this, intent.getData(), StorageFileUtil.getTempCacheDir(this), maxImageSize, 8);

        } catch (Exception e) {
            Timber.e(e,"");
            throw new RuntimeException(e);
        }

        //リサイズ済みの透過画像を通常のスタンプのように設定
        resultForRequestClipart(null, null, imageInfo.file);


    }

    //ペン選択後処理
    private void resultForRequestPen(final Intent intent) {
        mCurrentPenKind = intent.getIntExtra("penKind", 0);
        mCurrentPenWidth = intent.getFloatExtra("penWidth", 0);
        mCurrentPenColor = intent.getIntExtra("penColor", 0);
        mCurrentPenMidColor = intent.getIntExtra("penMidColor", 0);
        mCurrentPenOutColor = intent.getIntExtra("penOutColor", 0);
        mCurrentPenAlpha = intent.getIntExtra("penAlpha", 255);

        if (mPartObjectChangeTarget == null) {
            //手書きモードにする
            Paint[] paints = PartUtil.getPaint(mCurrentPenKind, mCurrentPenWidth, mCurrentPenColor,
                    mCurrentPenMidColor, mCurrentPenOutColor, mCurrentPenAlpha);
            mDrawBoardView.startDrawMode(false, false, paints[0], paints[1], paints[2]);
            controllOperation(OPERATION_MODE_DRAW);
        } else {
            //内容変更
            //オリジナルをコピーして新しいパーツを作る。その後オリジナルは削除
            SavablePath path = new SavablePath(mPartObjectChangeTarget.getPath().copyJsonPath());
            SavableMatrix matrix = new SavableMatrix(mPartObjectChangeTarget.getMatrix());

            int position = mPartsGroup.getPartPosition(mPartObjectChangeTarget);
            mPartsGroup.removePart(mPartObjectChangeTarget, true);
            PathObject newObject = new PathObject(mPartsGroup, path, matrix, mCurrentPenKind,
                    mCurrentPenWidth, mCurrentPenColor, mCurrentPenMidColor, mCurrentPenOutColor, mCurrentPenAlpha, true, Const.DISPLAY_SAMPLE_SIZE);
            mPartsGroup.changePosition(newObject, position, false);
            flash(false);

            mPartObjectChangeTarget = null;
        }


    }

    //テキスト選択後処理
    private void resultForRequestText(final Intent intent) {

        mCurrentFontPenKind = intent.getIntExtra("penKind", 0);
        mCurrentFontPenColor = intent.getIntExtra("penColor", 0);
        mCurrentFontPenMidColor = intent.getIntExtra("penMidColor", 0);
        mCurrentFontPenOutColor = intent.getIntExtra("penOutColor", 0);
        mCurrentFontPenAlpha = intent.getIntExtra("penAlpha", 255);


        if (mPartObjectChangeTarget == null) {
            //位置指定モードへ
            mDrawBoardView.startClickPlaceMode(new IClickPlaceListener() {
                @Override
                public void onSelected(float x, float y, int logicalX, int logicalY) {
                    SavableMatrix matrix = new SavableMatrix();
                    matrix.setTranslate(logicalX, logicalY);

                    //選択された場所にテキストを挿入
                    TextObject textObject = new TextObject(
                            mPartsGroup,
                            intent.getStringExtra("fontName"),
                            intent.getIntExtra("penKind", 0),
                            matrix,
                            intent.getStringExtra("text"),
                            intent.getIntExtra("penColor", 0),
                            intent.getIntExtra("penMidColor", 0),
                            intent.getIntExtra("penOutColor", 0),
                            intent.getIntExtra("penAlpha", 255),
                            true,
                            Const.DISPLAY_SAMPLE_SIZE);

                    //編集モードスタート
                    mDrawBoardView.startEditMode(textObject);
                    mDrawBoardView.getUndoManager().addModify(textObject); //確定後、描画されるようにダミー
                    controllOperation(OPERATION_MODE_SCROLL);
                }
            });

            controllOperation(OPERATION_MODE_PLACE);

        } else {
            //内容変更
            //オリジナルをコピーして新しいパーツを作る。その後オリジナルは削除
            SavableMatrix matrix = new SavableMatrix(mPartObjectChangeTarget.getMatrix());
            int position = mPartsGroup.getPartPosition(mPartObjectChangeTarget);
            mPartsGroup.removePart(mPartObjectChangeTarget, true);
            TextObject newObject = new TextObject(
                    mPartsGroup,
                    intent.getStringExtra("fontName"),
                    intent.getIntExtra("penKind", 0),
                    matrix,
                    intent.getStringExtra("text"),
                    intent.getIntExtra("penColor", 0),
                    intent.getIntExtra("penMidColor", 0),
                    intent.getIntExtra("penOutColor", 0),
                    intent.getIntExtra("penAlpha", 255),
                    true,
                    Const.DISPLAY_SAMPLE_SIZE);
            mPartsGroup.changePosition(newObject, position, false);
            flash(false);

            mPartObjectChangeTarget = null;

        }

    }

    //クリップアート選択後処理
    private void resultForRequestClipart(final ClipartKind kind, final Clipart clipart, final File file) {
        //位置指定モードへ
        mDrawBoardView.startClickPlaceMode(new IClickPlaceListener() {
            @Override
            public void onSelected(float x, float y, int logicalX, int logicalY) {

                //画像を追加
                ImageObject imageObject = editLogic.loadImage(
                        MainEditorActivity.this,mPartsGroup,file,StorageFileUtil.getImagePartsDir(MainEditorActivity.this), logicalX,logicalX);

                //編集モードスタート
                mDrawBoardView.startEditMode(imageObject);
                mDrawBoardView.getUndoManager().addModify(imageObject); //確定後、描画されるようにダミー
                controllOperation(OPERATION_MODE_SCROLL);

            }
        });

        controllOperation(OPERATION_MODE_PLACE);


    }

    //背景選択後処理
    private void resultForRequestBackground(ClipartKind kind, Clipart clipart, final File file) {
        if(file == null) return;

        EventBus.getDefault().postSticky(
                new ProgressDialogEvent(ProgressDialogEvent.Status.START,
                        R.string.progress_processing_background_image,ProgressDialog.STYLE_SPINNER));


        Task<Void> task = Task.callInBackground(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                editLogic.loadBackgroundClipart(MainEditorActivity.this,
                        StorageFileUtil.getImagePartsDir(MainEditorActivity.this),file,mPartsGroup);
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(Task<Void> task) throws Exception {
                if(task.isFaulted()){
                    EventBus.getDefault().post(new ErrorEvent(MainEditorActivity.class,task.getError()));
                }

                EventBus.getDefault().postSticky(new ProgressDialogEvent(ProgressDialogEvent.Status.END,0,0));
                MainEditorActivity.this.flash(false);

                return null;
            }
        },Task.UI_THREAD_EXECUTOR);

    }

    //画像加工後処理
    private void resultForRequestEditImage(Intent intent) {
        final Uri uri = intent.getData();
        if (uri != null) {

            final File imageFile = UriUtil.getFileFromUri(this,uri);

            //位置指定モードへ
            mDrawBoardView.startClickPlaceMode(new IClickPlaceListener() {
                @Override
                public void onSelected(float x, float y, int logicalX, int logicalY) {

                    //画像を追加
                    ImageObject imageObject = editLogic.loadImage(
                            MainEditorActivity.this,mPartsGroup,imageFile,StorageFileUtil.getImagePartsDir(MainEditorActivity.this),logicalX,logicalY);

                    //編集モードスタート
                    mDrawBoardView.startEditMode(imageObject);
                    mDrawBoardView.getUndoManager().addModify(imageObject); //確定後、描画されるようにダミー
                    controllOperation(OPERATION_MODE_SCROLL);

                }
            });
            controllOperation(OPERATION_MODE_PLACE);


        }

    }

    //フィルター後処理
    @DebugLog
    private void resultForRequestFilter(Intent intent) {

        if (intent.getData() != null) {
            try {
                File file = UriUtil.getFileFromUri(this,intent.getData());
                Bitmap bitmap = ImageUtil.restoreImageFromBufferCache(file, Const.DISPLAY_SAMPLE_SIZE,null);
                mPartsGroup.setBackground(bitmap, true, file, true);
            } catch (Exception e) {
                Timber.e(e,"");
                throw new RuntimeException(e);
            }

        }


        controllOperation(OPERATION_MODE_SCROLL);

    }


    //オペレーションコントローラー
    private void controllOperation(int mode) {

        switch (mode) {
            case OPERATION_MODE_SCROLL: //スクロールモードへ
                mHorizontalScrollView.setVisibility(View.VISIBLE);

                mButtonFinish.setVisibility(View.INVISIBLE);

                mButtonZoomUp.setVisibility(View.VISIBLE);
                mButtonZoomDown.setVisibility(View.VISIBLE);
                mButtonAutoScaleHeight.setVisibility(View.VISIBLE);
                mButtonAutoScaleWidth.setVisibility(View.VISIBLE);

                break;

            case OPERATION_MODE_DRAW: //手書きモードへ
                mHorizontalScrollView.setVisibility(View.GONE);

                mButtonFinish.setVisibility(View.VISIBLE);

                mButtonZoomUp.setVisibility(View.INVISIBLE);
                mButtonZoomDown.setVisibility(View.INVISIBLE);
                mButtonAutoScaleHeight.setVisibility(View.INVISIBLE);
                mButtonAutoScaleWidth.setVisibility(View.INVISIBLE);

                break;

            case OPERATION_MODE_PLACE: //位置選択モードへ
                mHorizontalScrollView.setVisibility(View.GONE);

                mButtonFinish.setVisibility(View.INVISIBLE);

                mButtonZoomUp.setVisibility(View.VISIBLE);
                mButtonZoomDown.setVisibility(View.VISIBLE);
                mButtonAutoScaleHeight.setVisibility(View.VISIBLE);
                mButtonAutoScaleWidth.setVisibility(View.VISIBLE);

                break;

            case OPERATION_MODE_STAMP: //スタンプモードへ
                mHorizontalScrollView.setVisibility(View.GONE);

                mButtonFinish.setVisibility(View.VISIBLE);

                mButtonZoomUp.setVisibility(View.INVISIBLE);
                mButtonZoomDown.setVisibility(View.INVISIBLE);
                mButtonAutoScaleHeight.setVisibility(View.INVISIBLE);
                mButtonAutoScaleWidth.setVisibility(View.INVISIBLE);

                break;
        }

        //現在のモードを変更
        mOperationMode = mode;

    }

    private void flash(final boolean changeToScrollMode) {
        EventBus.getDefault().postSticky(new ProgressViewEvent(ProgressViewEvent.Status.START));

        mPartsGroup.flashToBitmap().onSuccess(new Continuation() {
            @Override
            public Object then(Task task) throws Exception {
                if (mDrawBoardView == null) return null;

                mDrawBoardView.invalidate();

                if (changeToScrollMode == true)
                    controllOperation(OPERATION_MODE_SCROLL);

                EventBus.getDefault().postSticky(new ProgressViewEvent(ProgressViewEvent.Status.END));

                return null;
            }
        }, Task.UI_THREAD_EXECUTOR);

    }

    public void onEventMainThread(DrawBoardView.ModifyContentOfTargetEvent event) {
        mPartObjectChangeTarget = (PartObject) event.getArg();

        //各設定機能を呼び出し
        if (mPartObjectChangeTarget.mKind == PartObject.KIND_PATH) {
            PathObject pathObject = (PathObject) mPartObjectChangeTarget;

            //パス設定画面を呼び出し
            Intent intent = new Intent(MainEditorActivity.this, SelectPenActivity.class);
            intent.putExtra("penKind", pathObject.getKind());
            intent.putExtra("penWidth", pathObject.getStrokeWidth());
            intent.putExtra("penColor", pathObject.getColor());
            intent.putExtra("penMidColor", pathObject.getMidColor());
            intent.putExtra("penOutColor", pathObject.getOutColor());
            intent.putExtra("penAlpha", Color.alpha(pathObject.getColor()));
            startActivityForResult(intent, Const.REQUEST_PEN);

        } else if (mPartObjectChangeTarget.mKind == PartObject.KIND_TEXT) {
            TextObject textObject = (TextObject) mPartObjectChangeTarget;

            //文字設定画面を呼び出し
            Intent intent = new Intent(MainEditorActivity.this, SelectTextActivity.class);
            intent.putExtra("fontName", textObject.getFontName());
            intent.putExtra("penKind", textObject.getKind());
            intent.putExtra("penColor", textObject.getColor());
            intent.putExtra("penMidColor", textObject.getMidColor());
            intent.putExtra("penOutColor", textObject.getOutColor());
            intent.putExtra("penAlpha", Color.alpha(textObject.getColor()));
            intent.putExtra("text", textObject.getText());
            startActivityForResult(intent, Const.REQUEST_TEXT);

        }
    }

    /**
     * show progress dialog
     * @param event
     */
    public void onEventMainThread(ProgressDialogEvent event){
        EventUtil.showProgressDialog(this,event);
    }

    /**
     * show progress view
     * @param event
     */
    public void onEventMainThread(ProgressViewEvent event){
        if(event.getEventKey().equals(ProgressViewEvent.Status.START)){
            mProgressBar.setVisibility(View.VISIBLE);
        }else {
            mProgressBar.setVisibility(View.INVISIBLE);
        }

        //remove sticky event after using.
        EventBus.getDefault().removeStickyEvent(event);

    }

    /**
     * error handling
     * @param event
     */
    @DebugLog
    public void onEventMainThread(ErrorEvent event){

        LogicException e = (LogicException) event.getException();

        //show dialog when error was happened
        DialogFragment dialogFragment = AlertDialogFragment_.builder()
                .eventKey(AlertDialogFragment.class.getName())
                .titleRes(e.getErrorCode().getErrorRes())
                .messageRes(e.getReasonRes())
                .positiveRes(android.R.string.ok).build();
        dialogFragment.show(this.getFragmentManager()
                ,AlertDialogFragment.class.getName());

        //remove sticky event after using.
        EventBus.getDefault().removeStickyEvent(event);

    }

    @DebugLog
    @Override
    protected void onStart() {
        super.onStart();

        EventBus.getDefault().registerSticky(this);
    }

    @DebugLog
    @Override
    protected void onStop() {
        super.onStop();

        EventBus.getDefault().unregister(this);
    }

}