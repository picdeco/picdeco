package jp.hellodea.picolla.deco.activity;

import jp.hellodea.picolla.R;
import jp.hellodea.picolla.deco.Const;
import jp.hellodea.picolla.deco.graphics.parts.PartUtil;
import jp.hellodea.picolla.deco.view.SampleTextView;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;


public class SelectFontActivity extends ActionBarActivity {

	private String mCurrentFontName;		//現在のフォント
	private int mCurrentPenKind;		//現在のペンの種類
	private int mCurrentPenColor;		//色
	private int mCurrentPenMidColor;	//色
	private int mCurrentPenOutColor;	//色
	private int mCurrentPenAlpha;	//透過
	private String mCurrentText;

	private ListView mListView;
	private SampleTextView mSampleTextView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//レイアウトを指定
        setContentView(R.layout.activity_select_font);

        //フォントのロード
        PartUtil.loadPartAssets(this, false);

        //UI取得
        mListView = (ListView) findViewById(R.id.listViewFontList);
        mListView.setAdapter(new FontImageAdapter(this));
        mSampleTextView = (SampleTextView)findViewById(R.id.sampleTextView);
               
        //現在のペンをパラメータより取得
        Intent intent = getIntent();
        mCurrentFontName = intent.getStringExtra("fontName");
        mCurrentPenKind = intent.getIntExtra("penKind",0);
        mCurrentPenColor = intent.getIntExtra("penColor",0);
        mCurrentPenMidColor = intent.getIntExtra("penMidColor",0);
        mCurrentPenOutColor = intent.getIntExtra("penOutColor",0);
        mCurrentPenAlpha = intent.getIntExtra("penAlpha",0);
//        mCurrentText = intent.getStringExtra("text");
        //サンプル文字を統一
        mCurrentText = getString(R.string.text_for_font_list);

        actionChangeText(mCurrentText);

        //イベントリスナー設定
        mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View view, int position, long arg3) {
				actionSelectFont(position);
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		if(resultCode == RESULT_OK){
			
			switch(requestCode){
			case Const.REQUEST_FONT_MANAGER:
				PartUtil.loadPartAssets(this, true); //フォント再読み込み
		        mListView.setAdapter(new FontImageAdapter(this)); //フォントリスト更新
				break;
			}
			
		}
		
	}

	//一覧から選択されたペンをプレビューに表示
	private void actionSelectFont(int position){
        mCurrentFontName = (String)PartUtil.getTypefaceNames().toArray()[position];
        setPenToPreview();
        
	}

	//現在選択されているペンを呼び出し元に返す
	private void actionReturnToParent(){
        Intent intent = new Intent();
		intent.putExtra("fontName",mCurrentFontName);

		setResult(RESULT_OK, intent);
        finish();
        
	}

	//テキスト変更
	private void actionChangeText(String text){
		mCurrentText = text;
		mSampleTextView.setText(mCurrentText, 30);
		setPenToPreview();
		
	}

	//選択されたペンをプレビューに設定する
	private void setPenToPreview(){
        Paint[] paints = PartUtil.getPaint(mCurrentPenKind, 1,mCurrentPenColor,mCurrentPenMidColor,mCurrentPenOutColor,mCurrentPenAlpha);
        mSampleTextView.setPaints(mCurrentPenKind,paints,mCurrentFontName);
        
	}
	
	//フォント管理を呼び出す
	private void actionShowManager(){
		Intent intent = new Intent(this,DownloadFontsActivity.class);
		startActivityForResult(intent, Const.REQUEST_FONT_MANAGER);
		

	}

	//フォント選択画面に表示されるアダプター
	public class FontImageAdapter extends BaseAdapter{
	    private Context mContext;

		public FontImageAdapter(Context c) {
	        mContext = c;
	    }

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return PartUtil.getTypefaceNames().size();
		}

		@Override
		public Object getItem(int position) {
			return PartUtil.getTypefaceNames().toArray()[position];
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup viewGroup) {
	        TextView textView;
	        if (convertView == null) {  // if it's not recycled, initialize some attributes
	            textView = new TextView(mContext);
//	            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
//	            imageView.setPadding(8, 8, 8, 8);
	        } else {
	            textView = (TextView)convertView;
	        }
	        
	        //文字を描画
	        textView.setText(mCurrentText);
	        textView.setTypeface(PartUtil.getTypeface((String)PartUtil.getTypefaceNames().toArray()[position]));
	        textView.setTextSize(40);
	        textView.setTextColor(Color.BLACK);
	        
	        return textView;
		}
	}
	
	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater menuInflater = this.getMenuInflater();
        menuInflater.inflate(R.menu.menu_select_font,menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()){
            case R.id.actionDownload:
                actionShowManager();
                return true;
            case R.id.actionOk:
                actionReturnToParent();
                return true;
        }

        return false;
    }

}

