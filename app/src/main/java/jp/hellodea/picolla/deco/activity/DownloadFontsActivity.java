package jp.hellodea.picolla.deco.activity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import bolts.Continuation;
import bolts.Task;
import de.greenrobot.event.EventBus;
import hugo.weaving.DebugLog;
import jp.hellodea.picolla.R;
import jp.hellodea.picolla.common.eventbus.ErrorEvent;
import jp.hellodea.picolla.common.eventbus.ProgressEvent;
import jp.hellodea.picolla.common.eventbus.SelectEvent;
import jp.hellodea.picolla.common.exception.ErrorCode;
import jp.hellodea.picolla.common.exception.LogicException;
import jp.hellodea.picolla.common.fragment.AlertDialogFragment;
import jp.hellodea.picolla.common.fragment.AlertDialogFragment_;
import jp.hellodea.picolla.common.objectgraph.ObjectGraphHolder;
import jp.hellodea.picolla.deco.adapter.FontSampleImageAdapter;
import jp.hellodea.picolla.deco.entity.Font;
import jp.hellodea.picolla.deco.eventbus.ProgressDialogEvent;
import jp.hellodea.picolla.deco.graphics.parts.PartUtil;
import jp.hellodea.picolla.deco.logic.AssetLogic;
import jp.hellodea.picolla.deco.util.EventUtil;
import jp.hellodea.picolla.deco.util.StorageFileUtil;
import timber.log.Timber;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;

import javax.inject.Inject;

public class DownloadFontsActivity extends ActionBarActivity {

    @Inject
    AssetLogic assetLogic;

    private ListView mListViewFonts;

    private Font selectedFont;

    @Override
    @DebugLog
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ObjectGraphHolder.get().inject(this);

        //レイアウトを指定
        setContentView(R.layout.activity_download_fonts);

        //UI取得
        mListViewFonts = (ListView) findViewById(R.id.listViewFontList);

        //フォントのロード
        PartUtil.loadPartAssets(this, false);

        //イベントリスナー設定
        mListViewFonts.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int position, long arg3) {
                Log.d("kazino", "click");
                selectFont(position);
            }
        });

        //フォント読み込み
        loadFontList();

    }

    //フォントリストをサーバーより取得
    @DebugLog
    private void loadFontList() {

        List<Font> fonts;
        try {
            fonts = assetLogic.getFonts();
        } catch (LogicException e) {
            EventBus.getDefault().post(new ErrorEvent(this.getClass(), e));
            return;
        }

        //ダウンロード済みフォントを取得
        Set<String> existFonts = PartUtil.getTypefaceNames();

        List<Font> downloadedFonts = new ArrayList<>();

        //ダウンロード済みかチェック
        for (int i = 0; i < fonts.size(); i++) {
            Font font = fonts.get(i);
            if (existFonts.contains(font.getString(Font.F_PATH)) == true)
                downloadedFonts.add(font);
        }

        FontSampleImageAdapter fontSampleImageAdapter =
                new FontSampleImageAdapter(this, R.layout.item_font_sample, fonts, downloadedFonts);

        mListViewFonts.setAdapter(fontSampleImageAdapter);

    }

    //フォントをダウンロード
    @DebugLog
    private void selectFont(int position) {

        selectedFont = (Font) mListViewFonts.getAdapter().getItem(position);

        AlertDialogFragment dialogFragment = AlertDialogFragment_.builder()
                .eventKey(DownloadFontsActivity.class.getName())
                .titleRes(R.string.alert_title_inquiry)
                .messageRes(R.string.confirm_download_font)
                .positiveRes(android.R.string.ok)
                .negativeRes(android.R.string.cancel)
                .build();

        dialogFragment.show(getFragmentManager(), AlertDialogFragment.class.getName());
    }

    @DebugLog
    public void onEventMainThread(SelectEvent event) {
        if (!event.getEventKey().equals(DownloadFontsActivity.class.getName())) return;

        if (event.getArg().equals(android.R.string.ok)) performDownload();


        EventBus.getDefault().removeStickyEvent(event);
    }


    //フォントをダウンロードする
    @DebugLog
    private void performDownload() {

        EventBus.getDefault().postSticky(new ProgressDialogEvent(ProgressDialogEvent.Status.START,
                R.string.progress_download_font, ProgressDialog.STYLE_HORIZONTAL));

        String filePath = StorageFileUtil.getTempCacheDir(this) + "/" + StorageFileUtil.getSafeFileName(selectedFont.getUrl());

        Ion
                .with(this)
                .load(selectedFont.getUrl())
                .progress(new ProgressCallback() {
                    @Override
                    public void onProgress(long downloaded, long total) {
                        EventBus.getDefault().post(new ProgressEvent(DownloadFontsActivity.class, (int) total, (int) downloaded));
                    }
                })
                .write(new File(filePath))
                .setCallback(new FutureCallback<File>() {
                    @Override
                    public void onCompleted(Exception e, File result) {

                        if (e != null) {
                            Timber.e(e, "");
                            LogicException logicException = new LogicException(ErrorCode.CanNotDownload,R.string.error_download_font, e);
                            EventBus.getDefault().postSticky(new ErrorEvent(DownloadFontsActivity.class, logicException));
                            return;
                        }

                        unzipFont(result);
                    }
                });

    }

    //フォントを解凍する
    @DebugLog
    private void unzipFont(final File fontFile) {

        Task.callInBackground(new Callable<Object>() {
            @Override
            public Object call() throws Exception {

                InputStream is = new FileInputStream(fontFile);
                ZipInputStream zis = new ZipInputStream(is);

                //フォントフォルダ取得
                File fontDir = StorageFileUtil.getFontsDir(DownloadFontsActivity.this);

                ZipEntry ze;
                while ((ze = zis.getNextEntry()) != null) {
                    File fontFile = new File(fontDir, ze.getName());
                    FileOutputStream fos = new FileOutputStream(fontFile);
                    byte[] buf = new byte[1024];
                    int size = 0;

                    while ((size = zis.read(buf, 0, buf.length)) > -1) {
                        fos.write(buf, 0, size);
                    }
                    fos.close();
                    zis.closeEntry();

                }
                zis.close();

                return null;
            }
        }).onSuccess(new Continuation<Object, Object>() {
            @Override
            public Object then(Task<Object> task) throws Exception {
                FontSampleImageAdapter adapter = (FontSampleImageAdapter) mListViewFonts.getAdapter();
                adapter.add(selectedFont);
                adapter.notifyDataSetChanged();

                return null;
            }
        }, Task.UI_THREAD_EXECUTOR).continueWith(new Continuation<Object, Object>() {
            @Override
            public Object then(Task<Object> task) throws Exception {

                EventBus.getDefault().postSticky(new ProgressDialogEvent(ProgressDialogEvent.Status.END, 0, 0));

                if (task.isFaulted()) {

                }
                return null;
            }
        });
    }


    //閉じる
    private void actionReturnToParent() {
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().registerSticky(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater menuInflater = this.getMenuInflater();
        menuInflater.inflate(R.menu.menu_download_font, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case R.id.actionOk:
                actionReturnToParent();
                return true;
        }

        return false;
    }

    /**
     * show progress dialog
     *
     * @param event
     */
    public void onEventMainThread(ProgressDialogEvent event) {
        EventUtil.showProgressDialog(this, event);
    }

    /**
     * update progress dialog
     *
     * @param event
     */
    public void onEventMainThread(ProgressEvent event) {
        EventUtil.updateProgressDialog(this, event);
    }

    /**
     * error handling
     *
     * @param event
     */
    @DebugLog
    public void onEventMainThread(ErrorEvent event) {
        EventUtil.showErrorAlertDialog(this, event);
    }


}

