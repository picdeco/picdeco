package jp.hellodea.picolla.deco;

import android.content.Context;

import com.flurry.android.FlurryAgent;
import com.parse.Parse;
import com.parse.ParseCrashReporting;
import com.parse.ParseInstallation;
import com.parse.ParseObject;

import hugo.weaving.DebugLog;
import jp.hellodea.picolla.common.ParseConst;
import jp.hellodea.picolla.deco.entity.AppUser;
import jp.hellodea.picolla.deco.entity.Clipart;
import jp.hellodea.picolla.deco.entity.ClipartHist;
import jp.hellodea.picolla.deco.entity.ClipartKind;
import jp.hellodea.picolla.deco.entity.CuttingShape;
import jp.hellodea.picolla.deco.entity.Effect;
import jp.hellodea.picolla.deco.entity.Filter;
import jp.hellodea.picolla.deco.entity.Font;
import jp.hellodea.picolla.deco.entity.PenHist;
import jp.hellodea.picolla.deco.entity.SampleEntity;
import jp.hellodea.picolla.deco.entity.TextHist;
import jp.hellodea.picolla.deco.entity.Work;

/**
 * Created by kazuhiro on 2015/02/10.
 */
public class Flurry {

    public static String EVENT_ID_CREATE_NEW_PICTURE = "CREATE_NEW_PICTURE";
    public static String EVENT_ID_UPDATE_PICTURE = "UPDATE_PICTURE";
    public static String EVENT_ID_DELETE_PICTURE = "DELETE_PICTURE";
    public static String EVENT_ID_EXPORT_PICTURE = "EXPORT_PICTURE";
    public static String EVENT_ID_SHARE_PICTURE = "SHARE_PICTURE";
    public static String EVENT_ID_PICTURE_FROM_GALLERY = "PICTURE_FROM_GALLERY";
    public static String EVENT_ID_PICTURE_SIZE_SQUARE = "PICTURE_SIZE_SQUARE";
    public static String EVENT_ID_PICTURE_SIZE_LANDSCAPE = "PICTURE_SIZE_LANDSCAPE";
    public static String EVENT_ID_PICTURE_SIZE_PORTRAIT = "PICTURE_SIZE_PORTRAIT";

    @DebugLog
    public static void initialize(Context context) {
        FlurryAgent.setLogEnabled(false);
        FlurryAgent.init(context, "BN9TSMWVHKBZ2MMHQQ3G");
    }

}
