package jp.hellodea.picolla.deco.view;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by kazuhiro on 2015/04/22.
 */
@EViewGroup(resName = "item_color")
public class ColorItemView extends RelativeLayout {
    @ViewById
    RelativeLayout relativeLayoutColor;

    @ViewById
    ImageView imageViewCheck;

    public ColorItemView(Context context) {
        super(context);
    }


    public  void bind (int color, boolean selected)
    {
        //色を設定する
        relativeLayoutColor.setBackgroundColor(color);

        //選択されていたらチェックマークを表示
        if(selected == true)
            imageViewCheck.setVisibility(View.VISIBLE);
        else
            imageViewCheck.setVisibility(View.INVISIBLE);

    }
}
