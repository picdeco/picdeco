package jp.hellodea.picolla.deco.activity;

import de.greenrobot.event.EventBus;
import hugo.weaving.DebugLog;
import jp.hellodea.picolla.R;
import jp.hellodea.picolla.common.eventbus.SelectEvent;
import jp.hellodea.picolla.deco.adapter.ColorListPagerAdapter;
import jp.hellodea.picolla.deco.graphics.parts.PartUtil;
import jp.hellodea.picolla.deco.view.ColorListView;
import jp.hellodea.picolla.deco.view.ColorListView_;
import jp.hellodea.picolla.deco.view.SamplePenView;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.astuetz.PagerSlidingTabStrip;

import java.util.ArrayList;
import java.util.List;

public class SelectColorActivity extends ActionBarActivity {

	private SamplePenView mSamplePenView;

	private int mCurrentPenKind;		//現在のペンの種類
	private float mCurrentPenWidth;		//ペンの幅
	private int mCurrentPenColor;		//色
	private int mCurrentPenMidColor;	//色
	private int mCurrentPenOutColor;	//色

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_color);

        // ツールバーをアクションバーとしてセット
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        //UI取得
        mSamplePenView = (SamplePenView)findViewById(R.id.samplePenView);

        //現在のペンをパラメータより取得
        Intent intent = getIntent();
        mCurrentPenKind = intent.getIntExtra("penKind",0);
        mCurrentPenWidth = intent.getFloatExtra("penWidth",0);
        mCurrentPenColor = intent.getIntExtra("penColor",0);
        mCurrentPenMidColor = intent.getIntExtra("penMidColor",0);
        mCurrentPenOutColor = intent.getIntExtra("penOutColor",0);
        setPenToPreview();


        // Initialize the ViewPager and set an adapter
        ViewPager pager = (ViewPager) findViewById(R.id.viewPager);

        List<ColorListView> colorListViews = new ArrayList<>();

        if(mCurrentPenColor != 0){
            ColorListView colorListView = ColorListView_.build(this);
            colorListView.bind(ColorListView.KBN_COLOR1,mCurrentPenColor);
            colorListViews.add(colorListView);
        }
        if(mCurrentPenMidColor != 0){
            ColorListView colorListView = ColorListView_.build(this);
            colorListView.bind(ColorListView.KBN_COLOR2,mCurrentPenMidColor);
            colorListViews.add(colorListView);
        }
        if(mCurrentPenOutColor != 0){
            ColorListView colorListView = ColorListView_.build(this);
            colorListView.bind(ColorListView.KBN_COLOR3,mCurrentPenOutColor);
            colorListViews.add(colorListView);
        }

        pager.setAdapter(new ColorListPagerAdapter(this,colorListViews));

        // Bind the tabs to the ViewPager
        PagerSlidingTabStrip pagerSlidingTabStrip = (PagerSlidingTabStrip) findViewById(R.id.pagerSlidingTabStrip);
        pagerSlidingTabStrip.setViewPager(pager);


        pager.setCurrentItem(0);

	}

	//選択されたペンをプレビューに設定する
	private void setPenToPreview(){
        Paint[] paints = PartUtil.getPaint(mCurrentPenKind, mCurrentPenWidth, mCurrentPenColor,
                mCurrentPenMidColor, mCurrentPenOutColor, 255);
        mSamplePenView.setPaints(mCurrentPenKind,paints);
	}

	//現在選択されているペンを呼び出し元に返す
	private void actionReturnToParent(){
        Intent intent = new Intent();
		intent.putExtra("penColor", mCurrentPenColor);
		intent.putExtra("penMidColor", mCurrentPenMidColor);
		intent.putExtra("penOutColor", mCurrentPenOutColor);

		setResult(RESULT_OK, intent);
        finish();

	}

	//一覧から選択された色をプレビューに設定
    @DebugLog
	public void actionSelectColor(int kbn, int color){
		switch(kbn){
		case ColorListView.KBN_COLOR1:
	        mCurrentPenColor = color;
			break;
		case ColorListView.KBN_COLOR2:
	        mCurrentPenMidColor = color;
			break;
		case ColorListView.KBN_COLOR3:
	        mCurrentPenOutColor = color;
			break;
		}

        setPenToPreview();


	}

	@Override
	protected void onStart() {
		super.onStart();
        EventBus.getDefault().registerSticky(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
        EventBus.getDefault().unregister(this);
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater menuInflater = this.getMenuInflater();
        menuInflater.inflate(R.menu.menu_select_color,menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()){
            case R.id.actionOk:
                actionReturnToParent();;
                return true;
        }

        return false;
    }

    /**
     * show progress dialog
     * @param event
     */
    @DebugLog
    public void onEventMainThread(SelectEvent event){
        if(!event.getEventKey().equals(ColorListView.class)) return;

        int kbn = (int) event.getArgs()[0];
        int color = (int) event.getArgs()[1];

        actionSelectColor(kbn,color);

        //remove sticky event after using.
        EventBus.getDefault().removeStickyEvent(event);
    }

}

