package jp.hellodea.picolla.deco.view;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import bolts.Continuation;
import bolts.Task;
import de.greenrobot.event.EventBus;
import jp.hellodea.picolla.R;
import jp.hellodea.picolla.common.eventbus.BusEvent;
import jp.hellodea.picolla.deco.eventbus.ProgressViewEvent;
import jp.hellodea.picolla.deco.graphics.SavableMatrix;
import jp.hellodea.picolla.deco.graphics.SavablePath;
import jp.hellodea.picolla.deco.graphics.manipulate.UndoManager;
import jp.hellodea.picolla.deco.graphics.parts.PartObject;
import jp.hellodea.picolla.deco.graphics.parts.PartsGroup;
import jp.hellodea.picolla.deco.util.ImageUtil;

public class DrawBoardView extends View implements IDirectionListener,OnGestureListener {

    /**
     * 内容の編集を行うイベント
     */
    public static class ModifyContentOfTargetEvent extends BusEvent{
        public ModifyContentOfTargetEvent(PartObject partObject) {
            super(DrawBoardView.class, partObject);
        }
    }

	private GestureDetector mGestureDetector; //シングルタッチジェスチャーマネージャ
	
	private PartsGroup mPartsGroup; //ルートのパーツグループ
	
	private float mScale = 1.0f; //現在の表示スケール

	private int mLogicalViewLeft; //現在表示中の論理ビューの左端
	private int mLogicalViewTop; //現在表示中の論理ビューの上端
	
	private DirectionIcon mDirectionIconMove; //操作ボタン 移動
	private DirectionIcon mDirectionIconResize; //操作ボタン リサイズ
	private DirectionIcon mDirectionIconRotate; //操作ボタン 回転
	private DirectionIcon mDirectionIconResizeFree; //操作ボタン フリーリサイズ
	private DirectionIcon mDirectionIconDelete; //操作ボタン 削除
	private DirectionIcon mDirectionIconUp; //操作ボタン アップ
	private DirectionIcon mDirectionIconDown; //操作ボタン ダウン
	private DirectionIcon mDirectionIconChange; //操作ボタン 変更
	
	private PartObject mCurrentPart; //現在選択されているパーツ
	private int mCurrentPartIndex; //現在選択されているパーツの元のzindex
	
	private int mMode; //現在のモード
	public static final int MODE_SCROLL = 0; //Viewのスクロールモード
	public static final int MODE_DRAW_PATH = 1; //描画モード
	public static final int MODE_EDIT = 2; //オブジェクト編集モード
	public static final int MODE_STAMP = 3; //連続スタンプモード
	public static final int MODE_CLICK_PLACE = 4; //位置指定モード
	
	//
	// 描画モードの情報
	//
	private boolean mDrawLineEnable; //描画モードで直線も引けるようにするか
	private SavablePath mDrawPath; //描画モード時の保存用パス
	private int mPathSensitivity = 5; //描画モード時の手ぶれ許容度
	private Paint mDrawPaint; //描画モード時のペイント
	private Paint mDrawMidPaint; //描画モード時のペイント
	private Paint mDrawOutPaint; //描画モード時のペイント
	private float mDrawPointX; //１つ前のパス座標
	private float mDrawPointY; //１つ前のパス座標
	private ArrayList<Path> mArrayListPoints; //直線モードの時にタップした場所を記録（目印として表示用）
	private Paint mPaintPath; //直線モードのポイント表示用
	
	
	private float mMultiTouchLastX; //マルチタッチ時の前回のX座標
	private float mMultiTouchLastY; //マルチタッチ時の前回のY座標

	
	//
	// 連続スタンプモードの情報
	//
	private Bitmap mStampBitmap; //スタンプビットマップ
	private ArrayList<HashMap<String, Float>> mStampPlaces; //スタンプの位置
	
	//
	// 位置指定モードの情報
	//
	private IClickPlaceListener mClickPlaceListener; //位置指定のコールバック
	
	
	private UndoManager mUndoManager; //この描画ボード用のアンドゥマネージャ
	
	private Paint mBackgroundPaint; //背景画像

	public DrawBoardView(Context context) {
		super(context);

        initialize();
	}

    public DrawBoardView(Context context, AttributeSet attrs) {
        super(context, attrs);

        initialize();
    }

    private void initialize(){
        mScale = 1.0f;
        mMode = MODE_SCROLL;

        mGestureDetector = new GestureDetector(this);


	}

	public void setPartsGroup(PartsGroup partsGroup){
		mPartsGroup = partsGroup;
		
		//アンドゥマネージャ登録
		mUndoManager = new UndoManager();

		dismissDirectionIcon(); //もし表示されていたら非表示
	}
	
	
	public UndoManager getUndoManager() {
		return mUndoManager;
	}

    /**
     * 規定の範囲内の倍率かチェックする
     * @param scale
     * @return
     */
    private boolean isAcceptableScale(float scale){
        int viewWidth = getWidth();
        int viewHeight = getHeight();
        int imageWidth = mPartsGroup.getBitmap().getWidth();
        int imageHeight = mPartsGroup.getBitmap().getHeight();
        if(getWidth() > getHeight()){
            if(imageWidth * scale < viewWidth / 2 || imageWidth * scale > viewWidth * 2) return false;
        }else{
            if(imageHeight * scale < viewHeight / 2 || imageHeight * scale > viewHeight * 2) return false;
        }

        return true;
    }

	public void setScale(float scale) {

        //規定の範囲内か確認
        if(!isAcceptableScale(scale)) return;

        mScale = scale;
		
		if(mMode == MODE_EDIT)
			showDirectionIconByTarget();
		
	}
	
	public float getScale() {
		return this.mScale;
	}
	
	public void postScale(float scale) {
		float newScale = mScale + scale;

        //規定の範囲内か確認
        if(!isAcceptableScale(newScale)) return;


		//現在の表示位置の中心をスケール変更後に再度中心にくるように変更
		int oldWidth,newWidth,oldHeight,newHeight;
		
		oldWidth = (int)((float)this.getWidth() / mScale);
		newWidth = (int)((float)this.getWidth() / newScale);
		mLogicalViewLeft += (oldWidth - newWidth) / 2;
		
		oldHeight = (int)((float)this.getHeight() / mScale);
		newHeight = (int)((float)this.getHeight() / newScale);
		mLogicalViewTop += (oldHeight - newHeight) / 2;
	
		setScale(newScale);

	}

	public int[] getLogicalPoint(float x, float y){
		int[] points = new int[2];
		
		//タップされた物理座標を現在表示されている論理ビューの座標に変換
		points[0] = (int) (mLogicalViewLeft + (x * 1.0f / mScale));
		points[1] = (int) (mLogicalViewTop + (y * 1.0f / mScale));
		
		return points;
	}
	
	public void setLeftTop(int left, int top){
		mLogicalViewLeft = left;
		mLogicalViewTop = top;
	}
	
	public void setDirectionIcon(
			DirectionIcon move,
			DirectionIcon resize,
			DirectionIcon rotate,
			DirectionIcon resizeFree,
			DirectionIcon delete,
			DirectionIcon up,
			DirectionIcon down,
			DirectionIcon change){

		mDirectionIconMove = move;
		mDirectionIconResize = resize;
		mDirectionIconRotate = rotate;
		mDirectionIconResizeFree = resizeFree;
		mDirectionIconDelete = delete;
		mDirectionIconUp = up;
		mDirectionIconDown = down;
		mDirectionIconChange = change;
	}
	
	//手書きモードをスタートする
	public void startDrawMode(boolean enableLine, boolean autoClose, Paint paint, Paint midPaint, Paint outPaint){
		mMode = MODE_DRAW_PATH;

		mDrawLineEnable = enableLine;
		mDrawPath = new SavablePath(autoClose);
		mDrawPaint = new Paint(paint);
		mDrawPaint.setStrokeWidth(mDrawPaint.getStrokeWidth() * mScale);

		if(midPaint != null){
			mDrawMidPaint = new Paint(midPaint);
			mDrawMidPaint.setStrokeWidth(mDrawMidPaint.getStrokeWidth() * mScale);
		}
		else mDrawMidPaint = null;
		
		if(outPaint != null){
			mDrawOutPaint = new Paint(outPaint);
			mDrawOutPaint.setStrokeWidth(mDrawOutPaint.getStrokeWidth() * mScale);
		}
		else mDrawOutPaint = null;
		
		if(mDrawLineEnable == true) mArrayListPoints = new ArrayList<Path>();
		
		mDrawPointX = 0.0f;
		mDrawPointY = 0.0f;

	}

	//現在のモードを取得する
	public int getMode(){
		return mMode;
	}
	
	//スクロールモードを開始する
	public HashMap<String, Object> startScrollMode(){

		HashMap<String, Object> ret = new HashMap<String, Object>();
		
		if(mMode == MODE_DRAW_PATH && mDrawPath.isEmpty() == false){
			//直前が手書きモードだった場合
			//物理座標に表示されているパスを論理ビューの座標に変換
			SavableMatrix matrix = new SavableMatrix();
			RectF rectF = new RectF();
			mDrawPath.computeBounds(rectF,true);
			matrix.postScale(1.0f / mScale, 1.0f / mScale);
			matrix.postTranslate(
					mLogicalViewLeft, // + (rectF.left * 1.0f / mScale), 
					mLogicalViewTop); // + (rectF.top * 1.0f / mScale));
			
			ret.put("mode", MODE_DRAW_PATH);
			ret.put("path", mDrawPath);
			ret.put("matrix", matrix);
			
			//直線モードの時はタップポイントをクリア
			mArrayListPoints = null;
			
			
		}
		else if(mMode == MODE_STAMP){
			
			if(mStampPlaces.size() > 0){
				//直前がスタンプモードだった場合
				//論理座標に変換したビットマップを作成する
				//sampleSizeも考慮
				Bitmap bitmap = Bitmap.createBitmap((int)(this.getWidth() / mScale) * mPartsGroup.getDisplaySampleSize(),
						(int)(this.getHeight() / mScale) * mPartsGroup.getDisplaySampleSize(),Config.ARGB_8888);
				Canvas canvas = new Canvas(bitmap);
				float x,y;
				for (int i = 0; i < mStampPlaces.size(); i++) {
					x = mStampPlaces.get(i).get("x") / mScale * mPartsGroup.getDisplaySampleSize();
					y = mStampPlaces.get(i).get("y") / mScale * mPartsGroup.getDisplaySampleSize();
					canvas.drawBitmap(mStampBitmap, x, y, null);
				}
	
				//余白を削除
				HashMap<String, Object> utilRet = ImageUtil.getRealSizeBitmap(bitmap);
				
				ret.put("mode", MODE_STAMP);
				ret.put("bitmap", (Bitmap) utilRet.get("bitmap"));
				x = (float)mLogicalViewLeft + ((Integer)utilRet.get("left") / mPartsGroup.getDisplaySampleSize());
				y = (float)mLogicalViewTop + ((Integer)utilRet.get("top") / mPartsGroup.getDisplaySampleSize());
				ret.put("x", x); //余白分だけ移動
				ret.put("y", y); //余白分だけ移動
			}else{
				ret.put("bitmap",null);
			}

			ret.put("mode", MODE_STAMP);

			mStampBitmap.recycle();
			mStampBitmap = null;
			mStampPlaces.clear();
			mStampPlaces = null;
			
		}
		else if(mMode == MODE_EDIT){
			//直前が編集モードだった場合
			
			mCurrentPart = null;

			//再描画の必要があるかチェック
			boolean needToFlash;
			if(mUndoManager.getCurrentUndoObject() != null) needToFlash = true; //編集あり
			else needToFlash = false; //編集なし

			//今までの作業内容をUNDO履歴へ書き込む
			mUndoManager.fixModify();

            EventBus.getDefault().postSticky(new ProgressViewEvent(ProgressViewEvent.Status.START));

			//オフスクリーンモードをOFF
            mPartsGroup.clearEditTarget(needToFlash).continueWith(new Continuation() {
                @Override
                public Object then(Task task) throws Exception {
                    invalidate();
                    EventBus.getDefault().postSticky(new ProgressViewEvent(ProgressViewEvent.Status.END));
                    return null;
                }
            },Task.UI_THREAD_EXECUTOR);

			//編集アイコンを非表示
			dismissDirectionIcon();

			ret.put("mode", MODE_EDIT);
		}
		else{
			ret.put("mode", -1);
		}
		
		mMode = MODE_SCROLL;

		return ret;
	}

	//編集モードをスタートする
	public void startEditMode(final PartObject target){
		Log.d("kazino", "startEditMode");
		
		System.gc();
		
		//もし直前が編集モードで、今回のターゲットと違っていて、UNDO履歴があれば再描画
		if(mMode == MODE_EDIT && mCurrentPart != target && 
				mUndoManager.getCurrentUndoObject() != null){

			//今までの作業内容をUNDO履歴へ書き込む
			mUndoManager.fixModify();

            EventBus.getDefault().postSticky(new ProgressViewEvent(ProgressViewEvent.Status.START));

			//再描画させる
            mPartsGroup.clearEditTarget(true).continueWith(new Continuation() {
                @Override
                public Object then(Task task) throws Exception {
                    prepareEditMode(target); //編集モードスタート
                    EventBus.getDefault().postSticky(new ProgressViewEvent(ProgressViewEvent.Status.END));
					invalidate();
                    return null;
                }
            },Task.UI_THREAD_EXECUTOR);

		}
		else{
			//再描画の必要がなければ直ちにスタート
			prepareEditMode(target);
		}
	}
		
	//編集モードスタート
	public void prepareEditMode(PartObject target){
		Log.d("kazino", "prepareEditMode");

		mMode = MODE_EDIT; //編集モード
		mCurrentPart = target;
		
		//オフスクリーンモードをON
		mPartsGroup.setEditTarget(mCurrentPart).continueWith(new Continuation() {
			@Override
			public Object then(Task task) throws Exception {
				//操作アイコン表示
				showDirectionIconByTarget();
				invalidate();
				return null;
			}
		},Task.UI_THREAD_EXECUTOR);

	}
	
	//編集操作アイコンを対象の近くに表示する
	private void showDirectionIconByTarget(){

		RectF hitBounds = mCurrentPart.getBounds();

		//論理座標から物理座標へ変換
		Matrix matrix = new Matrix();
		matrix.postTranslate(mLogicalViewLeft * -1, mLogicalViewTop * -1);
		matrix.postScale(mScale, mScale);
		matrix.mapRect(hitBounds);
		
		//操作アイコン表示
		if(mDirectionIconMove != null)
			mDirectionIconMove.show((int)hitBounds.left,(int)hitBounds.top,(int)hitBounds.width(),(int)hitBounds.height());
		if(mDirectionIconResize != null)
			mDirectionIconResize.show((int)hitBounds.left,(int)hitBounds.top,(int)hitBounds.width(),(int)hitBounds.height());
		if(mDirectionIconResizeFree != null)
			mDirectionIconResizeFree.show((int)hitBounds.left,(int)hitBounds.top,(int)hitBounds.width(),(int)hitBounds.height());
		if(mDirectionIconRotate != null)
			mDirectionIconRotate.show((int)hitBounds.left,(int)hitBounds.top,(int)hitBounds.width(),(int)hitBounds.height());
		if(mDirectionIconDelete != null)
			mDirectionIconDelete.show((int)hitBounds.left,(int)hitBounds.top,(int)hitBounds.width(),(int)hitBounds.height());
		if(mDirectionIconUp != null)
			mDirectionIconUp.show((int)hitBounds.left,(int)hitBounds.top,(int)hitBounds.width(),(int)hitBounds.height());
		if(mDirectionIconDown != null)
			mDirectionIconDown.show((int)hitBounds.left,(int)hitBounds.top,(int)hitBounds.width(),(int)hitBounds.height());
		
		if(mCurrentPart.mKind == PartObject.KIND_PATH || mCurrentPart.mKind == PartObject.KIND_TEXT){
			if(mDirectionIconChange != null)
				mDirectionIconChange.show((int)hitBounds.left,(int)hitBounds.top,(int)hitBounds.width(),(int)hitBounds.height());
		}
		
	}
	
	//編集操作アイコンを非表示にする
	private void dismissDirectionIcon(){
		if(mDirectionIconMove != null)
			mDirectionIconMove.setActive(false);
		if(mDirectionIconResize != null)
			mDirectionIconResize.setActive(false);
		if(mDirectionIconRotate != null)
			mDirectionIconRotate.setActive(false);
		if(mDirectionIconResizeFree != null)
			mDirectionIconResizeFree.setActive(false);
		if(mDirectionIconDelete != null)
			mDirectionIconDelete.setActive(false);
		if(mDirectionIconUp != null)
			mDirectionIconUp.setActive(false);
		if(mDirectionIconDown != null)
			mDirectionIconDown.setActive(false);
		if(mDirectionIconChange != null)
			mDirectionIconChange.setActive(false);
	}

		//スタンプモードをスタートする
	public void startStampMode(Bitmap stampBitmap){
		mMode = MODE_STAMP;

		mStampBitmap = stampBitmap;
		mStampPlaces = new ArrayList<HashMap<String,Float>>();
		
		Toast.makeText(getContext(), R.string.please_touch_the_screen, Toast.LENGTH_SHORT).show();

	}
	
	//位置指定モードをスタートする
	public void startClickPlaceMode(IClickPlaceListener callback){
		mMode = MODE_CLICK_PLACE;
		
		mClickPlaceListener = callback;
		
		Toast.makeText(getContext(), R.string.please_touch_the_screen, Toast.LENGTH_SHORT).show();
	}
	
	//バックグラウンドを設定
	public void setBackground(Bitmap bitmap){
		BitmapShader shader = new BitmapShader(bitmap, TileMode.REPEAT, TileMode.REPEAT);
		mBackgroundPaint = new Paint();
		mBackgroundPaint.setShader(shader);
	}
	
	
	@Override
	protected void onDraw(Canvas canvas) {
		Log.d("kazino","DrawBoardView:onDraw");
		

		//背景塗りつぶし
		if(mBackgroundPaint != null)
			canvas.drawPaint(mBackgroundPaint);
		else
			canvas.drawColor(Color.GRAY);
		
		if(mPartsGroup != null)
			mPartsGroup.drawToView(canvas, mLogicalViewLeft, mLogicalViewTop, mScale);
		
		if(mMode == MODE_EDIT){

		}else if(mMode == MODE_DRAW_PATH){
			//描画モードの時はパスも描画する
			if(mDrawOutPaint != null && mDrawOutPaint.getStrokeWidth() > 0) canvas.drawPath(mDrawPath, mDrawOutPaint);  //一番外側の線
			if(mDrawMidPaint != null && mDrawMidPaint.getStrokeWidth() > 0) canvas.drawPath(mDrawPath, mDrawMidPaint);  //真ん中の線
			canvas.drawPath(mDrawPath, mDrawPaint);  //一番内側の線
			
			//直線モードの時はタップした場所の点を表示
			if(mArrayListPoints != null){
				if(mPaintPath == null){
					mPaintPath = new Paint();
					mPaintPath.setColor(Color.RED);
					mPaintPath.setStrokeWidth(10);
				}
				for (int i = 0; i < mArrayListPoints.size(); i++) {
					canvas.drawPath(mArrayListPoints.get(i), mPaintPath);
				}
			}
			
		}else if(mMode == MODE_STAMP){
			//スタンプモードの時はスタンプも描画する
			Matrix matrix = new Matrix();
			for (int i = 0; i < mStampPlaces.size(); i++) {
				matrix.reset();
				matrix.postScale(mScale / (float)mPartsGroup.getDisplaySampleSize(), mScale / (float)mPartsGroup.getDisplaySampleSize());
				matrix.postTranslate(mStampPlaces.get(i).get("x"),mStampPlaces.get(i).get("y"));
				canvas.drawBitmap(mStampBitmap, matrix, null);
			}
			
		}

	}

	@Override
	public void onResize(float scale) {
		onResizeFree(scale, scale);
	}

	@Override
	public void onResizeFree(float scaleX, float scaleY) {
		Log.d("kazino","DrawBoardView:onResizeFree::" + scaleX + ":" + scaleY);
		
		if(mCurrentPart == null) return;

		//UNDO履歴へ書き込み
		mUndoManager.addModify(mCurrentPart);
		
		mCurrentPart.scale(scaleX,scaleY);

        mPartsGroup.flashToBitmap().continueWith(new Continuation() {
            @Override
            public Object then(Task task) throws Exception {
                DrawBoardView.this.invalidate();
                return null;
            }
        },Task.UI_THREAD_EXECUTOR);

	}
	
	@Override
	public void onRotate(float degrees) {
		
		if(mCurrentPart == null) return;
		
		//UNDO履歴へ書き込み
		mUndoManager.addModify(mCurrentPart);

		mCurrentPart.rotate(degrees);

        mPartsGroup.flashToBitmap().continueWith(new Continuation() {
            @Override
            public Object then(Task task) throws Exception {
                DrawBoardView.this.invalidate();
                return null;
            }
        },Task.UI_THREAD_EXECUTOR);
	}

	@Override
	public void onMove(int moveX, int moveY) {
		
		if(mCurrentPart == null) return;
		
		//UNDO履歴へ書き込み
		mUndoManager.addModify(mCurrentPart);
		
		//移動距離を表示倍率で補正
		int x,y;
		x = (int) (moveX * (1.0f / mScale));
		y = (int) (moveY * (1.0f / mScale));
		
		mCurrentPart.move(x,y);

        mPartsGroup.flashToBitmap().continueWith(new Continuation() {
            @Override
            public Object then(Task task) throws Exception {
                DrawBoardView.this.invalidate();
                return null;
            }
        },Task.UI_THREAD_EXECUTOR);

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		Log.d("kazino", "onTouchEvent");
		
		//GestureDetectorに離されたイベントがないので独自判断
		if(event.getAction() == MotionEvent.ACTION_UP){
			if(onTapUp(event) == true) return true;
		}

		return mGestureDetector.onTouchEvent(event);

	}

	@Override
	public boolean onDown(MotionEvent e) {
		Log.d("kazino", "onDown");
		
		if(mMode == MODE_DRAW_PATH){
			
			//タップされた物理座標を現在表示されている論理ビューの座標に変換
			int[] logicalPoints = getLogicalPoint(e.getX(), e.getY());
			int logicalX = logicalPoints[0];
			int logicalY = logicalPoints[1];
			
			//もし描画エリアを超えていたら無効
			if(logicalX < 0 || logicalX > mPartsGroup.getBitmap().getWidth() || 
					logicalY < 0 || logicalY > mPartsGroup.getBitmap().getHeight()){
				mDrawPointX = 0;
				mDrawPointY = 0;
				return true;
			}
			
			
			//初めての時か、直線モードがOFFの時は移動
			if(mDrawPath.getJsonPath().length() == 0 || mDrawLineEnable == false){
				mDrawPath.moveTo(e.getX(), e.getY());
			}
			mDrawPointX = e.getX();
			mDrawPointY = e.getY();
		}
		
		return true;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		Log.d("kazino", "onFling");
		return false;
	}

	@Override
	public void onLongPress(MotionEvent e) {
		Log.d("kazino", "onLongPress");
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		Log.d("kazino", "onScroll");

		if(mMode == MODE_SCROLL || mMode == MODE_EDIT || mMode == MODE_CLICK_PLACE){
			//スクロールモード、編集モード、位置指定モードでは論理ビューを移動
			mLogicalViewLeft += distanceX;
			mLogicalViewTop += distanceY;
			invalidate();
			
			//エディットモードでは指示アイコンも一緒に移動
			int iconDistanceX = (int)(distanceX * mScale) * -1;
			int iconDistanceY = (int)(distanceY * mScale) * -1;
			if(mDirectionIconMove != null && mDirectionIconMove.getActive() == true)
				mDirectionIconMove.postMove(iconDistanceX, iconDistanceY);
			if(mDirectionIconResize != null && mDirectionIconResize.getActive() == true)
				mDirectionIconResize.postMove(iconDistanceX, iconDistanceY);
			if(mDirectionIconRotate != null && mDirectionIconRotate.getActive() == true)
				mDirectionIconRotate.postMove(iconDistanceX, iconDistanceY);
			if(mDirectionIconResizeFree != null && mDirectionIconResizeFree.getActive() == true)
				mDirectionIconResizeFree.postMove(iconDistanceX, iconDistanceY);
			if(mDirectionIconDelete != null && mDirectionIconDelete.getActive() == true)
				mDirectionIconDelete.postMove(iconDistanceX, iconDistanceY);
			if(mDirectionIconUp != null && mDirectionIconUp.getActive() == true)
				mDirectionIconUp.postMove(iconDistanceX, iconDistanceY);
			if(mDirectionIconDown != null && mDirectionIconDown.getActive() == true)
				mDirectionIconDown.postMove(iconDistanceX, iconDistanceY);
			if(mDirectionIconChange != null && mDirectionIconChange.getActive() == true)
				mDirectionIconChange.postMove(iconDistanceX, iconDistanceY);
			
			return true;
		}else if(mMode == MODE_DRAW_PATH){
			//手書きモードの時がパスを描画
			drawPath(e2,false);
        	return true;
		}
		
		return false;
	}

	@Override
	public void onShowPress(MotionEvent e) {
		Log.d("kazino", "onShowPress");
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		Log.d("kazino", "onSingleTapUp");
		float x = e.getX();
		float y = e.getY();
		int logicalX, logicalY;
		
		if(mPartsGroup == null) return false;

		//タップされた物理座標を現在表示されている論理ビューの座標に変換
		int[] logicalPoints = getLogicalPoint(x, y);
		logicalX = logicalPoints[0];
		logicalY = logicalPoints[1];

		if(mMode == MODE_SCROLL || mMode == MODE_EDIT){
			
			//タップされた場所のアイテムを取得
			PartObject clickPart = mPartsGroup.getPart(logicalX, logicalY);
			
			if(clickPart != null){
				//
				//タップされた場所にアイテムがあれば編集モード
				//
				startEditMode(clickPart);
				invalidate();
			}
			else{
				//
				//タップされた場所にパーツがなければスクロールモード
				//
				startScrollMode();
				invalidate();
				
			}

			return true;
		}
		else if(mMode == MODE_DRAW_PATH){
			
			//もし描画エリアを超えていたら無効
			if(logicalX < 0 || logicalX > mPartsGroup.getBitmap().getWidth() || 
					logicalY < 0 || logicalY > mPartsGroup.getBitmap().getHeight()){
				return true;
			}

			
			//描画モードではパスの記録
			if(mDrawLineEnable == true){
				//手書きモードの時はラインを引く
				if(mDrawPath.getJsonPath().length() > 0){
					mDrawPath.lineTo(x, y);
					invalidate();
				}else{
					//初めての時は移動
					mDrawPath.moveTo(x, y);
				}
				
				//タップされた場所を記録
				Path newPoint = new Path();
				newPoint.addCircle(x, y, 10, Direction.CW);
				mArrayListPoints.add(newPoint);
			}
			
			return true;
		}
		else if(mMode == MODE_STAMP){
			//新しいスタンプを追加
			HashMap<String, Float> stampPlace = new HashMap<String, Float>();
			stampPlace.put("x", x - (mStampBitmap.getWidth() / (float)mPartsGroup.getDisplaySampleSize() * mScale / 2));
			stampPlace.put("y", y - (mStampBitmap.getHeight() / (float)mPartsGroup.getDisplaySampleSize() * mScale / 2));
			mStampPlaces.add(stampPlace);
			this.invalidate();
			
			return true;
		}
		else if(mMode == MODE_CLICK_PLACE){
			mClickPlaceListener.onSelected(x, y, logicalX, logicalY);
			mClickPlaceListener = null;
						
			return true;
		}

		return false;

	}
	
	//ドラッグ後に画面から指が離れた時のイベント
	private boolean onTapUp(MotionEvent event){
		Log.d("kazino", "onTapUp");

		switch(mMode){
		case MODE_DRAW_PATH:
			//もし手書きモードでなにか書かれていたら最後の指の位置まで線を引く
			//(手ぶれ補正モードで最後の手の位置まで正しく線が引かれるように)
			if(mDrawPath.getJsonPath().length() > 0 ){
				//手書きモードの時がパスを描画
				drawPath(event,true);
			}
			break;
		default:
		}
		
		return false;
	}

	//手書きモードの時にパスを描画する
	private void drawPath(MotionEvent event, boolean ignoreSensitivity){
		//タップされた物理座標を現在表示されている論理ビューの座標に変換
		int[] logicalPoints = getLogicalPoint(event.getX(), event.getY());
		int logicalX = logicalPoints[0];
		int logicalY = logicalPoints[1];
		
		//もし描画エリアを超えていたら無効
		if(logicalX < 0 || logicalX > mPartsGroup.getBitmap().getWidth() || 
				logicalY < 0 || logicalY > mPartsGroup.getBitmap().getHeight()) return;
		
		//描画モードではパスを記録する
    	if(mDrawPointX == 0f && mDrawPointY == 0f){
    		//前回の位置が設定されてなければ移動
			mDrawPointX = event.getX();
			mDrawPointY = event.getY();
			mDrawPath.moveTo(mDrawPointX, mDrawPointY);
		}else{
            float dx = Math.abs(event.getX() - mDrawPointX);
            float dy = Math.abs(event.getY() - mDrawPointY);
            if (dx >= mPathSensitivity || dy >= mPathSensitivity || ignoreSensitivity == true) {  //手ブレ補正値以上で描画対象
            	if(mDrawPointX != 0f && mDrawPointY != 0f)
            		mDrawPath.quadTo(mDrawPointX, mDrawPointY, (event.getX() + mDrawPointX)/2, (event.getY() + mDrawPointY)/2);
                mDrawPointX = event.getX();
                mDrawPointY = event.getY();
            }

            invalidate();
		}
		
	}
	
	//選択したパーツを削除
	public void removePart(){
		if(mCurrentPart != null){
			mPartsGroup.removePart(mCurrentPart,true);

			startScrollMode();

            EventBus.getDefault().postSticky(new ProgressViewEvent(ProgressViewEvent.Status.START));

            mPartsGroup.flashToBitmap().continueWith(new Continuation() {
                @Override
                public Object then(Task task) throws Exception {
                    EventBus.getDefault().postSticky(new ProgressViewEvent(ProgressViewEvent.Status.END));
                    invalidate();
                    return null;
                }
            },Task.UI_THREAD_EXECUTOR);

		}
	}
	
	//選択したパーツを最前面へ移動
	public void upPart(){
		if(mCurrentPart != null){
			int partsCount = mPartsGroup.getPartsCount();
			mPartsGroup.changePosition(mCurrentPart, partsCount - 1, true);
			startScrollMode();

            EventBus.getDefault().postSticky(new ProgressViewEvent(ProgressViewEvent.Status.START));

            mPartsGroup.flashToBitmap().continueWith(new Continuation() {
                @Override
                public Object then(Task task) throws Exception {
                    EventBus.getDefault().postSticky(new ProgressViewEvent(ProgressViewEvent.Status.END));
                    invalidate();
                    return null;
                }
            },Task.UI_THREAD_EXECUTOR);

		}
	}

	//選択したパーツを最背面へ移動
	public void downPart(){
		if(mCurrentPart != null){
			mPartsGroup.changePosition(mCurrentPart, 0, true);
			startScrollMode();

            EventBus.getDefault().postSticky(new ProgressViewEvent(ProgressViewEvent.Status.START));

            mPartsGroup.flashToBitmap().continueWith(new Continuation() {
                @Override
                public Object then(Task task) throws Exception {
                    EventBus.getDefault().postSticky(new ProgressViewEvent(ProgressViewEvent.Status.END));
                    invalidate();
                    return null;
                }
            }, Task.UI_THREAD_EXECUTOR);

		}
	}
	
	//選択したパーツを変更対象に設定
	private void changeContentOfTarget(){
		if(mCurrentPart != null){
			//対象は文字とパスオブジェクトだけ
			if(mCurrentPart.mKind != PartObject.KIND_PATH && mCurrentPart.mKind != PartObject.KIND_TEXT) return;
			
			//リスナーに変更対象オブジェクトを知らせる
            EventBus.getDefault().post(new ModifyContentOfTargetEvent(mCurrentPart));
		}
	}

	@Override
	public void onSelected(int mode) {
		switch(mode){
		case DirectionIcon.MODE_MOVE:
			if(mDirectionIconResize != null)
				mDirectionIconResize.setActive(false);
			if(mDirectionIconResizeFree != null)
				mDirectionIconResizeFree.setActive(false);
			if(mDirectionIconRotate != null)
				mDirectionIconRotate.setActive(false);
			if(mDirectionIconDelete != null)
				mDirectionIconDelete.setActive(false);
			if(mDirectionIconUp != null)
				mDirectionIconUp.setActive(false);
			if(mDirectionIconDown != null)
				mDirectionIconDown.setActive(false);
			if(mDirectionIconChange != null)
				mDirectionIconChange.setActive(false);
			break;
		case DirectionIcon.MODE_RESIZE:
			if(mDirectionIconMove != null)
				mDirectionIconMove.setActive(false);
			if(mDirectionIconResizeFree != null)
				mDirectionIconResizeFree.setActive(false);
			if(mDirectionIconRotate != null)
				mDirectionIconRotate.setActive(false);
			if(mDirectionIconDelete != null)
				mDirectionIconDelete.setActive(false);
			if(mDirectionIconUp != null)
				mDirectionIconUp.setActive(false);
			if(mDirectionIconDown != null)
				mDirectionIconDown.setActive(false);
			if(mDirectionIconChange != null)
				mDirectionIconChange.setActive(false);
			break;
		case DirectionIcon.MODE_RESIZE_FREE:
			if(mDirectionIconMove != null)
				mDirectionIconMove.setActive(false);
			if(mDirectionIconResize != null)
				mDirectionIconResize.setActive(false);
			if(mDirectionIconRotate != null)
				mDirectionIconRotate.setActive(false);
			if(mDirectionIconDelete != null)
				mDirectionIconDelete.setActive(false);
			if(mDirectionIconUp != null)
				mDirectionIconUp.setActive(false);
			if(mDirectionIconDown != null)
				mDirectionIconDown.setActive(false);
			if(mDirectionIconChange != null)
				mDirectionIconChange.setActive(false);
			break;
		case DirectionIcon.MODE_ROTATE:
			if(mDirectionIconMove != null)
				mDirectionIconMove.setActive(false);
			if(mDirectionIconResize != null)
				mDirectionIconResize.setActive(false);
			if(mDirectionIconResizeFree != null)
				mDirectionIconResizeFree.setActive(false);
			if(mDirectionIconDelete != null)
				mDirectionIconDelete.setActive(false);
			if(mDirectionIconUp != null)
				mDirectionIconUp.setActive(false);
			if(mDirectionIconDown != null)
				mDirectionIconDown.setActive(false);
			if(mDirectionIconChange != null)
				mDirectionIconChange.setActive(false);
			break;
		case DirectionIcon.MODE_DELETE:
			removePart();
			break;
		case DirectionIcon.MODE_UP:
			upPart();
			break;
		case DirectionIcon.MODE_DOWN:
			downPart();
			break;
		case DirectionIcon.MODE_CHANGE:
			changeContentOfTarget();
			break;
			
		}
		
	}
	
	@Override
	public void onFinishMove(int mode, boolean isMoved) {
		if(mCurrentPart != null)
			startEditMode(mCurrentPart);
	}

	//物理画面にぴったり入るように縮尺する
	public void autoScale(){
	    //キャンバス全体が見えるように縮尺
	    int baseWidth,baseHeight,bitmapWidth,bitmapHeight;
	    float rateWidth,rateHeight;
	    baseWidth = getWidth();
	    baseHeight = getHeight();
	    bitmapWidth = mPartsGroup.getBitmap().getWidth();
	    bitmapHeight = mPartsGroup.getBitmap().getHeight();
    	rateWidth =  (float)baseWidth / (float)bitmapWidth;
    	rateHeight =  (float)baseHeight / (float)bitmapHeight;
    	if(rateWidth < rateHeight){
	    	setScale(rateWidth);
    	}else{
	    	setScale(rateHeight);
    	}
	}
	
	//物理画面の縦にぴったり合うように縮尺
	public void autoScaleHeight(){
	    //キャンバス全体が見えるように縮尺
	    int baseHeight,bitmapHeight;
	    float rateHeight;
	    baseHeight = getHeight();
	    bitmapHeight = mPartsGroup.getBitmap().getHeight();
    	rateHeight =  (float)baseHeight / (float)bitmapHeight;

        setScale(rateHeight);
    	
    	moveToCenter();
	}
	
	//物理画面の横にぴったり合うように縮尺
	public void autoScaleWidth(){
	    //キャンバス全体が見えるように縮尺
	    int baseWidth,bitmapWidth;
	    float rateWidth;
	    baseWidth = getWidth();
	    bitmapWidth = mPartsGroup.getBitmap().getWidth();
    	rateWidth =  (float)baseWidth / (float)bitmapWidth;

        setScale(rateWidth);
    	
    	moveToCenter();
	}

	//物理画面の中心に表示されるように調整する
	public void moveToCenter(){
	    //キャンパスが中心にくるように移動
	    // ((物理画面 - 論理画面) / 2) * -1
	    float centerX, centerY;
	    float baseWidth,baseHeight,logicalWidth,logicalHeight;
	    baseWidth = getWidth();
	    baseHeight = getHeight();
	    logicalWidth = (float)mPartsGroup.getBitmap().getWidth() * getScale();
	    logicalHeight = (float)mPartsGroup.getBitmap().getHeight() * getScale();
	    centerX = ((baseWidth - logicalWidth) / 2 / getScale()) * -1;
	    centerY = ((baseHeight - logicalHeight) / 2 / getScale()) * -1;
	    setLeftTop((int)centerX, (int)centerY);
	}
	
	public void setPathSensitivity(int pixcel){
		mPathSensitivity = pixcel;
	}

	//すべての情報をクリアする
	public void clear(){
		mPartsGroup = null;

		if(mUndoManager != null){
			mUndoManager.clear();
			mUndoManager = null;
		}
	}
	
	//Undo
	public void undo(){
		switch(mMode){
		case MODE_DRAW_PATH:
			//手書きのundo
			mDrawPath = mDrawPath.undo();
			
			//直線モードの時はタップポイントも１つ戻す
			if(mDrawLineEnable == true && mArrayListPoints != null && mArrayListPoints.size() > 0)
				mArrayListPoints.remove(mArrayListPoints.size() - 1);
			
			invalidate();
			break;
		case MODE_STAMP:
			//スタンプモードのundo
			if(mStampPlaces.size() > 0)
				mStampPlaces.remove(mStampPlaces.size() - 1);
			invalidate();
			break;
		case MODE_EDIT:
			break; //選択中はUndoしない
		case MODE_SCROLL:
			//undoマネージャでundo
			mUndoManager.undo();

            EventBus.getDefault().postSticky(new ProgressViewEvent(ProgressViewEvent.Status.START));

            mPartsGroup.flashToBitmap().continueWith(new Continuation() {
                @Override
                public Object then(Task task) throws Exception {
                    EventBus.getDefault().postSticky(new ProgressViewEvent(ProgressViewEvent.Status.END));
                    invalidate();
                    return null;
                }
            },Task.UI_THREAD_EXECUTOR);
			break;
		}
	}


}

