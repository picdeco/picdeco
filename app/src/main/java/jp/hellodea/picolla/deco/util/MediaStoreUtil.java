package jp.hellodea.picolla.deco.util;

import android.content.Context;
import android.content.Intent;

/**
 * Created by kazuhiro on 2015/04/09.
 */

public final class MediaStoreUtil {
    public static Intent getPickImageIntent(final Context context) {
        final Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        return Intent.createChooser(intent, "Select picture");
    }
}