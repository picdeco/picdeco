package jp.hellodea.picolla.deco.activity;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import bolts.Continuation;
import bolts.Task;
import de.greenrobot.event.EventBus;
import hugo.weaving.DebugLog;
import jp.hellodea.picolla.BuildConfig;
import jp.hellodea.picolla.R;
import jp.hellodea.picolla.common.eventbus.ErrorEvent;
import jp.hellodea.picolla.common.exception.LogicException;
import jp.hellodea.picolla.common.fragment.AlertDialogFragment;
import jp.hellodea.picolla.common.fragment.AlertDialogFragment_;
import jp.hellodea.picolla.common.objectgraph.ObjectGraphHolder;
import jp.hellodea.picolla.deco.Const;
import jp.hellodea.picolla.deco.entity.CuttingShape;
import jp.hellodea.picolla.deco.graphics.SavableMatrix;
import jp.hellodea.picolla.deco.graphics.SavablePath;
import jp.hellodea.picolla.deco.graphics.parts.ImageObject;
import jp.hellodea.picolla.deco.graphics.parts.PartUtil;
import jp.hellodea.picolla.deco.graphics.parts.PartsGroup;
import jp.hellodea.picolla.deco.graphics.parts.PathObject;
import jp.hellodea.picolla.deco.logic.AssetLogic;
import jp.hellodea.picolla.deco.util.CropImageAsyncTask;
import jp.hellodea.picolla.deco.util.ImageUtil;
import jp.hellodea.picolla.deco.util.StorageFileUtil;
import jp.hellodea.picolla.deco.util.UriUtil;
import jp.hellodea.picolla.deco.view.DirectionIcon;
import jp.hellodea.picolla.deco.view.DrawBoardView;
import jp.hellodea.picolla.deco.view.IClickPlaceListener;
import jp.hellodea.picolla.deco.view.SamplePathView;
import timber.log.Timber;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;

import javax.inject.Inject;

//写真選択時のプレビュー画面
public class CutPictureActivity extends ActionBarActivity {

    @Inject
    AssetLogic assetLogic;

    //各種操作モード
    public static final int OPERATION_MODE_SCROLL = 0; //スクロールモード
    public static final int OPERATION_MODE_DRAW = 1; //手書きモード
    public static final int OPERATION_MODE_STAMP = 2; //スタンプモード
    public static final int OPERATION_MODE_PLACE = 3; //位置選択モード

	private int mOperationMode; //操作モード

	//現在の編集モード
	public static final int MODE_MOVE = 1;
	public static final int MODE_FREE_HAND = 2;
	public static final int MODE_LINE = 3;
	public static final int MODE_DRAW = 4;
	public static final int MODE_SHAPE = 5;
	private int mCurrentMode = MODE_MOVE;
	private int mLastCutMode; //最後に選択した切り取りモード

	//画面のコンポーネント
	private HorizontalScrollView mHorizontalScrollView;
	private Button mButtonSelectByLine;
	private Button mButtonSelectByFreeHand;
	private Button mButtonSelectByDraw;
	private Button mButtonSelectByShape;
	private ImageButton mButtonZoomUp;
	private ImageButton mButtonZoomDown;
	private FrameLayout mBaseLayout;
	private Button mButtonFinish;
	private ProgressBar mProgressBar;

	//オプションダイアログ用ウィジェット
	private Button mButtonGoCut;
	private Button mButtonCancelCut;
	private SeekBar mSeekBarSensitivity;
	private CheckBox mCheckBoxRoundAngle;
	private SeekBar mSeekBarPenWidth;
	private TextView mTextViewHelp;
	private RelativeLayout mRelativeLayoutSensitivity;
	private RelativeLayout mRelativeLayoutRoundAngle;
	private RelativeLayout mRelativeLayoutPenWidth;
	
	//選択方法オプション
	private int mSensitivity;
	private boolean mRoundAngle;
	private float mWidth;
	
	//お待ちくださいダイアログ
	private ProgressDialog mProgressDialog;
	
	//オプション設定ダイアログ
	private Dialog mDialogOption;
	
	//オリジナルの画像
	private Uri mOriginalImageUri;
	private int mOriginalImageWidth;
	private int mOriginalImageHeight;
	
	//縮小した編集対象の画像
	private Uri mWorkImageUri;
	private int mWorkImageWidth;
	private int mWorkImageHeight;

	//ドローエリア
	private DrawBoardView mDrawBoardView;
	private PartsGroup mPartsGroup;
	
	//選択用ペン
	private Paint mPaint;
	
	//塗りつぶしモードでのペンの太さ
	static private float PEN_MIN_WIDTH_RATE = 0.05f;
	static private float PEN_MAX_WIDTH_RATE = 0.3f;
	private float mMaxPenWidth;
	private float mMinPenWidth;
	
	//線を書く時の補正値の範囲
	static private float SENSITIVITY_MIN_RATE = 0.005f;
	static private float SENSITIVITY_MAX_RATE = 0.05f;
	private int mMaxSensitivity;
	private int mMinSensitivity;

	//図形で切り取りのパスデータ
	private List<CuttingShape> cuttingShapeList; //切り取りパスデータ
	
	//切り取り図形表示選択ダイアログ用ウィジェット
	private Dialog mDialogCuttingShape; //切り取り図形表示選択ダイアログ
	private GridView mGridViewCuttingShape;

	//初期化処理完了フラグ
	private boolean mIsFinishInit = false;
	
	//ヘルプのURL
	private String mOptionHelpUrl;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        ObjectGraphHolder.get().inject(this);
		
        setContentView(R.layout.activity_cut_picture);
        
        //UI取得
        mHorizontalScrollView = (HorizontalScrollView)findViewById(R.id.horizontalScrollView);
        mButtonSelectByFreeHand = (Button)findViewById(R.id.buttonFreeHand);
        mButtonSelectByLine = (Button)findViewById(R.id.buttonLine);
        mButtonSelectByDraw = (Button)findViewById(R.id.buttonDraw);
//        mButtonSelectByDraw.setVisibility(View.GONE);
        mButtonSelectByShape = (Button)findViewById(R.id.buttonShape);
        mButtonZoomUp = (ImageButton)findViewById(R.id.buttonZoomUp);
        mButtonZoomDown = (ImageButton)findViewById(R.id.buttonZoomDown);
        mBaseLayout = (FrameLayout)findViewById(R.id.frameLayout);
        mButtonFinish = (Button)findViewById(R.id.buttonFinish);
        mProgressBar = (ProgressBar)findViewById(R.id.progressBar);

        //プログレスダイアログ生成
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getText(R.string.please_wait));
        
        //UIイベント設定
        mButtonSelectByFreeHand.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				actionShowOptionDialog(MODE_FREE_HAND);
			}
		});
        mButtonSelectByDraw.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				actionShowOptionDialog(MODE_DRAW);
			}
		});
        mButtonSelectByLine.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				actionShowOptionDialog(MODE_LINE);
			}
		});
        mButtonSelectByShape.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				actionSelectCuttingShape();
			}
		});
        mButtonZoomUp.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				actionZoom(0.1f);
			}
		});
        mButtonZoomDown.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				actionZoom(-0.1f);
			}
		});
        mButtonFinish.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				actionFinishSelect();
			}
		});

        //パラメータ取得
        Intent intent = getIntent();
        mOriginalImageUri = Uri.parse(intent.getStringExtra("originalImageUriString"));
        mOriginalImageWidth = intent.getIntExtra("originalImageWidth", 0);
        mOriginalImageHeight = intent.getIntExtra("originalImageHeight", 0);
        mWorkImageUri = Uri.parse(intent.getStringExtra("workImageUriString"));
        mWorkImageWidth = intent.getIntExtra("workImageWidth", 0);
        mWorkImageHeight = intent.getIntExtra("workImageHeight", 0);
        
        //ドローエリア設定
	    mDrawBoardView = new DrawBoardView(this);
	    mDrawBoardView.setBackground(BitmapFactory.decodeResource(getResources(),R.drawable.background));
	    mPartsGroup = new PartsGroup(mDrawBoardView,null,mWorkImageWidth, mWorkImageHeight,1, StorageFileUtil.getTempCacheDir(this));
	    mDrawBoardView.setScale(1.0f);
	    mDrawBoardView.setLeftTop(0, 0);
	    mDrawBoardView.setPathSensitivity(20);
	    mBaseLayout.addView(mDrawBoardView);
	    mDrawBoardView.setBackgroundColor(Color.GRAY);
	    DirectionIcon directionIconMove = new DirectionIcon(this, mDrawBoardView,DirectionIcon.MODE_MOVE);
	    DirectionIcon directionIconResize = new DirectionIcon(this, mDrawBoardView,DirectionIcon.MODE_RESIZE);
	    DirectionIcon directionIconRotate = new DirectionIcon(this, mDrawBoardView,DirectionIcon.MODE_ROTATE);
	    DirectionIcon directionIconResizeFree = new DirectionIcon(this, mDrawBoardView,DirectionIcon.MODE_RESIZE_FREE);
	    DirectionIcon directionIconDelete = new DirectionIcon(this, mDrawBoardView,DirectionIcon.MODE_DELETE);
	    mDrawBoardView.setDirectionIcon(directionIconMove,directionIconResize,directionIconRotate,directionIconResizeFree,directionIconDelete,null,null,null);
	    mBaseLayout.addView(directionIconMove);
	    mBaseLayout.addView(directionIconResize);
	    mBaseLayout.addView(directionIconRotate);
	    mBaseLayout.addView(directionIconResizeFree);
	    mBaseLayout.addView(directionIconDelete);
	    
	    //画像表示
    	ImageObject imageObject =
    		new ImageObject(mPartsGroup, 0, 0, new File(mWorkImageUri.getPath()),null,false, null,false,1);
    	imageObject.setLock(true);
    	flash(true);
	        	
        //手ぶれ補正最小値、最大値を取得
		mMaxSensitivity = (int)((float)Math.max(mWorkImageWidth,mWorkImageHeight) * SENSITIVITY_MAX_RATE);
		mMinSensitivity = (int)((float)Math.max(mWorkImageWidth,mWorkImageHeight) * SENSITIVITY_MIN_RATE);
		mSensitivity = (int) ((float)(mMaxSensitivity - mMinSensitivity) * 0.1f) + mMinSensitivity;
		
		//ペンの最大値、最小値を取得
		mMaxPenWidth = (float)Math.max(mWorkImageWidth,mWorkImageHeight) * PEN_MAX_WIDTH_RATE;
		mMinPenWidth = (float)Math.max(mWorkImageWidth,mWorkImageHeight) * PEN_MIN_WIDTH_RATE;
		mWidth = (mMinPenWidth + mMaxPenWidth) / 2.0f;

	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);

		if(mIsFinishInit == true) return; //既に処理済みなら何もしない

		//
		//viewの幅、高さはこのイベントでしかとれないので画像の縮尺の計算をここでする
		//

	    //キャンバス全体が見えるように縮尺
	    mDrawBoardView.autoScale();

	    //キャンパスが中心にくるように移動
	    mDrawBoardView.moveToCenter();
	    
    	mIsFinishInit = true;
	    	    
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		mPartsGroup.recycle();
		mDrawBoardView.clear();
		
		mPartsGroup = null;
		mDrawBoardView = null;
	}

	//現在の編集モードを切り替える
	private void actionFinishSelect(){
		//今まで書いたパスを記録
		HashMap<String, Object> ret = mDrawBoardView.startScrollMode();
		switch((Integer)ret.get("mode")){
		case DrawBoardView.MODE_DRAW_PATH:
			//直前が手書きモードの場合パスオブジェクト追加
			SavablePath path = (SavablePath)ret.get("path");
			SavableMatrix matrix = (SavableMatrix)ret.get("matrix");
			new PathObject(mPartsGroup, path,matrix, mPaint,1);
			flash(true);
			break;
		default:
		}

		controllOperation(OPERATION_MODE_SCROLL);
	
		mCurrentMode = MODE_MOVE;
	
	}

	//ズームする
	private void actionZoom(float zoom){
		mDrawBoardView.postScale(zoom);
		mDrawBoardView.invalidate();
	}
	
	//やり直し
	private void actionUndo(){
		mDrawBoardView.undo();
	}

	//クリップアート選択後処理
	private void resultForRequestClipart(Intent intent){
		final File file = (File)intent.getSerializableExtra("clipartFile");
		if (file != null) {
			//位置指定モードへ
			mDrawBoardView.startClickPlaceMode(new IClickPlaceListener() {
				@Override
				public void onSelected(float x, float y,int logicalX, int logicalY) {

			    	ImageObject imageObject = 
			    			new ImageObject(mPartsGroup, 0,0,file, StorageFileUtil.getTempCacheDir(CutPictureActivity.this), true, CutPictureActivity.this,true, 1);
			    	Bitmap bitmap = imageObject.getBitmap();
			    	imageObject.move(logicalX - bitmap.getWidth() / 2, logicalY - bitmap.getHeight() / 2);
			    	flash(true);
			    	controllOperation(OPERATION_MODE_SCROLL);
			    	
				}
			});

			controllOperation(OPERATION_MODE_PLACE);

		}
		

		
	}

	//切り取りを実行し次の処理へ遷移
	private void actionExecuteCut(){
		
		//作業中かチェック
		if(mDrawBoardView.getMode() != DrawBoardView.MODE_SCROLL){
			Toast.makeText(this, R.string.please_complete_the_final_task, Toast.LENGTH_SHORT).show();
			return;
		}
		
		//もし範囲が指定されていなければエラー
		if(mPartsGroup.getPartsCount() == 1){
			Toast.makeText(this, R.string.range_of_cut_has_not_been_selected, Toast.LENGTH_SHORT).show();
			return;
		}

		
		//パスのエリアを取得
		PathObject pathObject = (PathObject) mPartsGroup.getPart(1);
		RectF bounds = pathObject.getBounds();
		
		//幅を考慮して広げる
		if(mPaint.getStrokeWidth() > 0.1f){
			bounds.left -= mPaint.getStrokeWidth();
			bounds.top -= mPaint.getStrokeWidth();
			bounds.right += mPaint.getStrokeWidth();
			bounds.bottom += mPaint.getStrokeWidth();
		}
		
		final RectF finalBounds = bounds;

		File originalImageFile = UriUtil.getFileFromUri(this, mOriginalImageUri);
		File workImageFile = UriUtil.getFileFromUri(this, mWorkImageUri);
		
		//切り抜き実行
		CropImageAsyncTask.execute(
                this,
                originalImageFile, mOriginalImageWidth, mOriginalImageHeight,
                workImageFile, mWorkImageWidth, mWorkImageHeight,
                finalBounds,
                0,
                1,
                new CropImageAsyncTask.CropImageListener() {
                    @Override
                    public void onPreExecute() {
                        mProgressDialog.show();

                    }

                    @Override
                    public void onPostExecute(File imageFile, int width, int height) {

                        if (imageFile == null) {
                            Toast.makeText(CutPictureActivity.this, R.string.this_picture_does_not_exist_or_the_picture_cannot_be_handled_by_this_application, Toast.LENGTH_LONG).show();
                            return;
                        }

                        //workから切り取る際に上下左右ではみ出している長さをもとめる
                        float leftOffset = 0;
                        float topOffset = 0;
                        float rightOffset = 0;
                        float bottomOffset = 0;

                        if (finalBounds.left < 0) leftOffset = finalBounds.left;
                        if (finalBounds.top < 0) topOffset = finalBounds.top;
                        if (finalBounds.right > mWorkImageWidth)
                            rightOffset = finalBounds.right - mWorkImageWidth;
                        if (finalBounds.bottom > mWorkImageHeight)
                            bottomOffset = finalBounds.bottom - mWorkImageHeight;


                        executeCut(Uri.fromFile(imageFile), width, height, finalBounds, leftOffset, topOffset, rightOffset, bottomOffset); //パスで切り取り実行
                    }
                }
        );
		
	}
	
	//切り取りを実行し次の処理へ遷移
	private void executeCut(final Uri imageUri, final int width, final int height, final RectF bounds,
			final float leftOffset,final float topOffset,final float rightOffset,final float bottomOffset){
		
		////////////////////////////
		//非同期処理
		//画像にマスクを適用し余白を切り取る
		////////////////////////////
		AsyncTask<Object, Integer, File> task = new AsyncTask<Object, Integer, File>() {

			private int mHeight, mWidth;
			
			@Override
			protected void onPreExecute() {
				mProgressDialog.show();
			}

			@Override
			protected File doInBackground(Object... params) {
				
				System.gc();

				//張付け先キャンバス
				Bitmap imageBitmap = BitmapFactory.decodeFile(imageUri.getPath());
				Bitmap distBitmap = Bitmap.createBitmap(imageBitmap.getWidth(), imageBitmap.getHeight(), Config.ARGB_8888);
				Canvas distCanvas = new Canvas(distBitmap);
				distCanvas.drawBitmap(imageBitmap, 0, 0, null);
				imageBitmap.recycle();
								
				//マスクビットマップを作成
				System.gc();
				Bitmap maskBitmap = Bitmap.createBitmap(distBitmap.getWidth(), 
						distBitmap.getHeight(), Config.ARGB_8888);
				Canvas maskCanvas = new Canvas(maskBitmap);
				mPaint.setColor(Color.RED);
				PathObject pathObject = (PathObject) mPartsGroup.getPart(1);
				SavableMatrix matrix = pathObject.getMatrix();
				RectF pathObjectBounds = pathObject.getBounds();

				///////////////////////////////
				//最終的なパスの位置と縮尺を求める
				///////////////////////////////
				float originalWidth = bounds.width();
				float originalHeight = bounds.height();
				float leftTranslate = 0;
				float topTranslate = 0;
				float rateWidth,rateHeight;
				
				if(leftOffset < 0) originalWidth += leftOffset; //左にはみ出ていたらその分縮める
				if(topOffset < 0) originalHeight += topOffset; //上にはみ出ていたらその分縮める
				if(rightOffset > 0) originalWidth -= rightOffset; //左にはみ出ていたらその分縮める
				if(bottomOffset > 0) originalHeight -= bottomOffset; //上にはみ出ていたらその分縮める
				
				if(bounds.left > 0) leftTranslate = bounds.left * -1; //右に配置されていたらその分左に移動
				if(bounds.top > 0) topTranslate = bounds.top * -1; //下に配置されていたらその分上に移動
				
				rateWidth = distBitmap.getWidth() / originalWidth;
				rateHeight = distBitmap.getHeight() / originalHeight;

				//マスクパスの移動、縮尺				
				matrix.postTranslate(leftTranslate, topTranslate); 
				matrix.postScale(rateWidth,rateHeight);
				
				RectF newBounds = new RectF(pathObjectBounds); //テスト用（中身確認用）
				matrix.mapRect(newBounds); //テスト用（中身確認用）
				
				//パスの口を閉じる
				pathObject.getPath().close();

				//パスを不透明にしてビットマップに書き出す
				SavablePath tempPath = new SavablePath(pathObject.getPath().copyJsonPath());
				SavableMatrix tempMatrix = new SavableMatrix(matrix);
				PathObject tempPathObject = new PathObject(mPartsGroup, tempPath, tempMatrix, mPaint, 1); //一時的に追加
				tempPathObject.flashToBitmap(maskCanvas, null);

				//一時的に追加したパスを削除
				mPartsGroup.removePart(mPartsGroup.getPart(2), false);

				//マスクを貼付け
				Paint paint = new Paint();
				paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
				distCanvas.drawBitmap(maskBitmap, 0, 0, paint);
				maskBitmap.recycle();
				
				//余白を取り除く
				System.gc();
				HashMap<String, Object> ret = ImageUtil.getRealSizeBitmap(distBitmap);
				distBitmap = (Bitmap) ret.get("bitmap");
				mHeight = distBitmap.getHeight();
				mWidth = distBitmap.getWidth();
				
				//切り抜いた余白分、マスクを移動する
				pathObject = (PathObject) mPartsGroup.getPart(1);
				pathObject.move((Integer)ret.get("left") * -1, (Integer)ret.get("top") * -1);
				
				//切り取り完了画像をファイルへ保存する

		    	File cacheFile = StorageFileUtil.createNewRandomFile(StorageFileUtil.getTempCacheDir(CutPictureActivity.this),"");
				try {
					ImageUtil.storeBitmapToCache(distBitmap, cacheFile);
				} catch (Exception e) {
					Timber.e(e,"");
					throw new RuntimeException(e);
				}
		        
				distBitmap.recycle();

		        return cacheFile;
			}

			@Override
			protected void onPostExecute(File cacheFile) {
				mProgressDialog.dismiss();
				
				//枠追加のためマスク情報をシリアライズ化する
				String path = mPartsGroup.getPart(1).getPath().getJsonPath().toString();
				String matrix;
				try {
					matrix = mPartsGroup.getPart(1).getMatrix().getJson().toString();
				} catch (JSONException e) {
					Timber.e(e,"");
					throw new RuntimeException(e);
				}
				
				//切り取り完了後、フレーム編集画面へ遷移
				Intent send_intent = new Intent(CutPictureActivity.this,AddPictureFrameActivity.class);
				send_intent.setData(Uri.fromFile(cacheFile));
				send_intent.putExtra("executeMode", Const.REQUEST_MODIFY_IMAGE);
				send_intent.putExtra("cutMode", mLastCutMode);
				send_intent.putExtra("height", mHeight);
				send_intent.putExtra("width", mWidth);
				send_intent.putExtra("path", path);
				send_intent.putExtra("matrix", matrix);
				send_intent.putExtra("penMinWidth",mMinPenWidth );
				send_intent.putExtra("penMaxWidth",mMaxPenWidth );
				startActivity(send_intent);

				//メモリ解放
				mPartsGroup.recycle();
				mDrawBoardView.clear();

				finish();
				

			}

		};
		
		//タスクの実行
		task.execute();
	}

	//塗りつぶしで切り取りを実行し次の処理へ遷移
	private void actionExecuteCutByDraw(){
		
		//作業中かチェック
		if(mDrawBoardView.getMode() != DrawBoardView.MODE_SCROLL){
			Toast.makeText(this, R.string.please_complete_the_final_task, Toast.LENGTH_SHORT).show();
			return;
		}
		
		//もし範囲が指定されていなければエラー
		if(mPartsGroup.getPartsCount() == 1){
			Toast.makeText(this, R.string.range_of_cut_has_not_been_selected, Toast.LENGTH_SHORT).show();
			return;
		}

		
		//パスのエリアを取得
		PathObject pathObject = (PathObject) mPartsGroup.getPart(1);
		RectF bounds = pathObject.getBounds();
		
		//幅を考慮して広げる
		if(mPaint.getStrokeWidth() > 0.1f){
			bounds.left -= mPaint.getStrokeWidth();
			bounds.top -= mPaint.getStrokeWidth();
			bounds.right += mPaint.getStrokeWidth();
			bounds.bottom += mPaint.getStrokeWidth();
		}
		
		final RectF finalBounds = bounds;

		File originalImageFile = UriUtil.getFileFromUri(this, mOriginalImageUri);
		File workImageFile = UriUtil.getFileFromUri(this, mWorkImageUri);
		
		//切り抜き実行
		CropImageAsyncTask.execute(
				this, 
				originalImageFile,mOriginalImageWidth,mOriginalImageHeight, 
				workImageFile,mWorkImageWidth,mWorkImageHeight,
				finalBounds,
				0,
				1,
				new CropImageAsyncTask.CropImageListener() {
					@Override
					public void onPreExecute() {
						mProgressDialog.show();

					}
					
					@Override
					public void onPostExecute(File imageFile, int width, int height) {
						
						if(imageFile == null){
							Toast.makeText(CutPictureActivity.this, R.string.this_picture_does_not_exist_or_the_picture_cannot_be_handled_by_this_application, Toast.LENGTH_LONG).show();
							return;
						}
						
						//workから切り取る際に上下左右ではみ出している長さをもとめる
						float leftOffset = 0;
						float topOffset = 0;
						float rightOffset = 0;
						float bottomOffset = 0;
						
						if(finalBounds.left < 0) leftOffset = finalBounds.left;
						if(finalBounds.top < 0) topOffset = finalBounds.top;
						if(finalBounds.right > mWorkImageWidth) rightOffset = finalBounds.right - mWorkImageWidth;
						if(finalBounds.bottom > mWorkImageHeight) bottomOffset = finalBounds.bottom - mWorkImageHeight;
						
						
						executeCutByDraw(Uri.fromFile(imageFile), width, height, finalBounds,leftOffset,topOffset,rightOffset,bottomOffset); //パスで切り取り実行
					}
				}
		);
		

	}
	
	//塗りつぶしで切り取りを実行し次の処理へ遷移
	private void executeCutByDraw(final Uri imageUri, final int width, final int height, final RectF bounds,
			final float leftOffset,final float topOffset,final float rightOffset,final float bottomOffset){
		
		
		////////////////////////////
		//非同期処理
		//画像にマスクを適用し余白を切り取る
		////////////////////////////
		AsyncTask<Object, Integer, File> task = new AsyncTask<Object, Integer, File>() {

			private int mHeight, mWidth; //最終のビットマップのサイズ
			private float mStrokeWidth; //最終のペンの幅
			
			@Override
			protected void onPreExecute() {
				mProgressDialog.show();
			}

			@Override
			protected File doInBackground(Object... params) {
				
				System.gc();

				//張付け先キャンバス
				Bitmap imageBitmap = BitmapFactory.decodeFile(imageUri.getPath());
				Bitmap distBitmap = Bitmap.createBitmap(imageBitmap.getWidth(), imageBitmap.getHeight(), Config.ARGB_8888);
				Canvas distCanvas = new Canvas(distBitmap);
				distCanvas.drawBitmap(imageBitmap, 0, 0, null);
				imageBitmap.recycle();

                if(BuildConfig.DEBUG){
                    try {
                        ImageUtil.storeBitmapToCache(distBitmap, new File(getExternalCacheDir(), "002.png"));
                    } catch (Exception e) {
						Timber.e(e, "");
                    }
                }

				
				//マスクビットマップを作成
				System.gc();
				Bitmap maskBitmap = Bitmap.createBitmap(distBitmap.getWidth(), 
						distBitmap.getHeight(), Config.ARGB_8888);
				Canvas maskCanvas = new Canvas(maskBitmap);
				mPaint.setColor(Color.RED);
				PathObject pathObject = (PathObject) mPartsGroup.getPart(1);
				SavableMatrix matrix = pathObject.getMatrix();

				///////////////////////////////
				//最終的なパスの位置と縮尺を求める
				///////////////////////////////
				float originalWidth = bounds.width();
				float originalHeight = bounds.height();
				float leftTranslate = 0;
				float topTranslate = 0;
				float rateWidth,rateHeight;
				
				if(leftOffset < 0) originalWidth += leftOffset; //左にはみ出ていたらその分縮める
				if(topOffset < 0) originalHeight += topOffset; //上にはみ出ていたらその分縮める
				if(rightOffset > 0) originalWidth -= rightOffset; //左にはみ出ていたらその分縮める
				if(bottomOffset > 0) originalHeight -= bottomOffset; //上にはみ出ていたらその分縮める
				
				if(bounds.left > 0) leftTranslate = bounds.left * -1; //右に配置されていたらその分左に移動
				if(bounds.top > 0) topTranslate = bounds.top * -1; //下に配置されていたらその分上に移動
				
				rateWidth = distBitmap.getWidth() / originalWidth;
				rateHeight = distBitmap.getHeight() / originalHeight;
				
				//マスクパスの移動、縮尺				
				matrix.postTranslate(leftTranslate, topTranslate); 
				matrix.postScale(rateWidth,rateHeight);
				
				//実際のペンの太さを設定
				mStrokeWidth = ((float)width / (float)bounds.width()) * mPaint.getStrokeWidth();
				mPaint.setStrokeWidth(mStrokeWidth);
				
				//パスを不透明にしてビットマップに書き出す
				SavablePath tempPath = new SavablePath(pathObject.getPath().copyJsonPath());
				SavableMatrix tempMatrix = new SavableMatrix(matrix);
				PathObject tempPathObject = new PathObject(mPartsGroup, tempPath, tempMatrix, mPaint, 1); //一時的に追加
				tempPathObject.flashToBitmap(maskCanvas, null);

				//一時的に追加したパスを削除
				mPartsGroup.removePart(mPartsGroup.getPart(2), false);

                if(BuildConfig.DEBUG){
                    try {
                        ImageUtil.storeBitmapToCache(maskBitmap, new File(getExternalCacheDir(), "003.png"));
                    } catch (Exception e) {
						Timber.e(e, "");
                    }
                }

				//マスクを貼付け
				Paint paint = new Paint();
				paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
				distCanvas.drawBitmap(maskBitmap, 0, 0, paint);
				maskBitmap.recycle();
				
				//余白を取り除く
				System.gc();
				HashMap<String, Object> ret = ImageUtil.getRealSizeBitmap(distBitmap);
				distBitmap = (Bitmap) ret.get("bitmap");
				mHeight = distBitmap.getHeight();
				mWidth = distBitmap.getWidth();
				
				//切り抜いた余白分、マスクを移動する
				pathObject = (PathObject) mPartsGroup.getPart(1);
				pathObject.move((Integer)ret.get("left") * -1, (Integer)ret.get("top") * -1);
				
                if(BuildConfig.DEBUG){
                    try {
                        ImageUtil.storeBitmapToCache(distBitmap, new File(getExternalCacheDir(), "004.png"));
                    } catch (Exception e) {
						Timber.e(e, "");
                    }
                }

				
				//切り取り完了画像をファイルへ保存する

		    	File cacheFile = StorageFileUtil.createNewRandomFile(StorageFileUtil.getTempCacheDir(CutPictureActivity.this),"");
				try {
					ImageUtil.storeBitmapToCache(distBitmap, cacheFile);
				} catch (Exception e) {
					Timber.e(e,"");
					throw new RuntimeException(e);
				}
		        
				distBitmap.recycle();

		        return cacheFile;
			}

			@Override
			protected void onPostExecute(File cacheFile) {
				mProgressDialog.dismiss();
				
				//枠追加のためマスク情報をシリアライズ化する
				String path = mPartsGroup.getPart(1).getPath().getJsonPath().toString();
				String matrix;
				try {
					matrix = mPartsGroup.getPart(1).getMatrix().getJson().toString();
				} catch (Exception e) {
					Timber.e(e,"");
					throw new RuntimeException(e);
				}
				
				//切り取り完了後、フレーム編集画面へ遷移
				Intent send_intent = new Intent(CutPictureActivity.this,AddPictureFrameActivity.class);
				send_intent.setData(Uri.fromFile(cacheFile));
				send_intent.putExtra("executeMode", Const.REQUEST_MODIFY_IMAGE);
				send_intent.putExtra("cutMode", mLastCutMode);
				send_intent.putExtra("height", mHeight);
				send_intent.putExtra("width", mWidth);
				send_intent.putExtra("path", path);
				send_intent.putExtra("matrix", matrix);
				send_intent.putExtra("penMinWidth",mStrokeWidth );
				send_intent.putExtra("penMaxWidth",mStrokeWidth * 2.0f );
				startActivity(send_intent);

				//メモリ解放
				mPartsGroup.recycle();
				mDrawBoardView.clear();

				finish();
			}

		};
		
		//タスクの実行
		task.execute();
	}

	//オペレーションコントローラー
	private void controllOperation(int mode){
		
		switch(mode){
		case OPERATION_MODE_SCROLL: //スクロールモードへ
			if(mHorizontalScrollView.getVisibility() != View.VISIBLE)
				mHorizontalScrollView.setVisibility(View.VISIBLE);

			mButtonFinish.setVisibility(View.INVISIBLE);
			
			mButtonZoomUp.setVisibility(View.VISIBLE);
			mButtonZoomDown.setVisibility(View.VISIBLE);
			
			break;
			
		case OPERATION_MODE_DRAW: //手書きモードへ
			mHorizontalScrollView.setVisibility(View.INVISIBLE);

			mButtonFinish.setVisibility(View.VISIBLE);
			
			mButtonZoomUp.setVisibility(View.INVISIBLE);
			mButtonZoomDown.setVisibility(View.INVISIBLE);

			break;
			
		case OPERATION_MODE_PLACE: //位置選択モードへ
			mHorizontalScrollView.setVisibility(View.INVISIBLE);

			mButtonFinish.setVisibility(View.INVISIBLE);
			
			mButtonZoomUp.setVisibility(View.VISIBLE);
			mButtonZoomDown.setVisibility(View.VISIBLE);

			break;
			
		}

		//現在のモードを変更
		mOperationMode = mode;
	
	}

	private void flash(final boolean changeToScrollMode){
        Task task = mPartsGroup.flashToBitmap();
        task.onSuccess(new Continuation() {
            @Override
            public Object then(Task task) throws Exception {
                mDrawBoardView.invalidate();

                if (changeToScrollMode == true)
                    controllOperation(OPERATION_MODE_SCROLL);

                dismissProgress();
                return null;
            }
        }, Task.UI_THREAD_EXECUTOR);
	}

	public void showProgress() {
		mProgressBar.setVisibility(View.VISIBLE);
	}

	public void dismissProgress() {
		mProgressBar.setVisibility(View.INVISIBLE);
	}


	//ヘルプページ表示
	private void actionHelp(String url){
	    Intent intent = new Intent(Intent.ACTION_VIEW);
	    Uri uri = Uri.parse(url).buildUpon().build();
	    intent.setData(uri);
	    startActivity(intent);
	}

	//オプション設定ダイアログ準備
	private void actionShowOptionDialog(final int mode){
        //アップロード確認ダイアログ作成
		if(mDialogOption == null){
	        mDialogOption = new Dialog(this);
	        mDialogOption.setContentView(R.layout.activity_cut_picture_option_dialog);
	        mButtonGoCut = (Button)mDialogOption.findViewById(R.id.buttonOk);
	        mButtonCancelCut = (Button)mDialogOption.findViewById(R.id.buttonCancel);
	    	mSeekBarSensitivity = (SeekBar)mDialogOption.findViewById(R.id.seekBarSensitivity);
	    	mCheckBoxRoundAngle = (CheckBox)mDialogOption.findViewById(R.id.checkBoxRoundAngle);
	    	mSeekBarPenWidth = (SeekBar)mDialogOption.findViewById(R.id.SeekBarPenWidth);
	    	mTextViewHelp = (TextView)mDialogOption.findViewById(R.id.textViewHelp);
	    	mRelativeLayoutSensitivity = (RelativeLayout)mDialogOption.findViewById(R.id.relativeLayoutSensitivity);
	    	mRelativeLayoutRoundAngle = (RelativeLayout)mDialogOption.findViewById(R.id.relativeLayoutRoundAngle);
	    	mRelativeLayoutPenWidth = (RelativeLayout)mDialogOption.findViewById(R.id.relativeLayoutPenWidth);
	    		    		    	
	    	//角を丸くするチェックボックスリスナー
	    	mCheckBoxRoundAngle.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mRoundAngle = ((CheckBox)v).isChecked();
				}
			});


	        //実行ボタンリスナー
	        mButtonGoCut.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mDialogOption.dismiss();
					mSensitivity = mMinSensitivity + (int)((float)(mMaxSensitivity - mMinSensitivity) * (float)mSeekBarSensitivity.getProgress() / 100);
					mWidth = mMinPenWidth + (float)(mMaxPenWidth - mMinPenWidth) * (float)mSeekBarPenWidth.getProgress() / 100.0f;
					actionGoCut(mCurrentMode, mSensitivity, mRoundAngle, mWidth);
				}
			});
	        
	        //キャンセルリスナー
	        mButtonCancelCut.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mCurrentMode = MODE_MOVE;
					mDialogOption.dismiss();
				}
			});

		}
		
		//各モード毎に初期設定
		float progress;
		switch(mode){
			case MODE_FREE_HAND:
		        mDialogOption.setTitle(R.string.select_by_free_hand);
		        mRelativeLayoutSensitivity.setVisibility(View.VISIBLE);
		        mRelativeLayoutRoundAngle.setVisibility(View.GONE);
		        mRelativeLayoutPenWidth.setVisibility(View.GONE);
		        mTextViewHelp.setText(R.string.choose_by_free_hand);
		        mOptionHelpUrl = "http://www.chaa.am/select_by_free_hand";
		        
		        progress = (float)(mSensitivity - mMinSensitivity) / (float)(mMaxSensitivity - mMinSensitivity) * 100.0f;
		        mSeekBarSensitivity.setProgress((int) progress);
				break;
			case MODE_LINE:
		        mDialogOption.setTitle(R.string.select_by_line);
		        mRelativeLayoutSensitivity.setVisibility(View.GONE);
		        mRelativeLayoutRoundAngle.setVisibility(View.GONE);
		        mRelativeLayoutPenWidth.setVisibility(View.GONE);
		        mTextViewHelp.setText(R.string.choose_by_line);
		        mOptionHelpUrl = "http://www.chaa.am/select_by_line";
		        
		        mCheckBoxRoundAngle.setChecked(mRoundAngle);
				break;
			case MODE_DRAW:
		        mDialogOption.setTitle(R.string.select_by_draw);
		        mRelativeLayoutSensitivity.setVisibility(View.VISIBLE);
		        mRelativeLayoutRoundAngle.setVisibility(View.GONE);
		        mRelativeLayoutPenWidth.setVisibility(View.VISIBLE);
		        mTextViewHelp.setText(R.string.choose_by_draw);
		        mOptionHelpUrl = "http://www.chaa.am/select_by_draw";
		        
		        progress = (float)(mSensitivity - mMinSensitivity) / (float)(mMaxSensitivity - mMinSensitivity) * 100.0f;
		        mSeekBarSensitivity.setProgress((int) progress);

		        progress = (mWidth - mMinPenWidth) / (mMaxPenWidth - mMinPenWidth) * 100.0f;
		        mSeekBarPenWidth.setProgress((int) progress);
				break;
		}
		
		mCurrentMode = mode;
		mDialogOption.show();
		
	}

	//現在の編集モードを切り替える
	private void actionGoCut(int mode, int sensitivity, boolean roundAngle, float width){
			
		//既にマスクがあったら削除
		if(mPartsGroup.getPartsCount() > 1){
			mPartsGroup.removePart(mPartsGroup.getPart(1), true);
		}
		
		Paint[] paints;
		
		//各モードの準備
		switch(mode){
		case MODE_FREE_HAND:
			//フリーハンドモードへ
		    paints = PartUtil.getPaint(1, 5, Color.parseColor("#7fE12523"), 0, 0, 100);
		    mPaint = paints[0];
		    mPaint.setStyle(Style.FILL);
			mPaint.setStrokeWidth(0.1f);
			mPaint.setAntiAlias(true);

			mDrawBoardView.setPathSensitivity(sensitivity);
			mDrawBoardView.startDrawMode(false, true, mPaint,null,null);
			break;
		case MODE_LINE:
			//直線モードへ
		    paints = PartUtil.getPaint(1, 5,Color.parseColor("#7fE12523"),0,0,100);
		    mPaint = paints[0];
		    mPaint.setStyle(Style.FILL);
			mPaint.setStrokeWidth(0.1f);
			mPaint.setAntiAlias(true);

			mDrawBoardView.startDrawMode(true, true, mPaint,null,null);
			break;
		case MODE_DRAW:
			//塗りつぶしモードへ
		    paints = PartUtil.getPaint(1, 5,Color.parseColor("#7fE12523"),0,0,100);
		    mPaint = paints[0];
		    mPaint.setStyle(Style.STROKE);
			mPaint.setStrokeWidth(width);
			mPaint.setAntiAlias(true);

			Log.d("kazino", "width:" + width);
			Log.d("kazino", "max width:" + mMaxPenWidth);
			Log.d("kazino", "min width:" + mMinPenWidth);

			
			mDrawBoardView.setPathSensitivity(sensitivity);
			mDrawBoardView.startDrawMode(false, false, mPaint,null,null);
			break;
		default:
		}
		flash(false);
		controllOperation(OPERATION_MODE_DRAW);

		mLastCutMode = mode;
	
	}

	//図形ダイアログを表示する
	private void actionSelectCuttingShape(){

		if(mDialogCuttingShape == null){

            try {
                cuttingShapeList = assetLogic.getCuttingShapes();
            } catch (LogicException e) {
                EventBus.getDefault().postSticky(new ErrorEvent(CutPictureActivity.class,e));
                return;
            }

            mDialogCuttingShape = new Dialog(this);
	        mDialogCuttingShape.setContentView(R.layout.activity_cut_picture_cutting_path_dialog);
	        mDialogCuttingShape.setTitle(R.string.select_by_shape);
	        mGridViewCuttingShape = (GridView)mDialogCuttingShape.findViewById(R.id.gridViewPath);
            mGridViewCuttingShape.setAdapter(new PathAdapter(this));

	        mGridViewCuttingShape.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,final int position, long arg3) {
					
					mDialogCuttingShape.dismiss();
					controllOperation(OPERATION_MODE_PLACE);
					
					//既にマスクがあったら削除
					if(mPartsGroup.getPartsCount() > 1){
						mPartsGroup.removePart(mPartsGroup.getPart(1), true);
					}
					
					//位置指定モードへ
					mDrawBoardView.startClickPlaceMode(new IClickPlaceListener() {
						@Override
						public void onSelected(float x, float y,int logicalX, int logicalY) {

							Paint[] paints;
						    paints = PartUtil.getPaint(1, 5,Color.parseColor("#7fE12523"),0,0,100);
						    mPaint = paints[0];
						    mPaint.setStyle(Style.FILL_AND_STROKE);
							mPaint.setStrokeWidth(0.1f);
							mPaint.setAntiAlias(true);

                            CuttingShape cuttingShape = cuttingShapeList.get(position);
					        SavablePath savablePath = new SavablePath(cuttingShape.getString(CuttingShape.F_SVG));
							SavableMatrix savableMatrix = new SavableMatrix();

							PathObject pathObject = new PathObject(mPartsGroup, savablePath, savableMatrix, mPaint, 1);
					    	pathObject.moveWithSelfCenter(logicalX, logicalY);
					    	
					    	//編集モードスタート
					    	mDrawBoardView.startEditMode(pathObject);
							mDrawBoardView.getUndoManager().addModify(pathObject); //確定後、描画されるようにダミー
							controllOperation(OPERATION_MODE_SCROLL);
					    	
						}
					});

				}
	        	
			});
			
		}
		

		mDialogCuttingShape.show();
	}
	
	//図形選択画面に表示されるView
	public class PathAdapter extends BaseAdapter{
	    private Context mContext;

		public PathAdapter(Context c) {
	        mContext = c;
	    }

		@Override
		public int getCount() {
			return cuttingShapeList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup viewGroup) {
	        SamplePathView pathView;
	        if (convertView == null) {  // if it's not recycled, initialize some attributes
	            pathView = new SamplePathView(mContext);
//	            pathView.setLayoutParams(new GridView.LayoutParams(85, 85));
	        } else {
	            pathView = (SamplePathView)convertView;
	        }
	        
	        Paint paint = new Paint();
	        paint.setColor(Color.RED);
            paint.setStrokeWidth(2);
            paint.setStyle(Style.STROKE);

            CuttingShape cuttingShape = cuttingShapeList.get(position);
	        SavablePath savablePath = new SavablePath(cuttingShape.getString(CuttingShape.F_SVG));
	        pathView.setPath(savablePath,paint);

	        return pathView;
		}
	}
	
	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater menuInflater = this.getMenuInflater();
        menuInflater.inflate(R.menu.menu_cut_picture,menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()){
            case R.id.actionUndo:
                actionUndo();
                return true;
            case R.id.actionSave:
                if(mLastCutMode == MODE_DRAW)
                    actionExecuteCutByDraw();
                else
                    actionExecuteCut();
                return true;
        }

        return false;
    }

    /**
     * error handling
     * @param event
     */
    @DebugLog
    public void onEventMainThread(ErrorEvent event){

        LogicException e = (LogicException) event.getException();

        //show dialog when error was happened
        DialogFragment dialogFragment = AlertDialogFragment_.builder()
                .eventKey(AlertDialogFragment.class.getName())
                .titleRes(e.getErrorCode().getErrorRes())
                .messageRes(e.getReasonRes())
                .positiveRes(android.R.string.ok).build();
        dialogFragment.show(this.getFragmentManager()
                ,AlertDialogFragment.class.getName());

        //remove sticky event after using.
        EventBus.getDefault().removeStickyEvent(event);

    }

}
