package jp.hellodea.picolla.deco.activity;

import java.util.concurrent.Callable;

import bolts.Continuation;
import bolts.Task;
import de.greenrobot.event.EventBus;
import hugo.weaving.DebugLog;
import jp.hellodea.picolla.R;
import jp.hellodea.picolla.common.eventbus.ErrorEvent;
import jp.hellodea.picolla.common.exception.LogicException;
import jp.hellodea.picolla.common.fragment.AlertDialogFragment;
import jp.hellodea.picolla.common.fragment.AlertDialogFragment_;
import jp.hellodea.picolla.deco.Const;
import jp.hellodea.picolla.deco.util.ImageInfo;
import jp.hellodea.picolla.deco.util.ImageUtil;
import jp.hellodea.picolla.deco.util.ResizeImage;
import jp.hellodea.picolla.deco.util.ResizeImageAsyncTask;
import jp.hellodea.picolla.deco.util.StorageFileUtil;
import jp.hellodea.picolla.deco.util.Util;
import timber.log.Timber;

import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;


//写真選択時のプレビュー画面
public class PreviewPictureActivity extends ActionBarActivity {
	
	//編集対象のオリジナル画像
	private Uri mOriginalImageUri;
	private int mOriginalImageWidth;
	private int mOriginalImageHeight;
	
	//リサイズ済み画像
	private Uri mResizedImageUri;
	private int mResizedImageWidth;
	private int mResizedImageHeight;
			
	//初期化処理完了フラグ
	private boolean mIsFinishInit = false;
	
	//画面のコンポーネント
	private ImageView mImageView;
	private Button mButtonCut;
	private Button mButtonFrame;

	//お待ちくださいダイアログ
	private ProgressDialog mProgressDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
        setContentView(R.layout.activity_preview_picture);
        
        //UI取得
        mImageView = (ImageView)findViewById(R.id.imageView);
        mButtonCut = (Button)findViewById(R.id.buttonCut);
        mButtonFrame  = (Button)findViewById(R.id.buttonFrame);

        //UIイベント設定
        mButtonCut.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				actionPerformCut();
			}
		});
        mButtonFrame.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				actionPerformFrame();
			}
		});

        //プログレスダイアログ生成
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getText(R.string.please_wait));

        //パラメータ取得
        Intent intent = getIntent();
        mOriginalImageUri = intent.getData();
        
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);

		if(mIsFinishInit == true) return; //既に処理済みなら何もしない

        //オリジナル画像から表示用の画像を生成（表示するには大きすぎるので）
        resizeImage();

    	mIsFinishInit = true;
    	
    	Log.d("kazino", "PicturePreview.onWindowFocusChanged");
	    	    
	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {

		if (event.getAction() == KeyEvent.ACTION_DOWN) {

			//戻るボタンが押されたときはMainEditorを再起動する（メモリ節約のため終了させているので）
			if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
				Intent intent = new Intent(this, MainEditorActivity.class);
				startActivity(intent);
				
				finish();
			}

		}

		return super.dispatchKeyEvent(event);

	}
	
	
	//オリジナル画像を作業用画像にリサイズ
	private void resizeImage(){

        mProgressDialog.show();

        Task.callInBackground(new Callable<ImageInfo>() {
            @Override
            public ImageInfo call() throws Exception {

                ImageInfo imageInfo = null;
                System.gc();

                //縮小と回転のため一旦一時ファイルに保存する(Bitmapは使わない)
                imageInfo = ImageUtil.decodeAndStoreResizedImage(
                        PreviewPictureActivity.this,
                        mOriginalImageUri,
                        StorageFileUtil.getTempCacheDir(PreviewPictureActivity.this),
                        (int)((float)ImageUtil.getMaxImageSize(PreviewPictureActivity.this) * 2.0f),
                        8);

                return imageInfo;
            }
        }).onSuccess(new Continuation<ImageInfo, Object>() {
            @Override
            public Object then(Task<ImageInfo> task) throws Exception {
                ResizeImageAsyncTask.ResizeImageListener listener = new ResizeImageAsyncTask.ResizeImageListener() {

                    @Override
                    public void onPreExecute() {}

                    @Override
                    public void onPostExecute(ResizeImage resizeImage) {
                        mProgressDialog.dismiss();

                        //結果受け取り
                        mOriginalImageUri = Uri.fromFile(resizeImage.mOriginalImageFile);
                        mOriginalImageWidth = resizeImage.mOriginalImageWidth;
                        mOriginalImageHeight = resizeImage.mOriginalImageHeight;
                        mResizedImageUri = Uri.fromFile(resizeImage.mWorkImageFile);
                        mResizedImageWidth = resizeImage.mWorkImageWidth;
                        mResizedImageHeight = resizeImage.mWorkImageHeight;

                        //画像表示
                        mImageView.setImageURI(mResizedImageUri);

                    }
                };

                ResizeImageAsyncTask.execute(PreviewPictureActivity.this, task.getResult().file,listener);


                return null;
            }
        },Task.UI_THREAD_EXECUTOR).continueWith(new Continuation<Object, Object>() {
            @Override
            public Object then(Task<Object> task) throws Exception {
                if(task.isFaulted()){
                    mProgressDialog.dismiss();
                    Timber.e(task.getError(),"");

                    Toast.makeText(getApplication(),R.string.error_cannot_open_picture,Toast.LENGTH_LONG).show();

                    Intent intent = new Intent(PreviewPictureActivity.this, MainEditorActivity.class);
                    startActivity(intent);

                    finish();


                }
                return null;
            }
        });

	}
	

	//写真を切り取る
	private void actionPerformCut(){
		//画像を切り取る
		Intent send_intent = new Intent(this,CutPictureActivity.class);
		send_intent.putExtra("originalImageUriString", mOriginalImageUri.toString());
		send_intent.putExtra("originalImageWidth", mOriginalImageWidth);
		send_intent.putExtra("originalImageHeight", mOriginalImageHeight);
		send_intent.putExtra("workImageUriString", mResizedImageUri.toString());
		send_intent.putExtra("workImageWidth", mResizedImageWidth);
		send_intent.putExtra("workImageHeight", mResizedImageHeight);
		startActivity(send_intent);
		

	}

	//フレームをつける
	private void actionPerformFrame(){
		
		//画像を切り取る
		Intent send_intent = new Intent(this,AddClipartFrameActivity.class);
		send_intent.putExtra("originalImageUriString", mOriginalImageUri.toString());
		send_intent.putExtra("originalImageWidth", mOriginalImageWidth);
		send_intent.putExtra("originalImageHeight", mOriginalImageHeight);
		send_intent.putExtra("workImageUriString", mResizedImageUri.toString());
		send_intent.putExtra("workImageWidth", mResizedImageWidth);
		send_intent.putExtra("workImageHeight", mResizedImageHeight);
		startActivity(send_intent);
		
	}
	
	//呼び出し元に戻る
	private void actionReturnToParent(){

		//縮小済みの写真をそのまま戻す
		Intent send_intent = new Intent(this,MainEditorActivity.class);
		send_intent.setData(mResizedImageUri);
		send_intent.putExtra("executeMode", Const.REQUEST_MODIFY_IMAGE);
		send_intent.putExtra("width", mResizedImageWidth);
		send_intent.putExtra("height", mResizedImageHeight);
		startActivity(send_intent);
		
		finish();

	}

	@Override
	protected void onStart() {
		super.onStart();

        EventBus.getDefault().registerSticky(this);
	}

	@Override
	protected void onStop() {
		super.onStop();

        EventBus.getDefault().unregister(this);
	}


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater menuInflater = this.getMenuInflater();
        menuInflater.inflate(R.menu.menu_preview_picture,menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()){
            case R.id.actionOk:
                actionReturnToParent();
                return true;
        }

        return false;
    }

    /**
     * error handling
     * @param event
     */
    @DebugLog
    public void onEventMainThread(ErrorEvent event){

        LogicException e = (LogicException) event.getException();

        //show dialog when error was happened
        DialogFragment dialogFragment = AlertDialogFragment_.builder()
                .eventKey(AlertDialogFragment.class.getName())
                .titleRes(e.getErrorCode().getErrorRes())
                .messageRes(e.getReasonRes())
                .positiveRes(android.R.string.ok).build();
        dialogFragment.show(this.getFragmentManager()
                ,AlertDialogFragment.class.getName());

        //remove sticky event after using.
        EventBus.getDefault().removeStickyEvent(event);

    }


}
