package jp.hellodea.picolla.deco.view;

import jp.hellodea.picolla.R;
import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

public class TabView extends FrameLayout {

	public TabView(Context context) {
		super(context);
	}

    public TabView(Context context, int title, int icon) {  
        this(context);  
        
        View childView = View.inflate(context, R.layout.tab, null);
        TextView textView = (TextView) childView.findViewById(R.id.textView);
        ImageView imageView = (ImageView) childView.findViewById(R.id.imageView);
        textView.setText(title);  
        imageView.setImageResource(icon);  
        addView(childView);
    }
}
