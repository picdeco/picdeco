package jp.hellodea.picolla.deco.activity;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.Callable;

import bolts.Continuation;
import bolts.Task;
import de.greenrobot.event.EventBus;
import hugo.weaving.DebugLog;
import jp.hellodea.picolla.R;
import jp.hellodea.picolla.common.eventbus.ErrorEvent;
import jp.hellodea.picolla.common.exception.ErrorCode;
import jp.hellodea.picolla.common.exception.LogicException;
import jp.hellodea.picolla.common.fragment.AlertDialogFragment;
import jp.hellodea.picolla.common.fragment.AlertDialogFragment_;
import jp.hellodea.picolla.common.objectgraph.ObjectGraphHolder;
import jp.hellodea.picolla.deco.Const;
import jp.hellodea.picolla.deco.entity.Effect;
import jp.hellodea.picolla.deco.entity.Filter;
import jp.hellodea.picolla.deco.eventbus.ProgressDialogEvent;
import jp.hellodea.picolla.deco.eventbus.ProgressViewEvent;
import jp.hellodea.picolla.deco.logic.AssetLogic;
import jp.hellodea.picolla.deco.logic.EditLogic;
import jp.hellodea.picolla.deco.util.EventUtil;
import jp.hellodea.picolla.deco.util.ImageInfo;
import jp.hellodea.picolla.deco.util.ImageUtil;
import jp.hellodea.picolla.deco.util.StorageFileUtil;
import jp.hellodea.picolla.deco.util.Util;
import jp.hellodea.picolla.deco.view.SelectableImageLayout;
import jp.hellodea.picolla.deco.view.SelectableImageView;
import jp.hellodea.picolla.deco.view.SelectableImageView_;
import timber.log.Timber;

import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ProgressBar;

import javax.inject.Inject;

//写真選択時のプレビュー画面
public class SelectFilterActivity extends ActionBarActivity {

    @Inject
    AssetLogic assetLogic;

    @Inject
    EditLogic editLogic;
	
	private String mParentActivity; //呼び出し元アクティビティ
	
	private Uri mOriginalImageUri;	//オリジナルの画像

	private Bitmap mPreviewImageBitmap;	//加工中の画像
	private int mPreviewImageHeight;	//加工中の画像の高さ
	private int mPreviewImageWidth;	//加工中の画像の幅
	
	private List<Filter> mListFilters; //フィルターのリスト
	private List<Effect> mListEffects; //エフェクトのリスト

	private int mCurrentFilterNo; //現在選択されているフィルター
	private int mCurrentEffectNo; //現在選択されているエフェクト

	//画面のコンポーネント
	private ImageView mImageViewPreview;
	private ImageView mImageViewEffect;
	private SelectableImageLayout  selectableImageLayoutFilter;
	private SelectableImageLayout selectableImageLayoutEffect;
    private ProgressBar mProgressBar;
	
	//お待ちくださいダイアログ
	private ProgressDialog mProgressDialog;
	
	//進捗表示ダイアログ
	private ProgressDialog mProgressBarDialog;

	@Override
    @DebugLog
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
        setContentView(R.layout.activity_select_filter);

        ObjectGraphHolder.get().inject(this);

        //UI取得
        mImageViewPreview = (ImageView)findViewById(R.id.imageViewPreview);
        mImageViewEffect = (ImageView)findViewById(R.id.imageViewEffect);
        selectableImageLayoutFilter = (SelectableImageLayout)findViewById(R.id.selectableImageLayoutFilter);
        selectableImageLayoutEffect = (SelectableImageLayout)findViewById(R.id.selectableImageLayoutEffect);
        mProgressBar = (ProgressBar)findViewById(R.id.progressBar);
        

        //プログレスダイアログ生成
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getText(R.string.please_wait));

        //進捗表示ダイアログ生成
        mProgressBarDialog = new ProgressDialog(this);
        mProgressBarDialog.setMessage(getText(R.string.please_wait));
        mProgressBarDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressBarDialog.setCancelable(false);

        //パラメータ取得
        Intent intent = getIntent();
        mOriginalImageUri = intent.getData();
        mParentActivity = intent.getStringExtra("parent");
        
        System.gc();
        
        init();
	}
	
	@Override
    @DebugLog
	public boolean dispatchKeyEvent(KeyEvent event) {

		if (event.getAction() == KeyEvent.ACTION_DOWN) {

			//戻るボタンが押されたときはMainEditorを再起動する（メモリ節約のため終了させているので）
			if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
				Class returnClass;
				try {
					returnClass = Class.forName(mParentActivity);
					Intent intent = new Intent(this,returnClass);
					intent.setData(null);
					intent.putExtra("executeMode", Const.REQUEST_FILTER);
					startActivity(intent);
			        finish();
				} catch (ClassNotFoundException e) {
                    Timber.e(e,"");
					throw new RuntimeException(e);
				}

			}

		}

		return super.dispatchKeyEvent(event);

	}

	//初期処理
    @DebugLog
	private void init(){

        EventBus.getDefault().postSticky(new ProgressViewEvent(ProgressViewEvent.Status.START));

        Task.callInBackground(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                //作業用は1/4で行う
                ImageInfo imageInfo = ImageUtil.decodeAndStoreResizedImage(
                        SelectFilterActivity.this, mOriginalImageUri,
                        StorageFileUtil.getTempCacheDir(SelectFilterActivity.this),
                        ImageUtil.getMaxImageSize(SelectFilterActivity.this) / 2, 1);

                //作業用Uriへコピー
                mPreviewImageBitmap = imageInfo.bitmap;
                mPreviewImageWidth = imageInfo.width;
                mPreviewImageHeight = imageInfo.height;

                mCurrentEffectNo = -1; //未選択
                mCurrentFilterNo = -1; //未選択
                return null;
            }
        }).onSuccess(new Continuation<Object, Object>() {
            @Override
            public Object then(Task<Object> task) throws Exception {
                mImageViewPreview.setImageBitmap(mPreviewImageBitmap);

                initFilter();
                initEffect();

                return null;
            }
        }, Task.UI_THREAD_EXECUTOR).continueWith(new Continuation<Object, Object>() {
            @Override
            public Object then(Task<Object> task) throws Exception {

                EventBus.getDefault().postSticky(new ProgressViewEvent(ProgressViewEvent.Status.END));

                //error handling
                if(task.isFaulted()){
                    Exception e = task.getError();
                    Timber.e(e, "");
                    LogicException logicException = new LogicException(ErrorCode.UnexpectedError, R.string.error_select_filter_init,e);
                    EventBus.getDefault().postSticky(new ErrorEvent(SelectFilterActivity.class, logicException));
                    return null;

                }
                return null;
            }
        },Task.UI_THREAD_EXECUTOR);

	}


    @DebugLog
    private void initFilter(){
        try {
            mListFilters = assetLogic.getFilters();
        } catch (LogicException e) {
            EventBus.getDefault().post(new ErrorEvent(this.getClass(), e));
            return;
        }

        //フィルターを画面に設定
        for (int i = 0; i < mListFilters.size(); i++) {
            Filter filter = mListFilters.get(i);
            SelectableImageView filterItem = SelectableImageView_.build(this);
            filterItem.bind(Uri.parse(filter.getSampleUrl()));
            filterItem.setTag(i);
            filterItem.setSelectedStatus(false);

            selectableImageLayoutFilter.addView(filterItem);

            //クリップアートにイベント追加
            filterItem.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    //クリックされたら区分選択アクションを呼び出し
                    actionFilter((int) v.getTag());

                }
            });
        }



    }
	
	//プレビューにフィルター適用
    @DebugLog
	private void actionFilter(int index){
		mCurrentFilterNo = index;

		//フィルターを非同期で適用
		AsyncTask<Void, Void, Bitmap> task = new AsyncTask<Void, Void, Bitmap>() {
			
			@Override
			protected void onPreExecute() {
                EventBus.getDefault().postSticky(new ProgressViewEvent(ProgressViewEvent.Status.START));
			}

			@Override
            @DebugLog
			protected Bitmap doInBackground(Void... params) {
				Bitmap bitmap = mPreviewImageBitmap.copy(mPreviewImageBitmap.getConfig(), true);


                Filter filter = mListFilters.get(mCurrentFilterNo);

                try {
                    bitmap = editLogic.performFilter(SelectFilterActivity.this, bitmap, filter);
                } catch (LogicException e) {
                    EventBus.getDefault().postSticky(new ErrorEvent(SelectFilterActivity.class,e));
                    return null;
                }
                return bitmap;
			}

			@Override
            @DebugLog
			protected void onPostExecute(Bitmap bitmap) {
                EventBus.getDefault().postSticky(new ProgressViewEvent(ProgressViewEvent.Status.END));

                if(bitmap != null)
    				mImageViewPreview.setImageBitmap(bitmap);
			}
		};
		task.execute();
		
		//選択状態を更新
		selectableImageLayoutFilter.selectItem(mCurrentEffectNo, true);

	}

    @DebugLog
    private void initEffect(){

        try {
            mListEffects = assetLogic.getEffects();
        } catch (LogicException e) {
            EventBus.getDefault().post(new ErrorEvent(this.getClass(), e));
            return;
        }

        //フレームを画面に設定
        for (int i = 0; i < mListEffects.size(); i++) {
            Effect effect = mListEffects.get(i);
            SelectableImageView effectItem = SelectableImageView_.build(this);
            effectItem.bind((Uri.parse(effect.getSampleUrl())));
            effectItem.setTag(i); // 区分を埋め込み
            effectItem.setSelectedStatus(false);

            selectableImageLayoutEffect.addView(effectItem);

            //クリップアートにイベント追加
            effectItem.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    //クリックされたら区分選択アクションを呼び出し
                    actionEffect((Integer) v.getTag());
                }
            });
        }

    }

	//プレビューにエフェクト適用
    @DebugLog
	private void actionEffect(int i){
		mCurrentEffectNo = i;

        EventBus.getDefault().postSticky(new ProgressViewEvent(ProgressViewEvent.Status.START));

		//選択状態を更新
		selectableImageLayoutEffect.selectItem(i, true);


		final Effect effect = mListEffects.get(mCurrentEffectNo);

        Task.callInBackground(new Callable<Bitmap>() {
            @Override
            public Bitmap call() throws Exception {

                Bitmap bitmap = editLogic.getEffectBitmap(SelectFilterActivity.this,effect);

                //表示用に縮小
                Bitmap thumb = ImageUtil.resizeFree(bitmap, mPreviewImageWidth, mPreviewImageHeight);

                return thumb;
            }
        }).onSuccess(new Continuation<Bitmap, Object>() {
            @Override
            public Object then(Task<Bitmap> task) throws Exception {
                mImageViewEffect.setImageBitmap(task.getResult());
                mImageViewEffect.setLayoutParams(mImageViewPreview.getLayoutParams());

                return null;
            }
        },Task.UI_THREAD_EXECUTOR).continueWith(new Continuation<Object, Object>() {
            @Override
            public Object then(Task<Object> task) throws Exception {
                EventBus.getDefault().postSticky(new ProgressViewEvent(ProgressViewEvent.Status.END));

                //err handling
                if(task.isFaulted()){
                    Timber.e(task.getError(),"");
                    EventBus.getDefault().postSticky(new ErrorEvent(SelectFilterActivity.this,task.getError()));
                }

                return null;
            }
        });

    }


	//呼び出し元に戻る
    @DebugLog
	private void actionReturnToParent(){
		CreatePictureAsyncTask task = new CreatePictureAsyncTask();
		task.execute();
	}
	
	//やり直し
	private void actionUndo(){
		mCurrentFilterNo = -1; //現在選択されているフィルター
		mCurrentEffectNo = -1; //現在選択されているエフェクト

		mImageViewPreview.setImageBitmap(mPreviewImageBitmap);
		mImageViewEffect.setImageBitmap(null);

		selectableImageLayoutFilter.selectAll(false);
		selectableImageLayoutEffect.selectAll(false);

	}

	//画像を保存するための非同期処理
	class CreatePictureAsyncTask extends AsyncTask<Void, Void, File> {

		public CreatePictureAsyncTask() {
			super();
		}

		@Override
		protected void onPreExecute() {
            EventBus.getDefault().postSticky(new ProgressDialogEvent(
                    ProgressDialogEvent.Status.START,R.string.progress_create_image,ProgressDialog.STYLE_SPINNER));
		}

		@Override
        @DebugLog
		protected File doInBackground(Void... params) {

			Log.d("kazino", "CreatePicture doInBackground");
	        System.gc();
	        System.gc();
	        System.gc();

			File file = null;

			try{
				InputStream is = getContentResolver().openInputStream(mOriginalImageUri);
		        Bitmap bitmap = BitmapFactory.decodeStream(is);
		        bitmap = bitmap.copy(bitmap.getConfig(), true);
				is.close();

				//フィルター
                Filter filter = null;
				if(mCurrentFilterNo != -1){
                    filter = mListFilters.get(mCurrentFilterNo);
                }

				//エフェクト
                Effect effect = null;
				if(mCurrentEffectNo != -1){
                    effect = mListEffects.get(mCurrentEffectNo);
                }

                bitmap = editLogic.applyFilter(SelectFilterActivity.this,bitmap,filter,effect);

				//ファイルに保存
				file = StorageFileUtil.createNewRandomFile(StorageFileUtil.getImagePartsDir(SelectFilterActivity.this), "");
				ImageUtil.storeBitmapToCache(bitmap, file);
				bitmap.recycle();
					        	
	        } catch (LogicException e) {
                EventBus.getDefault().postSticky(new ErrorEvent(SelectFilterActivity.class,e));
                return null;
            } catch (IOException e) {
                Timber.e(e, "");
                LogicException logicException = new LogicException(ErrorCode.ErrorOnFileSystem,R.string.error_processing_filter);
                EventBus.getDefault().postSticky(new ErrorEvent(SelectFilterActivity.class,e));
                return null;
            }

            return file;
		}
		
		@Override
        @DebugLog
		protected void onPostExecute(File imageFile) {
			
			mProgressDialog.dismiss();
			
			if(imageFile == null) return;
			
			Uri uri = Uri.fromFile(imageFile);
			try {
				Class returnClass;
				returnClass = Class.forName(mParentActivity);
				Intent intent = new Intent(SelectFilterActivity.this,returnClass);
				intent.setData(uri);
				intent.putExtra("executeMode", Const.REQUEST_FILTER);
				startActivity(intent);
				
				finish();

			} catch (ClassNotFoundException e) {
                Timber.e(e,"");
                throw new RuntimeException(e);
			}
			
		}
	}

    /**
     * error handling
     * @param event
     */
    @DebugLog
    public void onEventMainThread(ErrorEvent event){

        LogicException e = (LogicException) event.getException();

        //show dialog when error was happened
        DialogFragment dialogFragment = AlertDialogFragment_.builder()
                .eventKey(AlertDialogFragment.class.getName())
                .titleRes(e.getErrorCode().getErrorRes())
                .messageRes(e.getReasonRes())
                .positiveRes(android.R.string.ok).build();
        dialogFragment.show(this.getFragmentManager()
                ,AlertDialogFragment.class.getName());

        //remove sticky event after using.
        EventBus.getDefault().removeStickyEvent(event);

    }

    /**
     * show progress dialog
     * @param event
     */
    @DebugLog
    public void onEventMainThread(ProgressDialogEvent event){
        EventUtil.showProgressDialog(this,event);
    }

    /**
     * show progress view
     * @param event
     */
    @DebugLog
    public void onEventMainThread(ProgressViewEvent event){
        if(event.getEventKey().equals(ProgressViewEvent.Status.START)){
            mProgressBar.setVisibility(View.VISIBLE);
        }else {
            mProgressBar.setVisibility(View.INVISIBLE);
        }

        //remove sticky event after using.
        EventBus.getDefault().removeStickyEvent(event);

    }
    @Override
	protected void onStart() {
		super.onStart();
        EventBus.getDefault().registerSticky(this);

    }

	@Override
	protected void onStop() {
		super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater menuInflater = this.getMenuInflater();
        menuInflater.inflate(R.menu.menu_select_filter,menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()){
            case R.id.actionUndo:
                actionUndo();
                return true;
            case R.id.actionOk:
                actionReturnToParent();;
                return true;
        }

        return false;
    }

}
