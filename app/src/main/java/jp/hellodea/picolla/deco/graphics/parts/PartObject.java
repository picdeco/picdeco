package jp.hellodea.picolla.deco.graphics.parts;

import org.json.JSONObject;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;

import jp.hellodea.picolla.deco.graphics.SavableMatrix;
import jp.hellodea.picolla.deco.graphics.SavablePath;

abstract public class PartObject {

	protected PartsGroup mPartsGroup;
	
	public int mKind;	//パーツの種類
	public static final int KIND_PATH = 1;
	public static final int KIND_TEXT = 2;
	public static final int KIND_PICT = 3;
	public static final int KIND_GROUP = 4;
	
	protected int mDisplaySampleSize; //表示用縮尺率
	
	protected Paint mPaint;
	protected SavablePath mPath;
	
	protected Paint mMaskPaint; //マスク用
	protected SavablePath mMaskPath; //マスク用

	protected SavableMatrix mMatrix;  //回転、拡大など
	protected RectF mMatrixRectF;  //回転、拡大計算用
	
	protected boolean mLock = false;	//移動や回転などできないようにロックするか
	
	public PartObject(PartsGroup partsGroup, int kind, int displaySampleSize) {
		mPartsGroup = partsGroup;
		mKind = kind;
		mDisplaySampleSize = displaySampleSize;
		
		mPaint = new Paint();
		mPath = new SavablePath();
		
		mMatrix = new SavableMatrix();
		mMatrix.reset();
//		mMatrix.setRotate(0);
//		mMatrix.setScale(1.0f, 1.0f);
//		mMatrix.setTranslate(0, 0);
		
		mMatrixRectF = new RectF();
	}

	public void setMaskColor(int color){
		mMaskPaint = new Paint();
		mMaskPaint.setColor(color);
		mMaskPaint.setStyle(Style.STROKE);
		mMaskPaint.setStrokeWidth(40);
	}
	
	public int getMaskColor(){
		if(mLock == true)
			return PartsGroup.MASK_LOCK_COLOR; //ロックされていたらロック用色を返す
		else
			return mMaskPaint.getColor();
	}
	
	public void setLock(boolean lock){
		mLock = lock;
	}
	
	public SavableMatrix getMatrix() {
		return mMatrix;
	}
	
	public void setMatrix(SavableMatrix matrix){
		mMatrix = matrix;
	}
		
	public SavablePath getPath() {
		return mPath;
	}

	public PartsGroup getPartsGroup() {
		return mPartsGroup;
	}

	public void setPosition(float x, float y){
		mMatrix.setTranslate(x, y);
	}
	
	//画像をキャンバスの中心に移動
	public void moveToCenter(){
    	int centerX, centerY;
	    centerX = (mPartsGroup.getBitmap().getWidth() - (int)getBounds().width()) / 2;
	    centerY = (mPartsGroup.getBitmap().getHeight() - (int)getBounds().height()) / 2;
	    this.setPosition(centerX, centerY);
	}
	
	public void setPaint(Paint newPaint){
		mPaint = newPaint;
	}
	
	public int getDisplaySampleSize() {
		return mDisplaySampleSize;
	}

	//描画メソッド
	abstract public void flashToBitmap(Canvas canvas, Canvas maskCanvas);

	//回転
	abstract public void rotate(float degrees);
	
	//縮小拡大
	abstract public void scale(float scaleX, float scaleY);
	
	//移動
	abstract public void move(int x, int y);
	
	//サイズを取得
	abstract public RectF getBounds();
	
	//保存用jsonデータ取得
	abstract public JSONObject getJsonObject();
	
	//編集対象になった時の処理
	abstract public void startEditTarget();

	//編集対象から外れた時の処理
	abstract public void endEditTarget();
	
	//メモリーを解放する
	abstract public void recycle();
	
	//実サイズに書き出す
	abstract public void drawRealSize(Canvas canvas) throws Exception;

	//使っているアセットを削除する
	abstract public void deleteAsset();
	
	
}
