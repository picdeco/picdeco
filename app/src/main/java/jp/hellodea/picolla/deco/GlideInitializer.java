package jp.hellodea.picolla.deco;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.load.engine.bitmap_recycle.LruBitmapPool;
import com.bumptech.glide.load.engine.cache.LruResourceCache;
import com.bumptech.glide.load.engine.cache.MemorySizeCalculator;
import com.bumptech.glide.module.GlideModule;

import hugo.weaving.DebugLog;
import jp.hellodea.picolla.deco.util.ContextUtil;
import timber.log.Timber;

/**
 * Created by kazuhiro on 2015/02/10.
 */
public class GlideInitializer implements GlideModule {
    @Override
    @DebugLog
    public void applyOptions(Context context, GlideBuilder builder) {
        MemorySizeCalculator calculator = new MemorySizeCalculator(context);
        int defaultMemoryCacheSize = calculator.getMemoryCacheSize();
        int defaultBitmapPoolSize = calculator.getBitmapPoolSize();

        Timber.i("defaultMemoryCacheSize %d defaultBitmapPoolSize %d", defaultMemoryCacheSize / 1024 / 1024, defaultBitmapPoolSize / 1024 / 1024);

        int memSize = ContextUtil.getAvailableMemoryMega(context);
        float cacheSize = (float)memSize / 10.0f;

        int cacheByte = (int) (cacheSize / 2.0f * 1024.0f * 1024.0f);

        Timber.i("memSize %d cacheSize %d Byte", memSize, cacheByte);

        builder.setMemoryCache(new LruResourceCache(cacheByte));
        builder.setBitmapPool(new LruBitmapPool(cacheByte));

    }

    @Override
    @DebugLog
    public void registerComponents(Context context, Glide glide) {
        // register ModelLoaders here.
    }
}