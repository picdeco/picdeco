package jp.hellodea.picolla.deco.graphics.parts;

import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.RectF;

import jp.hellodea.picolla.BuildConfig;
import jp.hellodea.picolla.deco.graphics.SavableMatrix;
import jp.hellodea.picolla.deco.graphics.SavablePath;
import timber.log.Timber;

public class PathObject extends PartObject {

	private int mKind; //線の種類
	private Paint mMidPaint; //内側の線
	private Paint mOutPaint; //外側の線
	
	private Path mTempPath; //作業用
	private RectF mTempRectF; //作業用

	public PathObject(PartsGroup group, SavablePath path, SavableMatrix matrix, int kind, float width, int color, int midColor, int outColor, int alpha,boolean undoable, int displaySampleSize) {
		super(group,PartObject.KIND_PATH,displaySampleSize);

		//ペン取得
		mKind = kind;
		Paint[] paints = PartUtil.getPaint(kind, width, color, midColor, outColor,alpha);
		
		mPath.addPath(path);
		mPath.computeBounds(mMatrixRectF, true);
		mPaint = paints[0];
		mMidPaint = paints[1];
		mOutPaint = paints[2];

		mMatrix = matrix;
		
		mTempPath = new Path();
		mTempRectF = new RectF();
		
		group.addPart(this,undoable);
		
	}
	
	public PathObject(PartsGroup group, SavablePath path, SavableMatrix matrix, int kind, Paint paint, Paint midPaint, Paint outPaint,int alpha, boolean undoable, int displaySampleSize){
		super(group,PartObject.KIND_PATH,displaySampleSize);
		
		//ペン取得
		mKind = kind;
		
		mPath.addPath(path);
		mPath.computeBounds(mMatrixRectF, true);
	
		mPaint = paint;
		mMidPaint = midPaint;
		mOutPaint = outPaint;

		//投下指定
		mPaint.setAlpha(alpha);
		if(mMidPaint != null) mMidPaint.setAlpha(alpha);
		if(mOutPaint != null) mOutPaint.setAlpha(alpha);

		mMatrix = matrix;
		
		mTempPath = new Path();
		mTempRectF = new RectF();
		
		group.addPart(this,undoable);

	}
	
	//範囲選択専用パス
	public PathObject(PartsGroup group,SavablePath path, SavableMatrix matrix,Paint paint, int displaySampleSize){
		super(group,PartObject.KIND_PATH,displaySampleSize);
		
		mPath.addPath(path);
		mPath.computeBounds(mMatrixRectF, true);
		mPaint = paint;
		mMidPaint = new Paint();
		mMidPaint.setStrokeWidth(0);
		mOutPaint = new Paint();
		mOutPaint.setStrokeWidth(0);

		mMatrix = matrix;
		
		mTempPath = new Path();
		mTempRectF = new RectF();
		
		group.addPart(this,true);
		
		mMaskPaint.setStyle(Style.FILL);
		mMaskPaint.setStrokeWidth(1);

	}
	
	public PathObject(PartsGroup group,JSONObject json,boolean undoable) throws JSONException{

		this(
				group,
				new SavablePath(json.getJSONArray("path")),
				new SavableMatrix(json.getJSONArray("matrix")),
				json.getInt("kind"),
				json.getInt("width"),
				json.getInt("color"),
				json.optInt("midColor",0),
				json.optInt("outColor",0),
				json.optInt("alpha",255),
				undoable,
				json.getInt("displaySampleSize")
				
		);

	}

	
	
	
	public int getKind() {
		return mKind;
	}
	public float getStrokeWidth(){
		return mPaint.getStrokeWidth();
	}
	public int getColor(){
		return mPaint.getColor();
	}
	public int getMidColor(){
		if(mMidPaint != null)
			return mMidPaint.getColor();
		else
			return 0;
	}
	public int getOutColor(){
		if(mOutPaint != null)
			return mOutPaint.getColor();
		else
			return 0;
	}
		
	@Override
	public void flashToBitmap(Canvas canvas,Canvas maskCanves) {
		
		if(mPaint.getStrokeWidth() == 0) return; //幅がなければ何もしない
		
		mTempPath.set(mPath);
		mTempPath.transform(mMatrix);

		if(mOutPaint.getStrokeWidth() > 0) canvas.drawPath(mTempPath, mOutPaint);
		if(mMidPaint.getStrokeWidth() > 0) canvas.drawPath(mTempPath, mMidPaint);
		
		canvas.drawPath(mTempPath, mPaint);
		
		//テスト中のみ表示
		if(BuildConfig.DEBUG){
			RectF bounds = getBounds();
			Paint paint = new Paint();
			paint.setColor(Color.RED);
			paint.setStrokeWidth(3.0f);
			paint.setStyle(Style.STROKE);
			Path path = new Path();
			path.moveTo(bounds.left,bounds.top);
			path.addRect(bounds,Direction.CW);
			canvas.drawPath(path, paint);
		}
		
		if(maskCanves != null)
			maskCanves.drawPath(mTempPath, mMaskPaint);
	}

	@Override
	public void rotate(float degrees){
		mTempRectF.set(mMatrixRectF);
		mMatrix.mapRect(mTempRectF);
		mMatrix.postRotate(degrees,mTempRectF.centerX(),mTempRectF.centerY());
	}

	@Override
	public void scale(float scaleX, float scaleY) {
		SavableMatrix tempMatrix = new SavableMatrix(mMatrix);

		mTempRectF.set(mMatrixRectF);
		mMatrix.mapRect(mTempRectF);
		mMatrix.postScale(scaleX,scaleY,mTempRectF.centerX(),mTempRectF.centerY());
		
		//NaNになっていないかチェック
		mTempRectF.set(mMatrixRectF);
		mMatrix.mapRect(mTempRectF);
		if(Math.abs(mTempRectF.width()) < 2 || Math.abs(mTempRectF.height()) < 2){
			mMatrix = tempMatrix;
			return;
		}
		if(mMatrix.toShortString().contains("NaN") == true)
			mMatrix = tempMatrix;

	}

	//指定された場所に移動
	@Override
	public void move(int x, int y) {
		mMatrix.postTranslate(x, y);
	}
	
	//指定された場所に移動（中心が指定された場所になるように調整）
	public void moveWithSelfCenter(float x, float y){
		RectF rectF = getBounds();
		float newX = x - (rectF.width() / 2.0f);
		float newY = y - (rectF.height() / 2.0f);
		
		move((int)newX, (int)newY);
	}

	@Override
	public RectF getBounds(){
		mTempRectF.set(mMatrixRectF);
		mMatrix.mapRect(mTempRectF);

		return mTempRectF;
	}

	@Override
	public JSONObject getJsonObject() {
		JSONObject json = new JSONObject();
		try {
			json.put("partKind", PartObject.KIND_PATH);
			json.put("kind", mKind);
			json.put("path", mPath.getJsonPath());
			json.put("matrix", mMatrix.getJson());
			json.put("width", mPaint.getStrokeWidth());
			json.put("color", mPaint.getColor());
			json.put("alpha", mPaint.getAlpha());
			json.put("displaySampleSize", mDisplaySampleSize);
			if(mMidPaint != null) json.put("midColor", mMidPaint.getColor());
			if(mOutPaint != null) json.put("outColor", mOutPaint.getColor());
			
		} catch (JSONException e) {
			Timber.e(e, "");
			throw new RuntimeException(e);
		}
		
		return json;
	}

	@Override
	public void startEditTarget() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void endEditTarget() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void recycle() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void drawRealSize(Canvas canvas) {
		
		if(mPaint.getStrokeWidth() == 0) return; //幅がなければ何もしない

		Matrix matrix = new Matrix(mMatrix);
		
		//ペン取得
		Paint[] paints = PartUtil.getPaint(mKind, mPaint.getStrokeWidth(), mPaint.getColor(), 
				mMidPaint.getColor(), mOutPaint.getColor(), mPaint.getAlpha(), mDisplaySampleSize); //実際のサイズに戻す
		Paint paint = paints[0];
		Paint midPaint = paints[1];
		Paint outPaint = paints[2];
		
		//実際の縮尺を適用する
		matrix.postScale((float)mDisplaySampleSize, (float)mDisplaySampleSize);

		mTempPath.set(mPath);
		mTempPath.transform(matrix);

		if(outPaint.getStrokeWidth() > 0)
			canvas.drawPath(mTempPath, outPaint);
		
		if(midPaint.getStrokeWidth() > 0)
			canvas.drawPath(mTempPath, midPaint);
		
		canvas.drawPath(mTempPath, paint);
	}

	@Override
	public void deleteAsset() {
		return;
	}

	@Override
	public void moveToCenter() {
    	//画像をキャンバスの中心に移動
    	int centerX, centerY,moveX,moveY;
    	RectF rectFBounds = getBounds();
	    centerX = (mPartsGroup.getBitmap().getWidth() - (int)rectFBounds.width()) / 2;
	    centerY = (mPartsGroup.getBitmap().getHeight() - (int)rectFBounds.height()) / 2;
	    moveX = (int) (rectFBounds.left - centerX) * -1;
	    moveY = (int) (rectFBounds.top - centerY) * -1;
	    move(moveX, moveY);
	}

	
	
}
