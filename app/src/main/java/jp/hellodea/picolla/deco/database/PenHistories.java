package jp.hellodea.picolla.deco.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public class PenHistories extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "pen_histories";
	
	private static final String CREATE_TABLE_PEN_HISTORES =
			"CREATE TABLE pen_histories " + 
				"(" +
					BaseColumns._ID + " INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE , " +
					"kind INTEGER NOT NULL , " + 
					"color INTEGER NOT NULL , " + 
					"mid_color INTEGER NOT NULL , " + 
					"out_color INTEGER NOT NULL , " + 
					"count INTEGER NOT NULL , " +
					"last_updated_at INTEGER NOT NULL " +
				")";
	
	
	public PenHistories(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE_PEN_HISTORES);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}
	
	//ペンの履歴を更新する
	public static void createOrUpdate(SQLiteDatabase database, int kind ,int color, int midColor, int outColor){
		String sqlStr = 
				"select * from pen_histories " +  
				"where kind = ? and " +
				"      color = ? and " +
				"      mid_color = ? and " +
				"      out_color = ? ";
		String[] args = {
				String.valueOf(kind),
				String.valueOf(color),
				String.valueOf(midColor),
				String.valueOf(outColor)};
		
		Cursor cursor = database.rawQuery(sqlStr, args);
		ContentValues contentValues = new ContentValues();

		if(cursor.getCount() == 0){
			//履歴がなければ新規作成
			contentValues.put("kind", kind);
			contentValues.put("color", color);
			contentValues.put("mid_color", midColor);
			contentValues.put("out_color", outColor);
			contentValues.put("count", 1);
			contentValues.put("last_updated_at", System.currentTimeMillis());
			database.insertOrThrow("pen_histories", null, contentValues);

		}else{
			//履歴があれば情報更新
			cursor.moveToFirst();
			String[] keys = {cursor.getString(cursor.getColumnIndex(BaseColumns._ID))};
			contentValues.put("count", cursor.getInt(cursor.getColumnIndex("count")) + 1);
			contentValues.put("last_updated_at", System.currentTimeMillis());
			database.update("pen_histories", contentValues, BaseColumns._ID + "= ?", keys);
			
		}
		
		cursor.close();
		return;
	}
	
	//ペン履歴を取得する
	public static Cursor findHistories(SQLiteDatabase database, int limit){
		String sqlStr = 
				"select * from pen_histories " +  
				"order by last_updated_at desc " +
				"limit ?";
		String[] args = {String.valueOf(limit)};
		
		return database.rawQuery(sqlStr, args);
	}
}
