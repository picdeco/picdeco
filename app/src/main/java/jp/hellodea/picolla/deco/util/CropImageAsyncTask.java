package jp.hellodea.picolla.deco.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.util.Log;

import timber.log.Timber;

public class CropImageAsyncTask {

	//呼び出し元に実装すべきリスナー
	public interface CropImageListener{
		public void onPreExecute();
		public void onPostExecute(File imageFile, int width, int height);
	}
		
	public static void execute(
			final Context context,
			final File originalImageFile, 
			final int originalImageWidth, 
			final int originalImageHeight,
			final File workImageFile, 
			final int workImageWidth, 
			final int workImageHeight,
			final RectF cropBounds,
			final int rotateDegree,
			final int power,
			final CropImageListener listener){
		

	    ////////////////////////////
		//非同期処理
		//写真を選択範囲で切り抜く
		////////////////////////////
		AsyncTask<Void, Void, File> task = new AsyncTask<Void, Void, File>() {

			private int mWidth,mHeight;
			
			@Override
			protected void onPreExecute() {
				listener.onPreExecute();
			}

			@Override
			protected File doInBackground(Void... params) {
				File imageFile = null;
				Bitmap bitmap;
				
				//現在選択されている範囲を実サイズの大きさに変換
				float widthRate = (float)originalImageWidth / (float)workImageWidth;
				float heightRate = (float)originalImageHeight / (float)workImageHeight;
				RectF boundsArea = new RectF(cropBounds);
				Matrix matrixArea = new Matrix();
				matrixArea.postScale(widthRate,heightRate);
				matrixArea.mapRect(boundsArea);
								
				Log.d("kazino", "CropImageAsyncTask.execute original:" + originalImageWidth + ":" + originalImageHeight);
				Log.d("kazino", "CropImageAsyncTask.execute work:" + workImageWidth + ":" + workImageHeight);
				Log.d("kazino", "CropImageAsyncTask.execute rate:" + widthRate + ":" + heightRate);
				Log.d("kazino", "CropImageAsyncTask.execute rotate:" + rotateDegree);
				Log.d("kazino", "CropImageAsyncTask.execute area:" + boundsArea);

				//実サイズの写真から選択範囲を切り出しファイルに保存
				bitmap = getBitmapFromAPI10(boundsArea);
				if(bitmap == null){
					mWidth = 0;
					mHeight = 0;
					return null;
				}
				
				mWidth = bitmap.getWidth();
				mHeight = bitmap.getHeight();
				
				Log.d("kazino", "CropImageAsyncTask.execute finalImage:" + bitmap.getWidth() + ":" + bitmap.getHeight());

				try {
					imageFile = StorageFileUtil.createNewRandomFile(StorageFileUtil.getTempCacheDir(context),"");
					ImageUtil.storeBitmapToCache(bitmap, imageFile);
					bitmap.recycle();
				} catch (Exception e) {
					Timber.e(e, "");
					throw new RuntimeException(e);
				}
				
		        return imageFile;
			}

			@Override
			protected void onPostExecute(File imageFile) {
				listener.onPostExecute(imageFile,mWidth,mHeight);
			}
			
			private Bitmap getBitmapFromAPI10(RectF boundsArea){
				Log.d("kazino", "CropImageAsyncTask.getBitmapFromAPI10 boundsArea0:" + boundsArea);

				InputStream is;
				Bitmap bitmap = null;
				
				int maxImageSize = ImageUtil.getMaxImageSize(context) * power;
				
				int originalImageWidthBeforeRotate = 0;
				int originalImageHeightBeforeRotate = 0;

				if(rotateDegree == 0){
					originalImageWidthBeforeRotate = originalImageWidth;
					originalImageHeightBeforeRotate = originalImageHeight;
				}
				if(rotateDegree != 0){
					//選択範囲を回転
					Matrix matrix = new Matrix();
					matrix.setRotate(rotateDegree * -1);
					
					if(rotateDegree == -90){
						originalImageWidthBeforeRotate = originalImageHeight;
						originalImageHeightBeforeRotate = originalImageWidth;
						matrix.postTranslate(originalImageWidthBeforeRotate,0);
					}else if(rotateDegree == -180){
						originalImageWidthBeforeRotate = originalImageWidth;
						originalImageHeightBeforeRotate = originalImageHeight;
						matrix.postTranslate(originalImageWidthBeforeRotate, originalImageHeightBeforeRotate);
					}else if(rotateDegree == -270){
						originalImageWidthBeforeRotate = originalImageHeight;
						originalImageHeightBeforeRotate = originalImageWidth;
						matrix.postTranslate(0, originalImageHeightBeforeRotate);
					}
					
					Log.d("kazino", "CropImageAsyncTask.getBitmapFromAPI10 sizeBeforeRotate:" + originalImageWidthBeforeRotate + ":" + originalImageHeightBeforeRotate);

					matrix.mapRect(boundsArea);
					Log.d("kazino", "CropImageAsyncTask.getBitmapFromAPI10 boundsArea1:" + boundsArea);

				}

				//選択エリアを実画像内に入るように修正
				if(boundsArea.left < 0) boundsArea.left = 0;
				if(boundsArea.left > originalImageWidthBeforeRotate) boundsArea.left = originalImageWidthBeforeRotate;
				if(boundsArea.top < 0) boundsArea.top = 0;
				if(boundsArea.top > originalImageHeightBeforeRotate) boundsArea.top = originalImageHeightBeforeRotate;
				if(boundsArea.right < 0) boundsArea.right = 0;
				if(boundsArea.right > originalImageWidthBeforeRotate) boundsArea.right = originalImageWidthBeforeRotate;
				if(boundsArea.bottom < 0) boundsArea.bottom = 0;
				if(boundsArea.bottom > originalImageHeightBeforeRotate) boundsArea.bottom = originalImageHeightBeforeRotate;
				boundsArea.sort();
				Log.d("kazino", "CropImageAsyncTask.getBitmapFromAPI10 boundsArea2:" + boundsArea);

				//選択範囲を部分読み込み
				try {
					is = new FileInputStream(originalImageFile);
					BitmapRegionDecoder bitmapRegionDecoder = BitmapRegionDecoder.newInstance(is, false);
					is.close();
					Rect rect = new Rect((int)boundsArea.left,(int)boundsArea.top,(int)boundsArea.right,(int)boundsArea.bottom);
					double scale = ImageUtil.getImageScale(rect.width(), rect.height(), maxImageSize);
					
					Log.d("kazino", "CropImageAsyncTask.getBitmapFromAPI10 rect:" + rect);
					Log.d("kazino", "CropImageAsyncTask.getBitmapFromAPI10 scale:" + scale);
					Log.d("kazino", "CropImageAsyncTask.getBitmapFromAPI10 bitmapRegionDecoder:" + bitmapRegionDecoder.getWidth() + ":" + bitmapRegionDecoder.getHeight());
					
					
					BitmapFactory.Options options = new BitmapFactory.Options();
					options.inSampleSize = (int) scale; //取り合えず少し大きめに縮尺
					options.inJustDecodeBounds = false;
					options.inPreferredConfig = Config.RGB_565;
					
					bitmap = bitmapRegionDecoder.decodeRegion(rect, options);
					scale = ImageUtil.getImageScale(bitmap.getWidth(), bitmap.getHeight(),maxImageSize);
					if(scale > 1.0D){
						//最終的にサイズが取り扱いできる最大値を超えていたら縮小
						scale = (double) (1.0D / scale);
					    // 最終的なサイズにするための縮小率を求める
						Matrix matrix = new Matrix();
						matrix.postScale((float)scale, (float)scale);
						bitmap = Bitmap.createBitmap(bitmap, 0, 0, 
								bitmap.getWidth(), bitmap.getHeight(), matrix, true);
						
						System.gc();
						System.gc();
						System.gc();
						
					}
					
					//画像を回転
					if(rotateDegree != 0){
						Matrix matrix = new Matrix();
						matrix.setRotate(rotateDegree);
						bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

						System.gc();
						System.gc();
						System.gc();
					}

					Log.d("kazino", "CropImageAsyncTask.getBitmapFromAPI10:" + bitmap.getWidth() + ":" + bitmap.getHeight() + ":" + bitmap.getConfig());

					return bitmap;
				
				} catch (FileNotFoundException e) {
					Timber.e(e,"");
					throw new RuntimeException(e);
				} catch (IOException e) {
					Timber.e(e,"");
					return null;
				}
				
				
				
			}

		};
		
		//タスクの実行
		task.execute();
		
	}
}
