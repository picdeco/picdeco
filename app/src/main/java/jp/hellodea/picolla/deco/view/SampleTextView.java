package jp.hellodea.picolla.deco.view;

import hugo.weaving.DebugLog;
import jp.hellodea.picolla.deco.graphics.parts.PartUtil;
import jp.hellodea.picolla.deco.util.ImageUtil;
import jp.hellodea.picolla.deco.util.Util;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.view.View;

public class SampleTextView extends View {

	private String mText = "a";
	private float mTextSize;
	private String mFontName;
	private Path mPath = new Path();
	private int mPen;
	private Paint[] mPaints;

	public SampleTextView(Context context) {
		super(context);

        initialize();
	}
	
	public SampleTextView(Context context, AttributeSet attrs) {
		super(context, attrs);

        initialize();
	}

    private void initialize(){
        //Blurが効かなくなるため、ハードウェアアクセラレーションをOffにする
        this.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        mTextSize = ImageUtil.getPx(getContext(), 48);
    }

    @DebugLog
	public void setText(String text, float size){
		mText = text;
		mTextSize = ImageUtil.getPx(getContext(), size);
		
		invalidate();
	}

    @DebugLog
	public void setPaints(int pen, Paint paints[],String fontName){
		mPen = pen;
		mPaints = paints;
		mFontName = fontName;
		
		mPaints[0].setStyle(Style.FILL_AND_STROKE);
		
		invalidate();
	}
	
	public int getPen() {
		return mPen;
	}

	public Paint[] getPaints() {
		return mPaints;
	}

	public String getFontName(){
		return mFontName;
	}

	public int getColor(){
		return mPaints[0].getColor();
	}
	
	public int getPenAlpha(){
		return mPaints[0].getAlpha();
	}

	public int getMidColor(){
		if(mPaints[1] != null) return mPaints[1].getColor();
		else return 0;
	}
	
	public int getOutColor(){
		if(mPaints[2] != null) return mPaints[2].getColor();
		else return 0;
	}

	@Override
    @DebugLog
	protected void onDraw(Canvas canvas) {
		
//		canvas.drawColor(Color.GREEN);
		

		if(mPaints != null){
			//文字のパス取得
			mPaints[0].setTextSize(mTextSize);
			mPaints[0].setTypeface(PartUtil.getTypeface(mFontName));
			mPaints[0].getTextPath(mText, 0, mText.length(), ImageUtil.getPx(getContext(), 7), mTextSize + ImageUtil.getPx(getContext(), 7),mPath);
            mPath.close();

            if(mPaints[1].getStrokeWidth() > 0){
                mPaints[1].setTypeface(PartUtil.getTypeface(mFontName));
                mPaints[1].setTextSize(mTextSize);
            }
            if(mPaints[2].getStrokeWidth() > 0){
                mPaints[2].setTypeface(PartUtil.getTypeface(mFontName));
                mPaints[2].setTextSize(mTextSize);
            }


            if(mPaints[2].getStrokeWidth() > 0) canvas.drawPath(mPath, mPaints[2]);
            if(mPaints[1].getStrokeWidth() > 0) canvas.drawPath(mPath, mPaints[1]);

            canvas.drawPath(mPath, mPaints[0]);

		}else{
			Paint paint = new Paint();
			paint.setColor(Color.GREEN);
			paint.setStrokeWidth(20);
			canvas.drawPath(mPath, paint);
		}

	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		
//		RectF bounds = new RectF();
		
//		mPath.computeBounds(bounds, true);
//		setMeasuredDimension(100,100);
	}
}
