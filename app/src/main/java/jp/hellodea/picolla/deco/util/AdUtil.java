package jp.hellodea.picolla.deco.util;

import com.google.android.gms.ads.AdRequest;

/**
 * Created by kazuhiro on 2015/05/12.
 */
public class AdUtil {

    public static AdRequest getAdRequest(){
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("22D3DA0C2BC3689EC81667C9CBD3BFDA")
                .build();

        return adRequest;
    }
}
