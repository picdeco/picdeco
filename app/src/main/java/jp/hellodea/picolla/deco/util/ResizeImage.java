package jp.hellodea.picolla.deco.util;

import java.io.File;

public class ResizeImage {

	//処理後の画像情報
    public File mOriginalImageFile;
    public int mOriginalImageWidth;
    public int mOriginalImageHeight;
    public File mWorkImageFile;
    public int mWorkImageWidth;
    public int mWorkImageHeight;
	
}
