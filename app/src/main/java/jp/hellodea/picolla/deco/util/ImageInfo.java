package jp.hellodea.picolla.deco.util;

import java.io.File;

import android.graphics.Bitmap;

public class ImageInfo {

	public Bitmap bitmap;
    public File file;
    public int width;
    public int height;

    public void recycle(){
        bitmap.recycle();
    }
	
}
