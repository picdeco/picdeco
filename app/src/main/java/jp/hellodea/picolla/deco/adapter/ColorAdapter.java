package jp.hellodea.picolla.deco.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import jp.hellodea.picolla.deco.graphics.ColorTable;
import jp.hellodea.picolla.deco.view.ColorItemView;
import jp.hellodea.picolla.deco.view.ColorItemView_;

/**
 * 色選択画面に表示されるデータ
 * Created by kazuhiro on 2015/04/22.
 */
public class ColorAdapter extends BaseAdapter {
    private Context mContext;

    public int selectedPosition;

    public ColorAdapter(Context c) {
        mContext = c;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return ColorTable.COLORS.length;
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        ColorItemView view;
        if (convertView == null) {  // if it's not recycled, initialize some attributes
            view = ColorItemView_.build(mContext);
        } else {
            view = (ColorItemView)convertView;
        }

        view.bind(Color.parseColor(ColorTable.COLORS[position]), position == selectedPosition);

        return view;
    }
}
