package jp.hellodea.picolla.deco.activity;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import bolts.Continuation;
import bolts.Task;
import hugo.weaving.DebugLog;
import jp.hellodea.picolla.R;
import jp.hellodea.picolla.deco.Const;
import jp.hellodea.picolla.deco.graphics.SavableMatrix;
import jp.hellodea.picolla.deco.graphics.SavablePath;
import jp.hellodea.picolla.deco.graphics.parts.ImageObject;
import jp.hellodea.picolla.deco.graphics.parts.PartObject;
import jp.hellodea.picolla.deco.graphics.parts.PartUtil;
import jp.hellodea.picolla.deco.graphics.parts.PartsGroup;
import jp.hellodea.picolla.deco.graphics.parts.PathObject;
import jp.hellodea.picolla.deco.util.ImageUtil;
import jp.hellodea.picolla.deco.util.StorageFileUtil;
import jp.hellodea.picolla.deco.view.DrawBoardView;
import timber.log.Timber;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.Paint;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.FrameLayout;
import android.widget.Toast;


/**
 * @author kazuhiro
 * 切り取った後の画像に枠を追加する
 */
public class AddPictureFrameActivity extends ActionBarActivity{

	//編集対象の画像
	private Uri mTargetImageUri;

	//ドローエリア
	private DrawBoardView mDrawBoardView;
	private PartsGroup mPartsGroup;

	//お待ちくださいダイアログ
	private ProgressDialog mProgressDialog;

	//画面のコンポーネント
	private FrameLayout mBaseLayout;
	private Button mButtonColor;
	private CheckBox checkBoxOnFrame;
	
	//画像情報
	private int mImageWidth;
	private int mImageHeight;
	private float mImageOffset;
	
	//マスク情報
	private SavablePath mSaveablePathFroMask;
	private SavableMatrix mSaveableMatrixForMask;
	
	//フレームのペンサイズ(塗りつぶしモードの時に使用)
	private float mPenMinWidth;
	private float mPenMaxWidth;
	
	//切り取りモード
	private int mCutMode;
	
	//フレームオブジェクト
	private ArrayList<PathObject> mArrayListFramePaths = new ArrayList<PathObject>();
		
	private int mCurrentPenKind = 1;			//現在のペンの種類
	private float mCurrentPenWidth = 20;		//ペンの幅
	private int mCurrentPenColor = Color.WHITE;		//色
	private int mCurrentPenMidColor = 0;	//色
	private int mCurrentPenOutColor = 0;	//色
	private int mCurrentPenAlpha = 255;		//透過
	private float mPrevPenWidth; //フレームを消す前の幅

	//初期化処理完了フラグ
	private boolean mIsFinishInit = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add_picture_frame);

        //UI取得
        mBaseLayout = (FrameLayout)findViewById(R.id.frameLayout);
        mButtonColor = (Button)findViewById(R.id.buttonColor);
        checkBoxOnFrame = (CheckBox)findViewById(R.id.checkBoxOnFrame);

        //UIイベント設定
        mButtonColor.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				actionShowColorActivity();
			}
		});
        checkBoxOnFrame.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				actionOnOffFrame(isChecked);
			}
		});

        //プログレスダイアログ生成
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getText(R.string.please_wait));


        //パラメータ取得
        Intent intent = getIntent();
        mTargetImageUri = intent.getData();
		mCutMode = intent.getIntExtra("cutMode",0);
        mImageHeight = intent.getIntExtra("height", 0);
        mImageWidth = intent.getIntExtra("width", 0);
        mPenMinWidth = intent.getFloatExtra("penMinWidth", 0.0f); //選択方式が塗りつぶしの場合に利用
        mPenMaxWidth = intent.getFloatExtra("penMaxWidth", 0.0f); //選択方式が塗りつぶしの場合に利用
        try {
			mSaveablePathFroMask = new SavablePath(new JSONArray(intent.getStringExtra("path")));
			mSaveableMatrixForMask = new SavableMatrix(new JSONArray(intent.getStringExtra("matrix")));
		} catch (JSONException e) {
			Timber.e(e,"");
			throw new RuntimeException(e);
		}

        //フレームの幅を考慮した画像サイズを求める
        mImageOffset = 0.0f;
        if(mCutMode == CutPictureActivity.MODE_DRAW) mImageOffset = mPenMaxWidth;
        else mImageOffset = PartUtil.DEFAULT_PEN_SIZES[PartUtil.DEFAULT_PEN_SIZES.length - 1] * 2.0f;

        //描画エリア
	    mDrawBoardView = new DrawBoardView(this);
	    mPartsGroup = new PartsGroup(mDrawBoardView,null,(int)(mImageWidth + mImageOffset), (int)(mImageHeight + mImageOffset),1,
                StorageFileUtil.getTempCacheDir(this));
	    mPartsGroup.setBackground(BitmapFactory.decodeResource(getResources(), R.drawable.background));

	    //ドローエリア設定
	    mBaseLayout.addView(mDrawBoardView);
	    mDrawBoardView.setBackground(BitmapFactory.decodeResource(getResources(),R.drawable.background));
	    
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		mPartsGroup.recycle();
		mDrawBoardView.clear();
		
		mPartsGroup = null;
		mDrawBoardView = null;
	}
	
	
	@Override
    @DebugLog
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);

		if(mIsFinishInit == true) return; //既に処理済みなら何もしない

		mIsFinishInit = true;

		//
		//viewの幅、高さはこのイベントでしかとれないので画像の縮尺の計算をここでする
		//

	    //キャンバス全体が見えるように縮尺
	    mDrawBoardView.autoScale();

	    //キャンパスが中心にくるように移動
	    mDrawBoardView.moveToCenter();
	    	    
	    //初期表示画像作成
	    createInitialImage();
		

	}



	/**
	 * パラメータの画像を受け取りフレームを作成して画面に初期表示
	 */
    @DebugLog
	private void createInitialImage(){

		////////////////////////////
		//非同期処理
		//画像を単色に塗って表示する
		////////////////////////////
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

			@Override
			protected void onPreExecute() {
				mProgressDialog.show();
			}

			@Override
			protected Void doInBackground(Void... params) {
				
		    	//フレーム用パスをIntentから受け取り画面に設定
				PathObject pathObject = new PathObject(mPartsGroup, mSaveablePathFroMask, mSaveableMatrixForMask, 
						mCurrentPenKind,mCurrentPenWidth,mCurrentPenColor,mCurrentPenMidColor,mCurrentPenOutColor,255,false, 1);
				pathObject.setLock(true);
				//パーツを中心に移動
				pathObject.move((int)(mImageOffset / 2.0f),(int)(mImageOffset / 2.0f));
				mArrayListFramePaths.add(pathObject);
								
				//画像表示
		    	ImageObject imageObject =
		        		new ImageObject(mPartsGroup, 0, 0, new File(mTargetImageUri.getPath()),null,false, null,false,1);
		    	imageObject.setLock(true);

		    	//画像をキャンバスの中心に移動
				imageObject.move((int)(mImageOffset / 2.0f),(int)(mImageOffset / 2.0f));

                Task task = mPartsGroup.flashToBitmap();
                try {
                    task.waitForCompletion();
                } catch (InterruptedException e) {
                    Timber.e(e,"waitForCompletion");
                    throw new RuntimeException(e);
                }
                return null;
			}

			@Override
			protected void onPostExecute(Void param) {
                mDrawBoardView.invalidate();
                mProgressDialog.dismiss();

			}
		};
		
		//タスクの実行
		task.execute();
	}
	
	
	/**
	 * フレームの色を指定してColorMatrixを取得する
	 * @param color
	 * @return
	 */
	private ColorMatrix getFrameColorMatrix(int color){
		int red,green,blue;
		
		red = Color.red(color);
		green = Color.green(color);
		blue = Color.blue(color);
		
    	ColorMatrix colorMatrix = new ColorMatrix(
    			new float[] {
                   0, 0, 0, 0, red,
                   0, 0, 0, 0, green,
                   0, 0, 0, 0, blue,
                   0, 0, 0, 255, 0 });
    	return colorMatrix;
	}

	//フレームをOn、Offする
    @DebugLog
	private void actionOnOffFrame(boolean isOn){
		if(isOn == true){
			mCurrentPenWidth = mPrevPenWidth;
		}else{
			mPrevPenWidth = mCurrentPenWidth;
			mCurrentPenWidth = 0.0f;
		}
		changeFrame();
		
	}

	
	//色選択画面を呼び出す
	private void actionShowColorActivity(){
		if(mPartsGroup.getPartsCount() < 2) return;

		Intent intent = new Intent(this,SelectPenActivity.class);
		intent.putExtra("penKind", mCurrentPenKind);
		intent.putExtra("penWidth", mCurrentPenWidth);
		intent.putExtra("penColor", mCurrentPenColor);
		intent.putExtra("penMidColor", mCurrentPenMidColor);
		intent.putExtra("penOutColor", mCurrentPenOutColor);
		intent.putExtra("penAlpha", mCurrentPenAlpha);
		
		//塗りつぶしモードならペンサイズを設定
		if(mCutMode == CutPictureActivity.MODE_DRAW){
			intent.putExtra("penMinWidth", mPenMinWidth);
			intent.putExtra("penMaxWidth", mPenMaxWidth);
		}
		
		startActivityForResult(intent, Const.REQUEST_PEN);
		

	}

	//フレームを合成してメインに戻る
	private void actionCreateNewImageWithFrame(){
		
		//作業中かチェック
		if(mDrawBoardView.getMode() != DrawBoardView.MODE_SCROLL){
			Toast.makeText(this, R.string.please_complete_the_final_task, Toast.LENGTH_SHORT).show();
			return;
		}

		////////////////////////////
		//非同期処理
		//フレームを合成する
		////////////////////////////
		AsyncTask<Void, Integer, File> task = new AsyncTask<Void, Integer, File>() {

			private int mHeight, mWidth;
			
			@Override
			protected void onPreExecute() {
				mProgressDialog.show();
			}

			@Override
			protected File doInBackground(Void... params) {
				//張付け先キャンバス
				System.gc();
				Bitmap distBitmap = Bitmap.createBitmap(
						mPartsGroup.getBitmap().getWidth(), mPartsGroup.getBitmap().getHeight(), 
						Bitmap.Config.ARGB_8888);
				Canvas distCanvas = new Canvas(distBitmap);

				//フレームと画像を貼付け
				Paint paint = new Paint();
				for (int i = 0; i < mPartsGroup.getPartsCount(); i++) {
					PartObject part = mPartsGroup.getPart(i);
					part.flashToBitmap(distCanvas, null);
				}
								
				//余白を取り除く
				System.gc();
				HashMap<String, Object> ret = ImageUtil.getRealSizeBitmap(distBitmap);
				distBitmap = (Bitmap) ret.get("bitmap");
				mHeight = distBitmap.getHeight();
				mWidth = distBitmap.getWidth();
				
				//切り取り完了画像をファイルへ保存する
		    	File cacheFile = StorageFileUtil.createNewRandomFile(StorageFileUtil.getTempCacheDir(AddPictureFrameActivity.this), "");
				try {
					ImageUtil.storeBitmapToCache(distBitmap, cacheFile);
				} catch (Exception e) {
					Timber.e(e,"");
					throw new RuntimeException(e);
				}

				distBitmap.recycle();
				
				//メモリ解放
				mPartsGroup.recycle();
				mDrawBoardView.clear();

				return cacheFile;
			}

			@Override
			protected void onPostExecute(File cacheFile) {
				mProgressDialog.dismiss();
				
				//切り取り完了後メイン編集画面へ遷移
				Intent send_intent = new Intent(AddPictureFrameActivity.this,MainEditorActivity.class);
				send_intent.setData(Uri.fromFile(cacheFile));
				send_intent.putExtra("executeMode", Const.REQUEST_MODIFY_IMAGE);
				send_intent.putExtra("height", mHeight);
				send_intent.putExtra("width", mWidth);
                send_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

				startActivity(send_intent);

				finish();

			}

		};
		
		//タスクの実行
		task.execute();
		

	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		
		if(resultCode == RESULT_OK){
			
			switch(requestCode){
			case Const.REQUEST_PEN:
				
				mCurrentPenKind = intent.getIntExtra("penKind", 0);
				mCurrentPenWidth = intent.getFloatExtra("penWidth", 0);
				mCurrentPenColor = intent.getIntExtra("penColor", 0);
				mCurrentPenMidColor = intent.getIntExtra("penMidColor", 0);
				mCurrentPenOutColor = intent.getIntExtra("penOutColor", 0);
				mCurrentPenAlpha = intent.getIntExtra("penAlpha", 0);

				//内容変更
				if(checkBoxOnFrame.isChecked())
					changeFrame();
				else
					checkBoxOnFrame.setChecked(true);

				break;
				
			}
			
			
		}
		
	}
	
	//現在のペン情報でフレームを再作成
    @DebugLog
	private void changeFrame(){
		//内容変更
		//オリジナルをコピーして新しいパーツを作る。その後オリジナルは削除
		for (int i = 0; i < mArrayListFramePaths.size(); i++) {
			PathObject oldPathObject = mArrayListFramePaths.get(i);
			SavablePath path = new SavablePath(oldPathObject.getPath().copyJsonPath());
			SavableMatrix matrix = new SavableMatrix(oldPathObject.getMatrix());
			int position = mPartsGroup.getPartPosition(oldPathObject);
			mPartsGroup.removePart(oldPathObject, false);
			PathObject newObject = new PathObject(mPartsGroup, path,matrix, mCurrentPenKind,
					mCurrentPenWidth,mCurrentPenColor,mCurrentPenMidColor,mCurrentPenOutColor,mCurrentPenAlpha, false, Const.DISPLAY_SAMPLE_SIZE);
			newObject.setLock(true);
			mPartsGroup.changePosition(newObject, position, false);

			mArrayListFramePaths.set(i, newObject);
		}

        Task task = mPartsGroup.flashToBitmap();
        task.onSuccess(new Continuation() {
            @Override
            public Object then(Task task) throws Exception {
                mDrawBoardView.invalidate();
                return null;
            }
        },Task.UI_THREAD_EXECUTOR);

	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater menuInflater = this.getMenuInflater();
        menuInflater.inflate(R.menu.menu_add_picture_frame,menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()){
            case R.id.actionOk:
                actionCreateNewImageWithFrame();
                return true;
        }

        return false;
    }


}
