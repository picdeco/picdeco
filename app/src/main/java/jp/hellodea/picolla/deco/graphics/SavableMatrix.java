package jp.hellodea.picolla.deco.graphics;

import org.json.JSONArray;
import org.json.JSONException;

import android.graphics.Matrix;

public class SavableMatrix extends Matrix {

	
	public SavableMatrix(){
		super();
		
		reset();
	}
	
	public SavableMatrix(Matrix src) {
		super(src);
	}

	public SavableMatrix(JSONArray jsonArray) throws JSONException {
		
		float[] matrixVals = new float[9];
		
		for (int i = 0; i < jsonArray.length(); i++) {
			matrixVals[i] = (float)(jsonArray.getDouble(i));
		}
		
		setValues(matrixVals);
		
	}

	//データをjson形式に変換
	public JSONArray getJson() throws JSONException{
		JSONArray jsonArray = new JSONArray();
		
		float[] matrixVals = new float[9];
		getValues(matrixVals);

		for (int i = 0; i < matrixVals.length; i++) {
			jsonArray.put(matrixVals[i]);
		}
		
		return jsonArray;
	}
}
