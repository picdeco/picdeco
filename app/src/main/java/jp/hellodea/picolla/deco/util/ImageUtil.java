package jp.hellodea.picolla.deco.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;

import hugo.weaving.DebugLog;
import jp.hellodea.picolla.deco.Const;
import jp.hellodea.picolla.deco.graphics.WidthHeight;
import timber.log.Timber;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;

public class ImageUtil {

	//画像最大サイズを取得(メモリクラスを考慮)
	@DebugLog
	public static int getMaxImageSize(Context context){

		int availableMemory = ContextUtil.getAvailableMemoryMega(context);

		//解像度は利用可能メモリの1/8の容量
		double size = Math.sqrt(availableMemory / 8.0 * 1024.0 * 1024.0 / 4.0);

		if(size > 1920) size = 1920; //最大サイズ

		return (int) (size / 2); //編集中の利用のためサイズの半分を返す
	}

	//解像度を考慮したピクセル値を返す
	public static int getPx(Context context, float dp){
		// Densityの値を取得
		float tmpDensity = context.getResources().getDisplayMetrics().density;

		// ピクセル値を求める
		int tmpPx = (int)(dp * tmpDensity + 0.5f );

		return tmpPx;
	}


	//画像の大きさの比率を計算する
    @DebugLog
	public static double getImageScale(InputStream is, int maxSize){

		//画像の読み込みサイズを調べる
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;

		BitmapFactory.decodeStream(is, null, options);
		
		double rate =  getImageScale(options.outWidth, options.outHeight, maxSize);
		
		return rate;
		
	}
	
	//画像の大きさの比率を計算する
    @DebugLog
	public static double getImageScale(int width, int height, int maxSize){
		double rate = (double)(width * height) / (double)(maxSize * maxSize);
		rate = Math.sqrt(rate); //1辺の縮小率に変換
		
		return rate;
	}


    /**
     * 画像を指定されたサイズに縮小しキャッシュファイルに保存する
     * @param inputStream
     * @param cacheDir
     * @param maxSize
     * @param displaySampleSize
     * @return
     * @throws Exception
     */
    @DebugLog
    public static ImageInfo decodeAndStoreResizedImage(
            InputStream inputStream, File cacheDir, int maxSize, int sampleSize, int displaySampleSize, float degree) throws IOException {

        File cacheFile = StorageFileUtil.createNewRandomFile(cacheDir,"");
        ImageInfo imageInfo = new ImageInfo();

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = sampleSize;
        options.inJustDecodeBounds = false;

        Bitmap bitmap = BitmapFactory.decodeStream(inputStream,null,options);

        //リサイズ
        bitmap = ImageUtil.resize(bitmap, maxSize,maxSize);

        //回転
        if(degree != 0){
            bitmap = rotateBitmap(bitmap,degree);
        }

        //キャッシュに保存
        storeBitmapToCache(bitmap,cacheFile);

        //画面表示用にリサイズ
        if(displaySampleSize > 1){
            bitmap = resize(bitmap,displaySampleSize);
        }

        imageInfo.bitmap = bitmap;
        imageInfo.file = cacheFile;
        imageInfo.width = bitmap.getWidth();
        imageInfo.height = bitmap.getHeight();

        return imageInfo;
    }

    /**
     * 画像を指定されたサイズに縮小しキャッシュファイルに保存する
     * @param context
     * @param imageUri
     * @param cacheDir
     * @param maxSize
     * @param displaySampleSize
     * @return
     * @throws Exception
     */
    @DebugLog
	public static ImageInfo decodeAndStoreResizedImage(
            Context context, Uri imageUri, File cacheDir, int maxSize, int displaySampleSize) throws IOException {

        File file = UriUtil.getFileFromUri(context,imageUri);

        ImageInfo imageInfo = decodeAndStoreResizedImage(file,cacheDir,maxSize,displaySampleSize);

        return imageInfo;

	}

    /**
     * 画像を指定されたサイズに縮小しキャッシュファイルに保存する
     * @param imageFile
     * @param cacheDir
     * @param maxSize
     * @param displaySampleSize
     * @return
     * @throws Exception
     */
    @DebugLog
    public static ImageInfo decodeAndStoreResizedImage(
            File imageFile, File cacheDir, int maxSize, int displaySampleSize) throws IOException {

        float degree = ImageUtil.getOrientation(imageFile);

        InputStream inputStream = new FileInputStream(imageFile);

        //読込サンプルサイズを求める
        int sampleSize = (int) getImageScale(inputStream,maxSize);
        inputStream.close();

        Timber.d("sampleSize: %d",sampleSize);

        inputStream = new FileInputStream(imageFile);

        //Bitmapを読み込む
        ImageInfo imageInfo = decodeAndStoreResizedImage(
                inputStream,cacheDir,maxSize,sampleSize,displaySampleSize,degree);

        inputStream.close();

        return imageInfo;

    }

	//ビットマップの余白(透明部分)を取り除いたサイズのビットマップを取得する
	public static HashMap<String, Object> getRealSizeBitmap(Bitmap bitmap){
		HashMap<String, Object> ret = new HashMap<String, Object>();
		
		int width = bitmap.getWidth() - 1; //ループで使うので実際のサイズ−１する
		int height = bitmap.getHeight() - 1; //ループで使うので実際のサイズ−１する
		int top = 0, bottom = 0, left = 0, right = 0; //透明以外が書かれている各座標位置
		boolean searched = false;
		
		//TOPの座標を求める
		searched = false;
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				if(bitmap.getPixel(x, y) != 0){
					top = y;
					searched = true;
					break;
				}
			}
			if(searched == true) break;
		}

		//BOTTOMの座標を求める
		searched = false;
		for (int y = height; y >= 0; y--) {
			for (int x = 0; x < width; x++) {
				if(bitmap.getPixel(x, y) != 0){
					bottom = y;
					searched = true;
					break;
				}
			}
			if(searched == true) break;
		}
		
		//LEFTの座標を求める
		searched = false;
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if(bitmap.getPixel(x, y) != 0){
					left = x;
					searched = true;
					break;
				}
			}
			if(searched == true) break;
		}

		//RIGHTの座標を求める
		searched = false;
		for (int x = width; x >= 0; x--) {
			for (int y = 0; y < height; y++) {
				if(bitmap.getPixel(x, y) != 0){
					right = x;
					searched = true;
					break;
				}
			}
			if(searched == true) break;
		}
		
		ret.put("bitmap", Bitmap.createBitmap(bitmap, left, top, right - left + 1, bottom - top + 1));
		ret.put("top", top); //カットしたサイズ
		ret.put("bottom", bottom); //カットしたサイズ
		ret.put("left", left); //カットしたサイズ
		ret.put("right", right); //カットしたサイズ

		return ret;
	}
	
	
	/**
	 * ビットマップにカラーフィルターを適用する
	 * @param bitmap
	 * @param colorMatrix
	 * @return
	 */
	public static void applyColorMatrix(Bitmap bitmap, ColorMatrix colorMatrix){
		Paint paint = new Paint();
		Canvas canvas = new Canvas(bitmap);
		
		paint.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
		
		canvas.drawBitmap(bitmap, 0, 0, paint);

	}
	
	/**
	 * ビットマップにトーンカーブを適用する
	 * @param bitmap
	 * @param redCurve
	 * @param greenCurve
	 * @param blueCurve
	 */
	public static void applyToneCurve(Bitmap bitmap,int[] curve, int[] redCurve, int[] greenCurve, int[] blueCurve) {
		// image size
		int width = bitmap.getWidth();
		int height = bitmap.getHeight();

		// color information
		int A, R, G, B;
		int pixel;

		// scan through all pixels
		for(int x = 0; x < width; ++x) {
			for(int y = 0; y < height; ++y) {
				// get pixel color
				pixel = bitmap.getPixel(x, y);
				A = Color.alpha(pixel);
				R = Color.red(pixel);
				G = Color.green(pixel);
				B = Color.blue(pixel);

				if(curve != null){
					R = curve[R];
					if(R > 255) { R = 255; }
					else if(R < 0) { R = 0; }
					G = curve[G];
					if(G > 255) { G = 255; }
					else if(G < 0) { G = 0; }
					B = curve[B];
					if(B > 255) { B = 255; }
					else if(B < 0) { B = 0; }
				}

				if(redCurve != null){
					R = redCurve[R];
					if(R > 255) { R = 255; }
					else if(R < 0) { R = 0; }
				}

				if(greenCurve != null){
					G = greenCurve[G];
					if(G > 255) { G = 255; }
					else if(G < 0) { G = 0; }
				}

				if(blueCurve != null){
					B = blueCurve[B];
					if(B > 255) { B = 255; }
					else if(B < 0) { B = 0; }
				}


				// apply new pixel color to output bitmap
				bitmap.setPixel(x, y, Color.argb(A, R, G, B));
			}
		}
	}

	
	/**
	 * キャッシュファイルからビットマップを復元
	 * @return
	 */
    @DebugLog
	public static Bitmap restoreImageFromBufferCache(File cacheFile, int sampleSize, Config config) throws IOException, FileNotFoundException{
    	Bitmap bitmap;

		InputStream is = new FileInputStream(cacheFile);

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = sampleSize;
		options.inJustDecodeBounds = false;
		options.inPreferredConfig = config;
		bitmap = BitmapFactory.decodeStream(is,null,options);
		
        is.close();
        
        return bitmap;
	}
	
	/**
	 * キャッシュファイルにビットマップを書き出し
	 */
    @DebugLog
	public static void storeBitmapToCache(Bitmap bitmap, File cacheFile) throws IOException {

    	OutputStream os = new FileOutputStream(cacheFile);
    	
		Bitmap.CompressFormat format;
//		if(bitmap.hasAlpha() == true)
			format = Bitmap.CompressFormat.PNG;
//		else
//			format = Bitmap.CompressFormat.JPEG;
//画像が劣化しないようにPNGでしか中間ファイルは保存しない
		
		bitmap.compress(format,100,os);
		
		os.close();
		
	}

    //指定されたサンプルサイズに縮小する
    public static Bitmap resize(Bitmap bitmap, int sampleSize){
        bitmap = resize(bitmap,bitmap.getWidth() / sampleSize, bitmap.getHeight() / sampleSize);
        return bitmap;
    }
	
	//画像を指定されたサイズに比率を保持したまま縮小
	public static Bitmap resize(Bitmap originalBitmap, int width, int height){
		Bitmap newBitmap;
		int originalWidth = originalBitmap.getWidth();
		int originalHeight = originalBitmap.getHeight();
		float widthRate = (float)width / (float)originalWidth;
		float heightRate = (float)height / (float)originalHeight;
		float rate = Math.min(widthRate, heightRate);

        //縮小の必要がなければ何もしない
        if(rate >= 1.0f) return originalBitmap;

		Matrix matrix = new Matrix();
		matrix.setScale(rate, rate);
		newBitmap = Bitmap.createBitmap(originalBitmap, 0, 0, originalWidth, originalHeight, matrix, true);
		
		return newBitmap;
	}

	//画像を指定されたサイズに変更（比率は保持しない）
	public static Bitmap resizeFree(Bitmap originalBitmap, int width, int height){
		Bitmap newBitmap;
		int originalWidth = originalBitmap.getWidth();
		int originalHeight = originalBitmap.getHeight();
		float widthRate = (float)width / (float)originalWidth;
		float heightRate = (float)height / (float)originalHeight;
		Matrix matrix = new Matrix();
		matrix.setScale(widthRate, heightRate);
		newBitmap = Bitmap.createBitmap(originalBitmap, 0, 0, originalWidth, originalHeight, matrix, true);
		
		return newBitmap;
	}
	
	//指定されたサイズ内で最大となる画像サイズを返す
	public static WidthHeight calcMaxImageSize(int width, int height, int maxSize){
		WidthHeight widthHeight = new WidthHeight();
		
		int size = Math.max(width, height);
		float rate = (float)maxSize / (float)size;
		
		widthHeight.width = (int)((float)width * rate);
		widthHeight.height = (int)((float)height * rate);
		
		return widthHeight;
	}

	//指定されたサイズ内で最大となる画像サイズを返す
	public static WidthHeight calcMaxImageSize(int width, int height, int maxWidth, int maxHeight){
		WidthHeight widthHeight = new WidthHeight();

		float rate = 0;
		if(width <= maxWidth && height <= maxHeight){
			//最大サイズより小さければ
			rate = Math.min((float)maxWidth / (float)width, (float)maxHeight / (float)height);
		}else if(width > maxWidth && height <= maxHeight){
			//もし横幅だけ最大サイズより大きければ
			rate = (float)maxWidth / (float)width;
		}else if(width <= maxWidth && height > maxHeight){
			//もし縦幅だけ最大サイズより大きければ
			rate = (float)maxHeight / (float)height;
		}else if(width > maxWidth && height > maxHeight){
			//最大サイズより大きければ
			rate = Math.min((float)maxWidth / (float)width, (float)maxHeight / (float)height);
		}
		
		widthHeight.width = (int)((float)width * rate);
		widthHeight.height = (int)((float)height * rate);
		
		return widthHeight;
	}

	//画像を指定されたサイズで切り取り
    @DebugLog
	public static Bitmap cropImageAtCenter(Bitmap bitmap, int width, int height){
		
		int x = (bitmap.getWidth() - width) / 2;
		int y = (bitmap.getHeight() - height) / 2;
		
		if(x < 0){
			x = 0;
			width = bitmap.getWidth();
		}
		
		if(y < 0){
			y = 0;
			height = bitmap.getHeight();
		}

		return cropImage(bitmap, x, y, width, height);
	}

	//画像を指定された場所で切る抜き
	public static Bitmap cropImage(Bitmap bitmap,int x, int y, int width, int height){
		Bitmap newBitmap;
		
		if(x < 0){
			width += x;
			x = 0;
		}
		
		if(y < 0){
			height += y;;
			y = 0;
		}
		
		if((x + width) > bitmap.getWidth()) width = bitmap.getWidth() - x;

		if((y + height) > bitmap.getHeight()) height = bitmap.getHeight() - y;
		

		newBitmap = Bitmap.createBitmap(bitmap, x, y, width, height);
		
		return newBitmap;
	}

    //画像を回転させる
    public static Bitmap rotateBitmap(Bitmap bitmap, float degree){
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap bitmapNew = Bitmap.createBitmap(bitmap,0,0,bitmap.getWidth(),bitmap.getHeight(),matrix,false);

        return bitmapNew;
    }

    //画像ファイルの向きを取得する
    public static float getOrientation(File imageFile) throws IOException{
        ExifInterface exifInterface;
        try {
            exifInterface = new ExifInterface(imageFile.getPath());
        } catch (IOException e) {
            Timber.e(e, "ExifInterface");
            throw e;
        }
        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION,-1);
        Timber.d("TAG_ORIENTATION %d",orientation);

        switch (orientation){
            case ExifInterface.ORIENTATION_NORMAL:
                return 0;
            case ExifInterface.ORIENTATION_ROTATE_90:
                return 90;
            case ExifInterface.ORIENTATION_ROTATE_180:
                return 180;
            case ExifInterface.ORIENTATION_ROTATE_270:
                return 270;
            default:
                return 0;
        }

    }
}
