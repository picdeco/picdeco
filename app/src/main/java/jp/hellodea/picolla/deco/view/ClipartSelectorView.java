package jp.hellodea.picolla.deco.view;

import java.io.File;
import java.util.List;
import java.util.concurrent.Callable;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.widget.GridView;
import android.widget.LinearLayout;

import javax.inject.Inject;

import bolts.Continuation;
import bolts.Task;
import de.greenrobot.event.EventBus;
import hugo.weaving.DebugLog;
import jp.hellodea.picolla.R;
import jp.hellodea.picolla.common.eventbus.BusEvent;
import jp.hellodea.picolla.common.eventbus.ErrorEvent;
import jp.hellodea.picolla.common.exception.LogicException;
import jp.hellodea.picolla.common.objectgraph.ObjectGraphHolder;
import jp.hellodea.picolla.deco.adapter.ClipartAdapter;
import jp.hellodea.picolla.deco.entity.Clipart;
import jp.hellodea.picolla.deco.entity.ClipartKind;
import jp.hellodea.picolla.deco.eventbus.ProgressDialogEvent;
import jp.hellodea.picolla.deco.logic.AssetLogic;
import jp.hellodea.picolla.deco.util.AnimationUtil;

@EViewGroup(resName = "view_clipart_selector")
public class ClipartSelectorView extends LinearLayout {

    /**
     * クリップアート選択時のイベント
     */
    public static class ClipArtSelectedEvent extends BusEvent{
        public ClipArtSelectedEvent(ClipartKind kind, Clipart clipart, File file) {
            super(ClipArtSelectedEvent.class,kind,clipart,file);
        }
    }

    @Inject
    AssetLogic assetLogic;

    @Click
    void buttonClose(){
        actionClose();
    }

    @ViewById
    LinearLayout layoutKind;

    @ViewById
    GridView viewClipList;

    @ItemClick
    void viewClipListItemClicked(int position){
        actionSelectClipart(position);
    }

    private String currentCategory = null;
    private ClipartKind currentKind = null;

    private boolean isProcessing = false; //同時実行を制限するためのフラグ


	public ClipartSelectorView(Context context) {
		super(context);
	}

	public ClipartSelectorView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public void initialize(){
        ObjectGraphHolder.get().inject(this);
	}

	//カテゴリ内のクリップアートを表示
    @DebugLog
	public void show(String categoryName){
		setVisibility(View.VISIBLE);
		
		startAnimation(AnimationUtil.inFromBottom());

        getKindsInCategory(categoryName);
		
	}

    @Background
    void getKindsInCategory(String categoryName){
        if(currentCategory != null && currentCategory.equals(categoryName))
            return; //カテゴリが前回のカテゴリと同じなら何もしない

        currentCategory = categoryName;
        currentKind = null;

        try {
            List<ClipartKind> kinds = assetLogic.getKindsInCategory(categoryName);
            constructView(kinds);
        }catch (LogicException e){
            EventBus.getDefault().post(new ErrorEvent(this.getClass(),e));
        }
    }

	//クリップアート画面全体を設定
    @DebugLog
    @UiThread
	public void constructView(List<ClipartKind> kinds){

        //表示済みのビューをクリアする
        layoutKind.removeAllViews();
        viewClipList.setAdapter(null);

        //区分ごとに一番初めのクリップアートをボタン表示
        for(ClipartKind kind: kinds){
            SelectableImageView clipartItem =
                    SelectableImageView_.build(getContext());

            clipartItem.bind(Uri.parse(kind.getThumbUrl()));

            clipartItem.setTag(kind); // 種類を埋め込み

            layoutKind.addView(clipartItem);

            //クリップアートにイベント追加
            clipartItem.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    //クリックされたら区分選択アクションを呼び出し
                    actionSelectKind((ClipartKind) v.getTag());
                }
            });
        }

		//初めの区分のクリップアートを初期表示
		actionSelectKind(kinds.get(0));
	}
	
		
	//種類選択アクション
	private void actionSelectKind(ClipartKind kind){
		if(currentKind != null && currentKind.getObjectId().equals(kind.getObjectId())) return;

        if(isProcessing == true) return;

		currentKind = kind;
        isProcessing = true;

        Task<List<Clipart>> task = Task.callInBackground(new Callable<List<Clipart>>() {
            @Override
            public List<Clipart> call() throws Exception {
                return assetLogic.getClipartsInKind(currentKind);
            }
        }).onSuccess(new Continuation<List<Clipart>, List<Clipart>>() {
            @Override
            public List<Clipart> then(Task<List<Clipart>> task) throws Exception {
                viewClipList.setAdapter(
                        new ClipartAdapter(
                                getContext(),
                                R.layout.item_selectable_image,
                                task.getResult()));

                //選択状態を更新
                for (int j = 0; j < layoutKind.getChildCount(); j++) {
                    SelectableImageView item = (SelectableImageView) layoutKind.getChildAt(j);
                    ClipartKind itemKind = (ClipartKind) item.getTag();
                    if(itemKind.getObjectId().equals(currentKind.getObjectId()))
                        item.setSelectedStatus(true);
                    else
                        item.setSelectedStatus(false);
                }

                return null;
            }
        },Task.UI_THREAD_EXECUTOR).continueWith(new Continuation<List<Clipart>, List<Clipart>>() {
            @Override
            public List<Clipart> then(Task<List<Clipart>> task) throws Exception {
                if(task.isFaulted()){
                    EventBus.getDefault().post(new ErrorEvent(this.getClass(), task.getError()));
                }

                isProcessing = false;

                return null;
            }
        },Task.UI_THREAD_EXECUTOR);

	}

	//クリップアート選択アクション
	private void actionSelectClipart(final int position){

        EventBus.getDefault().postSticky(new ProgressDialogEvent(
                ProgressDialogEvent.Status.START, R.string.clipart_downloading_message, ProgressDialog.STYLE_SPINNER));

        final Clipart clipart = (Clipart) viewClipList.getAdapter().getItem(position);

        Task.callInBackground(new Callable<File>() {
            @Override
            public File call() throws Exception {
                String url = clipart.getUrl();
                File file = assetLogic.getAssetFile(getContext(), url);
                return file;
            }
        }).continueWith(new Continuation<File, Object>() {
            @Override
            public Object then(Task<File> task) throws Exception {
                EventBus.getDefault().postSticky(new ProgressDialogEvent(ProgressDialogEvent.Status.END, 0,0));

                if(task.isFaulted()){
                    EventBus.getDefault().post(new ErrorEvent(this.getClass(),task.getError()));
                    return null;
                }

                EventBus.getDefault().post(
                        new ClipArtSelectedEvent(currentKind, clipart, task.getResult()));

                actionClose();
                return null;
            }
        },Task.UI_THREAD_EXECUTOR);

	}

	public void actionClose(){
		startAnimation(AnimationUtil.outToBottom());
		setVisibility(View.GONE);

	}


}
