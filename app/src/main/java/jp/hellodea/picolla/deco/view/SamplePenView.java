package jp.hellodea.picolla.deco.view;

import jp.hellodea.picolla.deco.util.ImageUtil;
import jp.hellodea.picolla.deco.util.Util;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

public class SamplePenView extends View {

	private Path mPath = new Path();
	private int mPen;
	private Paint[] mPaints;
	
	public SamplePenView(Context context) {
		super(context);

        initialize();
		

	}
	
	public SamplePenView(Context context, AttributeSet attrs) {
		super(context, attrs);

        initialize();

	}

    private void initialize(){
        //Blurが効かなくなるため、ハードウェアアクセラレーションをOffにする
        this.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        mPath.moveTo(ImageUtil.getPx(getContext(), 15), ImageUtil.getPx(getContext(), 15));
        mPath.lineTo(ImageUtil.getPx(getContext(), 60), ImageUtil.getPx(getContext(), 60));

    }

	private void setPath(Path path){
		mPath = path;
	}

	public void setPaints(int pen, Paint paints[]){
		mPen = pen;
		mPaints = paints;
		
		invalidate();
	}
	
	public int getPen() {
		return mPen;
	}

	public Paint[] getPaints() {
		return mPaints;
	}

	public float getPenWidth(){
		return mPaints[0].getStrokeWidth();
	}

	public int getColor(){
		return mPaints[0].getColor();
	}
	
	public int getMidColor(){
		if(mPaints[1] != null) return mPaints[1].getColor();
		else return 0;
	}
	
	public int getOutColor(){
		if(mPaints[2] != null) return mPaints[2].getColor();
		else return 0;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		
//		canvas.drawColor(Color.parseColor("#DEDFE0"));
		
		if(mPaints != null){
			for (int i = 2; i >= 0; i--) {
				if(mPaints[i] != null && mPaints[i].getStrokeWidth() > 0) canvas.drawPath(mPath, mPaints[i]);
			}
		}else{
			Paint paint = new Paint();
			paint.setColor(Color.GREEN);
			paint.setStrokeWidth(ImageUtil.getPx(getContext(), 15));
			canvas.drawPath(mPath, paint);
		}
		
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		setMeasuredDimension(ImageUtil.getPx(getContext(), 75),ImageUtil.getPx(getContext(), 75));
	}
}
