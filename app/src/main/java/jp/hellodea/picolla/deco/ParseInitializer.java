package jp.hellodea.picolla.deco;

import android.content.Context;

import com.parse.Parse;
import com.parse.ParseCrashReporting;
import com.parse.ParseInstallation;
import com.parse.ParseObject;

import hugo.weaving.DebugLog;
import jp.hellodea.picolla.common.ParseConst;
import jp.hellodea.picolla.deco.entity.AppUser;
import jp.hellodea.picolla.deco.entity.Clipart;
import jp.hellodea.picolla.deco.entity.ClipartHist;
import jp.hellodea.picolla.deco.entity.ClipartKind;
import jp.hellodea.picolla.deco.entity.CuttingShape;
import jp.hellodea.picolla.deco.entity.Effect;
import jp.hellodea.picolla.deco.entity.Filter;
import jp.hellodea.picolla.deco.entity.Font;
import jp.hellodea.picolla.deco.entity.PenHist;
import jp.hellodea.picolla.deco.entity.SampleEntity;
import jp.hellodea.picolla.deco.entity.TextHist;
import jp.hellodea.picolla.deco.entity.Work;

/**
 * Created by kazuhiro on 2015/02/10.
 */
public class ParseInitializer {
    @DebugLog
    public static void initialize(Context context){

        ParseObject.registerSubclass(SampleEntity.class);

        ParseObject.registerSubclass(AppUser.class);
        ParseObject.registerSubclass(ClipartKind.class);
        ParseObject.registerSubclass(Clipart.class);
        ParseObject.registerSubclass(ClipartHist.class);
        ParseObject.registerSubclass(Work.class);
        ParseObject.registerSubclass(Filter.class);
        ParseObject.registerSubclass(Effect.class);
        ParseObject.registerSubclass(TextHist.class);
        ParseObject.registerSubclass(PenHist.class);
        ParseObject.registerSubclass(Font.class);
        ParseObject.registerSubclass(CuttingShape.class);

        Parse.enableLocalDatastore(context);
        ParseCrashReporting.enable(context);

        Parse.initialize(context, ParseConst.APPLICATION_ID, ParseConst.CLIENT_ID);
        ParseInstallation.getCurrentInstallation().saveInBackground();

    }

}
