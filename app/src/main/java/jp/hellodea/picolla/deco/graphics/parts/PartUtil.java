package jp.hellodea.picolla.deco.graphics.parts;

import java.io.File;
import java.util.HashMap;
import java.util.Set;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.BlurMaskFilter;
import android.graphics.BlurMaskFilter.Blur;
import android.graphics.Color;
import android.graphics.EmbossMaskFilter;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Join;
import android.graphics.Paint.Style;
import android.graphics.Shader.TileMode;
import android.graphics.Typeface;

import jp.hellodea.picolla.R;
import jp.hellodea.picolla.deco.util.StorageFileUtil;

public class PartUtil {

	private static HashMap<String, Typeface> mTypefaces = new HashMap<String, Typeface>(); //フォントアセット
	private static HashMap<Integer, Bitmap> mBitmaps = new HashMap<Integer, Bitmap>(); //ビットマップアセット

	//ペンサイズ
	public static final int PEN_STEP_LENGTH = 15;
	public static final float[] DEFAULT_PEN_SIZES = {2,5,10,15,20,25,30,35,40,45,50,55,60,65,70};
	public static float[] mPenSizes = {2,5,10,15,20,25,30,35,40,45,50,55,60,65,70};
	
	//ペンサイズをリセットする
	public static void resetPenSize(){
		mPenSizes = new float[PEN_STEP_LENGTH];
		System.arraycopy(DEFAULT_PEN_SIZES, 0, mPenSizes, 0, PEN_STEP_LENGTH);
	}
	
	//ペンのサイズを返す
	public static int getPenSize(float width){
		
		for (int i = 0; i < mPenSizes.length; i++) {
			if(width <= mPenSizes[i]) return i;
		}
		
		return 0;
	}
	
	//ペンサイズを設定
	public static void setPenSize(float min, float max){
		//maxとminで与えられた範囲を１５段階に分割しサイズの配列に設定
		float step = (max - min) / PEN_STEP_LENGTH;
		for (int i = 0; i < mPenSizes.length; i++) {
			mPenSizes[i] = min + step * i;
		}
	}

	
	//指定されたペンを作成(倍率指定なし)
	public static Paint[] getPaint(int kind, float width, int color, int midColor, int outColor, int alpha){
		return getPaint(kind, width, color, midColor, outColor, alpha, 1.0f);
	}

	//指定されたペンを作成(倍率指定あり：実際の画像出力時に使用)
	public static Paint[] getPaint(int kind, float width, int color, int midColor, int outColor, int alpha, float power){
		Bitmap shaderBitmap; //背景ビットマップ

		Paint paint = new Paint();
		Paint midPaint = new Paint();
		Paint outPaint = new Paint();
		
		paint.setAntiAlias(true);
		midPaint.setAntiAlias(true);
		outPaint.setAntiAlias(true);

		paint.setStyle(Style.STROKE);
		midPaint.setStyle(Style.STROKE);
		outPaint.setStyle(Style.STROKE);

		paint.setStrokeCap(Cap.ROUND);
		midPaint.setStrokeCap(Cap.ROUND);
		outPaint.setStrokeCap(Cap.ROUND);

		paint.setStrokeJoin(Join.ROUND);
		midPaint.setStrokeJoin(Join.ROUND);
		outPaint.setStrokeJoin(Join.ROUND);
		
		//透過を反映した色
		color = Color.argb(alpha, Color.red(color), Color.green(color), Color.blue(color));
		midColor = Color.argb(alpha, Color.red(midColor), Color.green(midColor), Color.blue(midColor));
		outColor = Color.argb(alpha, Color.red(outColor), Color.green(outColor), Color.blue(outColor));
		

		switch (kind) {
		case 1:
			paint.setColor(color);
			paint.setStrokeWidth(width * power);

			midPaint.setColor(0); //ダミー
			midPaint.setStrokeWidth(-1); //ダミー
			outPaint.setColor(0); //ダミー
			outPaint.setStrokeWidth(-1); //ダミー
			break;
		case 2:
			paint.setColor(color);
			paint.setStrokeWidth(width * power);
			midPaint.setColor(midColor);
			midPaint.setStrokeWidth((width + 10.0f) * power);

			outPaint.setColor(0); //ダミー
			outPaint.setStrokeWidth(-1); //ダミー
			break;

		case 3:
			paint.setColor(color);
			paint.setStrokeWidth(width * power);
			midPaint.setColor(midColor);
			midPaint.setStrokeWidth((width + 10.0f) * power);
			outPaint.setColor(outColor);
			outPaint.setStrokeWidth((width + 20.0f) * power);
			break;

		case 4:
			paint.setColor(color);
			paint.setStrokeWidth(width * power);
			paint.setShadowLayer(10.0f * power, 5.0f * power, 5.0f * power, midColor);
			midPaint.setColor(midColor);

			midPaint.setStrokeWidth(-1); //ダミー
			outPaint.setStrokeWidth(-1); //ダミー
			outPaint.setColor(0); //ダミー
			break;
		case 5:
			paint.setColor(color);
			paint.setStrokeWidth(width * power);
			paint.setShadowLayer(10.0f * power, 5.0f * power, -5.0f * power, midColor);
			midPaint.setColor(midColor);

			midPaint.setStrokeWidth(-1); //ダミー
			outPaint.setStrokeWidth(-1); //ダミー
			outPaint.setColor(0); //ダミー
			break;
		case 6:
			paint.setColor(color);
			paint.setStrokeWidth(width * power);
			paint.setMaskFilter(new BlurMaskFilter(10.0f * power, Blur.SOLID));
			
			midPaint.setColor(0); //ダミー
			midPaint.setStrokeWidth(-1); //ダミー
			outPaint.setColor(0); //ダミー
			outPaint.setStrokeWidth(-1); //ダミー
			break;
		case 7:
			paint.setColor(color);
			paint.setStrokeWidth(width * power);
			
			midPaint.setColor(midColor);
			midPaint.setStrokeWidth((width + 10.0f) * power);
			midPaint.setMaskFilter(new BlurMaskFilter(15.0f * power, Blur.SOLID));
			
			outPaint.setColor(0); //ダミー
			outPaint.setStrokeWidth(-1); //ダミー
			break;
		case 8:
			paint.setColor(color);
			paint.setStrokeWidth(width * power);
			
			float[] direction = {
					3.0f * power,
					3.0f * power,
					3.0f * power
			};
			paint.setMaskFilter(new EmbossMaskFilter(direction,0.5f, 9.0f, 3.0f * power));
			
			midPaint.setColor(0); //ダミー
			midPaint.setStrokeWidth(-1); //ダミー
			outPaint.setColor(0); //ダミー
			outPaint.setStrokeWidth(-1); //ダミー
			break;
		case 9:
			paint.setColor(color);
			paint.setStrokeWidth(width * power);
			paint.setShader(new LinearGradient(0.0f, 0.0f, 10.0f * power, 10.0f * power, color, midColor, TileMode.REPEAT));
			midPaint.setColor(midColor);

			midPaint.setStrokeWidth(-1); //ダミー
			outPaint.setColor(0); //ダミー
			outPaint.setStrokeWidth(-1); //ダミー
			break;
		case 10:
			paint.setColor(color);
			paint.setStrokeWidth(width * power);
			
			shaderBitmap = mBitmaps.get(R.drawable.shader_bitmap001);
			if(shaderBitmap != null)
				paint.setShader(new BitmapShader(shaderBitmap, TileMode.REPEAT,TileMode.REPEAT));
			
			midPaint.setColor(0); //ダミー
			midPaint.setStrokeWidth(-1); //ダミー
			outPaint.setColor(0); //ダミー
			outPaint.setStrokeWidth(-1); //ダミー
			break;
		case 11:
			paint.setColor(color);
			paint.setStrokeWidth(width * power);
			
			shaderBitmap = mBitmaps.get(R.drawable.shader_bitmap002);
			if(shaderBitmap != null)
				paint.setShader(new BitmapShader(shaderBitmap, TileMode.REPEAT,TileMode.REPEAT));
			
			midPaint.setColor(0); //ダミー
			midPaint.setStrokeWidth(-1); //ダミー
			outPaint.setColor(0); //ダミー
			outPaint.setStrokeWidth(-1); //ダミー
			break;
		case 12:
			paint.setColor(color);
			paint.setStrokeWidth(width * power);
			
			shaderBitmap = mBitmaps.get(R.drawable.shader_bitmap003);
			if(shaderBitmap != null)
				paint.setShader(new BitmapShader(shaderBitmap, TileMode.REPEAT,TileMode.REPEAT));
			
			midPaint.setColor(0); //ダミー
			midPaint.setStrokeWidth(-1); //ダミー
			outPaint.setColor(0); //ダミー
			outPaint.setStrokeWidth(-1); //ダミー
			break;
		case 13:
			paint.setColor(color);
			paint.setStrokeWidth(width * power);
			
			shaderBitmap = mBitmaps.get(R.drawable.shader_bitmap004);
			if(shaderBitmap != null)
				paint.setShader(new BitmapShader(shaderBitmap, TileMode.REPEAT,TileMode.REPEAT));
			
			midPaint.setColor(0); //ダミー
			midPaint.setStrokeWidth(-1); //ダミー
			outPaint.setColor(0); //ダミー
			outPaint.setStrokeWidth(-1); //ダミー
			break;
		case 14:
			paint.setColor(color);
			paint.setStrokeWidth(width * power);
			
			shaderBitmap = mBitmaps.get(R.drawable.shader_bitmap011);
			if(shaderBitmap != null)
				paint.setShader(new BitmapShader(shaderBitmap, TileMode.REPEAT,TileMode.REPEAT));
			
			midPaint.setColor(0); //ダミー
			midPaint.setStrokeWidth(-1); //ダミー
			outPaint.setColor(0); //ダミー
			outPaint.setStrokeWidth(-1); //ダミー
			break;
		case 15:
			paint.setColor(color);
			paint.setStrokeWidth(width * power);
			
			shaderBitmap = mBitmaps.get(R.drawable.shader_bitmap012);
			if(shaderBitmap != null)
				paint.setShader(new BitmapShader(shaderBitmap, TileMode.REPEAT,TileMode.REPEAT));
			
			midPaint.setColor(0); //ダミー
			midPaint.setStrokeWidth(-1); //ダミー
			outPaint.setColor(0); //ダミー
			outPaint.setStrokeWidth(-1); //ダミー
			break;
		case 16:
			paint.setColor(color);
			paint.setStrokeWidth(width * power);
			
			shaderBitmap = mBitmaps.get(R.drawable.shader_bitmap013);
			if(shaderBitmap != null)
				paint.setShader(new BitmapShader(shaderBitmap, TileMode.REPEAT,TileMode.REPEAT));
			
			midPaint.setColor(0); //ダミー
			midPaint.setStrokeWidth(-1); //ダミー
			outPaint.setColor(0); //ダミー
			outPaint.setStrokeWidth(-1); //ダミー
			break;
		case 17:
			paint.setColor(color);
			paint.setStrokeWidth(width * power);
			
			shaderBitmap = mBitmaps.get(R.drawable.shader_bitmap014);
			if(shaderBitmap != null)
				paint.setShader(new BitmapShader(shaderBitmap, TileMode.REPEAT,TileMode.REPEAT));
			
			midPaint.setColor(0); //ダミー
			midPaint.setStrokeWidth(-1); //ダミー
			outPaint.setColor(0); //ダミー
			outPaint.setStrokeWidth(-1); //ダミー
			break;
		default:
			break;
		}
		
		Paint[] paints = new Paint[3];
		paints[0] = paint;
		paints[1] = midPaint;
		paints[2] = outPaint;
		
		return paints;
	}
	
	//パーツオブジェクトに必要なアセットを読み込み
	public static void loadPartAssets(Context context,boolean reload){
		
		if(reload == true){
			mTypefaces = new HashMap<String, Typeface>();
			mBitmaps = new HashMap<Integer, Bitmap>();
		}
		
		//フォント読み込み
		if(mTypefaces.size() == 0){
			//デフォルトフォント
//			PartUtil.addTypeface("MONOSPACE" , Typeface.MONOSPACE);
			PartUtil.addTypeface("TYPEFACE_SANS_SERIF" , Typeface.SANS_SERIF);
			PartUtil.addTypeface("TYPEFACE_SERIF" , Typeface.SERIF);

			//フォント読み込み
			File dir = StorageFileUtil.getFontsDir(context);
			File[] files = dir.listFiles();
			if(files != null){
				for (int i = 0; i < files.length; i++) {
					Typeface tf = Typeface.createFromFile(files[i]);
					PartUtil.addTypeface(files[i].getName() , tf);
				}
			}

		}
		
		//ビットマップ読み込み
		if(mBitmaps.size() == 0){
			mBitmaps.put(R.drawable.shader_bitmap001, BitmapFactory.decodeResource(context.getResources(),R.drawable.shader_bitmap001));
			mBitmaps.put(R.drawable.shader_bitmap002, BitmapFactory.decodeResource(context.getResources(),R.drawable.shader_bitmap002));
			mBitmaps.put(R.drawable.shader_bitmap003, BitmapFactory.decodeResource(context.getResources(),R.drawable.shader_bitmap003));
			mBitmaps.put(R.drawable.shader_bitmap004, BitmapFactory.decodeResource(context.getResources(),R.drawable.shader_bitmap004));
			mBitmaps.put(R.drawable.shader_bitmap011, BitmapFactory.decodeResource(context.getResources(),R.drawable.shader_bitmap011));
			mBitmaps.put(R.drawable.shader_bitmap012, BitmapFactory.decodeResource(context.getResources(),R.drawable.shader_bitmap012));
			mBitmaps.put(R.drawable.shader_bitmap013, BitmapFactory.decodeResource(context.getResources(),R.drawable.shader_bitmap013));
			mBitmaps.put(R.drawable.shader_bitmap014, BitmapFactory.decodeResource(context.getResources(),R.drawable.shader_bitmap014));
		}
		
	}
	
	//フォントの種類取得
	public static Set<String> getTypefaceNames(){
		return mTypefaces.keySet();
	}
	
	//フォントの登録
	public static void addTypeface(String name, Typeface tf){
		mTypefaces.put(name,tf);
	}
	
	//フォントの取得
	public static Typeface getTypeface(String name){
		return mTypefaces.get(name);
	}
}
