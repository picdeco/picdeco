package jp.hellodea.picolla.deco.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.OpenableColumns;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import hugo.weaving.DebugLog;
import jp.hellodea.picolla.deco.Const;
import timber.log.Timber;

/**
 * Created by kazuhiro on 2015/02/10.
 */
public class UriUtil {

    /**
     * UriInfo
     */
    public static class UriInfo{
        public String mimeType;
        public String displayName;
        public long size;
    }

    /**
     * get uri info from content provider
     * @param context
     * @param uri
     * @return
     */
    public static UriInfo getUriInfo(Context context, Uri uri){
        UriInfo uriInfo = new UriInfo();

        uriInfo.mimeType = context.getContentResolver().getType(uri);

        Cursor c = context.getContentResolver().query(uri, null, null, null, null);

        c.moveToFirst();
        uriInfo.displayName = c.getString(c.getColumnIndexOrThrow(OpenableColumns.DISPLAY_NAME));
        uriInfo.size = c.getLong(c.getColumnIndexOrThrow(OpenableColumns.SIZE));

        c.close();

        return uriInfo;
    }

    /**
     * Uriからファイルを取得する
     * @param context
     * @param uri
     * @return
     */
    @DebugLog
    public static File getFileFromUri(Context context, Uri uri){
        String scheme = uri.getScheme();

        String path = null;
        if ("file".equals(scheme)) {
            path = uri.getPath();
        } else if("content".equals(scheme)) {
            ContentResolver contentResolver = context.getContentResolver();
            Cursor cursor = contentResolver.query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();

                int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                if(index > 0){
                    path = cursor.getString(index);
                }else{
                    //pathが取得できない場合はInputstream経由でファイルにコピーする
                    File outFile = null;
                    try {
                        InputStream inputStream = context.getContentResolver().openInputStream(uri);
                        outFile = StorageFileUtil.createNewRandomFile(StorageFileUtil.getTempCacheDir(context),"");
                        OutputStream outputStream = new FileOutputStream(outFile);
                        StorageFileUtil.copyStream(inputStream,outputStream);
                    } catch (FileNotFoundException e) {
                        Timber.e(e,"FileNotFoundException");
                    } catch (IOException e) {
                        Timber.e(e,"IOException");
                    }

                    path = outFile.getPath();
                }
                cursor.close();
            }
        }
        File file = null == path ? null : new File(path);

        return file;
    }

    /**
     * システムの画像選択画面を表示する
     * @param activity
     */
    public static void startImagePicker(Activity activity){
        if (Build.VERSION.SDK_INT <19){
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY,true);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            activity.startActivityForResult(intent, Const.REQUEST_GALLERY_INTENT_CALLED);
        } else {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/*");
            activity.startActivityForResult(intent, Const.REQUEST_GALLERY_KITKAT_INTENT_CALLED);
        }
    }

    @SuppressWarnings("ResourceType")
    @SuppressLint("NewApi")
    public static Uri getResultFromImagePicker(Activity activity, Intent intent,int requestCode){
        if (null == intent) return null;
        Uri originalUri = null;

        if (requestCode == Const.REQUEST_GALLERY_INTENT_CALLED) {
            originalUri = intent.getData();
        } else if (requestCode == Const.REQUEST_GALLERY_KITKAT_INTENT_CALLED) {
            originalUri = intent.getData();
            final int takeFlags = intent.getFlags() & Intent.FLAG_GRANT_READ_URI_PERMISSION;
            // Check for the freshest data.
            activity.getContentResolver().takePersistableUriPermission(originalUri, takeFlags);
        }
        return originalUri;
    }
}
