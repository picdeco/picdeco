package jp.hellodea.picolla.deco.view;

import android.content.Context;
import android.widget.ImageView;
import android.widget.RelativeLayout;


import com.bumptech.glide.Glide;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import hugo.weaving.DebugLog;

/**
 * Created by kazuhiro on 2015/04/22.
 */
@EViewGroup(resName = "item_font_sample")
public class FontSampleItemView extends RelativeLayout {

    @ViewById
    ImageView imageViewSample;
    @ViewById
    ImageView imageViewDownloaded;

    public FontSampleItemView(Context context) {
        super(context);
    }


    @DebugLog
    public void bind(String url, boolean downloaded) {

        Glide.with(getContext()).load(url).into(imageViewSample);

        //選択されていたらチェックマークを表示
        if (downloaded == true)
            imageViewDownloaded.setImageResource(android.R.drawable.checkbox_on_background);
        else
            imageViewDownloaded.setImageResource(android.R.drawable.checkbox_off_background);

    }
}
