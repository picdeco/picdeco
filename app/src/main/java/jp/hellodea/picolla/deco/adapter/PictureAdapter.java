package jp.hellodea.picolla.deco.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

import jp.hellodea.picolla.deco.entity.Work;
import jp.hellodea.picolla.deco.view.PictureItemView;
import jp.hellodea.picolla.deco.view.PictureItemView_;

/**
 * 作品アダプター
 * Created by kazuhiro on 2015/04/03.
 */
public class PictureAdapter extends ArrayAdapter<Work> {


    public PictureAdapter(Context context, int resource, List<Work> objects) {
        super(context, resource, objects);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        PictureItemView itemView;

        if (convertView == null) {  // if it's not recycled, initialize some attributes
            itemView = PictureItemView_.build(getContext());
        } else {
            itemView = (PictureItemView)convertView;
        }

        itemView.bind(getItem(position));

        return itemView;
    }
}