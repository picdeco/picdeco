package jp.hellodea.picolla.deco.logic;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.inject.Inject;

import hugo.weaving.DebugLog;
import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.GPUImageToneCurveFilter;
import jp.hellodea.picolla.R;
import jp.hellodea.picolla.common.exception.ErrorCode;
import jp.hellodea.picolla.common.exception.LogicException;
import jp.hellodea.picolla.common.objectgraph.ObjectGraphHolder;
import jp.hellodea.picolla.deco.Const;
import jp.hellodea.picolla.deco.entity.Effect;
import jp.hellodea.picolla.deco.entity.Filter;
import jp.hellodea.picolla.deco.entity.Work;
import jp.hellodea.picolla.deco.graphics.BitmapCacheHolder;
import jp.hellodea.picolla.deco.graphics.parts.ImageObject;
import jp.hellodea.picolla.deco.graphics.parts.PartsGroup;
import jp.hellodea.picolla.deco.util.ContextUtil;
import jp.hellodea.picolla.deco.util.ImageInfo;
import jp.hellodea.picolla.deco.util.ImageUtil;
import jp.hellodea.picolla.deco.util.PreferenceUtil;
import jp.hellodea.picolla.deco.util.StorageFileUtil;
import jp.hellodea.picolla.deco.util.Util;
import jp.hellodea.picolla.deco.view.DrawBoardView;
import timber.log.Timber;

/**
 * Created by kazuhiro on 2015/04/07.
 */
public class EditLogic {

    @Inject
    WorkLogic workLogic;

    @Inject
    AssetLogic assetLogic;

    public EditLogic() {
        ObjectGraphHolder.get().inject(this);
    }

    /**
     * 現在編集対象の作品を取得する
     *
     * @param context
     * @return
     * @throws LogicException
     */
    @DebugLog
    public Work getCurrentWork(Context context) throws LogicException {
        String workProxyId = (String) PreferenceUtil.read(context, PreferenceUtil.Key.CURRENT_WORK, "");
        Work work = workLogic.getWork(workProxyId);

        return work;
    }

    /**
     * 背景画像を読み込む
     *
     * @param context
     * @param pictureUri
     * @param drawBoardView
     * @return
     * @throws Exception
     */
    @DebugLog
    public PartsGroup loadBackgroundPicture(Context context, File baseDir, Uri pictureUri, DrawBoardView drawBoardView) throws Exception {
        //指定された写真を背景に設定

        System.gc();

        //背景画像を設定
        ImageInfo imageInfo = ImageUtil.decodeAndStoreResizedImage(
                context,
                pictureUri,
                baseDir,
                ImageUtil.getMaxImageSize(context) * Const.DISPLAY_SAMPLE_SIZE,
                Const.DISPLAY_SAMPLE_SIZE);


        //指定された画像のサイズでキャンバスを作成
        PartsGroup partsGroup = new PartsGroup(
                drawBoardView,
                null,
                imageInfo.width,
                imageInfo.height,
                Const.DISPLAY_SAMPLE_SIZE,
                baseDir);


        partsGroup.setBackground(imageInfo.bitmap, true, imageInfo.file, false);

        return partsGroup;
    }

    /**
     * JSON一時ファイルから読み込み
     *
     * @param work
     * @param drawBoardView
     * @return
     * @throws Exception
     */
    @DebugLog
    public PartsGroup restoreFromJson(Work work, DrawBoardView drawBoardView, File baseDir) throws Exception {
        JSONObject json = new JSONObject(work.getString(work.F_DATA));
        PartsGroup partsGroup = new PartsGroup(drawBoardView, null, json, baseDir, false);

        Log.d("kazino", "MainEditor.restoreFromJson end");

        return partsGroup;

    }

    /**
     * データを保存
     * @param context
     * @param jsonData
     * @param thumbBitmap
     * @throws Exception
     */
    @DebugLog
    public void savePictureData(Context context, JSONObject jsonData, Bitmap thumbBitmap) throws Exception {
        Work work = getCurrentWork(context);

        String data = jsonData.toString();
        String oldData = work.getString(Work.F_DATA);

        //内容が変更されていなければ何もしない

        if(oldData != null && oldData.equals(data)){
            Timber.d("no change");
            return;
        }

        //現在のデータを一時保存
        work.put(Work.F_DATA, data);
        workLogic.saveWork(work);

        File tmpFile = new File(StorageFileUtil.getTempCacheDir(context),"temp");

        if(thumbBitmap == null) return;

        //サムネイルを保存
        OutputStream outputStream = null;
        outputStream = new BufferedOutputStream(
                new FileOutputStream(work.getThumbFile(context)));
        thumbBitmap.compress(Bitmap.CompressFormat.JPEG, 80, outputStream);
        outputStream.close();

        //一時ファイルを正式版にリネーム
        tmpFile.renameTo(work.getThumbFile(context));
    }

    /**
     * 背景用クリップアートを読み込む
     *
     * @param context
     * @param file
     * @param partsGroup
     */
    @DebugLog
    public void loadBackgroundClipart(Context context, File baseDir, File file, PartsGroup partsGroup) throws LogicException {

        try {
            ImageInfo imageInfo = ImageUtil.decodeAndStoreResizedImage(
                    file,
                    baseDir,
                    ImageUtil.getMaxImageSize(context) * 2,
                    partsGroup.getDisplaySampleSize());

            Bitmap bitmap = partsGroup.getBitmap();
            int baseWidth = bitmap.getWidth();
            int baseHeight = bitmap.getHeight();

            if (imageInfo.width <= baseWidth && imageInfo.height <= baseHeight) {
                //もしベースより背景が同じか小さければそのまま設定
                partsGroup.setBackground(imageInfo.bitmap, true, imageInfo.file, true);
            } else {

                //もしベースより背景が大きければ同じ大きさに切り取る
                bitmap = ImageUtil.restoreImageFromBufferCache(imageInfo.file, 1, Bitmap.Config.RGB_565); //実際の画像取得

                //いらないファイルを削除
                imageInfo.file.delete();
                imageInfo.recycle();

                bitmap = ImageUtil.cropImageAtCenter(
                        bitmap,
                        baseWidth * partsGroup.getDisplaySampleSize(),
                        baseHeight * partsGroup.getDisplaySampleSize()); //切り取り
                File cacheFile = StorageFileUtil.createNewRandomFile(baseDir, "");
                ImageUtil.storeBitmapToCache(bitmap, cacheFile);
                bitmap = ImageUtil.restoreImageFromBufferCache(cacheFile, partsGroup.getDisplaySampleSize(), Bitmap.Config.RGB_565);
                partsGroup.setBackground(bitmap, true, cacheFile, true);
            }

        } catch (IOException e) {
            Timber.e(e,"");
            throw new LogicException(ErrorCode.UnexpectedError, R.string.error_load_background_image, e);
        }

    }

    /**
     * 画像を指定された位置に追加する
     *
     * @param context
     * @param partsGroup
     * @param imageFile
     * @param x
     * @param y
     */
    @DebugLog
    public ImageObject loadImage(Context context, PartsGroup partsGroup, File imageFile, File baseDir, int x, int y) {

        ImageObject imageObject = new ImageObject(
                partsGroup, 0, 0, imageFile, baseDir, true, context, true, Const.DISPLAY_SAMPLE_SIZE);
        Bitmap bitmap = imageObject.getBitmap();
        imageObject.move(x - bitmap.getWidth() / 2, y - bitmap.getHeight() / 2);

        return imageObject;

    }

    //フィルター適用処理
    @DebugLog
    public Bitmap performFilter(Context context, Bitmap bitmap, Filter filter) throws LogicException {
        File file = assetLogic.getAssetFile(context, filter.getUrl());

        GPUImageToneCurveFilter toneCurveFilter = new GPUImageToneCurveFilter();
        try {
            InputStream inputStream = new FileInputStream(file);
            toneCurveFilter.setFromCurveFileInputStream(inputStream);
            inputStream.close();

        } catch (IOException e) {
            throw new LogicException(ErrorCode.UnexpectedError, R.string.error_perform_filter, e);
        }

        GPUImage gpuImage = new GPUImage(context);
        gpuImage.setImage(bitmap);
        gpuImage.setFilter(toneCurveFilter);
        bitmap = gpuImage.getBitmapWithFilterApplied();

        return bitmap;
    }

    public Bitmap getEffectBitmap(Context context, Effect effect) throws LogicException {
        File cacheKeyFile = new File(StorageFileUtil.getTempCacheDir(context),
                StorageFileUtil.getSafeFileName(effect.getUrl()));

        Bitmap effectBitmap = BitmapCacheHolder.get().getCachedImage(cacheKeyFile, 1);

        if (effectBitmap == null) {
            File file = assetLogic.getAssetFile(context, effect.getUrl());
            int size = (int) ((float) ImageUtil.getMaxImageSize(context) * 0.7f); //最大サイズの7割で

            ImageInfo imageInfo = null;
            try {
                imageInfo = ImageUtil.decodeAndStoreResizedImage(file,
                        StorageFileUtil.getTempCacheDir(context), size, 1);
            } catch (IOException e) {
                Timber.e(e,"");
                throw new LogicException(ErrorCode.UnexpectedError, R.string.error_get_effect_bitmap, e);
            }

            effectBitmap = imageInfo.bitmap;

            //今後のためにキャッシュに保存
            BitmapCacheHolder.get().setOriginalImage(cacheKeyFile, effectBitmap, 1);

        }

        return effectBitmap;
    }

    public Bitmap applyFilter(Context context, Bitmap bitmap, Filter filter, Effect effect) throws LogicException {
        //フィルター
        if (filter != null) {
            bitmap = performFilter(context, bitmap, filter);
        }

        Canvas canvas = new Canvas(bitmap);

        //エフェクト
        if (effect != null) {
            Bitmap bitmapEffect = getEffectBitmap(context, effect);
            bitmapEffect = ImageUtil.resizeFree(bitmapEffect, bitmap.getWidth(), bitmap.getHeight());
            canvas.drawBitmap(bitmapEffect, 0, 0, null);
        }

        return bitmap;

    }

    /**
     * 加工後の完成写真をTempフォルダに出力する
     *
     * @param context
     * @param work
     * @return
     */
    @DebugLog
    public File outputPicture(Context context, Work work) throws LogicException {
        Log.d("kazino", "CreatePicture doInBackground");
        System.gc();
        System.gc();
        System.gc();

        PartsGroup partsGroup;
        JSONObject json;
        int width;
        int height;
        int displaySampleSize;

        try {
            //jsonファイル読み込み
            json = new JSONObject(work.getString(Work.F_DATA));
            partsGroup = new PartsGroup(json, StorageFileUtil.getImagePartsDir(context));

            width = json.getInt("width"); //ベースビットマップの幅
            height = json.getInt("height"); //ベースビットマップの高さ
            displaySampleSize = json.getInt("displaySampleSize");

        } catch (Exception e) {
            Timber.e(e, "");
            throw new LogicException(ErrorCode.InaccurateData, R.string.error_load_json_data, e);
        }

        Bitmap bitmap = Bitmap.createBitmap(width * displaySampleSize,
                height * displaySampleSize, Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(bitmap);
        try {
            partsGroup.drawRealSize(canvas);
        } catch (Exception e) {
            Timber.e(e, "");
            throw new LogicException(ErrorCode.UnexpectedError, R.string.error_output_picture, e);
        }

        File outDir = StorageFileUtil.getTempCacheDir(context);

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        File pictureFile = new File(outDir, simpleDateFormat.format(calendar.getTime()) + ".jpg");

        try {
            OutputStream os = new FileOutputStream(pictureFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, os);
            bitmap.recycle();
            os.close();
        } catch (IOException e) {
            Timber.e(e, "");
            throw new LogicException(ErrorCode.ErrorOnFileSystem, R.string.error_output_picture, e);
        }

        return pictureFile;

    }

    /**
     * 完成後の写真をギャラリーに登録
     * @param context
     * @param work
     * @return
     */
    @DebugLog
    public File storeGallery(Context context, Work work) throws LogicException {
        File outDir = StorageFileUtil.getPictureOutputDir(context);

        File imageFIle = outputPicture(context,work);

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        File pictureFile = new File(outDir, simpleDateFormat.format(calendar.getTime()) + ".jpg");

        imageFIle.renameTo(pictureFile);

        //ギャラリーに登録
        MediaScannerConnection.scanFile(context,
                new String[]{pictureFile.toString()}, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    @DebugLog
                    public void onScanCompleted(String path, Uri uri) {
                        return;
                    }
                });


        return pictureFile;

    }

}
