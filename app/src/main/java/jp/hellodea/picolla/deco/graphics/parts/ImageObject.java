package jp.hellodea.picolla.deco.graphics.parts;

import java.io.File;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.RectF;
import android.util.Log;

import hugo.weaving.DebugLog;
import jp.hellodea.picolla.BuildConfig;
import jp.hellodea.picolla.deco.graphics.BitmapCacheHolder;
import jp.hellodea.picolla.deco.graphics.SavableMatrix;
import jp.hellodea.picolla.deco.graphics.SavablePath;
import jp.hellodea.picolla.deco.util.ImageUtil;
import jp.hellodea.picolla.deco.util.StorageFileUtil;
import timber.log.Timber;

public class ImageObject extends PartObject {

	private File mCacheFile;	//キャッシュファイル
	
	private Bitmap mEditTargetBitmap; //編集対象のときに参照するビットマップ（速くするため）
		
	private Bitmap.CompressFormat mFormat;
	private int mWidth;
	private int mHeight;
	
	private Bitmap mMaskBitmap; //マスクエリアのビットマップ
//	private RectF mMaskRectF = new RectF();

    /**
     * bitmapを指定して呼び出し
     * @param group
     * @param left
     * @param top
     * @param bitmap
     * @param cacheDir
     * @param undoable
     * @param displaySampleSize
     */
    @DebugLog
	public ImageObject(PartsGroup group, float left, float top,Bitmap bitmap, File cacheDir, boolean undoable, int displaySampleSize){
		super(group,PartObject.KIND_PICT,displaySampleSize);

		mMatrix.setTranslate(left, top);
		mMaskPath = new SavablePath();

        //Bitmapをキャッシュ
        mCacheFile = StorageFileUtil.createNewRandomFile(cacheDir, "");
        bitmap = BitmapCacheHolder.get().setOriginalImage(mCacheFile,bitmap,mDisplaySampleSize);

		if(bitmap.hasAlpha() == true)
			mFormat = Bitmap.CompressFormat.PNG;
		else
			mFormat = Bitmap.CompressFormat.JPEG;
		
		mWidth = bitmap.getWidth();
		mHeight = bitmap.getHeight();

		group.addPart(this,undoable);
	}

    /**
     * 保存されたJSONオブジェクトから読み込み
     * @param group
     * @param json
     * @param undoable
     * @throws JSONException
     */
    @DebugLog
    public ImageObject(PartsGroup group,JSONObject json,File baseDir, boolean undoable) throws JSONException{
		super(group,PartObject.KIND_PICT, json.getInt("displaySampleSize"));

		mCacheFile = new File(baseDir, json.getString("cachePath"));
		mFormat = Bitmap.CompressFormat.valueOf(json.getString("format"));
		mMatrix = new SavableMatrix(json.getJSONArray("matrix"));
		mMaskPath = new SavablePath();

		group.addPart(this,undoable);
	}
	
	//ファイルから読み込み

    /**
     * リサイズ済か十分に小さいサイズの画像ファイルから作成
     * @param group
     * @param left
     * @param top
     * @param originalFile
     * @param copyToCacheFile
     * @param context
     * @param undoable
     * @param displaySampleSize
     */
    @DebugLog
    public ImageObject(PartsGroup group, float left, float top, File originalFile,File baseDir, boolean copyToCacheFile, Context context, boolean undoable, int displaySampleSize){
		super(group,PartObject.KIND_PICT,displaySampleSize);
		
		if(copyToCacheFile == true){
			//ファイルをキャッシュにコピー
			try {
		    	mCacheFile = StorageFileUtil.createNewRandomFile(baseDir,"");
		    	StorageFileUtil.copyFile(originalFile, mCacheFile);
			} catch (Exception e) {
				Timber.e(e,"");
				throw new RuntimeException(e);
			}
		}else{
			mCacheFile = originalFile;
			//ファイルをそのまま流用
		}

		try {
            Bitmap bitmap = BitmapCacheHolder.get().getCachedImage(mCacheFile,mDisplaySampleSize);

			if(bitmap.hasAlpha() == true)
				mFormat = Bitmap.CompressFormat.PNG;
			else
				mFormat = Bitmap.CompressFormat.JPEG;
			
			mWidth = bitmap.getWidth();
			mHeight = bitmap.getHeight();
			
			mMatrix.setTranslate(left, top);
			mMaskPath = new SavablePath();
			
		} catch (Exception e) {
			Timber.e(e,"");
			throw new RuntimeException(e);
		}

		group.addPart(this,undoable);
	}

	//ビットマップをキャッシュから取得する
	public Bitmap getBitmap(){
		
		if(mEditTargetBitmap != null) return mEditTargetBitmap; //編集中ならそのまま返す
		
		return BitmapCacheHolder.get().getCachedImage(mCacheFile,mDisplaySampleSize);
	}
	
	@Override
	@DebugLog
	public void flashToBitmap(Canvas canvas,Canvas maskCanves) {
		Log.d("kazino","*start flash:" + System.currentTimeMillis());

		Bitmap bitmap = getBitmap();

        mWidth = bitmap.getWidth();
        mHeight = bitmap.getHeight();
		
		mPaint.setFilterBitmap(true);
		mPaint.setAntiAlias(true);
		canvas.drawBitmap(bitmap, mMatrix, mPaint);
		
		//テスト中のみ表示
		if(BuildConfig.DEBUG){
			RectF bounds = getBounds();
			Paint paint = new Paint();
			paint.setColor(Color.RED);
			paint.setStrokeWidth(3.0f);
			paint.setStyle(Style.STROKE);
			Path path = new Path();
			path.moveTo(bounds.left,bounds.top);
			path.addRect(bounds,Direction.CW);
			canvas.drawPath(path, paint);
		}

		//マスク作成
		if(maskCanves != null){
//			mMaskRectF.set(0, 0,bitmap.getWidth() ,bitmap.getHeight());
//			mMaskPath.rewind();
//			mMaskPath.addRect(mMaskRectF, Direction.CW);
//			mMaskPath.transform(mMatrix);
//			maskCanves.drawPath(mMaskPath, mMaskPaint);
			if(mMaskBitmap == null || mMaskBitmap.isRecycled() == true){
				
				//マスクカラーで塗りつぶしたマスクビットマップを作成
				Config config = bitmap.getConfig();
				if(config == null) config = Config.ARGB_8888; //特定の条件でnullになるらしいので強制的に設定
				mMaskBitmap = bitmap.copy(config,true);
Log.d("kazino", "**********1st " + mMaskBitmap);				
				ImageUtil.applyColorMatrix(mMaskBitmap, new ColorMatrix(
		    			new float[] {
		                   0, 0, 0, 0, Color.red(mMaskPaint.getColor()),
		                   0, 0, 0, 0, Color.green(mMaskPaint.getColor()),
		                   0, 0, 0, 0, Color.blue(mMaskPaint.getColor()),
		                   0, 0, 0, 255, 0 }));

Log.d("kazino", "********** " + mMaskBitmap);				
Log.d("kazino", "********** " + mMatrix);				
Log.d("kazino", "********** " + mPaint);				
				maskCanves.drawBitmap(mMaskBitmap, mMatrix, mPaint);
					
				//もし編集対象になっていなければメモリ解放
				//なっていればスピードアップのため解放しない
				if(mEditTargetBitmap == null){
					mMaskBitmap.recycle();
					mMaskBitmap = null;
				}
				
			}else{
				maskCanves.drawBitmap(mMaskBitmap, mMatrix, mPaint);
			}
			
		}

		Log.d("kazino","*end flash:" + System.currentTimeMillis());
	}

	@Override
	public void rotate(float degrees){
		RectF rect = new RectF();
		rect.set(0, 0,mWidth ,mHeight);
		mMatrix.mapRect(rect);
		mMatrix.postRotate(degrees,rect.centerX(),rect.centerY());
	}

	@Override
	public void scale(float scaleX,float scaleY) {
		SavableMatrix tempMatrix = new SavableMatrix(mMatrix);
		
		RectF rect = new RectF();
		rect.set(0, 0,mWidth ,mHeight);
		mMatrix.mapRect(rect);
		mMatrix.postScale(scaleX,scaleY,rect.centerX(),rect.centerY());
//		mMatrix.postScale(scaleX,scaleY,rect.left,rect.top);
		
		//NaNになっていないかチェック
		rect.set(0, 0,mWidth ,mHeight);
		mMatrix.mapRect(rect);
		if(Math.abs(rect.width()) < 2 || Math.abs(rect.height()) < 2){
			mMatrix = tempMatrix;
			return;
		}
		if(mMatrix.toShortString().contains("NaN") == true)
			mMatrix = tempMatrix;
				
	}

	public void scaleFromLeftAndTop(float scaleX,float scaleY) {
		mMatrix.postScale(scaleX,scaleY);
	}

	@Override
	public void move(int x, int y) {
		mMatrix.postTranslate(x,y);
	}

	@Override
	public RectF getBounds() {
		RectF rect = new RectF();

		rect.set(0, 0,mWidth ,mHeight);
		mMatrix.mapRect(rect);

		return rect;
	}

	@Override
	public JSONObject getJsonObject() {
		JSONObject json = new JSONObject();
		try {
			json.put("partKind", PartObject.KIND_PICT);
			json.put("cachePath", mCacheFile.getName());
			json.put("format", mFormat.toString());
			json.put("matrix", mMatrix.getJson());
			json.put("displaySampleSize", mDisplaySampleSize);
			
			
		} catch (JSONException e) {
			Timber.e(e,"");
			throw new RuntimeException(e);
		}
		
		return json;
	}
	
	public File getCacheFile(){
		return mCacheFile;
	}

	@Override
	public void startEditTarget() {
		mEditTargetBitmap = getBitmap(); //メモリ解放されないように保持
	}

	@Override
	public void endEditTarget() {
		mEditTargetBitmap = null; //解放
		
		//スピードアップのためのマスクのビットマップも解放
		if(mMaskBitmap != null && mMaskBitmap.isRecycled() == false){
			mMaskBitmap.recycle();
		}
		mMaskBitmap = null;
		
	}

	@Override
	public void recycle() {

		if(mEditTargetBitmap != null && mEditTargetBitmap.isRecycled() == false)
			mEditTargetBitmap.recycle();
		mEditTargetBitmap = null;
		
		if(mMaskBitmap != null && mMaskBitmap.isRecycled() == false)
			mMaskBitmap.recycle();
		mMaskBitmap = null;
	}

	@Override
	public void drawRealSize(Canvas canvas) {
		Bitmap bitmap;
		
		//縮小サイズのビットマップを取得
		bitmap = getBitmap();
		RectF oldRectF = new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight());
		mMatrix.mapRect(oldRectF);
		bitmap.recycle();
		
		try {
			//フルサイズのビットマップを取得
			System.gc();
			bitmap = ImageUtil.restoreImageFromBufferCache(mCacheFile, 1,Config.ARGB_8888);
		} catch (Exception e) {
			Timber.e(e, "");
			throw new RuntimeException(e);
		}

		RectF newRectF = new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight());
		Matrix newMatrix = new Matrix(mMatrix);
		newMatrix.mapRect(newRectF);
		
		//位置を計算
		float newX = (oldRectF.left * (float)mDisplaySampleSize) - newRectF.left;
		float newY = (oldRectF.top * (float)mDisplaySampleSize) - newRectF.top;
		
		newMatrix.postTranslate(newX, newY);

		//実際の縮尺を適用する
//		matrix.postScale((float)mDisplaySampleSize, (float)mDisplaySampleSize);
		
		canvas.drawBitmap(bitmap, newMatrix, mPaint);
		
		bitmap.recycle();
		bitmap = null;

	}

	@Override
	public void deleteAsset() {
		mCacheFile.delete();
		mCacheFile = null;
	}


}
