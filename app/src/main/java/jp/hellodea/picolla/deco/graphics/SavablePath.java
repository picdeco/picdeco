package jp.hellodea.picolla.deco.graphics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Path;
import android.graphics.RectF;

import hugo.weaving.DebugLog;
import timber.log.Timber;

public class SavablePath extends Path {

    public static final int COMMAND_MOVE = 0;
    public static final int COMMAND_LINE = 1; //直線
    public static final int COMMAND_RECT = 2;
    public static final int COMMAND_ROUND_RECT = 3;
    public static final int COMMAND_OVAL = 4;
    public static final int COMMAND_QUAD = 5;
    public static final int COMMAND_CLOSE = 6;

    public static final int COMMAND_RCUBIC = 7;
    public static final int COMMAND_RLINE = 8;
    public static final int COMMAND_RMOVE = 9;
    public static final int COMMAND_RQUAD = 10;

    public static final int COMMAND_CIRCLE = 11;
    public static final int COMMAND_CUBIC = 12;

    private boolean mIsAutoClose = false; //起点が移動した時にその前までのパスを自動で閉じるかどうか

    private JSONArray mPaths;

    public SavablePath() {
        mPaths = new JSONArray();
    }

    public SavablePath(boolean autoClose) {
        this();

        mIsAutoClose = autoClose;
    }

    public SavablePath(JSONArray jsonArray) {
        mPaths = jsonArray;
        rebuild();
    }

    public SavablePath(ArrayList<HashMap<String, Object>> arrayList) {

        mPaths = new JSONArray();

        for (Iterator iterator = arrayList.iterator(); iterator.hasNext(); ) {
            HashMap<String, Object> hashMap = (HashMap<String, Object>) iterator.next();
            mPaths.put(new JSONObject(hashMap));
        }

        rebuild();
    }

    public SavablePath(String svgString) {
        mPaths = new JSONArray();
        buildFromSvg(svgString);
    }

    /**
     * 以下のサイトで作成したSVG文字列からandroid用パスを作成する
     * site:http://editor.method.ac/
     *
     * @param svgString
     */
    public void buildFromSvg(String svgString) {

        moveTo(0, 0);

        List<String> commandList = new ArrayList<>();
        String tempCommand = null;

        //コマンドを分割
        String[] svgStrings = svgString.split("");
        for (String chara : svgStrings) {
            if (chara.equals("m")
                    || chara.equals("c")
                    || chara.equals("l")
                    || chara.equals("z")
                    || chara.equals("M")
                    || chara.equals("e")
                    || chara.equals("r")) {

                if (tempCommand != null) commandList.add(tempCommand);

                tempCommand = chara;
            } else {
                tempCommand = tempCommand + chara;
            }
        }

        if (tempCommand.length() > 0) commandList.add(tempCommand);

        //コマンド毎にJsonに変換する
        for (String commandStr : commandList) {
            String[] params = commandStr.substring(1, commandStr.length()).split(" ");
            float[] point0;
            float[] point1;
            float[] point2;

            switch (commandStr.charAt(0)) {
                case 'm': //相対移動
                    point0 = getPointFromString(params[0]);
                    rMoveTo(point0[0], point0[1]);
                    break;
                case 'M': //絶対移動
                    point0 = getPointFromString(params[0]);
                    moveTo(point0[0], point0[1]);

                    //後に座標が続いた場合はLineコマンドを追加
                    if (params.length > 1) {
                        for (int i = 1; i < params.length; i++) {
                            point0 = getPointFromString(params[i]);
                            lineTo(point0[0], point0[1]);
                        }
                    }
                    break;
                case 'z': //閉じる
                    close();
                    break;
                case 'l': //直線
                    point0 = getPointFromString(params[0]);
                    rLineTo(point0[0], point0[1]);
                    break;
                case 'c': //カーブ
                    point0 = getPointFromString(params[0]);
                    point1 = getPointFromString(params[1]);
                    point2 = getPointFromString(params[2]);
                    rCubicTo(point0[0], point0[1], point1[0], point1[1], point2[0], point2[1]);
                    break;
                case 'e': //マル（オリジナル）
                    point0 = getPointFromString(params[0]);
                    addCircle(point0[0], point0[1], point0[2]);
                    break;

                case 'r': //四角（オリジナル）
                    point0 = getPointFromString(params[0]);
                    point1 = getPointFromString(params[1]);
                    RectF rectF = new RectF(point0[0], point0[1], point1[0], point1[1]);
                    addRect(rectF, Direction.CW);
                    break;

            }


        }

    }

    private float[] getPointFromString(String str) {
        String[] pointStrs = str.split(",");
        float[] points = new float[pointStrs.length];
        for (int i = 0; i < pointStrs.length; i++) {
            points[i] = Float.valueOf(pointStrs[i]);

        }
        return points;
    }

    //パス情報を元にandroid用パスを再作成する
    public void rebuild() {

        super.rewind();

        JSONObject cmd;
        for (int i = 0; i < mPaths.length(); i++) {
            try {

                if (mPaths.get(i) == JSONObject.NULL || mPaths.get(i) == null) break; //nullは終端

                cmd = mPaths.getJSONObject(i);

                switch (cmd.getInt("cmd")) {
                    case COMMAND_MOVE:
                        //移動(絶対位置)
                        super.moveTo((float) cmd.getDouble("x"), (float) cmd.getDouble("y"));
                        break;
                    case COMMAND_RMOVE:
                        //移動（相対位置）
                        super.rMoveTo((float) cmd.getDouble("x"), (float) cmd.getDouble("y"));
                        break;
                    case COMMAND_LINE:
                        //ライン
                        super.lineTo((float) cmd.getDouble("x"), (float) cmd.getDouble("y"));
                        break;
                    case COMMAND_RLINE:
                        //ライン
                        super.rLineTo((float) cmd.getDouble("x"), (float) cmd.getDouble("y"));
                        break;
                    case COMMAND_CIRCLE:
                        //サークル
                        super.addCircle((float) cmd.getDouble("x"), (float) cmd.getDouble("y"), (float) cmd.getDouble("radius"), Direction.CW);
                        break;
                    case COMMAND_RECT:
                        //四角
                        super.addRect(
                                new RectF((float) cmd.getDouble("left"),
                                        (float) cmd.getDouble("top"),
                                        (float) cmd.getDouble("right"),
                                        (float) cmd.getDouble("bottom")),
                                Direction.valueOf(cmd.getString("dir")));
                        break;
                    case COMMAND_ROUND_RECT:
                        //丸角四角
                        super.addRoundRect(
                                new RectF((float) cmd.getDouble("left"),
                                        (float) cmd.getDouble("top"),
                                        (float) cmd.getDouble("right"),
                                        (float) cmd.getDouble("bottom")),
                                (float) cmd.getDouble("rx"),
                                (float) cmd.getDouble("ry"),
                                Direction.valueOf(cmd.getString("dir")));
                        break;
                    case COMMAND_OVAL:
                        //楕円
                        super.addOval(
                                new RectF((float) cmd.getDouble("left"),
                                        (float) cmd.getDouble("top"),
                                        (float) cmd.getDouble("right"),
                                        (float) cmd.getDouble("bottom")),
                                Direction.valueOf(cmd.getString("dir")));
                        break;
                    case COMMAND_QUAD:
                        //カーブ
                        super.quadTo(
                                (float) cmd.getDouble("x1"),
                                (float) cmd.getDouble("y1"),
                                (float) cmd.getDouble("x2"),
                                (float) cmd.getDouble("y2"));
                        break;
                    case COMMAND_RQUAD:
                        //カーブ
                        super.rQuadTo(
                                (float) cmd.getDouble("x1"),
                                (float) cmd.getDouble("y1"),
                                (float) cmd.getDouble("x2"),
                                (float) cmd.getDouble("y2"));
                        break;
                    case COMMAND_CUBIC:
                        //カーブ
                        super.cubicTo(
                                (float) cmd.getDouble("x1"),
                                (float) cmd.getDouble("y1"),
                                (float) cmd.getDouble("x2"),
                                (float) cmd.getDouble("y2"),
                                (float) cmd.getDouble("x3"),
                                (float) cmd.getDouble("y3"));
                        break;
                    case COMMAND_RCUBIC:
                        //カーブ
                        super.rCubicTo(
                                (float) cmd.getDouble("x1"),
                                (float) cmd.getDouble("y1"),
                                (float) cmd.getDouble("x2"),
                                (float) cmd.getDouble("y2"),
                                (float) cmd.getDouble("x3"),
                                (float) cmd.getDouble("y3"));
                        break;
                    case COMMAND_CLOSE:
                        //パスを閉じる
                        super.close();
                        break;
                    default:
                        break;
                }
            } catch (JSONException e) {
                Timber.e(e,"");
                throw new RuntimeException(e);
            }
        }

    }

    public JSONArray getJsonPath() {
        return mPaths;
    }

    public JSONArray copyJsonPath() {
        String stringJson = mPaths.toString();
        JSONArray jsonArray = null;

        try {
            jsonArray = new JSONArray(stringJson);
        } catch (JSONException e) {
            Timber.e(e,"");
            throw new RuntimeException(e);
        }

        return jsonArray;
    }

    @Override
    public void addPath(Path src) {

        JSONArray srcJsonArray = ((SavablePath) src).getJsonPath();

        for (int i = 0; i < srcJsonArray.length(); i++) {
            try {
                mPaths.put(srcJsonArray.get(i));
            } catch (JSONException e) {
                Timber.e(e,"");
                throw new RuntimeException(e);
            }
        }
        super.addPath(src);
    }

    //移動
    @Override
    @DebugLog
    public void moveTo(float x, float y) {

        try {
            JSONObject cmd;

            //自動クローズだったらclose()を記録
            if (mIsAutoClose == true) {
                cmd = new JSONObject();
                cmd.put("cmd", COMMAND_CLOSE);
                mPaths.put(cmd);
            }

            //移動を記録
            cmd = new JSONObject();
            cmd.put("cmd", COMMAND_MOVE);
            cmd.put("x", x);
            cmd.put("y", y);
            mPaths.put(cmd);
        } catch (JSONException e) {
            Timber.e(e,"");
            throw new RuntimeException(e);
        }

        super.moveTo(x, y);
    }

    @Override
    @DebugLog
    public void rMoveTo(float x, float y) {

        try {
            JSONObject cmd;

            //自動クローズだったらclose()を記録
            if (mIsAutoClose == true) {
                cmd = new JSONObject();
                cmd.put("cmd", COMMAND_CLOSE);
                mPaths.put(cmd);
            }

            //移動を記録
            cmd = new JSONObject();
            cmd.put("cmd", COMMAND_RMOVE);
            cmd.put("x", x);
            cmd.put("y", y);
            mPaths.put(cmd);
        } catch (JSONException e) {
            Timber.e(e,"");
            throw new RuntimeException(e);
        }

        super.rMoveTo(x, y);
    }

    //線
    @Override
    @DebugLog
    public void lineTo(float x, float y) {
        JSONObject cmd = new JSONObject();

        try {
            cmd.put("cmd", COMMAND_LINE);
            cmd.put("x", x);
            cmd.put("y", y);
            mPaths.put(cmd);
        } catch (JSONException e) {
            Timber.e(e,"");
            throw new RuntimeException(e);
        }

        super.lineTo(x, y);
    }

    @Override
    @DebugLog
    public void rLineTo(float x, float y) {
        JSONObject cmd = new JSONObject();

        try {
            cmd.put("cmd", COMMAND_RLINE);
            cmd.put("x", x);
            cmd.put("y", y);
            mPaths.put(cmd);
        } catch (JSONException e) {
            Timber.e(e,"");
            throw new RuntimeException(e);
        }

        super.rLineTo(x, y);
    }

    @DebugLog
    public void addCircle(float x, float y, float radius) {

        JSONObject cmd = new JSONObject();

        try {
            cmd.put("cmd", COMMAND_CIRCLE);
            cmd.put("x", x);
            cmd.put("y", y);
            cmd.put("radius", radius);
            mPaths.put(cmd);
        } catch (JSONException e) {
            Timber.e(e, "");
            throw new RuntimeException(e);
        }

        super.addCircle(x, y, radius, Direction.CW);
    }

    @Override
    @DebugLog
    public void quadTo(float x1, float y1, float x2, float y2) {
        JSONObject cmd = new JSONObject();

        try {
            cmd.put("cmd", COMMAND_QUAD);
            cmd.put("x1", x1);
            cmd.put("y1", y1);
            cmd.put("x2", x2);
            cmd.put("y2", y2);
            mPaths.put(cmd);
        } catch (JSONException e) {
            Timber.e(e,"");
            throw new RuntimeException(e);
        }

        super.quadTo(x1, y1, x2, y2);
    }

    @DebugLog
    @Override
    public void rQuadTo(float x1, float y1, float x2, float y2) {
        JSONObject cmd = new JSONObject();

        try {
            cmd.put("cmd", COMMAND_RQUAD);
            cmd.put("x1", x1);
            cmd.put("y1", y1);
            cmd.put("x2", x2);
            cmd.put("y2", y2);
            mPaths.put(cmd);
        } catch (JSONException e) {
            Timber.e(e,"");
            throw new RuntimeException(e);
        }


        super.rQuadTo(x1, y1, x2, y2);
    }

    @Override
    public void cubicTo(float x1, float y1, float x2, float y2, float x3, float y3) {
        JSONObject cmd = new JSONObject();

        try {
            cmd.put("cmd", COMMAND_CUBIC);
            cmd.put("x1", x1);
            cmd.put("y1", y1);
            cmd.put("x2", x2);
            cmd.put("y2", y2);
            cmd.put("x3", x3);
            cmd.put("y3", y3);
            mPaths.put(cmd);
        } catch (JSONException e) {
            Timber.e(e,"");
            throw new RuntimeException(e);
        }

        super.cubicTo(x1, y1, x2, y2, x3, y3);
    }

    @Override
    @DebugLog
    public void rCubicTo(float x1, float y1, float x2, float y2, float x3, float y3) {
        JSONObject cmd = new JSONObject();

        try {
            cmd.put("cmd", COMMAND_RCUBIC);
            cmd.put("x1", x1);
            cmd.put("y1", y1);
            cmd.put("x2", x2);
            cmd.put("y2", y2);
            cmd.put("x3", x3);
            cmd.put("y3", y3);
            mPaths.put(cmd);
        } catch (JSONException e) {
            Timber.e(e,"");
            throw new RuntimeException(e);
        }

        super.rCubicTo(x1, y1, x2, y2, x3, y3);
    }

    //角丸四角
    @Override
    @DebugLog
    public void addRoundRect(RectF rect, float rx, float ry, Direction dir) {
        JSONObject cmd = new JSONObject();

        try {
            cmd.put("cmd", COMMAND_ROUND_RECT);
            cmd.put("left", rect.left);
            cmd.put("top", rect.top);
            cmd.put("right", rect.right);
            cmd.put("bottom", rect.bottom);
            cmd.put("rx", rx);
            cmd.put("ry", ry);
            cmd.put("dir", dir);
            mPaths.put(cmd);
        } catch (JSONException e) {
            Timber.e(e,"");
            throw new RuntimeException(e);
        }

        super.addRoundRect(rect, rx, ry, dir);
    }

    //四角
    @Override
    @DebugLog
    public void addRect(RectF rect, Direction dir) {
        JSONObject cmd = new JSONObject();

        try {
            cmd.put("cmd", COMMAND_RECT);
            cmd.put("left", rect.left);
            cmd.put("top", rect.top);
            cmd.put("right", rect.right);
            cmd.put("bottom", rect.bottom);
            cmd.put("dir", dir);
            mPaths.put(cmd);
        } catch (JSONException e) {
            Timber.e(e,"");
            throw new RuntimeException(e);
        }

        super.addRect(rect, dir);
    }

    @Override
    @DebugLog
    public void addOval(RectF rect, Direction dir) {
        JSONObject cmd = new JSONObject();

        try {
            cmd.put("cmd", COMMAND_OVAL);
            cmd.put("left", rect.left);
            cmd.put("top", rect.top);
            cmd.put("right", rect.right);
            cmd.put("bottom", rect.bottom);
            cmd.put("dir", dir);
            mPaths.put(cmd);
        } catch (JSONException e) {
            Timber.e(e,"");
            throw new RuntimeException(e);
        }

        super.addOval(rect, dir);
    }

    //パスを閉じる
    @Override
    @DebugLog
    public void close() {

        JSONObject cmd = new JSONObject();

        try {
            cmd.put("cmd", COMMAND_CLOSE);
            mPaths.put(cmd);
        } catch (JSONException e) {
            Timber.e(e,"");
            throw new RuntimeException(e);
        }

        super.close();
    }

    @Override
    public void rewind() {
        mPaths = new JSONArray(); //今までの情報をクリア
        super.rewind();
    }

    @Override
    public void reset() {
        mPaths = new JSONArray(); //今までの情報をクリア
        super.reset();
    }

    //直前の開始点まで戻す
    public SavablePath undo() {

        if (mPaths.length() == 0) return new SavablePath();

        try {
            for (int i = mPaths.length() - 1; i >= 0; i--) {
                JSONObject cmd = mPaths.getJSONObject(i);
                if (cmd.getInt("cmd") != COMMAND_MOVE && cmd.getInt("cmd") != COMMAND_LINE)
                    mPaths.put(i, JSONObject.NULL);
                else {
                    mPaths.put(i, JSONObject.NULL);
                    break;
                }
            }

            //新しいパスを作る
            JSONArray newPaths = new JSONArray();
            for (int i = 0; i < mPaths.length(); i++) {
                if (mPaths.get(i) != JSONObject.NULL && mPaths.get(i) != null)
                    newPaths.put(mPaths.getJSONObject(i));
                else break;
            }

            return new SavablePath(newPaths); //新しいPathを返す

        } catch (Exception e) {
            Timber.e(e,"");
            throw new RuntimeException(e);
        }

    }


}
