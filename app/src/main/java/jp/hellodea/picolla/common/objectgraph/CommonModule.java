package jp.hellodea.picolla.common.objectgraph;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by kazuhiro on 2014/12/18.
 */
@Module(
        library = true
)
public class CommonModule {
    private Application application;

    public CommonModule(Application pApplication) {
        application = pApplication;
    }

    @Provides
    @Singleton
    public Application provideApplication(){
        return application;
    }

}
