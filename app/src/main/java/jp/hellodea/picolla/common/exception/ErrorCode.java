package jp.hellodea.picolla.common.exception;

import jp.hellodea.picolla.R;
import timber.log.Timber;

/**
 * error codes
 * @author kazuhiro
 *
 */
public enum ErrorCode {
    ErrorOnFileSystem("common_error_code_error_on_file_system"),
    ErrorOnDB("common_error_code_error_on_db"),
    InaccurateData("common_error_code_inaccurate_data"),
    NotFound("common_error_code_no_found"),
    NetworkError("common_error_code_network_error"),
    CanNotFind("common_error_code_can_not_find"),
    CanNotUpload("common_error_code_can_not_upload"),
    CanNotDownload("common_error_code_can_not_download"),
    CanNotSave("common_error_code_can_not_save"),
    CanNotAdd("common_error_code_can_not_add"),
    CanNotModify("common_error_code_can_not_modify"),
    CanNotDelete("common_error_code_can_not_delete"),
    ViolationOfRule("common_error_code_violation_of_rule"),
    UnexpectedError("common_error_code_unexpected_error");

    private final String res;

    ErrorCode(String pRes) {
        res = pRes;
    }

    /**
     * get res id for this error code
     * @return
     */
    public int getErrorRes(){
        try {
            return R.string.class.getDeclaredField(res).getInt(null);
        } catch (Exception e) {
            Timber.e(e,"%s",e.getMessage());
            throw new RuntimeException(e);
        }
    }
}
