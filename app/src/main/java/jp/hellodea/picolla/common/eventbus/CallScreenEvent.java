package jp.hellodea.picolla.common.eventbus;

/**
 * Request screen change event
 * @author kazuhiro
 *
 */
public class CallScreenEvent extends BusEvent {

	public CallScreenEvent(Object eventKey, Object arg) {
		super(eventKey, arg);
	}

    public CallScreenEvent(Object eventKey, Object... args) {
        super(eventKey, args);
    }

    public CallScreenEvent(Object eventKey) {
        super(eventKey);
    }

    public CallScreenEvent() {
    }
}
