package jp.hellodea.picolla.common.objectgraph;

import dagger.ObjectGraph;

/**
 * Created by kazuhiro on 2014/12/18.
 */
public class ObjectGraphHolder {
    private static ObjectGraph objectGraph;

    public static ObjectGraph get() {
        return objectGraph;
    }

    synchronized public static void plus(Object module){
        if(objectGraph == null){
            objectGraph = ObjectGraph.create(module);
        }else{
            objectGraph = objectGraph.plus(module);
        }
    }

}
