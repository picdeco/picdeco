package jp.hellodea.picolla.common;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by kazuhiro on 2014/12/17.
 */
public class JsonMapperHolder {
    private static ObjectMapper objectMapper;

    public static ObjectMapper get() {
        if(objectMapper == null)
            objectMapper = new ObjectMapper();

        return objectMapper;
    }
}
