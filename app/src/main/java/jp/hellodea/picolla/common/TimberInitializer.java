package jp.hellodea.picolla.common;

import com.flurry.android.FlurryAgent;

import jp.hellodea.picolla.BuildConfig;
import timber.log.Timber;

/**
 * Created by kazuhiro on 2015/02/12.
 */
public class TimberInitializer {
    public static void initialize(){
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new CrashReportingTree());
        }
    }

    /** A tree which logs important information for crash reporting. */
    private static class CrashReportingTree extends Timber.HollowTree {
        @Override public void i(String message, Object... args) {
            // TODO e.g., Crashlytics.log(String.format(message, args));
        }

        @Override public void i(Throwable t, String message, Object... args) {
            i(message, args); // Just add to the log.
        }

        @Override public void e(String message, Object... args) {
            i("ERROR: " + message, args); // Just add to the log.
        }

        @Override public void e(Throwable t, String message, Object... args) {
            FlurryAgent.onError(null,t.getMessage(),t);
        }
    }

}
