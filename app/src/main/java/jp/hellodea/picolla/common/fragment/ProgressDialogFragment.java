package jp.hellodea.picolla.common.fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;


@EFragment
public class ProgressDialogFragment extends DialogFragment {

    @FragmentArg
    int titleRes;

    @FragmentArg
    int messageRes;

    @FragmentArg
    int progressStyle;

    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setTitle(titleRes);
        dialog.setMessage(getActivity().getString(messageRes));
        dialog.setProgressStyle(progressStyle);

        return dialog;
    }
}