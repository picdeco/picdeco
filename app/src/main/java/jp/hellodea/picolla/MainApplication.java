package jp.hellodea.picolla;

import android.app.Application;
import android.util.Log;

import com.koushikdutta.ion.Ion;

import jp.hellodea.picolla.common.TimberInitializer;
import jp.hellodea.picolla.common.objectgraph.CommonModule;
import jp.hellodea.picolla.common.objectgraph.ObjectGraphHolder;
import jp.hellodea.picolla.deco.Flurry;
import jp.hellodea.picolla.deco.ParseInitializer;
import jp.hellodea.picolla.deco.DIModule;
import jp.hellodea.picolla.deco.util.ContextUtil;

/**
 * Created by kazuhiro on 2015/02/10.
 */
public class MainApplication extends Application{

    @Override
    public void onCreate() {
        super.onCreate();

        Log.d("MainApp.onCreate", BuildConfig.BUILD_TYPE);

        TimberInitializer.initialize();

        ObjectGraphHolder.plus(new CommonModule(this));
        ObjectGraphHolder.plus(new DIModule());

        Ion.getDefault(this).configure().setLogging("Ion", Log.DEBUG);

        ParseInitializer.initialize(this);

        Flurry.initialize(this);

        //initial memory size
        ContextUtil.showHeapMemoryInfo(this);

    }

}
