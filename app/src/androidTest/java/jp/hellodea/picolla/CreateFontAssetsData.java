package jp.hellodea.picolla;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import jp.hellodea.picolla.common.exception.LogicException;
import jp.hellodea.picolla.deco.entity.Font;

/**
 * Created by kazuhiro on 2015/04/17.
 */
@RunWith(AndroidJUnit4.class)
public class CreateFontAssetsData {

    @BeforeClass
    public static void initWholeTest(){
        Context context = InstrumentationRegistry.getTargetContext();
        return;

    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @AfterClass
    public static void finishWholeTest(){

    }

    @Test
    public void createData() throws ParseException, LogicException {

        //今あるデータを全て削除
        ParseObject.deleteAll(ParseQuery.getQuery(Font.class).find());

        saveFont("BLKCHCRY.TTF");
        saveFont("ChopinScript.otf");
        saveFont("Designer-Notes-Bold.ttf");
        saveFont("Essays1743-BoldItalic.ttf");
        saveFont("HuiFontP109.ttf");
        saveFont("LOLII___.TTF");
        saveFont("MakibaFontBP13.ttf");
        saveFont("MIZIKE.ttf");
        saveFont("mofuji.ttf");
        saveFont("OhisamaFontB11.ttf");
        saveFont("papercuts-2.ttf");
        saveFont("TanukiMagic.ttf");
        saveFont("THICKHEA.TTF");

    }

    private Font saveFont(String path) throws ParseException {
        Font font = new Font();
        font.put(Font.F_PATH,path);
        font.put(Font.F_ORDER, 1);
        font.put(Font.F_IS_ACTIVE,true);
        font.save();
        return font;
    }

}
