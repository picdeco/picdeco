package jp.hellodea.picolla;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import hugo.weaving.DebugLog;
import jp.hellodea.picolla.common.exception.LogicException;
import jp.hellodea.picolla.deco.entity.SampleEntity;
import timber.log.Timber;

/**
 * Created by kazuhiro on 2015/04/17.
 */
@RunWith(AndroidJUnit4.class)
public class ParseLocalStorageTest {

    @BeforeClass
    public static void initWholeTest() throws ParseException {
        Context context = InstrumentationRegistry.getTargetContext();

        return;

    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @AfterClass
    public static void finishWholeTest(){

    }

    @DebugLog
    private static void setupTestData(int max) throws ParseException {

        //今あるデータを全て削除
        ParseObject.unpinAll("test");

        int loop = max / 50;

        for(int l = 0; l < loop; l++){

            List<SampleEntity> entities = new ArrayList<>();

            for (int i = 0; i < 50; i++){
                entities.add(createEntity(i));
            }

            ParseObject.pinAll("test",entities);
            Timber.d("create %d", l * 50);

        }

    }

    private static SampleEntity createEntity(int i) throws ParseException {
        SampleEntity entity = new SampleEntity();
        entity.put(SampleEntity.F_COLUMN0,String.format("%03d",i));
        entity.put(SampleEntity.F_COLUMN1,String.format("%03d",i + 1));
        entity.put(SampleEntity.F_COLUMN2,String.format("%03d",i + 2));
        entity.put(SampleEntity.F_COLUMN3,String.format("%03d",i + 3));
        entity.put(SampleEntity.F_COLUMN4,String.format("%03d",i + 4));
        entity.put(SampleEntity.F_COLUMN5,String.format("%03d",i + 5));
        entity.put(SampleEntity.F_COLUMN6,String.format("%03d",i + 6));
        entity.put(SampleEntity.F_COLUMN7,String.format("%03d",i + 7));
        entity.put(SampleEntity.F_COLUMN8,String.format("%03d",i + 8));
        entity.put(SampleEntity.F_COLUMN9,String.format("%03d",i + 9));

        return entity;
    }


    @DebugLog
    private void commonTest(int max) throws ParseException {
        Timber.d("data count: %d", max);

        setupTestData(max);

        Timber.d("step1");

        List<SampleEntity> entities = ParseQuery.getQuery(SampleEntity.class)
                .fromPin("test")
                .whereEqualTo(SampleEntity.F_COLUMN0,String.format("%03d",20))
                .orderByDescending(SampleEntity.F_COLUMN5)
                .find();

        Timber.d("step2 count:%d",entities.size());

        SampleEntity entity = createEntity(999999);
        entity.pin();

        Timber.d("step3");

    }


    @Test
    @DebugLog
    public void test() throws ParseException, LogicException {
        commonTest(100);
//        commonTest(1000);
//        commonTest(10000);
//        commonTest(100000);
    }


}
