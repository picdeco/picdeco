package jp.hellodea.picolla;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import jp.hellodea.picolla.common.exception.LogicException;
import jp.hellodea.picolla.deco.entity.Effect;
import jp.hellodea.picolla.deco.entity.Filter;

/**
 * Created by kazuhiro on 2015/04/17.
 */
@RunWith(AndroidJUnit4.class)
public class CreateFilterEffectAssetsData {

    @BeforeClass
    public static void initWholeTest(){
        Context context = InstrumentationRegistry.getTargetContext();
        return;

    }

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {
    }

    @AfterClass
    public static void finishWholeTest(){

    }

    @Test
    public void createData() throws ParseException, LogicException {

        //今あるデータを全て削除
        ParseObject.deleteAll(ParseQuery.getQuery(Filter.class).find());
        ParseObject.deleteAll(ParseQuery.getQuery(Effect.class).find());

        saveFilter("01");
        saveFilter("02");
        saveFilter("03");
        saveFilter("04");
        saveFilter("05");
        saveFilter("06");
        saveFilter("07");
        saveFilter("08");
        saveFilter("09");
        saveFilter("10");
        saveFilter("11");
        saveFilter("12");
        saveFilter("13");
        saveFilter("14");
        saveFilter("15");

        saveEffect("e01");
        saveEffect("e02");
        saveEffect("e03");
        saveEffect("e04");
        saveEffect("e05");
        saveEffect("e06");
        saveEffect("e07");

    }

    private Filter saveFilter(String path) throws ParseException {
        Filter filter = new Filter();
        filter.put(Filter.F_PATH,path);
        filter.put(Filter.F_ORDER, 1);
        filter.put(Filter.F_IS_ACTIVE,true);
        filter.save();
        return filter;
    }

    private Effect saveEffect(String path) throws ParseException {
        Effect effect = new Effect();
        effect.put(Effect.F_PATH,path);
        effect.put(Effect.F_ORDER, 1);
        effect.put(Effect.F_IS_ACTIVE,true);
        effect.save();
        return effect;
    }

}
