package jp.hellodea.picolla;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import hugo.weaving.DebugLog;
import jp.hellodea.picolla.deco.Const;
import jp.hellodea.picolla.deco.entity.Clipart;
import jp.hellodea.picolla.deco.entity.ClipartKind;
import timber.log.Timber;

/**
 * Created by kazuhiro on 2015/04/17.
 */
@RunWith(AndroidJUnit4.class)
public class CreateClipartAssetsData {
    @BeforeClass
    public static void initWholeTest(){
        Context context = InstrumentationRegistry.getTargetContext();
        return;

    }

    @Before
    public void initEachTest() throws ParseException {
    }

    @After
    public void finishEachTest(){

    }


    @Test
    public void createData() throws Exception {

        //今あるデータを全て削除
        while (true){
            List<Clipart> cliparts = ParseQuery.getQuery(Clipart.class).setLimit(50).find();
            Timber.d("find clipart %d", cliparts.size());

            if(cliparts.size() == 0) break;

            ParseObject.deleteAll(cliparts);
            Timber.d("delete clipart %d", cliparts.size());

        }

        List<ClipartKind> kinds = ParseQuery.getQuery(ClipartKind.class).setLimit(100).find();
        ParseObject.deleteAll(kinds);

        Timber.d("delete clipartKind");


        saveFrame();
        saveBackground();
        saveClipart();

//        category = saveCategory("mask_stamp"));
    }

    @DebugLog
    private void saveFrame() throws Exception {
        ClipartKind kind;
        
        List<ParseObject> parseObjects = new ArrayList<>();

        kind = saveKind(Const.CLIPART_CATEGORY_FRAME, "01","01", ".png");

        parseObjects.add(createClipart(kind, "01", ".png"));
        parseObjects.add(createClipart(kind, "02", ".png"));
        parseObjects.add(createClipart(kind, "03", ".png"));
        parseObjects.add(createClipart(kind, "04", ".png"));
        parseObjects.add(createClipart(kind, "05", ".png"));
        parseObjects.add(createClipart(kind, "06", ".png"));
        parseObjects.add(createClipart(kind, "07", ".png"));

        ParseObject.saveAll(parseObjects);
        Timber.d("save frame 01");
        Timber.d("add %d",parseObjects.size());


        parseObjects = new ArrayList<>();


        kind = saveKind(Const.CLIPART_CATEGORY_FRAME, "02", "01", ".png");

        parseObjects.add(createClipart(kind, "01", ".png"));
        parseObjects.add(createClipart(kind, "02", ".png"));
        parseObjects.add(createClipart(kind, "03", ".png"));
        parseObjects.add(createClipart(kind, "04", ".png"));
        parseObjects.add(createClipart(kind, "05", ".png"));
        parseObjects.add(createClipart(kind, "06", ".png"));
        parseObjects.add(createClipart(kind, "07", ".png"));
        parseObjects.add(createClipart(kind, "08", ".png"));
        parseObjects.add(createClipart(kind, "09", ".png"));

        ParseObject.saveAll(parseObjects);
        Timber.d("save frame 02");
        Timber.d("add %d",parseObjects.size());


        parseObjects = new ArrayList<>();
        kind = saveKind(Const.CLIPART_CATEGORY_FRAME, "03", "01", ".png");

        parseObjects.add(createClipart(kind, "01", ".png"));
        parseObjects.add(createClipart(kind, "02", ".png"));
        parseObjects.add(createClipart(kind, "03", ".png"));
        parseObjects.add(createClipart(kind, "04", ".png"));
        parseObjects.add(createClipart(kind, "05", ".png"));
        parseObjects.add(createClipart(kind, "06", ".png"));
        parseObjects.add(createClipart(kind, "07", ".png"));
        parseObjects.add(createClipart(kind, "08", ".png"));
        parseObjects.add(createClipart(kind, "09", ".png"));
        parseObjects.add(createClipart(kind, "10", ".png"));

        ParseObject.saveAll(parseObjects);
        Timber.d("save frame 03");
        Timber.d("add %d",parseObjects.size());

        parseObjects = new ArrayList<>();
        kind = saveKind(Const.CLIPART_CATEGORY_FRAME, "04", "01", ".png");

        parseObjects.add(createClipart(kind, "01", ".png"));
        parseObjects.add(createClipart(kind, "02", ".png"));
        parseObjects.add(createClipart(kind, "03", ".png"));
        parseObjects.add(createClipart(kind, "04", ".png"));
        parseObjects.add(createClipart(kind, "05", ".png"));
        parseObjects.add(createClipart(kind, "06", ".png"));
        parseObjects.add(createClipart(kind, "07", ".png"));

        ParseObject.saveAll(parseObjects);
        Timber.d("save frame 04");
        Timber.d("add %d",parseObjects.size());

        parseObjects = new ArrayList<>();

        kind = saveKind(Const.CLIPART_CATEGORY_FRAME, "05", "01", ".png");

        parseObjects.add(createClipart(kind, "01", ".png"));
        parseObjects.add(createClipart(kind, "02", ".png"));
        parseObjects.add(createClipart(kind, "03", ".png"));
        parseObjects.add(createClipart(kind, "04", ".png"));
        parseObjects.add(createClipart(kind, "05", ".png"));
        parseObjects.add(createClipart(kind, "06", ".png"));
        parseObjects.add(createClipart(kind, "07", ".png"));

        ParseObject.saveAll(parseObjects);
        Timber.d("save frame 05");
        Timber.d("add %d",parseObjects.size());

        parseObjects = new ArrayList<>();

        kind = saveKind(Const.CLIPART_CATEGORY_FRAME, "06", "01", ".png");

        parseObjects.add(createClipart(kind, "01", ".png"));
        parseObjects.add(createClipart(kind, "02", ".png"));
        parseObjects.add(createClipart(kind, "03", ".png"));
        parseObjects.add(createClipart(kind, "04", ".png"));
        parseObjects.add(createClipart(kind, "05", ".png"));
        parseObjects.add(createClipart(kind, "06", ".png"));
        parseObjects.add(createClipart(kind, "07", ".png"));
        parseObjects.add(createClipart(kind, "08", ".png"));
        parseObjects.add(createClipart(kind, "09", ".png"));
        parseObjects.add(createClipart(kind, "10", ".png"));
        parseObjects.add(createClipart(kind, "11", ".png"));
        parseObjects.add(createClipart(kind, "12", ".png"));
        parseObjects.add(createClipart(kind, "13", ".png"));
        parseObjects.add(createClipart(kind, "14", ".png"));
        parseObjects.add(createClipart(kind, "15", ".png"));
        parseObjects.add(createClipart(kind, "16", ".png"));

        ParseObject.saveAll(parseObjects);
        Timber.d("save frame 06");
        Timber.d("add %d",parseObjects.size());

        parseObjects = new ArrayList<>();

        kind = saveKind(Const.CLIPART_CATEGORY_FRAME, "season_halloween", "01", ".png");

        parseObjects.add(createClipart(kind, "01", ".png"));
        parseObjects.add(createClipart(kind, "02", ".png"));
        parseObjects.add(createClipart(kind, "03", ".png"));
        parseObjects.add(createClipart(kind, "04", ".png"));
        parseObjects.add(createClipart(kind, "05", ".png"));
        parseObjects.add(createClipart(kind, "06", ".png"));
        parseObjects.add(createClipart(kind, "07", ".png"));
        parseObjects.add(createClipart(kind, "08", ".png"));
        parseObjects.add(createClipart(kind, "09", ".png"));
        parseObjects.add(createClipart(kind, "10", ".png"));

        ParseObject.saveAll(parseObjects);
        Timber.d("save frame season_halloween");
        Timber.d("add %d",parseObjects.size());

    }

    @DebugLog
    private void saveClipart() throws Exception {
        ClipartKind kind;

        List<ParseObject> parseObjects = new ArrayList<>();


        kind = saveKind(Const.CLIPART_CATEGORY_CLIPART, "01hart", "01", ".png");

        parseObjects.add(createClipart(kind, "01", ".png"));
        parseObjects.add(createClipart(kind, "02", ".png"));
        parseObjects.add(createClipart(kind, "03", ".png"));
        parseObjects.add(createClipart(kind, "04", ".png"));
        parseObjects.add(createClipart(kind, "05", ".png"));
        parseObjects.add(createClipart(kind, "06", ".png"));
        parseObjects.add(createClipart(kind, "07", ".png"));
        parseObjects.add(createClipart(kind, "08", ".png"));
        parseObjects.add(createClipart(kind, "09", ".png"));
        parseObjects.add(createClipart(kind, "10", ".png"));
        parseObjects.add(createClipart(kind, "11", ".png"));
        parseObjects.add(createClipart(kind, "12", ".png"));
        parseObjects.add(createClipart(kind, "13", ".png"));
        parseObjects.add(createClipart(kind, "14", ".png"));
        parseObjects.add(createClipart(kind, "15", ".png"));
        parseObjects.add(createClipart(kind, "16", ".png"));
        parseObjects.add(createClipart(kind, "17", ".png"));
        parseObjects.add(createClipart(kind, "18", ".png"));
        parseObjects.add(createClipart(kind, "19", ".png"));
        parseObjects.add(createClipart(kind, "20", ".png"));
        parseObjects.add(createClipart(kind, "21", ".png"));
        parseObjects.add(createClipart(kind, "22", ".png"));
        parseObjects.add(createClipart(kind, "23", ".png"));
        parseObjects.add(createClipart(kind, "24", ".png"));
        parseObjects.add(createClipart(kind, "25", ".png"));
        parseObjects.add(createClipart(kind, "26", ".png"));
        parseObjects.add(createClipart(kind, "27", ".png"));
        parseObjects.add(createClipart(kind, "28", ".png"));
        parseObjects.add(createClipart(kind, "29", ".png"));
        parseObjects.add(createClipart(kind, "30", ".png"));

        ParseObject.saveAll(parseObjects);
        Timber.d("save clipart 01-1");
        Timber.d("add %d",parseObjects.size());

        parseObjects = new ArrayList<>();

        parseObjects.add(createClipart(kind, "31", ".png"));
        parseObjects.add(createClipart(kind, "32", ".png"));
        parseObjects.add(createClipart(kind, "33", ".png"));
        parseObjects.add(createClipart(kind, "34", ".png"));
        parseObjects.add(createClipart(kind, "35", ".png"));
        parseObjects.add(createClipart(kind, "36", ".png"));
        parseObjects.add(createClipart(kind, "37", ".png"));
        parseObjects.add(createClipart(kind, "38", ".png"));
        parseObjects.add(createClipart(kind, "39", ".png"));
        parseObjects.add(createClipart(kind, "40", ".png"));
        parseObjects.add(createClipart(kind, "41", ".png"));
        parseObjects.add(createClipart(kind, "42", ".png"));
        parseObjects.add(createClipart(kind, "43", ".png"));
        parseObjects.add(createClipart(kind, "44", ".png"));
        parseObjects.add(createClipart(kind, "45", ".png"));
        parseObjects.add(createClipart(kind, "46", ".png"));
        parseObjects.add(createClipart(kind, "47", ".png"));
        parseObjects.add(createClipart(kind, "48", ".png"));
        parseObjects.add(createClipart(kind, "49", ".png"));
        parseObjects.add(createClipart(kind, "50", ".png"));
        parseObjects.add(createClipart(kind, "51", ".png"));
        parseObjects.add(createClipart(kind, "52", ".png"));
        parseObjects.add(createClipart(kind, "53", ".png"));
        parseObjects.add(createClipart(kind, "54", ".png"));
        parseObjects.add(createClipart(kind, "55", ".png"));
        parseObjects.add(createClipart(kind, "56", ".png"));
        parseObjects.add(createClipart(kind, "57", ".png"));
        parseObjects.add(createClipart(kind, "58", ".png"));
        parseObjects.add(createClipart(kind, "59", ".png"));
        parseObjects.add(createClipart(kind, "60", ".png"));

        ParseObject.saveAll(parseObjects);
        Timber.d("save clipart 01-2");
        Timber.d("add %d",parseObjects.size());

        parseObjects = new ArrayList<>();

        kind = saveKind(Const.CLIPART_CATEGORY_CLIPART, "02star", "01", ".png");

        parseObjects.add(createClipart(kind, "01", ".png"));
        parseObjects.add(createClipart(kind, "02", ".png"));
        parseObjects.add(createClipart(kind, "03", ".png"));
        parseObjects.add(createClipart(kind, "04", ".png"));
        parseObjects.add(createClipart(kind, "05", ".png"));
        parseObjects.add(createClipart(kind, "06", ".png"));
        parseObjects.add(createClipart(kind, "07", ".png"));
        parseObjects.add(createClipart(kind, "08", ".png"));
        parseObjects.add(createClipart(kind, "09", ".png"));
        parseObjects.add(createClipart(kind, "10", ".png"));
        parseObjects.add(createClipart(kind, "11", ".png"));
        parseObjects.add(createClipart(kind, "12", ".png"));
        parseObjects.add(createClipart(kind, "13", ".png"));
        parseObjects.add(createClipart(kind, "14", ".png"));
        parseObjects.add(createClipart(kind, "15", ".png"));
        parseObjects.add(createClipart(kind, "16", ".png"));
        parseObjects.add(createClipart(kind, "17", ".png"));
        parseObjects.add(createClipart(kind, "18", ".png"));
        parseObjects.add(createClipart(kind, "19", ".png"));
        parseObjects.add(createClipart(kind, "20", ".png"));
        parseObjects.add(createClipart(kind, "21", ".png"));
        parseObjects.add(createClipart(kind, "22", ".png"));
        parseObjects.add(createClipart(kind, "23", ".png"));
        parseObjects.add(createClipart(kind, "24", ".png"));
        parseObjects.add(createClipart(kind, "25", ".png"));
        parseObjects.add(createClipart(kind, "26", ".png"));
        parseObjects.add(createClipart(kind, "27", ".png"));
        parseObjects.add(createClipart(kind, "28", ".png"));
        parseObjects.add(createClipart(kind, "29", ".png"));
        parseObjects.add(createClipart(kind, "30", ".png"));
        parseObjects.add(createClipart(kind, "31", ".png"));
        parseObjects.add(createClipart(kind, "32", ".png"));
        parseObjects.add(createClipart(kind, "33", ".png"));
        parseObjects.add(createClipart(kind, "34", ".png"));
        parseObjects.add(createClipart(kind, "35", ".png"));

        ParseObject.saveAll(parseObjects);
        Timber.d("save clipart 02");
        Timber.d("add %d",parseObjects.size());

        parseObjects = new ArrayList<>();

        kind = saveKind(Const.CLIPART_CATEGORY_CLIPART, "03flower", "01", ".png");

        parseObjects.add(createClipart(kind, "01", ".png"));
        parseObjects.add(createClipart(kind, "02", ".png"));
        parseObjects.add(createClipart(kind, "03", ".png"));
        parseObjects.add(createClipart(kind, "04", ".png"));
        parseObjects.add(createClipart(kind, "05", ".png"));
        parseObjects.add(createClipart(kind, "06", ".png"));
        parseObjects.add(createClipart(kind, "07", ".png"));
        parseObjects.add(createClipart(kind, "08", ".png"));
        parseObjects.add(createClipart(kind, "09", ".png"));
        parseObjects.add(createClipart(kind, "10", ".png"));
        parseObjects.add(createClipart(kind, "11", ".png"));
        parseObjects.add(createClipart(kind, "12", ".png"));
        parseObjects.add(createClipart(kind, "13", ".png"));
        parseObjects.add(createClipart(kind, "14", ".png"));
        parseObjects.add(createClipart(kind, "15", ".png"));
        parseObjects.add(createClipart(kind, "16", ".png"));
        parseObjects.add(createClipart(kind, "17", ".png"));
        parseObjects.add(createClipart(kind, "18", ".png"));
        parseObjects.add(createClipart(kind, "19", ".png"));
        parseObjects.add(createClipart(kind, "20", ".png"));
        parseObjects.add(createClipart(kind, "21", ".png"));
        parseObjects.add(createClipart(kind, "22", ".png"));
        parseObjects.add(createClipart(kind, "23", ".png"));
        parseObjects.add(createClipart(kind, "24", ".png"));
        parseObjects.add(createClipart(kind, "25", ".png"));
        parseObjects.add(createClipart(kind, "26", ".png"));
        parseObjects.add(createClipart(kind, "27", ".png"));
        parseObjects.add(createClipart(kind, "28", ".png"));
        parseObjects.add(createClipart(kind, "29", ".png"));
        parseObjects.add(createClipart(kind, "30", ".png"));
        parseObjects.add(createClipart(kind, "31", ".png"));
        parseObjects.add(createClipart(kind, "32", ".png"));
        parseObjects.add(createClipart(kind, "33", ".png"));
        parseObjects.add(createClipart(kind, "34", ".png"));
        parseObjects.add(createClipart(kind, "35", ".png"));

        ParseObject.saveAll(parseObjects);

        Timber.d("save clipart 03");
        Timber.d("add %d",parseObjects.size());

        parseObjects = new ArrayList<>();

        kind = saveKind(Const.CLIPART_CATEGORY_CLIPART, "04ribbon", "01", ".png");

        parseObjects.add(createClipart(kind, "01", ".png"));
        parseObjects.add(createClipart(kind, "02", ".png"));
        parseObjects.add(createClipart(kind, "03", ".png"));
        parseObjects.add(createClipart(kind, "04", ".png"));
        parseObjects.add(createClipart(kind, "05", ".png"));
        parseObjects.add(createClipart(kind, "06", ".png"));
        parseObjects.add(createClipart(kind, "07", ".png"));
        parseObjects.add(createClipart(kind, "08", ".png"));
        parseObjects.add(createClipart(kind, "09", ".png"));
        parseObjects.add(createClipart(kind, "10", ".png"));
        parseObjects.add(createClipart(kind, "11", ".png"));
        parseObjects.add(createClipart(kind, "12", ".png"));
        parseObjects.add(createClipart(kind, "13", ".png"));
        parseObjects.add(createClipart(kind, "14", ".png"));
        parseObjects.add(createClipart(kind, "15", ".png"));
        parseObjects.add(createClipart(kind, "16", ".png"));
        parseObjects.add(createClipart(kind, "17", ".png"));
        parseObjects.add(createClipart(kind, "18", ".png"));
        parseObjects.add(createClipart(kind, "19", ".png"));

        ParseObject.saveAll(parseObjects);

        Timber.d("save clipart 04");
        Timber.d("add %d",parseObjects.size());

        parseObjects = new ArrayList<>();

        kind = saveKind(Const.CLIPART_CATEGORY_CLIPART, "06masking_tape", "01", ".png");

        parseObjects.add(createClipart(kind, "01", ".png"));
        parseObjects.add(createClipart(kind, "02", ".png"));
        parseObjects.add(createClipart(kind, "03", ".png"));
        parseObjects.add(createClipart(kind, "04", ".png"));
        parseObjects.add(createClipart(kind, "05", ".png"));
        parseObjects.add(createClipart(kind, "06", ".png"));
        parseObjects.add(createClipart(kind, "07", ".png"));
        parseObjects.add(createClipart(kind, "08", ".png"));
        parseObjects.add(createClipart(kind, "09", ".png"));
        parseObjects.add(createClipart(kind, "10", ".png"));
        parseObjects.add(createClipart(kind, "11", ".png"));
        parseObjects.add(createClipart(kind, "12", ".png"));
        parseObjects.add(createClipart(kind, "13", ".png"));
        parseObjects.add(createClipart(kind, "14", ".png"));
        parseObjects.add(createClipart(kind, "15", ".png"));

        ParseObject.saveAll(parseObjects);

        Timber.d("save clipart 05");
        Timber.d("add %d",parseObjects.size());

        parseObjects = new ArrayList<>();

        kind = saveKind(Const.CLIPART_CATEGORY_CLIPART, "08frame", "01", ".png");

        parseObjects.add(createClipart(kind, "01", ".png"));
        parseObjects.add(createClipart(kind, "02", ".png"));
        parseObjects.add(createClipart(kind, "03", ".png"));
        parseObjects.add(createClipart(kind, "04", ".png"));
        parseObjects.add(createClipart(kind, "05", ".png"));
        parseObjects.add(createClipart(kind, "06", ".png"));
        parseObjects.add(createClipart(kind, "07", ".png"));
        parseObjects.add(createClipart(kind, "08", ".png"));
        parseObjects.add(createClipart(kind, "09", ".png"));
        parseObjects.add(createClipart(kind, "10", ".png"));
        parseObjects.add(createClipart(kind, "11", ".png"));
        parseObjects.add(createClipart(kind, "12", ".png"));
        parseObjects.add(createClipart(kind, "13", ".png"));
        parseObjects.add(createClipart(kind, "14", ".png"));
        parseObjects.add(createClipart(kind, "15", ".png"));
        parseObjects.add(createClipart(kind, "16", ".png"));
        parseObjects.add(createClipart(kind, "17", ".png"));
        parseObjects.add(createClipart(kind, "18", ".png"));
        parseObjects.add(createClipart(kind, "19", ".png"));
        parseObjects.add(createClipart(kind, "20", ".png"));
        parseObjects.add(createClipart(kind, "21", ".png"));
        parseObjects.add(createClipart(kind, "22", ".png"));
        parseObjects.add(createClipart(kind, "23", ".png"));
        parseObjects.add(createClipart(kind, "24", ".png"));
        parseObjects.add(createClipart(kind, "25", ".png"));
        parseObjects.add(createClipart(kind, "26", ".png"));
        parseObjects.add(createClipart(kind, "27", ".png"));
        parseObjects.add(createClipart(kind, "28", ".png"));
        parseObjects.add(createClipart(kind, "29", ".png"));
        parseObjects.add(createClipart(kind, "30", ".png"));

        ParseObject.saveAll(parseObjects);

        Timber.d("save clipart 06");
        Timber.d("add %d",parseObjects.size());

        parseObjects = new ArrayList<>();

        kind = saveKind(Const.CLIPART_CATEGORY_CLIPART, "09face", "01", ".png");

        parseObjects.add(createClipart(kind, "01", ".png"));
        parseObjects.add(createClipart(kind, "02", ".png"));
        parseObjects.add(createClipart(kind, "03", ".png"));
        parseObjects.add(createClipart(kind, "04", ".png"));
        parseObjects.add(createClipart(kind, "05", ".png"));
        parseObjects.add(createClipart(kind, "06", ".png"));
        parseObjects.add(createClipart(kind, "07", ".png"));
        parseObjects.add(createClipart(kind, "08", ".png"));
        parseObjects.add(createClipart(kind, "09", ".png"));
        parseObjects.add(createClipart(kind, "10", ".png"));
        parseObjects.add(createClipart(kind, "11", ".png"));
        parseObjects.add(createClipart(kind, "12", ".png"));

        ParseObject.saveAll(parseObjects);

        Timber.d("save clipart 07");
        Timber.d("add %d",parseObjects.size());

        parseObjects = new ArrayList<>();

        kind = saveKind(Const.CLIPART_CATEGORY_CLIPART, "10balloon", "01", ".png");

        parseObjects.add(createClipart(kind, "01", ".png"));
        parseObjects.add(createClipart(kind, "02", ".png"));
        parseObjects.add(createClipart(kind, "03", ".png"));
        parseObjects.add(createClipart(kind, "04", ".png"));
        parseObjects.add(createClipart(kind, "05", ".png"));
        parseObjects.add(createClipart(kind, "06", ".png"));
        parseObjects.add(createClipart(kind, "07", ".png"));
        parseObjects.add(createClipart(kind, "08", ".png"));
        parseObjects.add(createClipart(kind, "09", ".png"));
        parseObjects.add(createClipart(kind, "10", ".png"));
        parseObjects.add(createClipart(kind, "11", ".png"));
        parseObjects.add(createClipart(kind, "12", ".png"));
        parseObjects.add(createClipart(kind, "13", ".png"));
        parseObjects.add(createClipart(kind, "14", ".png"));
        parseObjects.add(createClipart(kind, "15", ".png"));
        parseObjects.add(createClipart(kind, "16", ".png"));
        parseObjects.add(createClipart(kind, "17", ".png"));
        parseObjects.add(createClipart(kind, "18", ".png"));
        parseObjects.add(createClipart(kind, "19", ".png"));
        parseObjects.add(createClipart(kind, "20", ".png"));
        parseObjects.add(createClipart(kind, "21", ".png"));
        parseObjects.add(createClipart(kind, "22", ".png"));
        parseObjects.add(createClipart(kind, "23", ".png"));
        parseObjects.add(createClipart(kind, "24", ".png"));
        parseObjects.add(createClipart(kind, "25", ".png"));
        parseObjects.add(createClipart(kind, "26", ".png"));
        parseObjects.add(createClipart(kind, "27", ".png"));
        parseObjects.add(createClipart(kind, "28", ".png"));
        parseObjects.add(createClipart(kind, "29", ".png"));
        parseObjects.add(createClipart(kind, "30", ".png"));
        parseObjects.add(createClipart(kind, "31", ".png"));
        parseObjects.add(createClipart(kind, "32", ".png"));
        parseObjects.add(createClipart(kind, "33", ".png"));
        parseObjects.add(createClipart(kind, "34", ".png"));
        parseObjects.add(createClipart(kind, "35", ".png"));

        ParseObject.saveAll(parseObjects);

        Timber.d("save clipart 08");
        Timber.d("add %d",parseObjects.size());

        parseObjects = new ArrayList<>();

        kind = saveKind(Const.CLIPART_CATEGORY_CLIPART, "11text", "01", ".png");

        parseObjects.add(createClipart(kind, "01", ".png"));
        parseObjects.add(createClipart(kind, "02", ".png"));
        parseObjects.add(createClipart(kind, "03", ".png"));
        parseObjects.add(createClipart(kind, "04", ".png"));
        parseObjects.add(createClipart(kind, "05", ".png"));
        parseObjects.add(createClipart(kind, "06", ".png"));
        parseObjects.add(createClipart(kind, "07", ".png"));
        parseObjects.add(createClipart(kind, "08", ".png"));
        parseObjects.add(createClipart(kind, "09", ".png"));
        parseObjects.add(createClipart(kind, "10", ".png"));
        parseObjects.add(createClipart(kind, "11", ".png"));
        parseObjects.add(createClipart(kind, "12", ".png"));
        parseObjects.add(createClipart(kind, "13", ".png"));
        parseObjects.add(createClipart(kind, "14", ".png"));
        parseObjects.add(createClipart(kind, "15", ".png"));
        parseObjects.add(createClipart(kind, "16", ".png"));
        parseObjects.add(createClipart(kind, "17", ".png"));
        parseObjects.add(createClipart(kind, "18", ".png"));

        ParseObject.saveAll(parseObjects);

        Timber.d("save clipart 09");
        Timber.d("add %d",parseObjects.size());

        parseObjects = new ArrayList<>();

        kind = saveKind(Const.CLIPART_CATEGORY_CLIPART, "12girly", "01", ".png");

        parseObjects.add(createClipart(kind, "01", ".png"));
        parseObjects.add(createClipart(kind, "02", ".png"));
        parseObjects.add(createClipart(kind, "03", ".png"));
        parseObjects.add(createClipart(kind, "04", ".png"));
        parseObjects.add(createClipart(kind, "05", ".png"));
        parseObjects.add(createClipart(kind, "06", ".png"));
        parseObjects.add(createClipart(kind, "07", ".png"));
        parseObjects.add(createClipart(kind, "08", ".png"));
        parseObjects.add(createClipart(kind, "09", ".png"));
        parseObjects.add(createClipart(kind, "10", ".png"));
        parseObjects.add(createClipart(kind, "11", ".png"));
        parseObjects.add(createClipart(kind, "12", ".png"));
        parseObjects.add(createClipart(kind, "13", ".png"));
        parseObjects.add(createClipart(kind, "14", ".png"));
        parseObjects.add(createClipart(kind, "15", ".png"));
        parseObjects.add(createClipart(kind, "16", ".png"));
        parseObjects.add(createClipart(kind, "17", ".png"));
        parseObjects.add(createClipart(kind, "18", ".png"));
        parseObjects.add(createClipart(kind, "19", ".png"));
        parseObjects.add(createClipart(kind, "20", ".png"));
        parseObjects.add(createClipart(kind, "21", ".png"));
        parseObjects.add(createClipart(kind, "22", ".png"));
        parseObjects.add(createClipart(kind, "23", ".png"));
        parseObjects.add(createClipart(kind, "24", ".png"));
        parseObjects.add(createClipart(kind, "25", ".png"));
        parseObjects.add(createClipart(kind, "26", ".png"));
        parseObjects.add(createClipart(kind, "27", ".png"));
        parseObjects.add(createClipart(kind, "28", ".png"));
        parseObjects.add(createClipart(kind, "29", ".png"));
        parseObjects.add(createClipart(kind, "30", ".png"));
        parseObjects.add(createClipart(kind, "31", ".png"));
        parseObjects.add(createClipart(kind, "32", ".png"));
        parseObjects.add(createClipart(kind, "33", ".png"));
        parseObjects.add(createClipart(kind, "34", ".png"));
        parseObjects.add(createClipart(kind, "35", ".png"));
        parseObjects.add(createClipart(kind, "36", ".png"));
        parseObjects.add(createClipart(kind, "37", ".png"));
        parseObjects.add(createClipart(kind, "38", ".png"));
        parseObjects.add(createClipart(kind, "39", ".png"));
        parseObjects.add(createClipart(kind, "40", ".png"));
        parseObjects.add(createClipart(kind, "41", ".png"));
        parseObjects.add(createClipart(kind, "42", ".png"));
        parseObjects.add(createClipart(kind, "43", ".png"));
        parseObjects.add(createClipart(kind, "44", ".png"));
        parseObjects.add(createClipart(kind, "45", ".png"));

        ParseObject.saveAll(parseObjects);

        Timber.d("save clipart 10");
        Timber.d("add %d",parseObjects.size());

        parseObjects = new ArrayList<>();

        kind = saveKind(Const.CLIPART_CATEGORY_CLIPART, "13girly_frame", "01", ".png");

        parseObjects.add(createClipart(kind, "01", ".png"));
        parseObjects.add(createClipart(kind, "02", ".png"));
        parseObjects.add(createClipart(kind, "03", ".png"));
        parseObjects.add(createClipart(kind, "04", ".png"));
        parseObjects.add(createClipart(kind, "05", ".png"));
        parseObjects.add(createClipart(kind, "06", ".png"));
        parseObjects.add(createClipart(kind, "07", ".png"));
        parseObjects.add(createClipart(kind, "08", ".png"));
        parseObjects.add(createClipart(kind, "09", ".png"));
        parseObjects.add(createClipart(kind, "10", ".png"));
        parseObjects.add(createClipart(kind, "11", ".png"));
        parseObjects.add(createClipart(kind, "12", ".png"));

        ParseObject.saveAll(parseObjects);

        Timber.d("save clipart 11");
        Timber.d("add %d",parseObjects.size());

        parseObjects = new ArrayList<>();

        kind = saveKind(Const.CLIPART_CATEGORY_CLIPART, "14jewelry", "01", ".png");

        parseObjects.add(createClipart(kind, "01", ".png"));
        parseObjects.add(createClipart(kind, "02", ".png"));
        parseObjects.add(createClipart(kind, "03", ".png"));
        parseObjects.add(createClipart(kind, "04", ".png"));
        parseObjects.add(createClipart(kind, "05", ".png"));
        parseObjects.add(createClipart(kind, "06", ".png"));
        parseObjects.add(createClipart(kind, "07", ".png"));
        parseObjects.add(createClipart(kind, "08", ".png"));
        parseObjects.add(createClipart(kind, "09", ".png"));
        parseObjects.add(createClipart(kind, "10", ".png"));
        parseObjects.add(createClipart(kind, "11", ".png"));
        parseObjects.add(createClipart(kind, "12", ".png"));

        ParseObject.saveAll(parseObjects);

        Timber.d("save clipart 12");
        Timber.d("add %d",parseObjects.size());


        parseObjects = new ArrayList<>();

        kind = saveKind(Const.CLIPART_CATEGORY_CLIPART, "15doodle", "01", ".png");

        parseObjects.add(createClipart(kind, "01", ".png"));
        parseObjects.add(createClipart(kind, "02", ".png"));
        parseObjects.add(createClipart(kind, "03", ".png"));
        parseObjects.add(createClipart(kind, "04", ".png"));
        parseObjects.add(createClipart(kind, "05", ".png"));
        parseObjects.add(createClipart(kind, "06", ".png"));
        parseObjects.add(createClipart(kind, "07", ".png"));
        parseObjects.add(createClipart(kind, "08", ".png"));
        parseObjects.add(createClipart(kind, "09", ".png"));
        parseObjects.add(createClipart(kind, "10", ".png"));
        parseObjects.add(createClipart(kind, "11", ".png"));

        ParseObject.saveAll(parseObjects);

        Timber.d("save clipart 13");
        Timber.d("add %d",parseObjects.size());

        parseObjects = new ArrayList<>();

        kind = saveKind(Const.CLIPART_CATEGORY_CLIPART, "16lace", "01", ".png");

        parseObjects.add(createClipart(kind, "01", ".png"));
        parseObjects.add(createClipart(kind, "02", ".png"));
        parseObjects.add(createClipart(kind, "03", ".png"));
        parseObjects.add(createClipart(kind, "04", ".png"));
        parseObjects.add(createClipart(kind, "05", ".png"));
        parseObjects.add(createClipart(kind, "06", ".png"));

        ParseObject.saveAll(parseObjects);

        Timber.d("save clipart 14");
        Timber.d("add %d",parseObjects.size());

        parseObjects = new ArrayList<>();

        kind = saveKind(Const.CLIPART_CATEGORY_CLIPART, "99etc", "01", ".png");

        parseObjects.add(createClipart(kind, "01", ".png"));
        parseObjects.add(createClipart(kind, "02", ".png"));
        parseObjects.add(createClipart(kind, "03", ".png"));
        parseObjects.add(createClipart(kind, "04", ".png"));
        parseObjects.add(createClipart(kind, "05", ".png"));
        parseObjects.add(createClipart(kind, "06", ".png"));
        parseObjects.add(createClipart(kind, "07", ".png"));
        parseObjects.add(createClipart(kind, "08", ".png"));
        parseObjects.add(createClipart(kind, "09", ".png"));
        parseObjects.add(createClipart(kind, "10", ".png"));
        parseObjects.add(createClipart(kind, "11", ".png"));
        parseObjects.add(createClipart(kind, "12", ".png"));
        parseObjects.add(createClipart(kind, "13", ".png"));
        parseObjects.add(createClipart(kind, "14", ".png"));
        parseObjects.add(createClipart(kind, "15", ".png"));
        parseObjects.add(createClipart(kind, "16", ".png"));
        parseObjects.add(createClipart(kind, "17", ".png"));
        parseObjects.add(createClipart(kind, "18", ".png"));
        parseObjects.add(createClipart(kind, "19", ".png"));
        parseObjects.add(createClipart(kind, "20", ".png"));
        parseObjects.add(createClipart(kind, "21", ".png"));
        parseObjects.add(createClipart(kind, "22", ".png"));
        parseObjects.add(createClipart(kind, "23", ".png"));
        parseObjects.add(createClipart(kind, "24", ".png"));
        parseObjects.add(createClipart(kind, "25", ".png"));

        ParseObject.saveAll(parseObjects);

        Timber.d("save clipart 15-1");
        Timber.d("add %d",parseObjects.size());

        parseObjects = new ArrayList<>();

        parseObjects.add(createClipart(kind, "26", ".png"));
        parseObjects.add(createClipart(kind, "27", ".png"));
        parseObjects.add(createClipart(kind, "28", ".png"));
        parseObjects.add(createClipart(kind, "29", ".png"));
        parseObjects.add(createClipart(kind, "30", ".png"));
        parseObjects.add(createClipart(kind, "31", ".png"));
        parseObjects.add(createClipart(kind, "32", ".png"));
        parseObjects.add(createClipart(kind, "33", ".png"));
        parseObjects.add(createClipart(kind, "34", ".png"));
        parseObjects.add(createClipart(kind, "35", ".png"));
        parseObjects.add(createClipart(kind, "36", ".png"));
        parseObjects.add(createClipart(kind, "37", ".png"));
        parseObjects.add(createClipart(kind, "38", ".png"));
        parseObjects.add(createClipart(kind, "39", ".png"));
        parseObjects.add(createClipart(kind, "40", ".png"));
        parseObjects.add(createClipart(kind, "41", ".png"));
        parseObjects.add(createClipart(kind, "42", ".png"));
        parseObjects.add(createClipart(kind, "43", ".png"));
        parseObjects.add(createClipart(kind, "44", ".png"));
        parseObjects.add(createClipart(kind, "45", ".png"));
        parseObjects.add(createClipart(kind, "46", ".png"));
        parseObjects.add(createClipart(kind, "47", ".png"));
        parseObjects.add(createClipart(kind, "48", ".png"));
        parseObjects.add(createClipart(kind, "49", ".png"));
        parseObjects.add(createClipart(kind, "50", ".png"));
        parseObjects.add(createClipart(kind, "51", ".png"));
        parseObjects.add(createClipart(kind, "52", ".png"));
        parseObjects.add(createClipart(kind, "53", ".png"));
        parseObjects.add(createClipart(kind, "54", ".png"));

        ParseObject.saveAll(parseObjects);

        Timber.d("save clipart 15-2");
        Timber.d("add %d",parseObjects.size());

        parseObjects = new ArrayList<>();

        kind = saveKind(Const.CLIPART_CATEGORY_CLIPART, "season_halloween", "01", ".png");

        parseObjects.add(createClipart(kind, "01", ".png"));
        parseObjects.add(createClipart(kind, "02", ".png"));
        parseObjects.add(createClipart(kind, "03", ".png"));
        parseObjects.add(createClipart(kind, "04", ".png"));
        parseObjects.add(createClipart(kind, "05", ".png"));
        parseObjects.add(createClipart(kind, "06", ".png"));
        parseObjects.add(createClipart(kind, "07", ".png"));
        parseObjects.add(createClipart(kind, "08", ".png"));
        parseObjects.add(createClipart(kind, "09", ".png"));
        parseObjects.add(createClipart(kind, "10", ".png"));
        parseObjects.add(createClipart(kind, "11", ".png"));
        parseObjects.add(createClipart(kind, "12", ".png"));
        parseObjects.add(createClipart(kind, "13", ".png"));
        parseObjects.add(createClipart(kind, "14", ".png"));
        parseObjects.add(createClipart(kind, "15", ".png"));
        parseObjects.add(createClipart(kind, "16", ".png"));
        parseObjects.add(createClipart(kind, "17", ".png"));
        parseObjects.add(createClipart(kind, "18", ".png"));
        parseObjects.add(createClipart(kind, "19", ".png"));
        parseObjects.add(createClipart(kind, "20", ".png"));
        parseObjects.add(createClipart(kind, "21", ".png"));
        parseObjects.add(createClipart(kind, "22", ".png"));
        parseObjects.add(createClipart(kind, "23", ".png"));
        parseObjects.add(createClipart(kind, "24", ".png"));
        parseObjects.add(createClipart(kind, "25", ".png"));
        parseObjects.add(createClipart(kind, "26", ".png"));
        parseObjects.add(createClipart(kind, "27", ".png"));
        parseObjects.add(createClipart(kind, "28", ".png"));
        parseObjects.add(createClipart(kind, "29", ".png"));
        parseObjects.add(createClipart(kind, "30", ".png"));
        parseObjects.add(createClipart(kind, "31", ".png"));
        parseObjects.add(createClipart(kind, "32", ".png"));
        parseObjects.add(createClipart(kind, "33", ".png"));
        parseObjects.add(createClipart(kind, "34", ".png"));
        parseObjects.add(createClipart(kind, "35", ".png"));
        parseObjects.add(createClipart(kind, "36", ".png"));
        parseObjects.add(createClipart(kind, "37", ".png"));
        parseObjects.add(createClipart(kind, "38", ".png"));
        parseObjects.add(createClipart(kind, "39", ".png"));
        parseObjects.add(createClipart(kind, "40", ".png"));
        parseObjects.add(createClipart(kind, "41", ".png"));
        parseObjects.add(createClipart(kind, "42", ".png"));
        parseObjects.add(createClipart(kind, "43", ".png"));
        parseObjects.add(createClipart(kind, "44", ".png"));
        parseObjects.add(createClipart(kind, "45", ".png"));
        parseObjects.add(createClipart(kind, "46", ".png"));
        parseObjects.add(createClipart(kind, "47", ".png"));
        parseObjects.add(createClipart(kind, "48", ".png"));
        parseObjects.add(createClipart(kind, "49", ".png"));
        parseObjects.add(createClipart(kind, "50", ".png"));

        ParseObject.saveAll(parseObjects);

        Timber.d("save clipart 16");
        Timber.d("add %d",parseObjects.size());

    }

    @DebugLog
    private void saveBackground() throws Exception {
        ClipartKind kind;
        List<ParseObject> parseObjects = new ArrayList<>();

        kind = saveKind(Const.CLIPART_CATEGORY_BACKGROUND, "01", "01", ".png");

        parseObjects.add(createClipart(kind, "01", ".jpg"));
        parseObjects.add(createClipart(kind, "02", ".jpg"));
        parseObjects.add(createClipart(kind, "03", ".jpg"));
        parseObjects.add(createClipart(kind, "04", ".jpg"));
        parseObjects.add(createClipart(kind, "05", ".jpg"));
        parseObjects.add(createClipart(kind, "06", ".jpg"));

        ParseObject.saveAll(parseObjects);

        Timber.d("save background 01");
        Timber.d("add %d",parseObjects.size());

        parseObjects = new ArrayList<>();

        kind = saveKind(Const.CLIPART_CATEGORY_BACKGROUND, "02", "01", ".png");

        parseObjects.add(createClipart(kind, "01", ".png"));
        parseObjects.add(createClipart(kind, "02", ".jpg"));
        parseObjects.add(createClipart(kind, "03", ".jpg"));
        parseObjects.add(createClipart(kind, "04", ".png"));
        parseObjects.add(createClipart(kind, "05", ".jpg"));
        parseObjects.add(createClipart(kind, "06", ".jpg"));
        parseObjects.add(createClipart(kind, "07", ".png"));
        parseObjects.add(createClipart(kind, "08", ".png"));
        parseObjects.add(createClipart(kind, "09", ".png"));
        parseObjects.add(createClipart(kind, "10", ".jpg"));
        parseObjects.add(createClipart(kind, "11", ".png"));
        parseObjects.add(createClipart(kind, "12", ".png"));
        parseObjects.add(createClipart(kind, "13", ".png"));

        ParseObject.saveAll(parseObjects);

        Timber.d("save background 02");
        Timber.d("add %d",parseObjects.size());

        parseObjects = new ArrayList<>();

        kind = saveKind(Const.CLIPART_CATEGORY_BACKGROUND, "03", "01", ".png");

        parseObjects.add(createClipart(kind, "01", ".jpg"));
        parseObjects.add(createClipart(kind, "02", ".jpg"));
        parseObjects.add(createClipart(kind, "03", ".jpg"));
        parseObjects.add(createClipart(kind, "04", ".jpg"));
        parseObjects.add(createClipart(kind, "05", ".png"));
        parseObjects.add(createClipart(kind, "06", ".png"));
        parseObjects.add(createClipart(kind, "07", ".png"));
        parseObjects.add(createClipart(kind, "08", ".png"));
        parseObjects.add(createClipart(kind, "09", ".png"));
        parseObjects.add(createClipart(kind, "10", ".png"));

        ParseObject.saveAll(parseObjects);

        Timber.d("save background 03");
        Timber.d("add %d",parseObjects.size());

        parseObjects = new ArrayList<>();

        kind = saveKind(Const.CLIPART_CATEGORY_BACKGROUND, "04", "01", ".png");

        parseObjects.add(createClipart(kind, "01", ".jpg"));
        parseObjects.add(createClipart(kind, "02", ".jpg"));
        parseObjects.add(createClipart(kind, "03", ".jpg"));
        parseObjects.add(createClipart(kind, "04", ".jpg"));
        parseObjects.add(createClipart(kind, "05", ".png"));
        parseObjects.add(createClipart(kind, "06", ".png"));
        parseObjects.add(createClipart(kind, "07", ".png"));

        ParseObject.saveAll(parseObjects);

        Timber.d("save background 04");
        Timber.d("add %d",parseObjects.size());

        parseObjects = new ArrayList<>();

        kind = saveKind(Const.CLIPART_CATEGORY_BACKGROUND, "05", "01", ".png");

        parseObjects.add(createClipart(kind, "01", ".png"));
        parseObjects.add(createClipart(kind, "02", ".png"));
        parseObjects.add(createClipart(kind, "03", ".png"));
        parseObjects.add(createClipart(kind, "04", ".png"));
        parseObjects.add(createClipart(kind, "05", ".png"));
        parseObjects.add(createClipart(kind, "06", ".png"));
        parseObjects.add(createClipart(kind, "07", ".png"));
        parseObjects.add(createClipart(kind, "08", ".png"));
        parseObjects.add(createClipart(kind, "09", ".png"));
        parseObjects.add(createClipart(kind, "10", ".png"));
        parseObjects.add(createClipart(kind, "11", ".png"));
        parseObjects.add(createClipart(kind, "12", ".png"));
        parseObjects.add(createClipart(kind, "13", ".png"));
        parseObjects.add(createClipart(kind, "14", ".png"));
        parseObjects.add(createClipart(kind, "15", ".png"));
        parseObjects.add(createClipart(kind, "16", ".png"));

        ParseObject.saveAll(parseObjects);

        Timber.d("save background 05");
        Timber.d("add %d",parseObjects.size());

        parseObjects = new ArrayList<>();

        kind = saveKind(Const.CLIPART_CATEGORY_BACKGROUND, "06", "01", ".jpg");

        parseObjects.add(createClipart(kind, "01", ".jpg"));
        parseObjects.add(createClipart(kind, "02", ".jpg"));
        parseObjects.add(createClipart(kind, "03", ".jpg"));
        parseObjects.add(createClipart(kind, "04", ".jpg"));
        parseObjects.add(createClipart(kind, "05", ".jpg"));

        ParseObject.saveAll(parseObjects);

        Timber.d("save background 06");
        Timber.d("add %d",parseObjects.size());

        parseObjects = new ArrayList<>();

        kind = saveKind(Const.CLIPART_CATEGORY_BACKGROUND, "07", "01", ".png");

        parseObjects.add(createClipart(kind, "01", ".png"));
        parseObjects.add(createClipart(kind, "02", ".png"));
        parseObjects.add(createClipart(kind, "03", ".png"));
        parseObjects.add(createClipart(kind, "04", ".png"));

        ParseObject.saveAll(parseObjects);

        Timber.d("save background 07");
        Timber.d("add %d",parseObjects.size());

        parseObjects = new ArrayList<>();

        kind = saveKind(Const.CLIPART_CATEGORY_BACKGROUND, "08", "01", ".jpg");

        parseObjects.add(createClipart(kind, "01", ".jpg"));
        parseObjects.add(createClipart(kind, "02", ".jpg"));
        parseObjects.add(createClipart(kind, "03", ".jpg"));
        parseObjects.add(createClipart(kind, "04", ".jpg"));
        parseObjects.add(createClipart(kind, "05", ".jpg"));
        parseObjects.add(createClipart(kind, "06", ".jpg"));
        parseObjects.add(createClipart(kind, "07", ".jpg"));
        parseObjects.add(createClipart(kind, "08", ".jpg"));
        parseObjects.add(createClipart(kind, "09", ".jpg"));
        parseObjects.add(createClipart(kind, "10", ".jpg"));
        parseObjects.add(createClipart(kind, "11", ".jpg"));

        ParseObject.saveAll(parseObjects);

        Timber.d("save background 08");
        Timber.d("add %d",parseObjects.size());

        parseObjects = new ArrayList<>();

        kind = saveKind(Const.CLIPART_CATEGORY_BACKGROUND, "09", "01", ".png");

        parseObjects.add(createClipart(kind, "01", ".png"));
        parseObjects.add(createClipart(kind, "02", ".png"));
        parseObjects.add(createClipart(kind, "03", ".png"));
        parseObjects.add(createClipart(kind, "04", ".png"));
        parseObjects.add(createClipart(kind, "05", ".png"));
        parseObjects.add(createClipart(kind, "06", ".png"));
        parseObjects.add(createClipart(kind, "07", ".png"));
        parseObjects.add(createClipart(kind, "08", ".png"));
        parseObjects.add(createClipart(kind, "09", ".png"));
        parseObjects.add(createClipart(kind, "10", ".png"));
        parseObjects.add(createClipart(kind, "11", ".png"));
        parseObjects.add(createClipart(kind, "12", ".png"));
        parseObjects.add(createClipart(kind, "13", ".png"));
        parseObjects.add(createClipart(kind, "14", ".png"));
        parseObjects.add(createClipart(kind, "15", ".png"));

        ParseObject.saveAll(parseObjects);

        Timber.d("save background 09");
        Timber.d("add %d",parseObjects.size());

        parseObjects = new ArrayList<>();

        kind = saveKind(Const.CLIPART_CATEGORY_BACKGROUND, "99", "01", ".png");

        parseObjects.add(createClipart(kind, "01", ".png"));
        parseObjects.add(createClipart(kind, "02", ".png"));
        parseObjects.add(createClipart(kind, "03", ".png"));
        parseObjects.add(createClipart(kind, "04", ".png"));
        parseObjects.add(createClipart(kind, "05", ".png"));
        parseObjects.add(createClipart(kind, "06", ".png"));
        parseObjects.add(createClipart(kind, "07", ".png"));
        parseObjects.add(createClipart(kind, "08", ".png"));
        parseObjects.add(createClipart(kind, "09", ".png"));
        parseObjects.add(createClipart(kind, "10", ".png"));
        parseObjects.add(createClipart(kind, "11", ".png"));
        parseObjects.add(createClipart(kind, "12", ".png"));
        parseObjects.add(createClipart(kind, "13", ".png"));
        parseObjects.add(createClipart(kind, "14", ".png"));
        parseObjects.add(createClipart(kind, "15", ".png"));
        parseObjects.add(createClipart(kind, "16", ".png"));
        parseObjects.add(createClipart(kind, "17", ".png"));
        parseObjects.add(createClipart(kind, "18", ".png"));
        parseObjects.add(createClipart(kind, "19", ".png"));
        parseObjects.add(createClipart(kind, "20", ".png"));
        parseObjects.add(createClipart(kind, "21", ".png"));
        parseObjects.add(createClipart(kind, "22", ".png"));
        parseObjects.add(createClipart(kind, "23", ".png"));

        ParseObject.saveAll(parseObjects);

        Timber.d("save background 10");
        Timber.d("add %d",parseObjects.size());

        parseObjects = new ArrayList<>();

        kind = saveKind(Const.CLIPART_CATEGORY_BACKGROUND, "season_halloween", "01", ".png");

        parseObjects.add(createClipart(kind, "01", ".png"));
        parseObjects.add(createClipart(kind, "02", ".png"));
        parseObjects.add(createClipart(kind, "03", ".png"));
        parseObjects.add(createClipart(kind, "04", ".png"));
        parseObjects.add(createClipart(kind, "05", ".png"));
        parseObjects.add(createClipart(kind, "06", ".png"));
        parseObjects.add(createClipart(kind, "07", ".png"));
        parseObjects.add(createClipart(kind, "08", ".png"));
        parseObjects.add(createClipart(kind, "09", ".png"));
        parseObjects.add(createClipart(kind, "10", ".png"));
        parseObjects.add(createClipart(kind, "11", ".png"));
        parseObjects.add(createClipart(kind, "12", ".png"));
        parseObjects.add(createClipart(kind, "13", ".png"));
        parseObjects.add(createClipart(kind, "14", ".png"));
        parseObjects.add(createClipart(kind, "15", ".png"));
        parseObjects.add(createClipart(kind, "16", ".png"));

        ParseObject.saveAll(parseObjects);

        Timber.d("save background 11");
        Timber.d("add %d",parseObjects.size());

    }


    private ClipartKind saveKind(String category, String name,String path, String ext) throws ParseException {
        ClipartKind kind = new ClipartKind();
        kind.put(ClipartKind.F_CATEGORY,category);
        kind.put(ClipartKind.F_NAME,name);
        kind.put(ClipartKind.F_PATH,path);
        kind.put(ClipartKind.F_EXT,ext);
        kind.put(ClipartKind.F_ORDER, 1);
        kind.put(ClipartKind.F_IS_ACTIVE,true);
        kind.save();
        return kind;
    }

    private Clipart createClipart(ClipartKind kind, String path, String ext) throws ParseException {
        Clipart clipart = new Clipart();
        clipart.put(Clipart.F_CLIPART_KIND,kind);
        clipart.put(Clipart.F_CATEGORY_AND_KIND,kind.getString(ClipartKind.F_CATEGORY) + kind.getString(ClipartKind.F_NAME));
        clipart.put(Clipart.F_ORDER, 1);
        clipart.put(Clipart.F_IS_ACTIVE,true);
        clipart.put(Clipart.F_PATH,path);
        clipart.put(Clipart.F_EXT,ext);
        return clipart;
    }

}
