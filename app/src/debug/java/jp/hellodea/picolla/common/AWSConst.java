package jp.hellodea.picolla.common;

/**
 * Created by kazuhiro on 2015/02/10.
 */
public class AWSConst {
    //アセットPath
    public static final String ASSETS_CLIPART_PATH = "https://s3-ap-northeast-1.amazonaws.com/pic-editor-pro/images/cliparts/";
    public static final String ASSETS_FILTER_PATH = "https://s3-ap-northeast-1.amazonaws.com/pic-editor-pro/assets/filters/";
    public static final String ASSETS_EFFECT_PATH = "https://s3-ap-northeast-1.amazonaws.com/pic-editor-pro/images/effects/";
    public static final String ASSETS_FONT_PATH = "https://s3-ap-northeast-1.amazonaws.com/pic-editor-pro/fonts/";

    public static final String LEGAL_DOCUMENTS_PATH = "https://s3-ap-northeast-1.amazonaws.com/pic-editor-pro/html/";

}

