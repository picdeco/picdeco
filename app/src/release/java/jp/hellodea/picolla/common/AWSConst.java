package jp.hellodea.picolla.common;

/**
 * Created by kazuhiro on 2015/02/10.
 */
public class AWSConst {
    //アセットPath
    public static final String ASSETS_CLIPART_PATH = "http://d36irpv1pgpjm4.cloudfront.net/images/cliparts/";
    public static final String ASSETS_FILTER_PATH = "http://d36irpv1pgpjm4.cloudfront.net/assets/filters/";
    public static final String ASSETS_EFFECT_PATH = "http://d36irpv1pgpjm4.cloudfront.net/images/effects/";
    public static final String ASSETS_FONT_PATH = "http://d36irpv1pgpjm4.cloudfront.net/fonts/";

    public static final String LEGAL_DOCUMENTS_PATH = "http://d36irpv1pgpjm4.cloudfront.net/html/";
}

